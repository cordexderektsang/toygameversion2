Shader "iPhone/Transparent_LightOff" 
{
	Properties 
	{
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_Color ("Main Color", COLOR) = (1,1,1,1)
	}
	
	SubShader 
	{
		Tags{ "Queue" = "Transparent+1" }

		Pass 
		{
			Lighting On
			Blend SrcAlpha OneMinusSrcAlpha

			SetTexture [_MainTex] 
			{
				constantColor [_Color]
				Combine texture * constant
        	}
		}
	} 
	FallBack "Diffuse"
}
