Shader "iPhone/CullOff_LightOff" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_Color ("Main Color", COLOR) = (1,1,1,1)
	}
	SubShader {
		LOD 200
		Pass {
			ColorMaterial AmbientAndDiffuse
			Lighting Off
			Cull Off

			SetTexture [_MainTex] {
				constantColor [_Color]
        	}
		}
	} 
	FallBack "Mobile/Diffuse"
}
