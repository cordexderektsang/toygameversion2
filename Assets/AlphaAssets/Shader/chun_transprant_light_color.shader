﻿Shader "Chun/Transparent Lighting With Color Shader" 
{
	Properties 
	{
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_Color ("Color", Color) = (1.0, 1.0, 1.0, 1.0)
	}
	
	SubShader 
	{
		Tags { "RenderType"="Transparent+1" }
		Blend SrcAlpha OneMinusSrcAlpha
		
		CGPROGRAM
		#pragma surface surf Lambert

		sampler2D _MainTex;
		fixed4 _Color;
		
		struct Input 
		{
			float2 uv_MainTex;
		};

		void surf (Input IN, inout SurfaceOutput o) 
		{
			half4 c = tex2D (_MainTex, IN.uv_MainTex);
			o.Albedo = c.rgb * _Color.rgb;
			//o.Emission = c.rgb * _Color.rgb * _Color.a;
			o.Alpha = _Color.a;
		}
		
		ENDCG
		
		Pass 
		{
			Lighting on 					
			Blend SrcAlpha OneMinusSrcAlpha

			SetTexture [_MainTex] 
			{
				Combine texture * constant
        	}
		}
	} 
	FallBack "Transparent"
}
