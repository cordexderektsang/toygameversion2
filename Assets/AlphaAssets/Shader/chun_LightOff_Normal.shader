﻿Shader "Chun/Light Off Bump" 
{
	Properties 
	{
		_MainTex ("Texture", 2D) = "white" {}
		_NormalMap ("Normal Map", 2D) = "bump" {}
	}
	
	SubShader 
	{
		CGPROGRAM
		#pragma surface surf Lambert
		
		struct Input 
		{
			float2 uv_MainTex;
			float2 uv_NormalMap;
		};
		
		sampler2D _MainTex;
		sampler2D _NormalMap;
		
		void surf (Input IN, inout SurfaceOutput o) 
		{
			o.Emission = tex2D (_MainTex, IN.uv_MainTex).rgb;
			//o.Albedo = tex2D (_MainTex, IN.uv_MainTex).rgb;
			o.Normal = UnpackNormal (tex2D (_NormalMap, IN.uv_NormalMap));
		}
		ENDCG
	} 
	Fallback "Diffuse"
}