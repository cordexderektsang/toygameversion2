﻿using UnityEngine;
using UnityEditor;

public class CordexEditor : Editor 
{
	[MenuItem ("Cordex/Reposition Selected Objs")]
	static void RepositionSelection() 
	{
		var objs = Selection.transforms;
		if(objs.Length < 1) EditorUtility.DisplayDialog("Reposition Error", "Please select an object(s)", "ok");
		foreach (Transform obj in objs)
		{
			obj.position += new Vector3(1 * (obj.GetSiblingIndex ()), 0, 0);
		}
	}
	
	[MenuItem ("Cordex/Texture Dithering")]
	static void TextureDithering () 
	{
		// Get existing open window or if none, make a new one:
		TextureSelectorWindow window = (TextureSelectorWindow)EditorWindow.GetWindow (typeof (TextureSelectorWindow));
		window.Show ();
	}
	
	[MenuItem ("Cordex/Add Animation Event")]
	static void AddAnimationEvent () 
	{
		// Get existing open window or if none, make a new one:
		CustomAnimationEventWindow window = (CustomAnimationEventWindow)EditorWindow.GetWindow (typeof (CustomAnimationEventWindow));
		window.Show ();
	}
	
	[MenuItem ("Cordex/RepositionGameModeUIController %g")]
	static void RepositionController () 
	{
		// -4521.889, 0, 0
		
		string _name = UnityEngine.SceneManagement.SceneManager.GetActiveScene ().name;
			// Application.loadedLevelName;
		_name = _name.Replace ("GameMode","");
		Transform obj = GameObject.Find("GameMode"+ _name[0]  +"Controller").transform;
		obj.position = new Vector3 (0, 0, 100);
		obj.localScale = new Vector3 (200, 200, 200);
	}
	
	[MenuItem ("Cordex/AddPlaySoundComponent %#a")]
	static void AddPlaySoundComponent () 
	{
		var obj = Selection.activeGameObject;
		var behaviour = obj.AddMissingComponent <UIPlaySound> ();
		behaviour.trigger = UIPlaySound.Trigger.OnRelease;
	}
	
	[MenuItem ("Cordex/ResetAllLocalPlayerPrefs %#r")]
	static void ResetAllLocalPlayerPrefs () 
	{
		CCLocalStorageAPI.DeleteAll ();
	}
}

public class TextureSelectorWindow : EditorWindow 
{	
	Texture2D textureRef;
	
	void OnGUI () 
	{
		GUILayout.Label ("Texture Selector", EditorStyles.boldLabel);
		textureRef = EditorGUILayout.ObjectField ("Texture Ref.", textureRef, typeof (Texture2D), false, null) as Texture2D;
		if (textureRef != null && GUILayout.Button ("Process"))
		{		
			Texture2D texture = new Texture2D (textureRef.width, textureRef.height);
			texture.SetPixels32 (textureRef.GetPixels32 ());
			
			var texw = texture.width;
	        var texh = texture.height;
	
	        var pixels = texture.GetPixels ();
	        var offs = 0;
	
	        var k1Per15 = 1.0f / 15.0f;
	        var k1Per16 = 1.0f / 16.0f;
	        var k3Per16 = 3.0f / 16.0f;
	        var k5Per16 = 5.0f / 16.0f;
	        var k7Per16 = 7.0f / 16.0f;
	
	        for (var y = 0; y < texh; y++) 
			{
	            for (var x = 0; x < texw; x++) 
				{
	                float a = pixels [offs].a;
	                float r = pixels [offs].r;
	                float g = pixels [offs].g;
	                float b = pixels [offs].b;
	
	                var a2 = Mathf.Clamp01 (Mathf.Floor (a * 16) * k1Per15);
	                var r2 = Mathf.Clamp01 (Mathf.Floor (r * 16) * k1Per15);
	                var g2 = Mathf.Clamp01 (Mathf.Floor (g * 16) * k1Per15);
	                var b2 = Mathf.Clamp01 (Mathf.Floor (b * 16) * k1Per15);
	
	                var ae = a - a2;
	                var re = r - r2;
	                var ge = g - g2;
	                var be = b - b2;
	
	                pixels [offs].a = a2;
	                pixels [offs].r = r2;
	                pixels [offs].g = g2;
	                pixels [offs].b = b2;
	
	                var n1 = offs + 1;
	                var n2 = offs + texw - 1;
	                var n3 = offs + texw;
	                var n4 = offs + texw + 1;
	
	                if (x < texw - 1) {
	                    pixels [n1].a += ae * k7Per16;
	                    pixels [n1].r += re * k7Per16;
	                    pixels [n1].g += ge * k7Per16;
	                    pixels [n1].b += be * k7Per16;
	                }
	
	                if (y < texh - 1) {
	                    pixels [n3].a += ae * k5Per16;
	                    pixels [n3].r += re * k5Per16;
	                    pixels [n3].g += ge * k5Per16;
	                    pixels [n3].b += be * k5Per16;
	
	                    if (x > 0) {
	                        pixels [n2].a += ae * k3Per16;
	                        pixels [n2].r += re * k3Per16;
	                        pixels [n2].g += ge * k3Per16;
	                        pixels [n2].b += be * k3Per16;
	                    }
	
	                    if (x < texw - 1) {
	                        pixels [n4].a += ae * k1Per16;
	                        pixels [n4].r += re * k1Per16;
	                        pixels [n4].g += ge * k1Per16;
	                        pixels [n4].b += be * k1Per16;
	                    }
	                }
	
	                offs++;
	            }
	        }
	
	        texture.SetPixels (pixels);
	        EditorUtility.CompressTexture (texture, TextureFormat.RGBA4444, TextureCompressionQuality.Best);
			
			string path = AssetDatabase.GetAssetPath(textureRef.GetInstanceID());
			path = path.Substring (0, path.LastIndexOf ('.')) + "_Dithering.png";
			
			System.IO.File.WriteAllBytes (path, texture.EncodeToPNG ());
			AssetDatabase.Refresh ();
			
			TextureImporter importer = AssetImporter.GetAtPath(path) as TextureImporter;
			if(importer == null)
			{
				Debug.Log ("Output texture have error");
				return;
			}
			TextureImporterSettings settings = new TextureImporterSettings();
			importer.ReadTextureSettings(settings);
			settings.readable = true;
			settings.npotScale = TextureImporterNPOTScale.None;
			settings.alphaIsTransparency = false;
			settings.mipmapEnabled = false;
			settings.textureFormat = TextureImporterFormat.Automatic16bit;
			importer.SetTextureSettings(settings);
			AssetDatabase.ImportAsset(path, ImportAssetOptions.ForceUpdate | ImportAssetOptions.ForceSynchronousImport);
			
			AssetDatabase.Refresh ();
			Debug.Log("Finish!! Save to " + path);

		}
	}
}

public class CustomAnimationEventWindow : EditorWindow 
{
	AnimationClip clip;
	string function = "";
	float sec = 0;
	
	void OnGUI () 
	{
		GUILayout.Label ("Custom Animation Event Inspector", EditorStyles.boldLabel);
		
		clip = EditorGUILayout.ObjectField ("Animation Clip", clip, typeof (AnimationClip), false, null) as AnimationClip;
		function = EditorGUILayout.TextField ("Callback function name", function);
		sec = EditorGUILayout.FloatField ("Time in sec", Mathf.Clamp(sec, 0, ((clip == null) ? 0 : clip.length)));
		
		if (clip != null && GUILayout.Button ("Add"))
		{
			if(clip == null)
			{
				Debug.Log("Selected object is not an AnimationClip;");
				return;
			}
			
			if(string.IsNullOrEmpty (function))
			{
				Debug.Log("Callback function name cannot be null;");
				return;
			}
			
			AnimationEvent evt = new AnimationEvent ();
			evt.functionName = function;
			evt.time = sec;
			AnimationUtility.SetAnimationEvents(clip, new AnimationEvent[]{evt});
			Debug.Log("Finish!!;");
		}
	}
}