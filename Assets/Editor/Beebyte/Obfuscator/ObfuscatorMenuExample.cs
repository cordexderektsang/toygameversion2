﻿/*
using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.IO;

namespace Beebyte.Obfuscator
{
	public class ObfuscatorMenuExample 
	{
		private static Options options = null;

		[MenuItem("Tools/Obfuscate External DLL")]
		private static void ObfuscateExternalDll()
		{
			string dllPath = @"C:\path\to\External.dll";

			Debug.Log("Obfuscating");

			if (System.IO.File.Exists(dllPath))
			{
				if (options == null) options = Postbuild.LoadOptions();

				bool oldSkipRenameOfAllPublicMonobehaviourFields = options.skipRenameOfAllPublicMonobehaviourFields;
				try
				{
					//Preserving monobehaviour public field names is an essential step for obfuscating external DLLs
					options.skipRenameOfAllPublicMonobehaviourFields = true;

					Beebyte.Obfuscator.Obfuscator.Obfuscate(dllPath, options, EditorUserBuildSettings.activeBuildTarget);
				}
				finally
				{
					options.skipRenameOfAllPublicMonobehaviourFields = oldSkipRenameOfAllPublicMonobehaviourFields;
				}
			}
			else Debug.Log("Obfuscating could not find file at " + dllPath);
		}
	}
}
*/
