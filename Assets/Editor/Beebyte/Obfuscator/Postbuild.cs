﻿/*
 * Copyright (c) 2015-2016 Beebyte Limited. All rights reserved.
 */
#if !UNITY_IOS
using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.IO;
using System.Text;

namespace Beebyte.Obfuscator
{
	public class Postbuild
	{
		private static readonly string CSHARP_DLL = "Assembly-CSharp.dll";
		private static Options options = null;
		private static readonly string[] extraAssemblyDirectories = new string[]
		{
			/* 
			 * AssemblyResolutionException can sometimes be thrown when obfuscating.
			 * Although most of these have now been prevented, due to individual system configuartions it is sometimes
			 * necessary to add an assembly's directory here, so that it may be found.
			 */
			@"C:\Path\To\Assembly\Directory",
			@"C:\Program Files (x86)\Microsoft SDKs\NETCoreSDK\System.Runtime\4.0.20\lib\netcore50"
		};

		private static bool obfuscatedAfterScene = false;
		private static bool noCSharpScripts = false;

		[PostProcessBuild(1)]
		private static void PostBuildHook(UnityEditor.BuildTarget buildTarget, string pathToBuildProject)
		{
			if (obfuscatedAfterScene == false)
			{
				if (noCSharpScripts) Debug.LogWarning("No obfuscation required because no C# scripts were found");
				else Debug.LogError("Failed to obfuscate");
			}
		}

		[PostProcessScene(1)]
		public static void Obfuscate()
		{
			if (!EditorApplication.isPlayingOrWillChangePlaymode && !obfuscatedAfterScene)
			{
				EditorApplication.LockReloadAssemblies();
				string cSharpLocation = FindDllLocation(CSHARP_DLL);

				if (cSharpLocation == null) noCSharpScripts = true;
				else
				{
					if (options == null) options = LoadOptions();

					Beebyte.Obfuscator.Obfuscator.FixHexBug(options);
					Beebyte.Obfuscator.Obfuscator.SetExtraAssemblyDirectories(extraAssemblyDirectories);
					Beebyte.Obfuscator.Obfuscator.Obfuscate(cSharpLocation, options, EditorUserBuildSettings.activeBuildTarget);
					obfuscatedAfterScene = true;

					EditorApplication.UnlockReloadAssemblies();
				}
			}
		}

		private static string FindDllLocation(string suffix)
		{
			foreach (System.Reflection.Assembly assembly in System.AppDomain.CurrentDomain.GetAssemblies())
			{
				if (assembly.Location.EndsWith(suffix))
				{
					return assembly.Location;
				}
			}
			return null;
		}

		private static string FindOptionPath()
		{
			//I'm unsure whether FindAssets was added earlier than 5_2. However it's not a big deal.
#if UNITY_3_1 || UNITY_3_2 || UNITY_3_3 || UNITY_3_4 || UNITY_3_5 || UNITY_4_0 || UNITY_4_1 || UNITY_4_2 || UNITY_4_3 || UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9 || UNITY_5_0 || UNITY_5_1
			return @"Assets/Editor/Beebyte/Obfuscator/ObfuscatorOptions.asset";
#else
			string path = null;

			string[] optionPaths = AssetDatabase.FindAssets("ObfuscatorOptions");

			if (optionPaths.Length == 0)
			{
				Debug.LogError("Could not find ObfuscatorOptions file!");
				return null;
			}
			else if (optionPaths.Length == 1) return optionPaths[0];
			else if (optionPaths.Length >= 2)
			{
				foreach (string s in optionPaths)
				{
					if (s != null && s.Equals(@"Assets/Editor/Beebyte/Obfuscator/ObfuscatorOptions.asset"))
					{
						return s;
					}
				}
				if (path == null) path = optionPaths[0];

				Debug.Log(optionPaths.Length + " ObfuscatorOptions assets found! Using " + path);
			}
			return path;
#endif
		}

		public static Options LoadOptions()
		{
			Options o = null;
			string optionPath = FindOptionPath();

#if UNITY_3_1 || UNITY_3_2 || UNITY_3_3 || UNITY_3_4 || UNITY_3_5 || UNITY_4_0 || UNITY_4_1 || UNITY_4_2 || UNITY_4_3 || UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9 || UNITY_5_0 || UNITY_5_1
			o = (Options)Resources.LoadAssetAtPath(optionPath, typeof(Options));
#else
			o = AssetDatabase.LoadAssetAtPath<Options>(AssetDatabase.GUIDToAssetPath(optionPath));
#endif
			if (o == null)
			{
				Debug.LogError("Failed to load ObfuscatorOptions asset at " + optionPath);
				return null;
			}
			else return o;
		}
	}
}
#endif