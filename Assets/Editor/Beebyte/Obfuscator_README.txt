Obfuscator v1.16.2 Copyright (c) 2015-2016 Beebyte Limited. All Rights Reserved


Usage
=====

The obfuscator is designed to work out of the box. When you build your project it automatically replaces the Assembly-CSharp.dll with an obfuscated version.

The default settings provide a good level of obfuscation, but it's worth looking at Options to enable or disable extra features (such as string literal obfuscation).

See https://youtu.be/Hk-iE9bpYLo for a video example.


Options
=======

From within Unity, select the ObfuscatorOptions asset in the Assets/Editor/Beebyte/Obfuscator directory.

From the Inspector window, you can now see the Obfuscation options available along with descriptions where relevant. The default settings provide a solid configuration that obfuscates the majority of your code, but here you have general control over what is obfuscated.


Code Attributes
===============

Methods often need to be left unobfuscated so that they can be referenced from an external plugin via reflection, or for some other reason. Or maybe you just want a field called "password" to appear as "versionId" when viewed by a decompiler.

You can achieve this by adding Attributes to your code. Have a look at Assets/Editor/Beebyte/Obfuscator/ObfuscatorExample.cs to see how this is done.

The standard System.Reflection.Obfuscation attribute is supported.

The following Beebyte specific attributes are supported:

[SkipRename]                - The obfuscator will not rename this class/method/field, but will continue to obfuscate its contents (if relevant).
[Skip]                      - The obfuscator will not rename this class/method/field, nor will it obfuscate its contents.
[Rename("MyRenameExample")] - The obfuscator will rename this class/method/field to the specified name.


Troubleshooting F.A.Q
=====================

Q. After obfuscating, my 3rd party plugin has stopped working! It has scripts that aren't in the Plugins folder.

A. The simplest way to fix this is to look at the plugin's script to see what namespace they use. Then, towards the bottom of the inspector window in ObfuscatorOptions.asset there is an array called "Skip Namespaces". Add the plugin's namespace to this array and the obfuscator will ignore any matching namespaces. Occassionally a plugin will forget to use namespaces for its scripts, in which case you have three choices: Either move them into a Plugins folder, or annotate each class with [Beebyte.Obfuscator.Skip], or add each class name to the "Skip Classes" array.


Q. After obfuscating, my 3rd party plugin has stopped working! It only has scripts in the Plugins folder.

A. The obfuscator won't have touched files that live in the Plugins folder, however it's likely that the plugin at some point required you to create a specifically named class and/or method. You'll need to add a [SkipRename] attribute to the class and/or method you created in your code.


Q. Button clicks don't work anymore!

A. Check your Options and see if you enabled the "Include public mono methods". If you did, then make sure you've added a [SkipRename] attribute to the button click method.


Q. How do I get string literal encryption to work, my secret field is still showing as plaintext in a decompiler?

A. You need to take the following steps:
     - Enable "Obfuscate Literals" in the ObfuscatorOptions asset.
     - Either leave the default Unicode to 94 (the ^ character), or change it as required.
     - In your code, surround the string with the chosen character, e.g. "secretpass" becomes "^secretpass^";


Q. It's not working for a certain platform.

A. Regardless of the platform, send me an email (beebyte.limited@gmail.com) with the error and I'll see what I can do, but remember that it's only officially tested for Standalone, WebPlayer, and Android platforms.


Q. How can we run obfuscation later in the build process?

A. You can control this in the Assets/Editor/Beebyte/Obfuscator/Postbuild.cs script. The PostProcessScene attribute on the Obfuscate method has an index number that you can freely change to enable other scripts to be called first.


Q. Can I obfuscate externally created DLLs?

A. You can. To do this open Assets/Editor/Beebyte/Obfuscator/ObfuscatorMenuExample.cs. Uncomment this file and change the DLL filepath to point to your DLL. Now use the newly created menu option.


Q. How do I obfuscate local variables?

A. Local variable names are not stored anywhere, so there is nothing to obfuscate. A decompiler tries to guess a local variable's name based on the name of its class, or the method that instantiated it.


Q. I'm getting ArgumentException: The Assembly UnityEditor is referenced by obfuscator ('Assets/ThirdParty/Beebyte/Obfuscator/Plugins/obfuscator.dll'). But the dll is not allowed to be included or could not be found.

A. It's important to keep the directory structure of the obfuscator package. Specifically, the correct location should have an "Editor" folder somewhere on its path. Unity treats files within Editor folders differently. Assets/Editor/Beebyte will ensure that the obfuscator can correctly run and that the obfuscator tool itself won't be included in your production builds.


Q. Is is possible to obfuscate certain public MonoBehaviour methods even when I've told it not to obfuscate them by default?

A. Yes, you override this by using the attribute [System.Reflection.Obfuscation(Exclude=false)] on the method.


Q. Why aren't Namespaces obfuscated?

A. Unity requires that MonoBehaviour classes' namespaces are unchanged. Because most of the time this will be the majority of namespaces, it felt unnecessary to add this feature. If it's something you really want, please let me know otherwise I'll assume it's not wanted.


Q. Something's still not working, how can I get in touch with you?

A. Please email me at beebyte.limited@gmail.com giving as much information about the problem as possible.



Notable 3rd Party Plugins (Beebyte is not affiliated with these products)
=========================================================================

    * NGUI 2.7
      The simplest way to make NGUI compatible is to move all the files in the NGUI package into Assets/Plugins
      

Update History
==============

1.16.2 - 1st April 2016

    * ScriptableObject classes are now treated as Serializable by default (i.e. fields and properties are not renamed). This can be overriden by setting options.treatScriptableObjectsAsSerializable to false, or on a case-by-case basis by making use of [System.Reflection.Obfuscation] on each field, or [System.Reflection.Obfuscation(Exclude = false, ApplyToMembers = true)] on the class.
    * Fixed a TypeLoadException for methods such as public abstract T Method() where a deriving class creates a method replacing the generic placeholder, i.e. public override int Method().
    * Added hidden option for classes to inherit Beebyte attributes Skip and SkipRename that are on an ancestor class. To use, set options.inheritBeebyteAttributes = true prior to obfuscation.

1.16.1 - 23rd March 2016

    * Fixed an issue with 1.16.0 where internal methods would not obfuscate.

1.16.0 - 10th March 2016

    * New option to obfuscate Unity reflection methods instead of simply skipping them.
    * Methods replacing string literals that share the same name now also share the same obfuscated name, so that replaced literals correctly point to their intended method.
    * Faster obfuscation time for methods.
    * Fixed a TypeLoadException.
    * The name translation file now has a consistent order.

1.15.0 - 25th February 2016

    * Added option to include skipped classes and namespaces when searching for string literal replacement via [ReplaceLiteralsWithName] or through the RPC option.
    * Fixed a bug where classes within skipped namespaces could sometimes have their references broken if they link to obfuscated classes.

1.14.0 - 25th February 2016

    * Added option to search SkipNamespaces recursively (this was the default behaviour)
    * Added option to restrict obfuscation to user-specified Namespaces.

1.13.1 - 24th February 2016

    * Fixed a NullReferenceException that could sometimes (very rarely but consistent) occur during the write process.
    * Removed the dependance of the version of Mono.Cecil shipped with Unity by creating a custom library. This is necessary to avoid "The imported type `...` is defined multiple times" errors.

1.13.0 - 23rd February 2016

    * Added a master "enabled" option to easily turn obfuscation on/off through scripts or the GUI.
    * Add Fake Code is now available for WebPlayer builds.
    * Fixed a "UnityException: Failed assemblies stripper" exception that occurs when selecting both fake code and player preference stripping levels.
    * Improvements to Fake Code generation.
    * Obfuscation times can now be printed with a call to Obfuscator.SetPrintChronology(..).
    * Building a project with no C# scripts will no longer cause an error to occur.

1.12.0 - 12th February 2016

    * Added finer control to exclude public or protected classes, methods, properties, fields, events from being renamed. This might be useful to keep a DLLs public API unchanged, for it to then be used in another project.
    * Fixed a bug in the Options inspector that could revert some changes after an option's array's size is altered.

1.11.0 - 3rd February 2016

    * Added an option to specify annotations that should be treated in the same way as [RPC], to cater for third party RPC solutions.

1.10.0 - 28th January 2016

    * Previous obfuscation mappings can now be reused.
    * Unity Networking compatibility (old & new).
    * [RPC] annotated methods are no longer renamed unless an explicit [Rename("newName")] attribute is added, or if an option is enabled.
    * A new [ReplaceLiteralsWithName] attribute exists that can be applied to classes, methods, properties, fields, and events. It should be used with caution since it searches every string literal in the entire assembly replacing any instance of the old name with the obfuscated name. This is useful for certain situations such as replacing the parameters in nView.RPC("MyRPCMethod",..) method calls. It may also be useful for reflection, but note that only exact strings are replaced.

1.9.0 - 23rd January 2016

    * Added a new option "Skip Classes" that is equivalent to adding [Skip] to a class. It's a good long-term solution for 3rd party assets that place files outside of a Plugin directory in the default namespace.
    * Added a way to resolve any future AssemblyResolutionExceptions via the Postscript.cs file without requiring a bespoke fix from Beebyte.
    * Fixed a bug in Postscript.cs for Unity 4.7
    * Added a workaround for an unusual Unity bug where the strings within the Options asset would sometimes be turned into hexadecimals, most noticable when swapping build targets often.

1.8.4 - 7th January 2016

    * Fixed an AssemblyResolutionException for UnityEngine that could sometimes occur.

1.8.3 - 6th January 2016

    * Obfuscation attributes can now be applied to Properties.

1.8.2 - 6th January 2016

    * Serializable classes now retain their name, field names, and property names without requiring an explicit [Skip].
    * Fixed issues using generics that extend from MonoBehaviour.
    * Fixed an issue where two identically named methods in a type could cause obfuscation to fail if one is a generic, i.e. A() , A<T>().
    * Fixed an issue where fake code generation could sometimes result in a failed obfuscation when generic classes are involved.

1.8.1 - 1st January 2016

    * Fixed various issues using generics.
    * Fixed an AssemblyResolutionException for UnityEngine.UI when using interfaces from that assembly.

1.8.0 - 29th December 2015

    * Properties can now be obfuscated.

1.7.3 - 29th December 2015

    * Undocumented fix.

1.7.2 - 28th December 2015

    * Fixed a TypeLoadException error.
    * Fixed an issue with inheritence.
    * Undocumented fix.

1.7.1 - 27th December 2015

    * Fixed a TypeLoadException error.
    * Fixed a "Script behaviour has a different serialization layout when loading." error.
    * Private Fields marked with the [SerializeField] attribute are now preserved by default.
    * Classes extending from Unity classes that have MonoBehaviour as an ancestor were not being treated as such (i.e. UIBehaviour).

1.7.0.1 - 14th December 2015

    * Unity 5.3 compatibility.

1.7.0 - 11th December 2015

    * Improved the "Works first time" experience by searching for reflection methods referenced by certain Unity methods such as StartCoroutine. New option added to disable this feature.
    * WebPlayer support (string literal obfuscation, public field obfuscation, fake code are disabled for WebPlayer builds).
    * Added an option to strip string literal obfuscation markers from strings when choosing not to use string literal obfuscation.
    * ObfuscatorMenuExample.cs added showing how you can Obfuscate other dlls from within Unity.
    * Added protection against accidently obfuscating the same dll multiple times.
    * Added an advanced option to skip rename of public MonoBehaviour fields.

1.6.1 - 16th November 2015

    * First public release

