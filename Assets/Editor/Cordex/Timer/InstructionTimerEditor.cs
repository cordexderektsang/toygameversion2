﻿#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEditor;

[CustomEditor(typeof(BaseTutorialController), true), CanEditMultipleObjects]
public class InstructionTimerEditor : Editor
{
    // Setup the SerializedProperties
    public List<SerializedProperty> serializedProperties;

    void OnEnable()
    {
        serializedProperties = new List<SerializedProperty>();
        serializedProperties.Add(serializedObject.FindProperty(AlphabetCoreConstants.MAGIC_SCREEN));
        serializedProperties.Add(serializedObject.FindProperty(AlphabetCoreConstants.MAGIC_SCREEN2));
        serializedProperties.Add(serializedObject.FindProperty(AlphabetCoreConstants.IS_OVERRIDE_TUTORIAL_SETTING));
        serializedProperties.Add(serializedObject.FindProperty(AlphabetCoreConstants.IS_NEED_TUTORIAL));
        serializedProperties.Add(serializedObject.FindProperty(AlphabetCoreConstants.IS_OVERRIDE_WORD_SETTING));
        serializedProperties.Add(serializedObject.FindProperty(AlphabetCoreConstants.OVERRIDE_WORDS));
        serializedProperties.Add(serializedObject.FindProperty(AlphabetCoreConstants.TUTORIAL_STATES));
        serializedProperties.Add(serializedObject.FindProperty(AlphabetCoreConstants.INSTRUCTION_STATES));
        serializedProperties.Add(serializedObject.FindProperty(AlphabetCoreConstants.END_INSTRUCTION_RESET_TIME));
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        foreach(SerializedProperty sp in serializedProperties)
        {
            InstructionStateCustomList.Show(sp);
        }

        serializedObject.ApplyModifiedProperties();
    }
}
#endif