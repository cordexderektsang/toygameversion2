﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(AlphabetCoreAudioSystem))]
public class AudioEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        AlphabetCoreAudioSystem alphabetCoreAudioSystem = (AlphabetCoreAudioSystem)target;

        if (GUILayout.Button("Load"))
        {
            AlphabetCoreAudioCustomList.LoadData(alphabetCoreAudioSystem);
            //edit.LoadData();
        }
    }
}
#endif

