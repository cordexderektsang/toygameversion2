﻿using UnityEngine;
using System.Collections;

public class BubbleFollowTouchEffect : MonoBehaviour {


    public TouchControllor touchControllor;
    public GameObject bubble;
    public GameObject UiRoot;
    public GameObject panel;
    public Camera nguiCam;
    private UIRoot uiroot;
    private bool isOnButtonDown = false;
    public float depth = 2560;
    private float time = 1;
    private float maxTime = 0.3f;
    public int directionRandom = 0;
    private float mouseDownPosition = 0;
    private BubbleOnClickmovement bubbleMovement;
    private float mouseXFirstClicked;
    private float mouseXSecondClicked;
    private float mouseYFirstClicked;
    private float mouseYSecondClicked;


    void Start()
    {
        uiroot = UiRoot.GetComponent<UIRoot>();
        touchControllor = GetComponent<TouchControllor>();
    }

    // Update is called once per frame
    void Update ()
    {
       if (touchControllor.returnNumberOfTouch() == 0)
        {

            if (Input.GetMouseButtonDown(0))
            {
          
                isOnButtonDown = true;
                if (Input.mousePosition.x > Input.mousePosition.y)
                {
                    if (Input.mousePosition.x - Input.mousePosition.y > 100)
                    {
                        mouseDownPosition = Input.mousePosition.x;
                        directionRandom = Random.Range(0, 2);

                    }
                    else if (Input.mousePosition.x - Input.mousePosition.y < -100)
                    {
                        mouseDownPosition = Input.mousePosition.x;
                        directionRandom = Random.Range(2, 4);

                    } else
                    {
                        mouseDownPosition = Input.mousePosition.x;
                        directionRandom = 4;
                    }
                }

            }
            if (Input.GetMouseButtonUp(0))
            {
                isOnButtonDown = false;
                time = 1;
            }
            if (isOnButtonDown && time < maxTime )
            {
                time += 0.03f;

            }

            else if (isOnButtonDown && time >= maxTime && Input.GetMouseButton(0))
            {
                GameObject currentBubble = Instantiate(bubble);//spawn rock
                bubbleMovement = currentBubble.GetComponent<BubbleOnClickmovement>();
                if (directionRandom == 0)
                {
                    bubbleMovement.movement = BubbleOnClickmovement.direction.DOWN;
                }
                else if (directionRandom == 1)
                {
                    bubbleMovement.movement = BubbleOnClickmovement.direction.UP;
                }
                else if (directionRandom == 2)
                {
                    bubbleMovement.movement = BubbleOnClickmovement.direction.LEFT;
                }
                else if (directionRandom == 3)
                {
                    bubbleMovement.movement = BubbleOnClickmovement.direction.RIGHT;
                }
                else if (directionRandom == 4)
                {
                    bubbleMovement.movement = BubbleOnClickmovement.direction.CENTER;
                }
                currentBubble.transform.parent = panel.transform;
				currentBubble.transform.localPosition = Vector3.zero;
				currentBubble.transform.localScale = new Vector3 (1, 1, 1);
				currentBubble.transform.localEulerAngles = new Vector3 (0, 0, 0);
				Vector3 position = convertToToNguiScreenPosition (Input.mousePosition);
				currentBubble.transform.localPosition = new Vector3(position.x , position.y , 0);


                currentBubble.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
              //  Debug.Log(currentBubble.transform.localPosition);
                time = 0;
            }
        } else
        {
 
            if (touchControllor.returnNumberOfTouch() > 0)
            {


                // for (int i = 0; i < nbTouches; i++)
                //{
                Touch touch = touchControllor.currentTouch();
                     TouchPhase phase = touch.phase;

                if (phase == TouchPhase.Began)
                {
                 mouseXFirstClicked = touch.position.x;
                 mouseYFirstClicked = touch.position.y;
                isOnButtonDown = true;
                }
                //  if (touch.position.x > touch.position.y)
                if (phase == TouchPhase.Moved)
                {
                    mouseXSecondClicked = touch.position.x;
                    mouseYSecondClicked = touch.position.y;
                 
                    if (Mathf.Abs(mouseXFirstClicked - mouseXSecondClicked) > 80 || Mathf.Abs(mouseYFirstClicked - mouseYSecondClicked) > 80)
                    {
                 
                        directionRandom = Random.Range(0, 4);
                    } else
                    {
    
                        directionRandom = 4;
                    }
                       // if (touch.position.x > touch.position.y )
                       // {
                           // directionRandom = Random.Range(0, 4);
                       // }
                       // else
                       // {
                         //   directionRandom = Random.Range(2, 4);
                       // }
                 

                    }
           
                    if (phase == TouchPhase.Ended)
                    {

                    isOnButtonDown = false;
                        time = 1;
                    }
                    if (isOnButtonDown && time < maxTime)
                    {
                        time += 0.03f;

                     }
                    else if (isOnButtonDown && time >= maxTime && phase == TouchPhase.Moved)
                    {
  
                    maxTime = 0.1f;
                    GameObject currentBubble = Instantiate(bubble);//spawn rock
                    bubbleMovement = currentBubble.GetComponent<BubbleOnClickmovement>();
                    if (directionRandom == 0)
                        {
                        bubbleMovement.movement = BubbleOnClickmovement.direction.DOWN;
                        }
                        else if (directionRandom == 1)
                        {
                        bubbleMovement.movement = BubbleOnClickmovement.direction.UP;
                        } else if (directionRandom == 2)
                        {
                        bubbleMovement.movement = BubbleOnClickmovement.direction.LEFT;
                        }
                        else if (directionRandom == 3)
                        {
                        bubbleMovement.movement = BubbleOnClickmovement.direction.RIGHT;
                        } else if (directionRandom == 4)
                        {
                        bubbleMovement.movement = BubbleOnClickmovement.direction.CENTER;
                         }
        
                       // GameObject currentBubble = Instantiate(bubble);//spawn rock
						currentBubble.transform.parent = panel.transform;
						currentBubble.transform.localPosition = Vector3.zero;
						currentBubble.transform.localScale = new Vector3 (1, 1, 1);
						currentBubble.transform.localEulerAngles = new Vector3 (0, 0, 0);
						Vector3 position = convertToToNguiScreenPosition (touch.position);
						Debug.Log (position);
						currentBubble.transform.localPosition = new Vector3(position.x , position.y , 0);


						currentBubble.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);


                        currentBubble.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
                       // Debug.Log(currentBubble.transform.localPosition);
                        time = 0;
                    }
                /*else if (isOnButtonDown && time >= maxTime && phase == TouchPhase.Stationary)
                     {
                    GameObject currentBubble = Instantiate(bubble);//spawn rock
                    currentBubble.transform.parent = UiRoot.transform;
                    Vector3 mousePosition = nguiCam.ScreenToWorldPoint(touch.position);
                    currentBubble.transform.localPosition = new Vector3(mousePosition.x * (uiroot.manualWidth / 2.5f), mousePosition.y * (uiroot.manualHeight / 2), depth);
                    currentBubble.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
                    BubbleOnClickmovement.movement = BubbleOnClickmovement.direction.NONE;
                    time = 0;
                }*/

                }
            }
        }


	/// <summary>
	/// Converts to to ngui screen position.
	/// </summary>
	/// <returns>The to to ngui screen position.</returns>
	/// <param name="mouseClickPosition">Mouse click position.</param>
	public Vector2 convertToToNguiScreenPosition (Vector3 mouseClickPosition)
	{
		Vector3 convertToViewPort = nguiCam.ScreenToViewportPoint(mouseClickPosition);
		int nguiRootWidth = uiroot.manualWidth;
		int nguiRootHeight = uiroot.manualHeight;
		int offsetWidth = nguiRootWidth/2;
		int offsetHeight = nguiRootHeight/2;

		float x = (nguiRootWidth * convertToViewPort.x) - offsetWidth;
		float y = (nguiRootHeight * convertToViewPort.y) - offsetHeight;

		Vector2 covertedValue = new Vector2 (x ,y);
		return covertedValue; 

	}
   }

