﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class LetterID : MonoBehaviour {
    public AlphabetCoreSystem coreSystem;
    public GameObject letterGrid;
    public GameObject panelForWord;
    public GameObject panelForLetter;
    public GameObject dictionaryButtom;
    public GameObject diplayLetter;
    private UISprite displaySprite;
    private UIButton displayButton;
    private int numberOfRow;
    public List<UILabel> columObject = new List<UILabel>();
    public GameObject backButton;
    public GameObject capButton;
    public GameObject wordDisplay;
    public GameObject teemo;
    public AudioSource audio;
    public List<string> word = new List<string>();
    public StageTrackableEventHandler checkIsTrackOn;
  //  public GameObject triggerObjectAnimation;
    
    public void Update()
    {

      /*  if (checkIsTrackOn.isTrackerFind)
        {
           // triggerObjectAnimation.SetActive(true);
        } else
        {
           // triggerObjectAnimation.SetActive(false);
        }*/
    }
    public void Start()
    {
        AlphabetCoreAudioSystem.IsPlaying = false;
        displaySprite = diplayLetter.GetComponent<UISprite>();
        displayButton = diplayLetter.GetComponent<UIButton>();

        foreach (Transform child in letterGrid.transform)
        {
            foreach (Transform row in child)
            {
                columObject.Add(row.gameObject.transform.GetComponent<UILabel>());
            }

        }
     
    }

    /// <summary>
    /// on Button Click Close Both word and letter
    /// </summary>
    public void CloseWordAndLetter()
    {
        panelForLetter.SetActive(false);
        panelForWord.SetActive(false);
        dictionaryButtom.SetActive(true);;
        backButton.SetActive(true);
        capButton.SetActive(true);
       // wordDisplay.SetActive(true);
        teemo.SetActive(true);
        audio.mute = false;
    }


    /// <summary>
    /// on Button Click Close Both word and letter
    /// </summary>
    public void CloseWord()
    {
        panelForLetter.SetActive(true);
        panelForWord.SetActive(false);
        dictionaryButtom.SetActive(false);
        backButton.SetActive(false);
        capButton.SetActive(false);
      //  wordDisplay.SetActive(false);
        teemo.SetActive(false);
    }

    /// <summary>
    /// on button click close word then open letter
    /// </summary>
    public void CloseWordOpenLetter()
    {
        panelForLetter.SetActive(true);
        panelForWord.SetActive(false);
        dictionaryButtom.SetActive(false);
    }


    /// <summary>
    /// on button click open dictionary 
    /// </summary>
    public void OpenDictionary()
    {
        panelForLetter.SetActive(true);
        panelForWord.SetActive(false);
        dictionaryButtom.SetActive(false);
         backButton.SetActive(false);
        capButton.SetActive(false);
      //  wordDisplay.SetActive(false);
         teemo.SetActive(false);
          audio.mute = true;
    }

    /// <summary>
    /// on letter click show related word
    ///  - use letter to find all word
    ///  - add in all letters to show words
    ///  - first spot show letter then rest show word
    /// </summary>
    /// <param name="word"></param>
    public void OnClickLetter(GameObject letter)
    {
       
        dictionaryButtom.SetActive(false);
        panelForLetter.SetActive(false);
        word = coreSystem.setupHeader(letter.name);
        displaySprite.spriteName = letter.name.ToUpper();
        displayButton.normalSprite = letter.name.ToUpper();

        if (word == null)
        {
            for (int i = 0; i < columObject.Count; i++)
            {
                columObject[i].gameObject.SetActive(false);
            }
        } else { 
            for (int i = 0; i < columObject.Count; i++) {
                if (i < word.Count)
                {
                    columObject[i].text = word[i].ToUpper();
                    columObject[i].gameObject.SetActive(true);
                    Debug.Log(columObject[i].gameObject.name);
                    UIButton button = columObject[i].GetComponent<UIButton>();
                    button.onClick.Clear();
                    AddParameterFunction(this, new object[] { columObject[i] }, button, "onClickaddAudio");
                } else
                {
                    columObject[i].gameObject.SetActive(false);
                }
			} 

		} 
        panelForWord.SetActive(true);

    }

    public void  onClickaddAudio(UILabel target)
    {
        if (!AlphabetCoreAudioSystem.IsPlaying)
        {
            string text = target.text.ToLower();
            StartCoroutine(word_pronounciation(text));
        }
     }


    public void onClickLetterAudio(UISprite target)
    {
    
        if (!AlphabetCoreAudioSystem.IsPlaying)
        {
            string text = target.spriteName.ToLower();
            StartCoroutine(letter_pronounciation(text));
        }
    }

    private IEnumerator word_pronounciation(string word)
    {
        while (AlphabetCoreAudioSystem.IsPlaying)
        {
            yield return 0;
        }
        AlphabetCoreAudioSystem.instance.playOnce(word, AlphabetCoreConstants.AUDIO_Selections.WORD_PRONUNCIATION);
    }

    private IEnumerator letter_pronounciation(string letter)
    {
        while (AlphabetCoreAudioSystem.IsPlaying)
        {
            yield return 0;
        }
        AlphabetCoreAudioSystem.instance.playOnce(letter, AlphabetCoreConstants.AUDIO_Selections.LETTER_PRONUNCIATION);
    }


    public void AddParameterFunction(MonoBehaviour Script, object[] Parameter, UIButton button, string methodName)
    {
        if (Script != null && button != null && methodName != null)
        {
            EventDelegate btnFunctionToBeAdd = new EventDelegate(Script, methodName); // add a function from a script
            if (Parameter != null)
            {//if there are parameter
                for (int i = 0; i < Parameter.Length; i++)
                {
                    btnFunctionToBeAdd.parameters[i].value = Parameter[i];// add all parameter
                }
            }
            if (button.onClick.Count == 0)
            {
                EventDelegate.Set(button.onClick, btnFunctionToBeAdd);//set the first script function
            }
            else
            {
                EventDelegate.Add(button.onClick, btnFunctionToBeAdd);  // add another script function 
            }
        }

    }
}
