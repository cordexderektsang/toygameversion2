﻿using UnityEngine;
using UnityEditor;

[CanEditMultipleObjects]
#if UNITY_3_5
[CustomEditor(typeof(UITextureButton))]
#else
[CustomEditor(typeof(UITextureButton), true)]
#endif
public class UITextureButtonEditor : UIButtonEditor
{
	public override void OnInspectorGUI ()
	{
		GUILayout.Space (6f);
		NGUIEditorTools.SetLabelWidth (86f);
		
		/*
		serializedObject.Update ();
		
		serializedObject.FindProperty ("tweenTarget");
		
		DrawProperties ();
		serializedObject.ApplyModifiedProperties ();
		*/
		
		serializedObject.Update();
		NGUIEditorTools.DrawProperty("Tween Target", serializedObject, "tweenTarget");
		DrawProperties();
		serializedObject.ApplyModifiedProperties();

		if (target.GetType () == typeof(UITextureButton))
		{
			GUILayout.Space(3f);

			if (GUILayout.Button ("Click to change to sprite button"))
			{
				NGUIEditorTools.ReplaceClass (serializedObject, typeof(UIButton));
				Selection.activeGameObject = null;
			}
		}
	}
	
	protected override void DrawProperties ()
	{
		DrawTransition();
		DrawColors();

		UITextureButton btn = (target as UITextureButton);
		
		if (btn.tweenTarget != null)
		{
			UITexture texture = btn.tweenTarget.GetComponent<UITexture> ();

			if (texture != null)
			{
				if (NGUIEditorTools.DrawHeader ("Texture", "Texture", false, true))
				{
					NGUIEditorTools.BeginContents (true);
					EditorGUI.BeginDisabledGroup (serializedObject.isEditingMultipleObjects);
					{
						SerializedObject obj = new SerializedObject (texture);
						obj.Update ();
						
						NGUIEditorTools.DrawProperty ("Normal", obj, "mTexture", true);
						obj.ApplyModifiedProperties ();
					}
					EditorGUI.EndDisabledGroup ();
					
					NGUIEditorTools.DrawProperty ("Hover", serializedObject, "hoverTexture", true);
					NGUIEditorTools.DrawProperty ("Pressed", serializedObject, "pressedTexture", true);
					NGUIEditorTools.DrawProperty ("Disabled", serializedObject, "disabledTexture", true);
					
					NGUIEditorTools.DrawPadding ();
					NGUIEditorTools.DrawProperty ("Pixel Snap", serializedObject, "pixelSnap");
					
					NGUIEditorTools.EndContents ();
				}
			}
		}

		UITextureButton button = (target as UITextureButton);
		NGUIEditorTools.DrawEvents ("On Click", button, button.onClick, false);
	}
}
