﻿using UnityEngine;
using System.Collections;

public class playAudio : MonoBehaviour {
    public AudioClip _clip = null;
    public AudioSource audioSoruce;
    private bool IsPlaying = false;
    // Use this for initialization


    // Update is called once per frame
   public void PlayClickSound()
    {
        if (!IsPlaying)
        {
            IsPlaying = true;
            audioSoruce.PlayOneShot(_clip);
            StartCoroutine("countTime", _clip.length);
        }
    }
    private IEnumerator countTime(float t)
    {
        yield return new WaitForSeconds(t);
        IsPlaying = false;
    }
}
