﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class scrollViewFinalVersion: MonoBehaviour
{

    [Header("VERTICAL Gap ")]
    public float gapY = -160; // gapy for each object
    [Header("HORTORIZAL Gap ")]
    public float gapX = 0; // gapx for each object
    [Header("show the start point value (no need fill) ")]
    public float startLenght;
    [Header("show the end point value (no need fill) ")]
    public float endLenght;


    private bool isAllowToScroll = false;
    private bool buildChildList = false;

    [Header("Variable For Mouse ray cast to call")]
    public  bool isHorizotalScroll = false;
    public  bool isVerticalScroll = false;



    public enum OnTouchDirection {NONE,Up,Down }
    [Header("Value for Left and Right")]
    private OnTouchDirection touchMovement = OnTouchDirection.NONE;

    private float firstValue = 0;// first touch
    private float secondValue = 0;//second touch
    private float firstTouch = 0;//first touch down
    private float releaseTouch = 0;// release touch
    private float touchGap = 0;// gap for touch
    private float counter = 0;// counter for force
    private float force = 0.1f;// release force
    private float referenceForce = 0.1f; // reference force
    private bool isClickPressed = false;// did mouse clicked
    private float mouseXFirst;
    private float mouseXSecond;
    private float mouseYFirst;
    private float mouseYSecond;
    private bool canMoveUp = true;
    private bool canMoveDown = true;
    private bool canMoveLeft = true;
    private bool canMoveRight = true;

    private int totalItem = 0;
    private GameObject childObject ;

    public UIPanel middlescrollview;/// middle panel is require

    private List<GameObject> listScrollerObject ;

    public enum ScrollType { NONE, VERTICAL, HORTORIZAL }
    [Header("Type Of Scroll")]
    public ScrollType scroll = ScrollType.NONE;
    private float gapValue = -1;


   

  
    /// <summary>
    /// variable to allow to scroll
    /// </summary>
    /// <returns></returns>
    public bool returnisAllowToScroll()
    {
        return isAllowToScroll;
    }

    /// <summary>
    /// setting for allow to scroll
    /// </summary>
    /// <param name="value"></param>
    public void setIsAllowToScroll(bool value)
    {
        isAllowToScroll = value;
    }

    /// <summary>
    /// return state for current user touch
    /// </summary>
    /// <returns></returns>
    public OnTouchDirection returnOnTouchState()
    {
        return touchMovement;
    }


    /// <summary>
    /// set state for touch
    /// </summary>
    /// <param name="value"></param>
    public void setOnTouchState(OnTouchDirection value)
    {
        touchMovement = value;
    }


    /// <summary>
    /// return all list of child
    /// </summary>
    /// <returns></returns>
    public List<GameObject> returnListOfChildObject()
    {
        return listScrollerObject;
    }

    /// <summary>
    /// return middle scroll panel value
    /// </summary>
    /// <returns></returns>
    public int ReturnScrollPanelSize()
    {
        if (scroll == ScrollType.HORTORIZAL)
        {
            return (int)middlescrollview.GetViewSize().x;
        } else
        {
            return (int)middlescrollview.GetViewSize().y;
        }
    }
   
    /// returnTotalChild
    public int returnTotalItem()
    {
        return totalItem;
    }


    /// <summary>
    /// organise position 
    /// </summary>
    /// <param z value for child object="depth"></param>
    /// <param name="isAutoSetStartPosition"></param>
    /// <param take box collider half way as a start value="shouldTakeHalfSizeFromBoxCollider"></param>
    /// <param setup startlength="shouldSetUpStartLengthValue"></param>
    /// <param assign start length to scroll="shouldAssignStartLengthValueAtGameStart"></param>
    public void OrganisingPosition(float depth = 0, bool isAutoSetStartPosition = true ,bool shouldTakeHalfSizeFromBoxCollider = false , bool shouldSetUpStartLengthValue = true , bool shouldAssignStartLengthValueAtGameStart = true)
    {
  
        totalItem = 0;
        float localX = 0;// create local x
        float localY = 0;// create local y
        listScrollerObject = new List<GameObject>();
        foreach (Transform child in transform)
        {


            if (scroll == ScrollType.HORTORIZAL)
            {
                if (buildChildList)
                {
                    listScrollerObject.Add(child.gameObject);
                }
                child.gameObject.transform.localPosition = new Vector3(localX, child.gameObject.transform.localPosition.y, depth);// set each child position
                localX = localX + gapX;
         
            
                child.gameObject.SetActive(true);
                childObject = child.gameObject;
                if (child.gameObject.activeSelf)
                {
                    totalItem++;
                }

            }
            else if (scroll == ScrollType.VERTICAL)
            {
                if (buildChildList)
                {
                    listScrollerObject.Add(child.gameObject);
                }
                child.gameObject.transform.localPosition = new Vector3(child.transform.localPosition.x, localY, depth);// set each child position

           
                child.gameObject.SetActive(true);
                if (child.gameObject.activeSelf)
                {
                    totalItem++;
                }
                localY = localY + gapY; //
              
            }
        }
   
        if (shouldSetUpStartLengthValue)
        {
            if (scroll == ScrollType.VERTICAL)
            {
                if (totalItem > 0 && shouldTakeHalfSizeFromBoxCollider)
                {
        
                    if (childObject.GetComponent<BoxCollider>() != null)
                    {

                        startLenght = childObject.GetComponent<BoxCollider>().size.y / 2;

                    }
                }
                else
                {
      
                    startLenght = this.gameObject.transform.localPosition.y;
                }

            }
            if (scroll == ScrollType.HORTORIZAL)
            {
                 startLenght = this.gameObject.transform.localPosition.x;
                if (totalItem > 0 && shouldTakeHalfSizeFromBoxCollider)
                {
     
                    if (childObject.GetComponent<BoxCollider>() != null)
                    {
     
                        startLenght = childObject.GetComponent<BoxCollider>().size.x / 2;
  
                    }
                } else
                {
                    startLenght = this.gameObject.transform.localPosition.x;
                }
            }

        }
        if (shouldAssignStartLengthValueAtGameStart)
        {
            if (scroll == ScrollType.VERTICAL)
            {
                this.gameObject.transform.localPosition = new Vector2(this.gameObject.transform.localPosition.x, startLenght);
            }
            if (scroll == ScrollType.HORTORIZAL)
            {
                this.gameObject.transform.localPosition = new Vector2(startLenght, this.gameObject.transform.localPosition.y);
            }
        }
     
        if (GetComponent<UITexture>() != null)
        {
            GetComponent<UITexture>().SetAnchor((Transform)null);
        }

        isAllowToScroll = true;
    }


   public virtual void OnDisable()
    {
        resetAllValue();
    }

    public virtual void resetAllValue()
    {
        if (GetComponent<UITexture>() != null)
        {
            GetComponent<UITexture>().SetAnchor((Transform)null);
        }
        StopCoroutine("verticalSlowEmotion");
        StopCoroutine("hortizontalSlowEmotion");
        touchGap = 0;
        counter = 0;
        gapValue = 0;
        firstValue = 0;// first touch
        secondValue = 0;//second touch
        firstTouch = 0;//first touch down
        releaseTouch = 0;// release touch
        mouseXFirst = 0; ;
        mouseXSecond = 0;
        mouseYFirst = 0;
        mouseYSecond = 0;
    }

    public virtual void OnEnable()
    {
        resetAllValue();
    }

  
    /// <summary>
    ///  check is object reach end point or start point
    ///  if yes reset
    ///  if no scroll
    /// </summary>
    void  Update()
    {

        if (!isAllowToScroll )
        {
  
            return;
        }
        else
        {

            if (scroll == ScrollType.VERTICAL && Mathf.Abs(this.gameObject.transform.childCount * gapY) > middlescrollview.GetViewSize().y && isVerticalScroll )
            {
                if (Input.GetMouseButtonUp(0))
                {
                    resetPosition();
                    touchMovement = OnTouchDirection.NONE;
                }


                SetUp();

                if (this.gameObject.transform.localPosition.y >= startLenght && this.gameObject.transform.localPosition.y <= endLenght)
                {
                    REPEATSCROLL();
                }
         

                if (this.gameObject.transform.localPosition.y >= startLenght && this.gameObject.transform.localPosition.y <= endLenght && !Input.GetMouseButton(0))
                {
                    StartCoroutine("verticalSlowEmotion");

               }
            }
            else if (scroll == ScrollType.HORTORIZAL  && Mathf.Abs(this.gameObject.transform.childCount * gapX) > middlescrollview.GetViewSize().x && isHorizotalScroll)
            {
        
                if (Input.GetMouseButtonUp(0))
                {

                    resetPosition();
                }
                SetUp();

                if (this.gameObject.transform.localPosition.x <= startLenght && this.gameObject.transform.localPosition.x >= endLenght)
                {
                    REPEATSCROLL();
                }
               

                if (touchGap != 0 && this.gameObject.transform.localPosition.x <= startLenght && this.gameObject.transform.localPosition.x >= endLenght && !Input.GetMouseButton(0))
                {
                    StartCoroutine("hortizontalSlowEmotion");
      
                }
            }
        }    
    }

   
  /// <summary>
  ///  return active child number
  /// </summary>
    public void resetScrollerValue()
    {
        if (scroll == ScrollType.VERTICAL)
        {
            this.gameObject.transform.localPosition = new Vector2(this.gameObject.transform.localPosition.x, startLenght);
        }
        if (scroll == ScrollType.HORTORIZAL)
        {
            this.gameObject.transform.localPosition = new Vector2(startLenght, this.gameObject.transform.localPosition.y);
        }
    }

    public int ReturncurrentActiveChildCount()
    {
        int count = 0;
        foreach (Transform child in transform)
        {
            if (child.gameObject.activeSelf)
            {
                count++;
            }
        }
        return count;
    }

    public void SetUp()
    {
    
            if (Input.GetMouseButtonDown(0))
            {

                int childnumber = ReturncurrentActiveChildCount();
                counter = 0;
                touchGap = 0;
                if (scroll == ScrollType.VERTICAL)
                {
                    //get the view size from panel then get the number of child for this parnet 
                    // set the end limit to be the lenght for all child length substract the middle view  size (vertical use negative gapY value)
                    int scrollviewsize = (int)middlescrollview.GetViewSize().y;
                    int containItemInBar = scrollviewsize / -((int)gapY);
                    int remainer = (scrollviewsize % -((int)gapY));
                    int gapForEachItem = 50; // space for item 

                    int limitvalue = (int)startLenght;

                    if (childnumber > containItemInBar)
                    {
                        limitvalue = childnumber - containItemInBar;
                        //  Debug.Log("limited Value : " + limitvalue + "containItemInBar Value : " + containItemInBar);
                        endLenght = -(limitvalue * gapY) + startLenght - remainer + gapForEachItem;
                    }
                    else
                    {
                        endLenght = limitvalue;
                    }
                    // endLenght = -(((childnumber- limitEndRange) * gapY) + scrollviewsize);

                    firstValue = Input.mousePosition.y;
                    firstTouch = Input.mousePosition.y;
                    mouseXFirst = Input.mousePosition.x;

                }
                else if (scroll == ScrollType.HORTORIZAL)
                {
                    //get the view size from panel then get the number of child for this parnet 
                    // set the end limit to be the lenght for all child length substract the middle view  size (vertical use position gapX value)
                    int scrollviewsize = (int)middlescrollview.GetViewSize().x;
                    int containItemInBar = ((scrollviewsize - (int)startLenght) / ((int)gapX)) + 1;

                    int limitvalue = (int)startLenght;
                    if (childnumber > containItemInBar)
                    {
                        limitvalue = childnumber - containItemInBar;
                        Debug.Log("limited Value : " + limitvalue + "containItemInBar Value : " + containItemInBar);
                        endLenght = -(limitvalue * gapX);
                    }
                    else
                    {
                        endLenght = limitvalue;
                    }
                    // endLenght = -(limitvalue * gapX) + scrollviewsize);     
                    firstValue = Input.mousePosition.x;
                    firstTouch = Input.mousePosition.x;
                    mouseYFirst = Input.mousePosition.y;

                }      
        }
        
    }

   

    /// <summary>
    /// scroll up , down , left ,right
    /// </summary>
    private void REPEATSCROLL()
    {
       
        if (Input.GetMouseButtonUp(0))
        {
            if (scroll == ScrollType.VERTICAL)
            {
                firstValue = Input.mousePosition.y;
                releaseTouch = Input.mousePosition.y;
                touchGap = firstTouch - releaseTouch;
            }
            else if (scroll == ScrollType.HORTORIZAL)
            {
                firstValue = Input.mousePosition.x;
                releaseTouch = Input.mousePosition.x;
                touchGap = firstTouch - releaseTouch;
            }


        }
        if (scroll == ScrollType.VERTICAL)
        {
           
            VerticalScrolling();
        }
        else if (scroll == ScrollType.HORTORIZAL)
        {
            HortizontalScrolling();
        }
    }


    /// <summary>
    /// reset position to either endpoint or startpoint
    /// then return status about such reset
    /// true is reset
    /// false did not rest
    /// </summary>
    /// <returns></returns>
    private void resetPosition()
    {
   
        if (scroll == ScrollType.VERTICAL)
        {
            if (this.gameObject.transform.localPosition.y < startLenght)
            {

                StopCoroutine("verticalSlowEmotion");
                this.gameObject.transform.localPosition = new Vector2(this.gameObject.transform.localPosition.x, startLenght);
                canMoveUp = false;
                canMoveDown = true;
            }
            else

             if (this.gameObject.transform.localPosition.y > endLenght)
            {
                StopCoroutine("verticalSlowEmotion");
                this.gameObject.transform.localPosition = new Vector2(this.gameObject.transform.localPosition.x, endLenght);
                canMoveDown = false;
                canMoveUp = true;

            } else
            {
                canMoveDown = true;
                canMoveUp = true;
            }
        }
        else if (scroll == ScrollType.HORTORIZAL)
        {
            if (this.gameObject.transform.localPosition.x > startLenght)
            {
                StopCoroutine("hortizontalSlowEmotion");
                this.gameObject.transform.localPosition = new Vector2(startLenght, this.gameObject.transform.localPosition.y);
                canMoveLeft = false;
                canMoveRight = true;
}
            else

             if (this.gameObject.transform.localPosition.x < endLenght)
            {
                StopCoroutine("hortizontalSlowEmotion");
                this.gameObject.transform.localPosition = new Vector2(endLenght, this.gameObject.transform.localPosition.y);
                canMoveLeft = true;
                canMoveRight = false; 

            } else
            {
                canMoveLeft = true;
                canMoveRight = true;

            }
        }

    }
    

    /// <summary>
    /// Vertical scroll
    /// </summary>
    private void VerticalScrolling()
    {
        if (Input.GetMouseButton(0))
        {
            secondValue = Input.mousePosition.y;
            mouseXSecond = Input.mousePosition.x;
            float mouseYDistance = Mathf.Abs(firstValue - secondValue);
            float mouseYActalDistance = firstValue - secondValue;
            float mouseXDistance = Mathf.Abs(mouseXFirst - mouseXSecond);
    
                resetPosition();
    
            if (firstValue > secondValue && canMoveDown && mouseYActalDistance > 10 )
                {
                touchMovement = OnTouchDirection.Down;

                 this.gameObject.transform.localPosition = new Vector2(this.gameObject.transform.localPosition.x, this.gameObject.transform.localPosition.y - ((firstValue - secondValue) * 3)/*counter*/);
         
                //float value = (firstValue - secondValue) /10* Time.deltaTime;    
                   // transform.Translate(0, -value, 0);
                }
                else if (firstValue < secondValue && canMoveUp && mouseYActalDistance < -10)
                {

                 this.gameObject.transform.localPosition = new Vector2(this.gameObject.transform.localPosition.x, this.gameObject.transform.localPosition.y + ((secondValue - firstValue) * 3)/*counter*/);
                touchMovement = OnTouchDirection.Up;
                //float value = (secondValue - firstValue) /10* Time.deltaTime;
              
                 // transform.Translate(0, value , 0);
                }
                firstValue = secondValue;
                if (!Input.GetMouseButtonUp(0))
                {
                    firstTouch = Input.mousePosition.y;
                }
           
        }
    }



    /// <summary>
    /// HortizontalScroll
    /// </summary>
    private void HortizontalScrolling()
    {
        if (Input.GetMouseButton(0))
        {

            secondValue = Input.mousePosition.x;
            mouseYSecond = Input.mousePosition.y;
            float mouseYDistance = Mathf.Abs(mouseYFirst - mouseYSecond);
            float mouseXDistance = Mathf.Abs(firstValue - secondValue);

            if (mouseXDistance > mouseYDistance)
            {
                resetPosition();
                if (firstValue > secondValue && canMoveLeft)
                {
                   
                   // transform.Translate(0, -0.1f, 0);
                    this.gameObject.transform.localPosition = new Vector2(this.gameObject.transform.localPosition.x - (firstValue - secondValue), this.gameObject.transform.localPosition.y);

                }
                else if (firstValue < secondValue && canMoveRight)
                {
                 
                   // transform.Translate(0, 0.1f, 0);
                    this.gameObject.transform.localPosition = new Vector2(this.gameObject.transform.localPosition.x + (secondValue - firstValue), this.gameObject.transform.localPosition.y);

                }
                firstValue = secondValue;

            }
        }
    }

    /// <summary>
    ///fade  slow Force after touch release
    /// </summary>
    IEnumerator  verticalSlowEmotion()
    {
        if (touchGap > 0)
        {
            gapValue = Mathf.Abs(touchGap /100);
            if (counter < gapValue)
            {
                resetPosition();
                yield return new WaitForSeconds(0.01f);
                transform.Translate(0, -0.01f, 0);
                //this.gameObject.transform.localPosition = new Vector2(this.gameObject.transform.localPosition.x, this.gameObject.transform.localPosition.y - counter);
                counter += 0.2f;

            }
            resetPosition();
          
       
        }
         else if (touchGap < 0)
        {
             gapValue = Mathf.Abs(touchGap / 100);

            if (counter < gapValue )
            {
                resetPosition();
                yield return new WaitForSeconds(0.01f);
                transform.Translate(0, 0.1f, 0);
                //this.gameObject.transform.localPosition = new Vector2(this.gameObject.transform.localPosition.x, this.gameObject.transform.localPosition.y - counter);
                counter += 0.2f;
         

            }
            resetPosition();
            

        } 

    }


    /// <summary>
    ///fade  slow Force after touch release
    /// </summary>
    IEnumerator hortizontalSlowEmotion()
    {

        if (touchGap > 0)
        {
            float gapValue = Mathf.Abs(touchGap / 100);
            if (counter < gapValue)
            {
                resetPosition();
                yield return new WaitForSeconds(0.01f);
                transform.Translate(-0.01f, 0, 0);
                //this.gameObject.transform.localPosition = new Vector2(this.gameObject.transform.localPosition.x, this.gameObject.transform.localPosition.y - counter);
                counter += 0.1f;

            }
            resetPosition();
            yield return new WaitForSeconds(0.01f);
        }
        else if (touchGap < 0)
        {
            float gapValue = Mathf.Abs(touchGap / 100);
            if (counter < gapValue)
            {
                resetPosition();
                yield return new WaitForSeconds(0.01f);
                transform.Translate(0.01f, 0, 0);
                //this.gameObject.transform.localPosition = new Vector2(this.gameObject.transform.localPosition.x, this.gameObject.transform.localPosition.y - counter);
                counter += 0.1f;

            }
            resetPosition();
            yield return new WaitForSeconds(0.01f);
        }

    }


}