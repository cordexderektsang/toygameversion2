﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class LookAtUser : MonoBehaviour
{
    public GameObject neck;
    //public List<GameObject> neckList;
    //public List<GameObject> dummyNeckList;
    public GameObject dummyNeck;
    public GameObject cam;
    public float maxAngle = 30;
    public GameObject imageTarget;
    // Use this for initialization
    void Start ()
    {
        cam = Camera.main.gameObject;
        SetObjectLookAtUser(GameObject.Find("woman"));
    }

    // Update is called once per frame
    /// <summary>
    /// update the neck's angle 
    /// </summary>
    void Update ()
    {
        //for (int i = 0; i < neckList.Count; i++)
        //{
        //    Vector3 pos = cam.transform.position;
        //   // pos.y -= 0.62f;
        //    dummyNeckList[i].transform.LookAt(pos, Vector3.up);
        //    Vector3 angle = new Vector3();
        //    angle.z = -dummyNeckList[i].transform.eulerAngles.x;
        //    angle.y = -dummyNeckList[i].transform.eulerAngles.z;
        //    angle.x = -(dummyNeckList[i].transform.eulerAngles.y - 180);

        //    angle = updateAngle(angle);
        //    angle = setMaxAngle(angle);
        //    neckList[i].transform.localEulerAngles = angle;
        //}
        if (neck != null)
        {
            Vector3 pos = cam.transform.position;
            // pos.y -= 0.62f;
            dummyNeck.transform.LookAt(pos, Vector3.up);
            Vector3 angle = new Vector3();
            angle.z = -dummyNeck.transform.eulerAngles.x;
            angle.y = -dummyNeck.transform.eulerAngles.z;
            angle.x = -(dummyNeck.transform.eulerAngles.y - 180);

            angle = updateAngle(angle);
            angle = setMaxAngle(angle);
            neck.transform.localEulerAngles = angle;
        }
    }

    public void SetObjectLookAtUser(GameObject item)
    {
        //neck = item.transform.FindChild("Neck_M").gameObject;
        neck = GameObject.Find("Neck_M") ;
        dummyNeck.transform.position = neck.transform.position;
        //for (int i = 0; i < neckList.Count; i++)
        //{
        //    GameObject dummyNeck1 = new GameObject();
        //    dummyNeck1.transform.SetParent(imageTarget.transform);
        //    dummyNeck1.transform.position = neckList[i].transform.position;
        //    dummyNeckList.Add(dummyNeck1);

        //}
    }
    /// <summary>
    /// set the angle between -180 to 180;
    /// </summary>
    /// <param name="angle"></param>
    /// <returns>angle</returns>
    private Vector3 updateAngle(Vector3 angle)
    {
        if (angle.x > 180)
        {
            angle.x -= 360;
        }
        else if (angle.x < -180)
        {
            angle.x += 360;
        }
        if (angle.y > 180)
        {
            angle.y -= 360;
        }
        else if (angle.y < -180)
        {
            angle.y += 360;
        }
        if (angle.z > 180)
        {
            angle.z -= 360;
        }
        else if (angle.z < -180)
        {
            angle.z += 360;
        }
        return angle;
    }

    /// <summary>
    /// set the maximum angle 
    /// </summary>
    /// <param name="angle"></param>
    /// <returns></returns>
    private Vector3 setMaxAngle(Vector3 angle)
    {
        if (angle.x > maxAngle)
        {
            angle.x = maxAngle;
        }
        else if (angle.x < -maxAngle)
        {
            angle.x = -maxAngle;
        }

        if (angle.y > maxAngle)
        {
            angle.y = maxAngle;
        }
        else if (angle.y < -maxAngle)
        {
            angle.y = -maxAngle;
        }

        if (angle.z > maxAngle)
        {
            angle.z = maxAngle;
        }
        else if (angle.z < -maxAngle)
        {
            angle.z = -maxAngle;
        }
        return angle;
    }
}
