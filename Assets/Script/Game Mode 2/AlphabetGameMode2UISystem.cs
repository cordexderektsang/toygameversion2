﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class AlphabetGameMode2UISystem : MonoBehaviour 
{
	//Please respell after removing all ARlphabet cards
	public AlphabetCoreSystem mSystem;

    public List<Texture> listofTextureName = new List<Texture>();
    public UITexture broad;
    public ShakepositionEffect Bubble;
	public ParticleSystem particle;

    public string last_spelling = "", last_voice = "";
	private string currentSpelling = "";
	// Command on version 1.3.7
	// private bool isScrolling = false,
	//			 isWordHit = false; //Beta version 1.1
				/*, isSetupFinish = false;*/ //Beta version 1.0
	private int label_list_index = 0;
	private UIScrollView _scrollview;
	private UIPanel _scroll_panel;
	private CCManualLoadResourceObj loader;
	private bool isTurnOffByManual = false;
	private bool wordCorrect = false;
    private bool isCheckingWord = false;
	private Transform objectSpawnonWhichLetter;
	private GameObject previousletterIDBody;
	private GameObject previousletterIDControl;
	
	// Var of tutorial system
	private GameMode2TutorialController tutorialController;
//	private AlphabetTutorialBehaviour tutorialSystem;
	private bool isTutorialTime;
	
	public Transform objParent;
	public GameObject notice_obj, refresh_button;
	public GameObject[] dict_selection;
	public UILabel prefeb, dictLabel;
	public Texture[] encouragementTexture;
	public UITexture WordCompletionTexture;
	//public UIGrid _container;
	public UIWrapContent _container;
	private List<UILabel> label_list = new List<UILabel> ();

    public GameObject backButton;
    public GameObject dictionary;
    private BoxCollider backButtonCollider;
    private BoxCollider dictionaryCollider;
    private UIButton dictionaryButtonUI;
    private UIButton backButtonUI;
    public GameObject captureShareButton;
    private BoxCollider captureShareButtonCollider;
    private UIButton captureShareButtonUI;
	private GameMode2CoreonTrackFunction gameMode2CoreonTrackFunction;
	private TipsForOnshot tipsForOnShot;

    public Renderer markerBg;

	public List<string> letterDisplayMode = new List<string> ();
	
	public static readonly string COLOR_CODE_BLUE = "[6aa2db]";
	public static readonly string COLOR_CODE_END = "[-]";

    // Use this for initialization
    //void Start ()
    public void initialize()
    {
		/*for (int i = 0; i < dict_selection.Length; i++)
		{
			dict_selection[i].SetActive (false);
		}*/
		tipsForOnShot = GetComponent<TipsForOnshot> ();
		gameMode2CoreonTrackFunction = GetComponent<GameMode2CoreonTrackFunction> ();
		//mSystem = GetComponent <AlphabetCoreSystem> ();
			// GetComponentInParent<AlphabetCoreSystem> ();
		letterDisplayMode = mSystem.initDict ("dictionary/dict", 6);
		
		_scrollview = NGUITools.FindInParents <UIScrollView> (_container.gameObject);
		_scroll_panel = NGUITools.FindInParents <UIPanel> (_container.gameObject);

        backButtonCollider = backButton.GetComponent<BoxCollider>();
        backButtonUI = backButton.GetComponent<UIButton>();

        captureShareButtonCollider = captureShareButton.GetComponent<BoxCollider>();
        captureShareButtonUI = captureShareButton.GetComponent<UIButton>();

        dictionaryCollider = dictionary.GetComponent<BoxCollider>();
        dictionaryButtonUI = dictionary.GetComponent<UIButton>();

        //mSystem.OnSetHeaderAlphabet   += OnHeaderAlphabetSet;
        //mSystem.OnUnsetHeaderAlphabet += OnHeaderAlphabetUnset;
        mSystem.OnWordHit		  += OnWordHitPass;
        mSystem.OnInputChange += OnInputChange;
        mSystem.OnMarkerAlphabetCheck += OnMarkerAlphabetCheck;

        //_scrollview.onDragStarted   += OnScrollStart;
        //_scrollview.onStoppedMoving += OnScrollStopMoving;

        loader = new CCManualLoadResourceObj ();
		
		AppController.ManualReleaseMemory ();
		
//		tutorialSystem = GetComponent <AlphabetTutorialBehaviour> ();
		tutorialController = GetComponent <GameMode2TutorialController> ();
        isTutorialTime = tutorialController.isNeedTutorial;
        if (isTutorialTime)
        {
            dictionaryCollider.enabled = false;
            dictionaryButtonUI.SetState(UIButtonColor.State.Disabled, false);
        }
        
        //		if (isTutorialTime)
        //		{
        //			tutorialSystem.onTutorialFinish += OnTutorialFinish;
        //			mSystem.onStageMarkerFound += OnStageMarkerFound;
        //		}
    }
	
    void Update ()
    {
        //disable interaction with other button durring capture
       /* if (AlphabetCoreSystem.capturing == true)
        {
            backButtonCollider.enabled = false;
            captureShareButtonCollider.enabled = false;
        }
        else if (backButtonCollider.enabled == false && tutorialController.isTutorialFinished())
        {
            backButtonCollider.enabled = true;
            captureShareButtonCollider.enabled = true;
        }*/
    }


	public void UnLoad3DObject ()
	{

		loader.unloadGameObject ();
	}

	void OnDestroy ()
	{
		loader = null;
	
		//_scrollview.onDragFinished  -= OnScrollStart;
		//_scrollview.onStoppedMoving -= OnScrollStopMoving;
		
		//mSystem.OnSetHeaderAlphabet   -= OnHeaderAlphabetSet;
		//mSystem.OnUnsetHeaderAlphabet -= OnHeaderAlphabetUnset;
		mSystem.OnWordHit		  -= OnWordHitPass;
		mSystem.OnInputChange	  -= OnInputChange;
		
		//mSystem.onStageMarkerFound = null;
//		tutorialSystem.onTutorialFinish = null;
	}

    //	private bool canStage0EventSendOut = true, canStage1EventSendOut = false;

    //	private void sendToTutorialSystem (int stageID, float delayTime = 1.5f)
    //	{
    //		if (!isTutorialTime) 
    //			return;
    //		
    //		if (stageID == 0 && !tutorialSystem.isButtonClicked)
    //		{
    //			canStage0EventSendOut = false;
    //			tutorialSystem.onTutorialOkButtonClicked += OnTutorialOkButtonClick;
    //			return;
    //		}
    //		
    //		tutorialSystem.QueueCompleteStageEvent (stageID, delayTime);
    //	}

    //	void OnStageMarkerFound ()
    //	{
    //		sendToTutorialSystem (0);
    //	}

    //	void OnTutorialOkButtonClick ()
    //	{
    //		if (tutorialSystem.tutorialOkButtonClickCounter == 1)
    //		{
    //			if (!canStage0EventSendOut && tutorialSystem.isTrackerFind)
    //			{
    //				canStage0EventSendOut = true;
    //				sendToTutorialSystem (0);
    //				
    //				AlphabetCoreAudioSystem.instance.stopAll ();
    //				AlphabetCoreAudioSystem.instance.playOnce (AlphabetCoreSystem.CorrectPronounciation (last_spelling));
    //				last_voice = last_spelling;
    //			}
    //			else if (!tutorialSystem.isTrackerFind)
    //			{
    //				canStage0EventSendOut = false;
    //			}
    //		}
    //		else if (tutorialSystem.tutorialOkButtonClickCounter == 2)
    //		{
    //			tutorialSystem.onTutorialOkButtonClicked -= OnTutorialOkButtonClick;
    //		}
    //	}

    public void disableLeftSideUIButtons()
    {
        backButtonCollider.enabled = false;
        backButtonUI.SetState(UIButtonColor.State.Disabled, false);

        captureShareButtonCollider.enabled = false;
        captureShareButtonUI.SetState(UIButtonColor.State.Disabled, false);

        dictionaryCollider.enabled = false;
        dictionaryButtonUI.SetState(UIButtonColor.State.Disabled, false);
    }

    private void enableUIButtons()
    {
        backButtonCollider.enabled = true;
        backButtonUI.SetState(UIButtonColor.State.Normal, false);

        captureShareButtonCollider.enabled = true;
        captureShareButtonUI.SetState(UIButtonColor.State.Normal, false);

        dictionaryCollider.enabled = true;
        dictionaryButtonUI.SetState(UIButtonColor.State.Normal, false);
    }

    //public void enableTutorialNextButton()
    //{
    //    tutorialNextButtonCollider.enabled = false;
    //    tutorialNextButtonUI.SetState(UIButtonColor.State.Disabled, false);
    //}

    public void OnTutorialFinish ()
	{
		CCLog.SpecialMsg ("OnTutorialFinish");
        isTutorialTime = false;
        enableUIButtons();
        //OnResetHeaderBtnClick();
//		tutorialSystem.onTutorialFinish = null;
	}	
	
	//string last_input = "";
	void OnInputChange (string input)
	{
		/* 
		 // Beta Version 1.0
		 // if(!isSetupFinish) return;
		
		// Release All Label if input = empty
		if (string.IsNullOrEmpty (input))
		{
			OnResetHeaderBtnClick ();
			return;
		}
		*/

		currentSpelling = input;
        // Consult in 1.4.0 //
        //Debug.Log("input.Length: " + input.Length);
        //Debug.Log("mSystem.isHeaderSet (): " + mSystem.isHeaderSet());
        //Debug.Log("mSystem.getCurrentHeader (): " + mSystem.getCurrentHeader());
        //Debug.Log("input [0].ToString (): " + input[0].ToString());
        if (input.Length > 0 && mSystem.isHeaderSet () && !(mSystem.getCurrentHeader ().Equals(input [0].ToString ()))/*&& last_input.Length > 0 && (!(last_input.StartsWith (input)) || (last_input [0] != input [0]))*/)
		{
            //CCLog.SpecialMsg("OnInputChange release header");
			mSystem.releaseHeader ();
			mSystem.setupHeader (""+input[0]);
		}

		if (isCheckingWord) {
			Debug.Log ("clear");
			dictLabel.text = "";
			resetDisplayColorAnddisEnableDisplayObjecr ();
			isCheckingWord = false;
		}
		// last_input = input;
		///////////////////////
		
		/*
		foreach(UILabel label in label_list)
		{
			if (label == null) continue;
			
			string tmp = label.text.Replace (COLOR_CODE_BLUE, "").Replace (COLOR_CODE_END, "");
			if(tmp.StartsWith (input)) //Contains
			{
				int index_of_word = tmp.IndexOf (input);
				if (index_of_word == 0)
					tmp = input + COLOR_CODE_BLUE + tmp.Substring (index_of_word + input.Length) + COLOR_CODE_END;
				else if (index_of_word + input.Length == tmp.Length)
					tmp = COLOR_CODE_BLUE + tmp.Substring (0, index_of_word) + COLOR_CODE_END + input;
				else
					tmp = COLOR_CODE_BLUE + tmp.Substring (0, index_of_word) + COLOR_CODE_END + input + COLOR_CODE_BLUE + tmp.Substring (index_of_word + input.Length, tmp.Length - input.Length - index_of_word) + COLOR_CODE_END; 
			}
			else if (tmp [0] == input [0]) 
			{
				int index = 1;
				for (; index < tmp.Length; index++)
				{
					if (tmp [index] != input [index]) break;
				}
				tmp = tmp.Substring (0, index) + COLOR_CODE_BLUE + tmp.Substring (index) + COLOR_CODE_END;
			}
			else
			{
				// Alpha Version
				// tmp = COLOR_CODE_BLUE + tmp + COLOR_CODE_END;
				
				
				// Beta Version 1.0 & 1.1  //
				// isSetupFinish = false;
				mSystem.releaseHeader (); // or OnResetHeaderBtnClick ();
				mSystem.setupHeader (mSystem.getFirstAlphabetId ());
				OnInputChange (mSystem.result_spelling);
				/////////////////////////////
				
				// mSystem.resetHeader (); // Can be replace the beta area by it!
				break;
			}
			label.text = tmp;
		}
		*/
	}
	
	public void OnWordHitPass (string spelling)
    {
		
        if (isCheckingWord
            || (isTutorialTime
                && (tutorialController.tutorialState != 1
                || tutorialController.tutorialState == 1 && (!tutorialController.isFadeInAudioPlayedOnce() || TutorialInstructionAudioController.IsPlaying))))
        {
            return;
        }

        isCheckingWord = true;

		// Spell once
		CCLog.SpecialMsg ("Hit : " + spelling + " Last : " + last_spelling);
		//CCPlayerStatusManager.ChangePlayerStatus (AlphabetCoreConstants.PLAYER_STATUS.WORD);
		
		// isWordHit = true;
		
		// Beta version 1.4 //
		//dict_selection [0].SetActive (true);
		dictLabel.text = spelling;

        broad.mainTexture = listofTextureName[1];
      //  Bubble.turnShakingOn();
    //label_list_index = label_list.IndexOf ((label_list.FindAll(delegate (UILabel label) {return label.text.Replace (COLOR_CODE_BLUE, "").Replace (COLOR_CODE_END, "") == spelling;}) [0]));
    //(label_list [label_list_index]).transform.parent = dict_selection [0].transform;
    //(label_list [label_list_index]).transform.localPosition = new Vector3 (0, 10, 0);
    //////////////////////

    /*
    label_list_index = label_list.IndexOf ((label_list.FindAll(delegate (UILabel label) {return label.text.Replace (COLOR_CODE_BLUE, "").Replace (COLOR_CODE_END, "") == spelling;}) [0]));
    startSpring ();
    */

    refresh_button.SetActive (true);
		refresh_button.GetComponent<UISprite> ().ResizeCollider ();
		string tmp_voice = "";
		for(int i = 0; i < spelling.Length; i++)
		{
			tmp_voice += spelling[i] + " ";
		}
		tmp_voice += spelling;
		
		// Consult the beta version by Cordex  //
		// Control the audio
		tmp_voice = spelling;
		//////////////////////////////////////
		
        /*
		if (isTutorialTime)
		{
//			if (tutorialSystem.tutorialOkButtonClickCounter > 0)
//			{
			if(last_voice != tmp_voice)
			{
                AlphabetCoreAudioSystem.instance.stopAll();
                //AlphabetCoreAudioSystem.instance.playOnce (AlphabetCoreSystem.CorrectPronounciation (spelling));
                AlphabetCoreAudioSystem.instance.playOnce(spelling);
                last_voice = tmp_voice;
            }
//			}
		}
        */

        if (!isTutorialTime /*&& !last_voice.Equals(tmp_voice)*/)
		{
			AlphabetCoreAudioSystem.instance.stopAll ();
            //AlphabetCoreAudioSystem.instance.playOnce (AlphabetCoreSystem.CorrectPronounciation (spelling));
            AlphabetCoreAudioSystem.instance.playOnce(spelling , AlphabetCoreConstants.AUDIO_Selections.WORD_PRONUNCIATION);
            last_voice = tmp_voice;
		}

        // Consult the beta version by Cordex  //
        // Control the animation
        //if (last_spelling == spelling)
       // if (!last_spelling.Equals(spelling))
        //{
           // last_spelling = spelling;
            //////////////////////////////////////
		if (!last_spelling.Equals (spelling)) {
			try {
				last_spelling = spelling;
				loader.unloadGameObject ();
			} catch {
			}
	
			Debug.Log (AlphabetCoreSystem.WordCategory(spelling));
          markerBg.material.mainTexture = loader.loadTexture("floor/" + AlphabetCoreSystem.WordCategory(spelling));


            try
            {
                Transform obj = loader.loadGameObject("obj/" + spelling).transform;
                /*if (isTutorialTime)
                {
                    tutorialController.setLoad3dObject(spelling);
                    //obj.GetComponentInChildren<Animation>().Stop();
                }

                if (!tutorialController.magicScreen.isTrackerFind && !tutorialController.magicScreen2.isTrackerFind)
                {
                    Renderer[] components = obj.GetComponentsInChildren<Renderer>();
                    foreach (Renderer render in components)
                    {
                        render.enabled = false;
                    }
                }*/

                // Reset position after assigning to parent
                Vector3 pos = obj.localPosition;
                Vector3 scale = obj.localScale;
				Vector3 rotation = obj.localEulerAngles;
				obj.parent = markerBg.transform;
				//objParent.gameObject.SetActive (true);
				char [] lastLetter = spelling.ToCharArray();

				objectSpawnonWhichLetter = GetLastLetterFromTheWord(lastLetter,mSystem.return_ListOfTrackableEventHandle()).transform;
			//	Transform currentLastLetter =  mSystem.return_ListOfTrackableEventHandle()[mSystem.return_ListOfTrackableEventHandle().Count-1].transform;
			if(objectSpawnonWhichLetter != null) {
					//objectSpawnonWhichLetter.GetComponent<BoxCollider>().enabled = false;
				//	Destroy(objectSpawnonWhichLetter.GetComponent<Rigidbody>());
					//GameObject letterIDBody =objectSpawnonWhichLetter.GetChild(0).gameObject;
					//GameObject letterIDControl =objectSpawnonWhichLetter.GetChild(1).gameObject;
					//objectSpawnonWhichLetter.gameObject.SetActive(false);
					objParent.parent = objectSpawnonWhichLetter.transform.parent;
					//letterIDBody.SetActive(false);
					//letterIDControl.SetActive(false);
					//previousletterIDBody = letterIDBody;
					//previousletterIDControl = letterIDControl;
					objParent.localPosition =new Vector3(0,1,1.33f);
					objParent.localScale = new Vector3(40,40,40);
					objParent.localEulerAngles = Vector3.zero;
	                obj.localPosition = pos;
	                obj.localScale = scale;
					//obj.localScale = new Vector3(0.01f,0.01f,0.01f);;
	                obj.localEulerAngles = rotation;   
					var em = particle.emission;
					em.enabled = false;
					//AlphabetCoreTrackableEventHandler currentTrackEvent = TipsForOnshot.CurrentOnHitTarget.GetComponent<AlphabetCoreTrackableEventHandler>();
					//setRender(currentTrackEvent.isTrack,objParent.gameObject);
					//currentTrackEvent.ResetActiveTrackableEvent();
					AlphabetCoreTrackableEventHandler currentTrackEvent = objectSpawnonWhichLetter.transform.parent.gameObject.GetComponent<AlphabetCoreTrackableEventHandler>();
					// currentTrackEvent = TipsForOnshot.CurrentOnHitTarget.GetComponent<AlphabetCoreTrackableEventHandler>();
					currentTrackEvent.ResetActiveTrackableEvent();

				}
		

            }
		catch (System.Exception ex){Debug.Log (ex);}

            wordCorrect = true;
			TipsForOnshot.IsCorrect = true;
			playEncouragement (spelling);
        //}

        //isCheckingWord = false;
		}
		objParent.gameObject.SetActive (true);
    }

	private GameObject GetLastLetterFromTheWord (char [] lastLetter , List<AlphabetCoreTrackableEventHandler> trackableEvent)
	{
	//	foreach(char letter in  lastLetter) 
		//{
		int length = trackableEvent.Count;
		int center = 0;
		if (length % 2 == 0) {
			center = length / 2-1;
		} else {

			center = (length / 2);
		}

		return trackableEvent[center].transform.GetChild (0).gameObject;
	
			//foreach (AlphabetCoreTrackableEventHandler track in trackableEvent) 
			//{
				//Debug.Log (track.transform.GetChild(0).gameObject.name + " : " + letter);
				//if (track.transform.GetChild(0).gameObject.name.Equals (letter.ToString())) {
				//	return track.gameObject;

				//}

			//}
		//}
		return null;
	}


	public void resetIscheckWordBoolean ()
	{
		isCheckingWord = false;
		/*if (previousletterIDControl != null)
		{
			Debug.Log ("previousDisableLetter");
			if (objectSpawnonWhichLetter != null) {
				objectSpawnonWhichLetter.GetComponent<BoxCollider> ().enabled = true;
				if (objectSpawnonWhichLetter.gameObject.GetComponent<Rigidbody> () == null) {
					Rigidbody rig = objectSpawnonWhichLetter.gameObject.AddComponent<Rigidbody> ();
					rig.constraints = RigidbodyConstraints.FreezeAll;
				}
				previousletterIDControl.SetActive (true);
				previousletterIDBody.SetActive (true);
				objectSpawnonWhichLetter.gameObject.SetActive(true);
			}
		}*/
	}

	private void setRender (bool status, GameObject target) 
	{
		Debug.Log ("render " + status);

		Renderer[] rendererComponents = target.GetComponentsInChildren<Renderer>(true);
		Collider[] colliderComponents = target.GetComponentsInChildren<Collider>(true);
		Animation[] animationComponents = target.GetComponentsInChildren<Animation>(true);

		// Enable rendering:
		foreach (Renderer component in rendererComponents)
		{
			component.enabled = status;
		}

		// Enable colliders:
		foreach (Collider component in colliderComponents)
		{
			component.enabled = status;
		}

		// Enable animations:
		foreach (Animation component in animationComponents)
		{
			component.Play ();
		}


	}


    public void playTutorialWordPronounciation()
    {
        AlphabetCoreAudioSystem.instance.stopAll();
        //AlphabetCoreAudioSystem.instance.playOnce(AlphabetCoreSystem.CorrectPronounciation(last_voice));
        AlphabetCoreAudioSystem.instance.playOnce(last_spelling, AlphabetCoreConstants.AUDIO_Selections.WORD_PRONUNCIATION);
    }

	/// <summary>
	/// new add in encouragemnet after spell a word
	/// </summary>
	/// <param name="last_spelling">Last spelling.</param>
	public void playEncouragement(string last_spelling)	
	{
		int randomPickEncouragement =UnityEngine.Random.Range (0, encouragementTexture.Length);
		AlphabetCoreAudioSystem.instance.stopAll();
		//AlphabetCoreAudioSystem.instance.playOnce(AlphabetCoreSystem.CorrectPronounciation(last_voice));
		AlphabetCoreAudioSystem.instance.playOnce(encouragementTexture[randomPickEncouragement].name, AlphabetCoreConstants.AUDIO_Selections.ENCOURAGEMENT);
		WordCompletionTexture.gameObject.SetActive (true);
		WordCompletionTexture.mainTexture = encouragementTexture[randomPickEncouragement];
		StartCoroutine (pronounciation(last_spelling));
	}



	[System.Reflection.Obfuscation(ApplyToMembers=false)]
	private IEnumerator pronounciation (string last_spelling)
	{
		while (true)
		{
			if (AlphabetCoreAudioSystem.IsPlaying) {
				yield return null;
			} else {
				WordCompletionTexture.gameObject.SetActive (false);
				AlphabetCoreAudioSystem.instance.playOnce(last_spelling ,AlphabetCoreConstants.AUDIO_Selections.WORD_PRONUNCIATION);
				break;

			}
		}

	}

	/// <summary>
	/// Checks if the word is spelled correctly
	/// </summary>
	/// <returns>True if word is correctly spelled</returns>
	public bool returnIsWordCorrect() {
		return wordCorrect;
	}
	
	void OnHeaderAlphabetSet ()
	{		
		List<string> tmp_list = mSystem.getHeaderList ();
		
		if(tmp_list == null || tmp_list.Count < 1 || tmp_list.Capacity < 1) 
		{
			return;
		}
		
		foreach(string word in tmp_list)
		{
			string tmp = COLOR_CODE_BLUE + word + COLOR_CODE_END;
			
			UILabel label = GameObject.Instantiate (prefeb) as UILabel;
			label.transform.parent 			= _container.transform;
			label.transform.localPosition 	= Vector3.zero;
			label.transform.rotation 		= Quaternion.identity;
			label.transform.localScale 		= Vector3.one;
			label.text = tmp;
			label_list.Add (label);
		}
		
		bool active = !(tmp_list.Count == 1);
		// Beta version 1.4 //
		// dict_selection [0].SetActive (true);
		// dict_selection [1].SetActive (active);
		// dict_selection [2].SetActive (active);
		/////////////////////
		if (active) 
		{
			// _container.repositionNow = true;
			 _container.Sort ();
			 // _container.WrapContent ();
		}
		// isSetupFinish = true;
	}
	
	void OnHeaderAlphabetUnset ()
	{
		// isSetupFinish = false;  
		//AlphabetCoreAudioSystem.instance.stopAll ();
		
		// isWordHit = false;
		// CChun New Reflesh //
		/*
		// Consult the beta version by Cordex  //
		// Reset the last spelling
		last_spelling = "";
		////////////////////////////
		
		if(mSystem != null)
		{
			
			try
			{
				loader.unloadGameObject ();
			}
			catch {}
			
			markerBg.material.mainTexture = loader.loadTexture ("floor/default");
		}
		for (int i = 0; i < dict_selection.Length; i++)
		{
			dict_selection[i].SetActive (false);
		}
		*/
		/////////////////////////
		
		foreach(UILabel label in label_list)
		{
			if (label == null) continue;
			GameObject.DestroyImmediate (label.gameObject);
		}
		label_list.Clear ();
		
		// CChun New Reflesh //
		if (isTurnOffByManual)
		{
			isTurnOffByManual = false;
			mSystem.needTracing = true;
			
			AlphabetCoreTrackableEventHandler[] markers = GetComponentsInChildren<AlphabetCoreTrackableEventHandler> (true);
			for (int i = 0; i < markers.Length; i++)
			{
				markers [i].Init ();
			}
		}
		////////////////////////
	}
	
	void startSpring ()
	{
		// isScrolling = true;
		// OnNewObjectBecomeCenter (label_list [label_list_index].gameObject);
		Vector3 offset = -_scroll_panel.cachedTransform.InverseTransformPoint(label_list [label_list_index].transform.position);
		offset.y = _scroll_panel.cachedTransform.localPosition.y;
		SpringPanel _spring = SpringPanel.Begin(_scroll_panel.cachedGameObject, offset, 6f);
		_spring.onFinished = OnSpringStop;
		
		CancelInvoke ("coldDownTimer");
		canScroll = false;
		Invoke ("coldDownTimer", .3f);
	}
	
	void OnSpringStop ()
	{
		if(SpringPanel.current != null) 
			SpringPanel.current.onFinished = null;
		// isScrolling = false;
	}
	
	void OnScrollStart ()
	{
		// isScrolling = true;
	}
	
	void OnScrollStopMoving ()
	{
		label_list_index = label_list.IndexOf (_scrollview.centerOnChild.centeredObject.GetComponent <UILabel> ());
		// isScrolling = false;
	} 
	
	void OnNewObjectBecomeCenter (GameObject obj) 
	{
		/*if (label_list [0].gameObject == obj)
		{
			// CCLog.SpecialMsg ("< " + obj.GetComponent<UILabel> ().text);
			dict_selection [1].SetActive (false);
		}
		else if (label_list [label_list.Count - 1].gameObject == obj)
		{
			// CCLog.SpecialMsg ("> " + obj.GetComponent<UILabel> ().text);
			dict_selection [2].SetActive (false);
		}
		else 
		{
			// CCLog.SpecialMsg (obj.GetComponent<UILabel> ().text);
			dict_selection [1].SetActive (true);
			dict_selection [2].SetActive (true);
		}*/
	}

	public void OnResetModel ()
	{
		if (mSystem != null) {
			try {
				loader.unloadGameObject ();
			} catch {
			}
		}

	}

	public void OnResetHeaderBtnClick ()
	{
		// CChun New Refresh //
		if(mSystem != null)
		{
			try
			{
				loader.unloadGameObject ();
			}
			catch {}

			markerBg.material.mainTexture = loader.loadTexture ("floor/markerBG_spell");
  
        }

		//for (int i = 0; i < dict_selection.Length; i++)
		//{
		//	dict_selection[i].SetActive (false);
            

       // }
        broad.mainTexture = listofTextureName[0];
        last_spelling = "";
		last_voice = "";
		isTurnOffByManual = true;
		wordCorrect = false;
		//		mSystem.needTracing = false;

		//		refresh_button.SetActive (false);
		mSystem.resetSpelling (true);

		//		AlphabetCoreTrackableEventHandler[] markers = GetComponentsInChildren<AlphabetCoreTrackableEventHandler> (true);
		//		for (int i = 0; i < markers.Length; i++)
		//		{
		//			markers [i].Deinit ();
		//		}

		return;
		/* Command on version 1.3.8
		///////////////////////////
		if(mSystem != null)
		{
			
			// May comment these area on Beta Version 1.0  //
			if(!string.IsNullOrEmpty (mSystem.result_spelling)) 
			{
				notice_obj.SetActive (true);
			}
			else 
			////////////////////////////////////////////////
			{
				refresh_button.SetActive (false);
				mSystem.releaseHeader ();
			}	
			
			// Consult it on Beta Version 1.3.5 //
//			if(!string.IsNullOrEmpty (mSystem.result_spelling)) 
//			{
//				notice_obj.SetActive (true);
//			}
//			refresh_button.SetActive (false);
//			mSystem.releaseHeader ();
			////////////////////////////////////////////////
		}
		*/
	}

	public void OnResetWords ()
	{
		Debug.Log ("reset");
		if(mSystem != null)
		{
			try
			{
			//	loader.unloadGameObject ();
			}
			catch {}

		}
//		for (int i = 0; i < dict_selection.Length; i++)
//		{
//			//dict_selection[i].SetActive (false);
//		}
//		last_spelling = "";
		last_voice = "";
		isTurnOffByManual = true;
		wordCorrect = false;
		//		mSystem.needTracing = false;

		//		refresh_button.SetActive (false);
		mSystem.resetSpelling (true);

		//		AlphabetCoreTrackableEventHandler[] markers = GetComponentsInChildren<AlphabetCoreTrackableEventHandler> (true);
		//		for (int i = 0; i < markers.Length; i++)
		//		{
		//			markers [i].Deinit ();
		//		}

		return;
		/* Command on version 1.3.8
		///////////////////////////
		if(mSystem != null)
		{
			
			// May comment these area on Beta Version 1.0  //
			if(!string.IsNullOrEmpty (mSystem.result_spelling)) 
			{
				notice_obj.SetActive (true);
			}
			else 
			////////////////////////////////////////////////
			{
				refresh_button.SetActive (false);
				mSystem.releaseHeader ();
			}	
			
			// Consult it on Beta Version 1.3.5 //
//			if(!string.IsNullOrEmpty (mSystem.result_spelling)) 
//			{
//				notice_obj.SetActive (true);
//			}
//			refresh_button.SetActive (false);
//			mSystem.releaseHeader ();
			////////////////////////////////////////////////
		}
		*/
	}

    /// <summary>
    /// reset the header words list
    /// </summary>
    public void OnMarkerAlphabetCheck(bool isFreeMarkerScaned)
    {
        mSystem.setupHeader(mSystem.getFirstAlphabetId());
    }

    [System.Reflection.Obfuscation(ApplyToMembers=false)]
	private IEnumerator pronounciation ()
	{
		while (AlphabetCoreAudioSystem.IsPlaying)
		{
			yield return 0;
		}
        //AlphabetCoreAudioSystem.instance.playOnce(AlphabetCoreSystem.CorrectPronounciation(last_spelling));
        AlphabetCoreAudioSystem.instance.playOnce(last_spelling , AlphabetCoreConstants.AUDIO_Selections.WORD_PRONUNCIATION);
    }

    
	public void resetDisplayColorAnddisEnableDisplayObjecr ()
	{
		Debug.Log ("turnofff parent");
		broad.mainTexture = listofTextureName[0];
		objParent.gameObject.SetActive (false);
		//loader.unloadGameObject ();
	}
	
	public void On3dObjClick ()
	{
        // Consult the beta version by Cordex
        // Click the marker area to play animation and audio
        if (string.IsNullOrEmpty(last_spelling) || !tutorialController.isTutorialFinished())
        {
			return;
        }
		
		try
		{
            broad.mainTexture = listofTextureName[1];
			Bubble.turnShakingOn();
            //			if (isTutorialTime)
            //			{

            //				if(tutorialController.currentState == 1 && !loader.current_obj.GetComponentInChildren<Animation>().isPlaying /*&& !canStage1EventSendOut*/)
            //				{
            ////					canStage1EventSendOut = true;
            //					// StartCoroutine ("pronounciation");
            //					loader.current_obj.GetComponentInChildren<Animation> ().Play ();
            ////					sendToTutorialSystem (1, loader.current_obj.GetComponentInChildren <Animation> ().clip.length);
            //				}
            //			}
            //			else 
            Debug.Log("On3dObjClick");
			if(loader.current_obj != null) {
	            if (!loader.current_obj.GetComponentInChildren<Animation>().isPlaying)
				{
					//AlphabetCoreAudioSystem.instance.stopAll ();
					//AlphabetCoreAudioSystem.instance.playOnce (last_spelling);
					StartCoroutine (pronounciation(last_spelling));
					loader.current_obj.GetComponentInChildren<Animation>().Play ();
				}
			}
			/*
			if(!loader.current_obj.GetComponent<Animation>().isPlaying)
			{
				//AlphabetCoreAudioSystem.instance.stopAll ();
				//AlphabetCoreAudioSystem.instance.playOnce (last_spelling);
				StartCoroutine ("pronounciation");
				loader.current_obj.GetComponent<Animation>().Play ();
			}
			*/
			
		}
		catch (Exception ex)
		{
			Debug.Log (ex);
		}
		/////////////////////////////////////////
	}

    /// <summary>
    /// Public method for force rechecking current spelling
    /// </summary>
    public void recheckSpellingForTutorial()
    {
        //OnWordHitPass(currentSpelling);
        if (!string.IsNullOrEmpty(currentSpelling))
        {
            mSystem.resetSpelling(false);
            Debug.Log(currentSpelling);
            mSystem.setupHeader(currentSpelling.Substring(0, 1));
            mSystem.needTracing = true;
        }
    }

    bool canScroll = true;
	
	public void OnDictLeftButClick ()
	{
		if(mSystem != null && mSystem.isHeaderSet () && label_list != null && canScroll /*&& !isScrolling*/)
		{
			label_list_index = (label_list_index + 1) % label_list.Count;
			/*
			if (++label_list_index >= label_list.Count - 1)
				label_list_index = label_list.Count - 1;
			*/
			startSpring ();
		}
	}
	
	public void OnDictRightButClick ()
	{
		if(mSystem != null && mSystem.isHeaderSet () && label_list != null && canScroll /*&& !isScrolling*/) 
		{
			if (--label_list_index < 0)
				label_list_index = label_list.Count - 1;//0;//
			startSpring ();
		}
	}	
	
	private void coldDownTimer ()
	{
		canScroll = true;
	}

	public bool hasSpelling()
	{
		if (string.IsNullOrEmpty (currentSpelling))
		{
			return false;
		}

		return true;
	}

	/// <summary>
	/// Public method for force reset current spelling
	/// </summary>
	public void resetSpellingForTutorial()
	{
		OnInputChange("");
	}

	/// <summary>
	/// get last spelling
	/// </summary>
	public string getLastSpelling() 
	{
		return last_spelling;
	}
}