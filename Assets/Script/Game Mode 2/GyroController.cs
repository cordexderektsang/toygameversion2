// Referenced from Heyworks Unity Studio http://unity.heyworks.com/

using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;

/// <summary>
/// Gyroscope controller that works with any device orientation.
/// </summary>
public class GyroController : MonoBehaviour
{
	public UILabel debug;

    public GameObject arCam;
	public AlphabetCoreSystem mSystem;
    private int positionForPlayer = 0;
	public static bool requireVerse = false;

    private float Gypox = 0;
    private float Gypoy = 0;
    private float intialGypox = 0;
    private float currentGypox = 0;
    public static bool upLook = false;
    public static bool mainLook = true;
    private const float settTimeForUpdate = 0.2f;
    private float counterTimeForUpdate = 0;
    private bool turn = false;
    private Vector3 startAngle;
	private Vector3 startPointForBullet;
	public static bool isFirstStart = false;
	private Vector3 initalPosition;


    /// <summary>
    /// enable gyro get the start  gyro x
    /// and get pizza box collider
    /// </summary>
    protected void Start()
    {
//        positionForPlayer = 0;
        mainLook = true;
		isFirstStart = false;
//        upLook = false;
		Input.gyro.enabled = false;
//        intialGypox = Mathf.Abs(Input.gyro.attitude.x);
		//StartCoroutine(timer());
    }


    /// <summary>
    /// constantly compare  intialGypox with  currentGypox to check what direction is player going
    ///  if it is look up then disable pizza collider
    ///  else 
    /// start pizz collider 
    /// </summary>
   


	public  void Reset()
	{
	//	startAngle = Vector3.zero;
	} 


	public void SetUpIntialPoint ()
	{
		//arCam.transform.localEulerAngles = Vector2.zero;
		//Quaternion quaternion = new Quaternion (0.5f, 0.5f, -0.5f, 0.5f) * Input.gyro.attitude;//  new Quaternion(0, 0, 1, 0);
		//startPointForBullet = quaternion.eulerAngles;
	}



	void FixedUpdate()
	{
		#if UNITY_EDITOR
		return;
		#endif
		//if (startGyro) {
		//arCam.transform.position = new Vector3 (0, 1.5f, 0);
//        if (mainLook)
//        {

		// if (PizzaController.isStart)
		//  {
	
		Quaternion quaternion = new Quaternion (0.5f, 0.5f, -0.5f, 0.5f) * Input.gyro.attitude;//  new Quaternion(0, 0, 1, 0);
		Vector3 arCamAngle = quaternion.eulerAngles - startAngle;
		//Vector3 gap = startPointForBullet - quaternion.eulerAngles;
		//Vector3 arCamAngle = startPointForBullet - quaternion.eulerAngles;
		//arCamAngle.x = 0;
		//arCamAngle.x = 0;
		arCamAngle.z = quaternion.eulerAngles.z - 180;
	
		//Debug.Log ("safe for gycro");
		if (!isFirstStart) {
			isFirstStart = true;
			initalPosition = arCamAngle;
			arCam.transform.localEulerAngles = Vector3.zero;
		} else {
			Vector3 setOffSetPosition = arCamAngle - initalPosition;
			arCam.transform.localEulerAngles = setOffSetPosition;
		}
		Debug.Log ("x : " + arCam.transform.localEulerAngles.x  + "y : " + arCam.transform.localEulerAngles.y + " z : " + arCam.transform.localEulerAngles.z + " result " +  mSystem.result_spelling);
		//debug.text = " start " + startPointForBullet.ToString () + " : quaternion " + quaternion.eulerAngles.ToString ()  + " : ar cam " + arCam.transform.localEulerAngles;
		//debug.text = arCam.transform.localEulerAngles.ToString () + "\n " + startAngle.ToString() + "\n " + quaternion.eulerAngles.ToString();


		if (arCam.transform.localEulerAngles.y <= 90  || arCam.transform.localEulerAngles.y >=280 || arCam.transform.localEulerAngles.z <= 84 ||arCam.transform.localEulerAngles.z >= 265) {
			requireVerse = false;

		} else {

			requireVerse = true;
		}
		if (startAngle == Vector3.zero) {
			startAngle = arCamAngle;
			startAngle.x = 0;
			startAngle.z = 0;
		}
		CCScreenLogger.LogStatic (" requireVerse  : " + requireVerse  ,1);
		CCScreenLogger.LogStatic (" cam Position  : " + arCam.transform.localEulerAngles + " word : " + mSystem.ReturnCurrnetLetter() ,2);

		 //else {
			
			/*if (!isChange) {
				if (arCam.transform.localEulerAngles.y >= 80) {
					Debug.Log ("y over 80");
					arCamAngle.y = 75;
				}

				if (arCam.transform.localEulerAngles.y <= -80) {
					Debug.Log ("y less than -80");
					arCamAngle.y = -75;
				}

				if (arCam.transform.localEulerAngles.z >= 70) {
					Debug.Log ("y over 70");
					arCamAngle.z = 65;
				}

				if (arCam.transform.localEulerAngles.z <= -70) {
					Debug.Log ("y less than 70");
					arCamAngle.z = -65;
				}

				arCam.transform.localEulerAngles = arCamAngle;
				isChange = true;
			}*/
		}
	}
		//} else {
		//	debug.text = " nothing happen ";

		//}
           // }
            /*if (Input.gyro.attitude.y >= -0.31 && Input.gyro.attitude.y <= 0.31)
            {
                arCam.transform.rotation = Quaternion.Euler(0, Gypoy, 0);
            }
            else
            if (Input.gyro.attitude.y < -0.4)
            {
                Gypoy = 180 * -0.31f;
                arCam.transform.rotation = Quaternion.Euler(0, Gypoy, 0);
            }
            else if (Input.gyro.attitude.y > 0.4)
            {
                Gypoy = 180 * 0.31f;
                arCam.transform.rotation = Quaternion.Euler(0, Gypoy, 0);
            }*/

//        }
//        else if (upLook)
//        {
//            Gypox = 180 * listOfPosition.transform.position.y * -0.48f;
//           // arCam.transform.rotation = Quaternion.Euler(Gypox, 0, 0);
//           // Gypoy = 0;
//           if (Input.gyro.attitude.y >= -0.31 && Input.gyro.attitude.y <= 0.31)
//             {
//                 arCam.transform.rotation = Quaternion.Euler(Gypox, Gypoy, 0);
//             }
//             else if (Input.gyro.attitude.y < -0.4)
//             {
//                 Gypoy = 180 * -0.31f;
//                 arCam.transform.rotation = Quaternion.Euler(Gypox, Gypoy, 0);
//                 arCam.transform.position = Vector3.zero;
//
//             }
//             else if (Input.gyro.attitude.y > 0.4)
//             {
//                 Gypoy = 180 * 0.31f;
//                 arCam.transform.rotation = Quaternion.Euler(Gypox, Gypoy, 0);
//                 arCam.transform.position = Vector3.zero;
//             }
//
//        }

    //}
	