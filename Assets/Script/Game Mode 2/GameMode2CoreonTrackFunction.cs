﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameMode2CoreonTrackFunction : MonoBehaviour 
{

	public int maxWordCount = 0;
	public GameObject spawnOnTargetObject;
	public GameObject blueLayout;
	public List<string> referenceForCurrentListOfWord = new List<string>();
	public GameObject displayPlaform;
	public UITexture aimTexture;
	public ParticleSystem particle;
	private int index = 0;
	private GameObject appearModel;
	private AlphabetCoreSystem mSystem;
	private string currentTipReference = "";
	private GameObject referenceCurrentWord;
	private TipsForOnshot tipsForOnshot;
	private AlphabetGameMode2UISystem alphabetGameMode2UISystem;


	// Use this for initialization
	void Start () 
	{
		var em = particle.emission;
		em.enabled = false;
		mSystem = GetComponent <AlphabetCoreSystem> ();
		alphabetGameMode2UISystem = GetComponent <AlphabetGameMode2UISystem> ();
		tipsForOnshot = GetComponent<TipsForOnshot> ();
		mSystem.OnInputChange+= OnHitWordChangeTip;
		mSystem.OnWordHit += OnHitWordCorrentObjectSet;
	}
	
	// Update is called once per frame



	#region  delegate Function
	public void OnHitWordChangeTip (string tmp)
	{
		Debug.Log ("change" + tmp  + "previousCorrectWord : " + tipsForOnshot.previousCorrectWord  + "is correct " + TipsForOnshot.IsCorrect );
		tipsForOnshot.MatchLetterWithWord (tmp);
		//GetCloseHeaderFromList (tmp);
		tipsForOnshot.StartAutoReset (tmp);

	}

	public void GetCurrentScaningLetter ()
	{
		mSystem.ReactiveCurrentSameLetter();
	}

	/// <summary>
	/// spawn object to the center of the word
	/// </summary>
	/// <param name="ch">Ch.</param>
	/// <param name="tmp">Tmp.</param>
	public void OnHitWordCorrentObjectSet(/*List<AlphabetCoreTrackableEventHandler> ch,*/ string tmp)
	{
		Debug.Log ("hittt");
		tipsForOnshot.StopCheckBlackScreen ();
		tipsForOnshot.SetStatusForTipParentObject (false);
		tipsForOnshot.intialStartLetterForWord = false;
		//alphabetGameMode2UISystem.resetIscheckWordBoolean ();
		TipsForOnshot.IsCorrect = true;
		tipsForOnshot.previousCorrectWord = tmp;
	

	}

	/// <summary>
	/// Raises the destroy event.
	/// </summary>
	void OnDestroy ()
	{

		mSystem.OnWordHit-= OnHitWordCorrentObjectSet;
		mSystem.OnInputChange -= OnHitWordChangeTip;
	}

	#endregion



	/// <summary>
	/// destroy current display object
	/// </summary>
	public void destoryCurrentDisplayModel () 
	{
		if (appearModel != null) 
		{
			Debug.Log ("remove");
			alphabetGameMode2UISystem.dict_selection [0].SetActive (false);
			alphabetGameMode2UISystem.OnResetModel ();
			appearModel = null;
		}

	}




	/// <summary>
	/// attach blue out lay to letter
	/// </summary>
	/// <param name="currentLetter">Current letter.</param>
	public void SetCurrentOnTrackLetter(GameObject currentLetter)
	{
		if (blueLayout != null )
		{
			blueLayout.SetActive (true);
			aimTexture.color = Color.blue;
			referenceCurrentWord = currentLetter; 
			//shotForGameModeTwo.CollusionObject = currentLetter;
			blueLayout.transform.parent = currentLetter.transform;
			blueLayout.transform.localPosition = new Vector3 (0, 0, 0);
			blueLayout.transform.localScale = new Vector3 (0.1f, 0.1f, 0.1f);
			blueLayout.transform.localEulerAngles = Vector3.zero;
			particle.transform.parent = currentLetter.transform;
			particle.transform.localPosition = Vector3.zero;
			particle.transform.localScale = Vector3.one;
			EnableRenderForBlueOutlay ();

		}

	}


	public void ChangeAimTextureColor (bool Default = true)
	{
		if (!Default) {
			aimTexture.color = Color.blue;
		} else {
			aimTexture.color = Color.white;
		}
	}
	/// <summary>
	/// Resets the current letter.
	/// </summary>
	public void ResetCurrentLetter ()
	{

		referenceCurrentWord = null;
		aimTexture.color = Color.white;
		DisableRenderForBlueOutlay ();
	}


	/// <summary>
	/// Returns the current on ray hit letter.
	/// </summary>
	/// <returns>The current on ray hit letter.</returns>
	public GameObject ReturnCurrentOnRayHitLetter ()
	{
		return referenceCurrentWord;
	}


#region  render function
	/// <summary>
	/// set up current Dispay object
	/// </summary>
	/// <param name="model">Model.</param>
	/*public void setUpCurrentDisplayModel (GameObject model) 
	{
		appearModel = model;
		Renderer[] rendererComponents = model.GetComponentsInChildren<Renderer>(true);
		Collider[] colliderComponents = model.GetComponentsInChildren<Collider>(true);
		Animation[] animationComponents = model.GetComponentsInChildren<Animation>(true);

		// Enable rendering:
		foreach (Renderer component in rendererComponents)
		{
			component.enabled = true;
		}

		// Enable colliders:
		foreach (Collider component in colliderComponents)
		{
			component.enabled = true;
		}

		// Enable animations:
		foreach (Animation component in animationComponents)
		{
			component.Play ();
		}

		// Enable rendering:
		foreach (Renderer component in rendererComponents)
		{
			component.enabled = true;
		}
	}*/

	/// <summary>
	/// Enables the render for blue outlay.
	/// </summary>
	public  void EnableRenderForBlueOutlay ()
	{
		Renderer[] rendererComponents = blueLayout.GetComponentsInChildren<Renderer>(true);

		// Enable rendering:
		foreach (Renderer component in rendererComponents)
		{
			component.enabled = true;
		}
		var em = particle.emission;
		em.enabled = true;
	}


	/// <summary>
	/// Disables the render for blue outlay.
	/// </summary>
	public void DisableRenderForBlueOutlay ()
	{
		Renderer[] rendererComponents = blueLayout.GetComponentsInChildren<Renderer>(true);
		// Enable rendering:
		foreach (Renderer component in rendererComponents)
		{
			component.enabled = false;
		}
		var em = particle.emission;
		em.enabled = false;
	}

	#endregion

}
