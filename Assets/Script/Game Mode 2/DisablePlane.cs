﻿using UnityEngine;
using System.Collections;

public class DisablePlane : MonoBehaviour 
{
	public BoxCollider parentCollider;
	public GameObject targetEnableOrDisableObject;
	public AlphabetGameMode1UISystemver2 alphabetGameMode1UISystemver2;
	
	// Update is called once per frame
	void Update () 
	{
		if (parentCollider.enabled) 
		{

			if (!targetEnableOrDisableObject.activeSelf) 
			{
				targetEnableOrDisableObject.SetActive (true);
			}
		} else {
			if (targetEnableOrDisableObject.activeSelf) 
			{
				targetEnableOrDisableObject.SetActive (false);
			}

		}
	
	}
	public void OnCallModelAnimation()
	{

		alphabetGameMode1UISystemver2.On3dObjModelClick();

	}
}
