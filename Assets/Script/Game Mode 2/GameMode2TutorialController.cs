﻿using UnityEngine;
using System.Collections.Generic;

public class GameMode2TutorialController : BaseTutorialController
{
	private AlphabetGameMode2UISystem gameModeSystem;
    private GameObject car;
    private Animation carAnimation;
    
	private bool isInstructionAudioPlayedOnce = false;
    private bool isWordRechecked = false;
    private bool isLoad3dObject = false;
    private bool isPlayTutorialWord = false;
    private string scannedObjectName;
	private string currentSpelling;

    private float endInstructionTimer = 0.0f;
    //private const float END_INSTRUCTION_RESET_TIME = 15.0f;
    public GameObject imageMarkerObjects;

    public override void setupVariablesAndConstants()
	{
		gameModeNumber = 2;
		gameModeSystem = GetComponent<AlphabetGameMode2UISystem>();
        gameModeSystem.initialize();
	}

	public override void setupGameModeTutorial()
	{

	}

	public override void setupGameModeInstructions()
	{
		
	}

    public override void checkTutorialState()
    {
        // Don't process any logic until voice finishes talking
        if (TutorialInstructionAudioController.IsPlaying)
        {
            return;
        }

        // On the first page if finish stop audio
        // encouragement for first accomplishment
        if (tutorialState == 0)
        {
            if (magicScreen.isTrackerFind == true || magicScreen2.isTrackerFind == true)
            {
                playTutorialInstructionAudio(ENCOURAGEMENT + "1");
                isFadeOut = true;
            }
        }
        // encouragement for second accomplishment
        else if (tutorialState == 1)
        {
            //Debug.Log("IsClicked " + IsClicked);
            //if (IsClicked == true)
            if (isFadeInAudioPlayed && !TutorialInstructionAudioController.IsPlaying && !isWordRechecked)
            {
                gameModeSystem.recheckSpellingForTutorial();
                isWordRechecked = true;
            }
            else if (gameModeSystem.returnIsWordCorrect())
            {
                if (car == null && isLoad3dObject)
                {
                    Transform imageMarkerObjects;
                    imageMarkerObjects  = magicScreen.transform.FindChild("ImageMarkerObjects");
                    if(imageMarkerObjects == null)
                    {
                        imageMarkerObjects = magicScreen2.transform.FindChild("ImageMarkerObjects");
                    }
                    car = imageMarkerObjects.transform.FindChild(scannedObjectName + "(Clone)").gameObject;
                    carAnimation = car.GetComponent<Animation>();
                    playTutorialInstructionAudio(ENCOURAGEMENT + "2");
                }
                else if (car != null && !isPlayTutorialWord)
                {
                    isPlayTutorialWord = true;
                    gameModeSystem.playTutorialWordPronounciation();
                }
                else if (car != null && !carAnimation.isPlaying)
                {
                    isFadeOut = true;
                }
            }
        }
        /// at the last complete page after the audio then the character will go back
        else if (tutorialState == 2)
        {
            characterSystem.setCharacterToMoveIn();
            checkEnableTutorialNextButton();
            onTutorialNextButtonClick();
        }
    }

    public override void onInstructionStart()
    {
        if (isInstructionStart)
        {
            return;
        }

        // Setup timer but dont set max time yet to prevent clock from running
        //CCTimerSuperClass.SetupTimer(0, 0, maxTimeReachCount, timeIncrement, glassCeiling);
        timerSystem.setTimerActive(true);
        setNextInstructionState(2);
        isInstructionStart = true;
    }

    public override void checkInstructionState ()
	{
		//instruction 
		// condition 1 nothing scanned for 10 sec
		if (instructionState == 0 && gameModeSystem.hasSpelling())
		{
			setNextInstructionState ();
		}

		// condition 2 Scanned buddies but no word for 10 sec*/
		else if(instructionState == 1 && gameModeSystem.returnIsWordCorrect())
		{
			setNextInstructionState();
		}

		// condition 3 Scanned word but no board for 10 sec
		else if (instructionState == 2 && (magicScreen.isTrackerFind || magicScreen2.isTrackerFind))
		{
			currentSpelling = gameModeSystem.getLastSpelling ();
			setNextInstructionState();
		}
		else if (instructionState == 3)
		{
			if (TutorialInstructionAudioController.IsPlaying && !isInstructionAudioPlayedOnce) {
				isInstructionAudioPlayedOnce = true;
			} else if (!TutorialInstructionAudioController.IsPlaying && isInstructionAudioPlayedOnce) {
				endInstructionTimer = 0.0f;
				isInstructionAudioPlayedOnce = false;
				setNextInstructionState();
			}
		}
		// condition 5 not action
		else if (instructionState == 4)
		{
            //Debug.Log ("!gameModeSystem.hasSpelling () " + !gameModeSystem.hasSpelling ());

            // if scan word correct and if current spelling is not equal
            if (gameModeSystem.returnIsWordCorrect () && !currentSpelling.Equals(gameModeSystem.getLastSpelling()))
			{
				gameModeSystem.resetSpellingForTutorial ();
				gameModeSystem.OnResetWords ();
				endInstructionTimer = 0.0f;
				isInstructionAudioPlayedOnce = false;
				setNextInstructionState (2);
			} 
			else if (!gameModeSystem.hasSpelling () && (!magicScreen.isTrackerFind && !magicScreen2.isTrackerFind))
            {
      
				if (endInstructionTimer >= endInstructionResetTime && !TutorialInstructionAudioController.IsPlaying)
				{
					resetAllInstructions ();
					gameModeSystem.OnResetHeaderBtnClick ();
				}
                else
                {
                    endInstructionTimer += Time.deltaTime;
                }
			}
            else
            {
				endInstructionTimer = 0.0f;
			}
			// completed!
//			setNextInstructionState();
		}
	}

    public override void onTutorialEnd()
    {
        //Debug.Log ("onTutorialEnd");
        if (isTutorialDone)
        {
            return;
        }
        //Debug.Log ("onTutorialEnd isTutorialDone");

        isClickFinishButton = true;
    }

    public void setLoad3dObject(string objectName)
    {
        isLoad3dObject = true;
        scannedObjectName = objectName;
    }
}
