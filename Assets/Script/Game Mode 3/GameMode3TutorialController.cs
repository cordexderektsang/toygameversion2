﻿using System.Collections.Generic;
using UnityEngine;

public class GameMode3TutorialController : BaseTutorialController
{
    [Header("Override Game Word Setting (for debugging purposes)")]
    public bool isOverrideWordSetting;
    public List<string> overrideWords;
    public int overrideWordIndex = 0;
    
    private AlphabetGameMode3UISystem gameModeSystem;
    private GameObject car;
    private Animation carAnimation;
    private bool isWordRechecked = false;
    private bool isInstructionAudioPlayedOnce = false;
    private bool isHintsButtonClicked = false;
    private bool is3DObjectClicked = false;

    public override void setupVariablesAndConstants()
    {
        gameModeNumber = 3;
        gameModeSystem = GetComponent<AlphabetGameMode3UISystem>();
        gameModeSystem.initialize();
    }

    public override void setupGameModeTutorial()
    {

    }

    public override void setupGameModeInstructions()
    {
        
    }

    public override void checkTutorialState()
    {
       /* if(car == null)
        {
            car = magicScreen.transform.FindChild("car(Clone)").gameObject;
            carAnimation = car.GetComponent<Animation>();
        }*/

        // Don't process any logic until voice finishes talking
        if (TutorialInstructionAudioController.IsPlaying)
        {
            return;
        }

        // First tutorial page conditions
        if (tutorialState == 0)
        {
            if (magicScreen.isTrackerFind || magicScreen2.isTrackerFind)
            {
                playTutorialInstructionAudio(ENCOURAGEMENT + "1");
                isFadeOut = true;
            }
        }

        // Second tutorial page conditions
        else if (tutorialState == 1)
        {
            if (isFadeInAudioPlayed && !isWordRechecked)
            {
                gameModeSystem.recheckSpellingForTutorial();
                isWordRechecked = true;
            }
            else if (gameModeSystem.returnIsWordCorrect())
            {
                //if (!carAnimation.isPlaying)
                //{
                    //RotateAlphaCharacter.ResetReverseStatus();
                    characterSystem.setCharacterToMoveIn();
                    gameModeSystem.disableLeftSideUIButtons();
                    isFadeOut = true;
                //}
            }
        }

        else if (tutorialState == 2)
        {
            checkEnableTutorialNextButton();
            onTutorialNextButtonClick();
        }
    }

    [Beebyte.Obfuscator.SkipRename]
    public override void onTutorialEnd()
    {
        if (isTutorialDone)
        {
            return;
        }

        isClickFinishButton = true;
    }

    [Beebyte.Obfuscator.SkipRename]
    public override void onInstructionStart()
    {
        if (isInstructionStart)
        {
            return;
        }

        // Setup timer but dont set max time yet to prevent clock from running
        //CCTimerSuperClass.SetupTimer(0, 0, maxTimeReachCount, timeIncrement, glassCeiling);
        timerSystem.setTimerActive(true);
        setNextInstructionState(3);
        isInstructionStart = true;   
    }

    public override void checkInstructionState()
    {
        if (instructionState == 0)
        {
            // If magic screen is scanned, go to next instruction
            if (gameModeSystem.returnIsWordCorrect() || (magicScreen.isTrackerFind || magicScreen2.isTrackerFind))
            {
               setNextInstructionState();
                isMagicScreenTracked = true;

               // if magic screen is scanned before , then pick an other instructrion state in between 1 and 2
            } else if (isMagicScreenTracked && !TutorialInstructionAudioController.IsPlaying && isInstructionAudioPlayedOnce)
            {
                SetRandomInstructrionState(1);
                isInstructionAudioPlayedOnce = false;
            } else if (isMagicScreenTracked && TutorialInstructionAudioController.IsPlaying && !isInstructionAudioPlayedOnce)
            {
                isInstructionAudioPlayedOnce = true;
            }
            else if (gameModeSystem.returnIsWordCorrect() && isMagicScreenTracked)
            {
                //   setNextInstructionState();
                playLastIndex();

            }
        }
        else if (instructionState == 1)
        {
            // If word is not correct, go to next instruction after audio has been played at least once
            //if (gameModeSystem.returnIsWordCorrect() || (!gameModeSystem.returnIsWordCorrect() && CCTimerSuperClass.MaxTime > instructionStates[instructionState].time))
            //{
            //    setNextInstructionState();
            //}

            // If word is not correct, go to next instruction after audio has been played at least once
            if (gameModeSystem.returnIsWordCorrect() || isHintsButtonClicked || (!TutorialInstructionAudioController.IsPlaying && isInstructionAudioPlayedOnce))
            {     
                isHintsButtonClicked = false;
                isInstructionAudioPlayedOnce = false;
               setNextInstructionState();
            }
            else if (TutorialInstructionAudioController.IsPlaying && !isInstructionAudioPlayedOnce)
            {
                isInstructionAudioPlayedOnce = true;
            }
            else if (gameModeSystem.returnIsWordCorrect())
            {
                //   setNextInstructionState();
                playLastIndex();

            }
        }
        else if (instructionState == 2)
        {
 
            // If word is correct, go to next instruction
            if (gameModeSystem.returnIsWordCorrect())
            {
                //   setNextInstructionState();
                playLastIndex();

            }
            else if (TutorialInstructionAudioController.IsPlaying && !isInstructionAudioPlayedOnce)
            {
                isInstructionAudioPlayedOnce = true;
            }          //  pick an other instructrion state in between 0 and 1
            else if ((!TutorialInstructionAudioController.IsPlaying && isInstructionAudioPlayedOnce))
            {
                SetRandomInstructrionState(0);
                isInstructionAudioPlayedOnce = false;
            }
        }
        else if (instructionState == 3)
        {
            // Go to next instruction after audio has been played at least once
            if (is3DObjectClicked || !TutorialInstructionAudioController.IsPlaying && isInstructionAudioPlayedOnce)
            {
                is3DObjectClicked = false;
                isInstructionAudioPlayedOnce = false;
                setNextInstructionState();
            }
            else if (TutorialInstructionAudioController.IsPlaying && !isInstructionAudioPlayedOnce)
            {
                isInstructionAudioPlayedOnce = true;
            }
        }
    }

    public void setHintsButtonClicked()
    {
        if (instructionState == 1)
        {
            isHintsButtonClicked = true;
        }
    }

    public void set3DObjectClicked()
    {
        if (instructionState == 3)
        {
            is3DObjectClicked = true;
        }
    }
}
