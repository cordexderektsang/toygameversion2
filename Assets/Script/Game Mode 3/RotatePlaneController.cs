﻿using UnityEngine;

public class RotatePlaneController : MonoBehaviour {

    [Range(0f, 360f)]
    public float xRotationTarget = 180f;
    [Range(1f, 200f)]
    public float rotateSpeed = 10.0f;

    private bool isRotating = false;
    private bool isDoneRotation = false;
    private Vector3 localEulerAngles;
    private Transform backTransform;

	// Use this for initialization
	void Awake ()
    {
        localEulerAngles = transform.localEulerAngles;
        backTransform = transform.FindChild("Back");
        Debug.Log("back " + backTransform.name);
    }
	
    public void setRotateSpeed(float rotateSpeed)
    {
        this.rotateSpeed = rotateSpeed;
    }

    public void startRotate()
    {
        isRotating = true;
        // enable back renderer
        SpriteRenderer renderer = backTransform.GetComponent<SpriteRenderer>();
        renderer.enabled = true;
    }

    public void setOpenRotation()
    {
        setRotation(xRotationTarget);
    }

    public void resetRotation()
    {
        setRotation(0f);
        // reset renderers to enable (col) and disable back
        SpriteRenderer frontRenderer = GetComponent<SpriteRenderer>();
        SpriteRenderer backRenderer = backTransform.GetComponent<SpriteRenderer>();
        frontRenderer.enabled = true;
        backRenderer.enabled = false;
    }

    private void setRotation(float targetEulerAngle)
    {
        localEulerAngles.x = targetEulerAngle;
        transform.localEulerAngles = localEulerAngles;
        isRotating = false;
    }

	// Update is called once per frame
	void Update ()
    {
        if (isRotating)
        {
            localEulerAngles.x += (rotateSpeed * Time.deltaTime);
            if (localEulerAngles.x >= xRotationTarget)
            {
                localEulerAngles.x = xRotationTarget;
                isRotating = false;
                // disable front (col) renderer
                SpriteRenderer renderer = GetComponent<SpriteRenderer>();
                renderer.enabled = false;
            }
            transform.localEulerAngles = localEulerAngles;
        }
	}
}
