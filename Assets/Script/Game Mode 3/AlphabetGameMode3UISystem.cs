using UnityEngine;
using System.Collections;
using System.Collections.Generic; // import it for List class

public class AlphabetGameMode3UISystem : MonoBehaviour
{
    // Var of core system
    public List<string> excludeList = new List<string>();
    public static bool IsBeginnerMode = false;
    private AlphabetCoreSystem mSystem;
    private bool isStageMarkerFound = false;
    public Sprite[] wordSprites;
    public Texture2D[] source;

    // Tools for loading Resources obj
    private CCManualLoadResourceObj loader;
	public Renderer markerBg;

    // Set for initDict() parameters
    private string filePath = "dictionary/dict";
    private int wordLengthLimit = 5;

    private int wordListIndex = 0;
    protected string currentWord;
    private string currentSpelling = "";
	private string last_spelling = "";
    protected List<SpellingCharacter> currentWordList = new List<SpellingCharacter>();
    private readonly string LETTER_BOX_NAME_PREFIX = "Letter_Box";
    private readonly string LETTER_NAME_PREFIX = "Letter";
    private bool wordCorrect = false;
    private bool isCheckingWord = false;

    private bool useHint = false;
    private float hintTimer;
    private bool isHintTimer = false;
    private readonly float HINT_DISPLAY_DUR_S = 10;

    // Var for tips
    private int numOfCharsForTips = 0;

    public GameObject tipChars1;
    public GameObject tipChars2;

    // Var for flipping planes
    public int numberOfPlanesOpenedAtStart;
    public float timeUntilPlaneFlips;
    public GameObject[] flipPlaneRows;
    private int rowCount;
    private int colCount;
    private int totalCount;
    private Queue<int> flipPlaneIndexes = new Queue<int>();
    private float timeUntilPlaneFlipsTimer = 0.0f;

    // Var of tutorial system
    private GameMode3TutorialController tutorialController;
    private bool isSystemHasTutorial, isTutorialTime;

    private readonly float WORD_COMPLETION_TEXTURE_DUR_S = 3;

    public UIGrid grid;
	public Transform objParent;
    public UITexture WordCompletionTexture;
    public Texture[] completionTexturesWithOutHint;
    public Texture[] completionTexturesWithHint;

    public GameObject backButton;
    private BoxCollider backButtonCollider;
    private UIButton backButtonUI;
    public GameObject captureShareButton;
    private BoxCollider captureShareButtonCollider;
    private UIButton captureShareButtonUI;

    public GameObject rightSideButtonsParent;
    private BoxCollider[] rightColliders;
    private UIButton[] rightUiButtons;

	public GameObject flipPlanes;
    #region Unity Life Cycle
    // Use this for initialization
    public void initialize()
    {
        mSystem = GetComponent<AlphabetCoreSystem>();
        // GetComponentInParent<AlphabetCoreSystem> ();
		
		loader = new CCManualLoadResourceObj();

        tutorialController = GetComponent<GameMode3TutorialController>();
        isSystemHasTutorial = isTutorialTime = tutorialController.isNeedTutorial;

        CCLog.SpecialMsg("isTutorialTime: " + isTutorialTime);

        backButtonCollider = backButton.GetComponent<BoxCollider>();
        backButtonUI = backButton.GetComponent<UIButton>();

        captureShareButtonCollider = captureShareButton.GetComponent<BoxCollider>();
        captureShareButtonUI = captureShareButton.GetComponent<UIButton>();

        rightColliders = rightSideButtonsParent.GetComponentsInChildren<BoxCollider>(true);
        rightUiButtons = rightSideButtonsParent.GetComponentsInChildren<UIButton>(true);

        if (!isSystemHasTutorial)
        {
            enableUIButtons();
        }

        mSystem.OnSetHeaderAlphabet += OnSpellingWordSet;
        mSystem.OnUnsetHeaderAlphabet += OnSpellingWordUnset;
        mSystem.OnWordHit += OnWordSpellingCorrect;
        mSystem.OnInputChange += OnInputChange;
        mSystem.OnMarkerAlphabetCheck += OnMarkerAlphabetCheck;
//        mSystem.onStageMarkerFound += OnStageMarkerFound;
     //   mSystem.onStageMarkerLost += OnStageMarkerLost;

        mSystem.initDict(filePath, wordLengthLimit);

        if (tutorialController.isOverrideWordSetting)
        {
            setNextWord(tutorialController.overrideWords[tutorialController.overrideWordIndex]);
            tutorialController.overrideWordIndex++;
        }
        else
        {
           mSystem.randomOrderOfWordList();

            setNextWord();
        }
        setLabelDisplay();

        rowCount = flipPlaneRows.Length;
       colCount = flipPlaneRows[0].transform.childCount;
        totalCount = rowCount * colCount;

        setupFlipPlanes();

        AppController.ManualReleaseMemory();
    }

    void Update()
    {

        if (isStageMarkerFound && flipPlaneIndexes.Count > 0)
        {
            timeUntilPlaneFlipsTimer += Time.deltaTime;
            if (timeUntilPlaneFlipsTimer >= timeUntilPlaneFlips)
            {
                timeUntilPlaneFlipsTimer = 0f;
                flipPlane();
            }
        }

        if (isHintTimer)
        {
            hintTimer += Time.deltaTime;
            if (hintTimer > HINT_DISPLAY_DUR_S)
            {
                foreach (SpellingCharacter character in currentWordList)
                {
                    character.setIsHint(false);
                }
                isHintTimer = false;

                OnInputChange(currentSpelling);
            }
        }
        
        if (AlphabetCoreSystem.capturing)
        {
            backButtonCollider.enabled = false;
            captureShareButtonCollider.enabled = false;
            for (int i = 0; i < rightColliders.Length; i ++ )
            {
                rightColliders[i].enabled = false;
            }
          
        }
        else if (backButtonCollider.enabled == false && tutorialController.isTutorialFinished())
        {
            for (int i = 0; i < rightColliders.Length; i++)
            {
                rightColliders[i].enabled = true;
            }
            backButtonCollider.enabled = true;
            captureShareButtonCollider.enabled = true;
        }
    }

    /// <summary>
	/// Destroy objects when scene ends
	/// </summary>
    void OnDestroy()
    {
		StopCoroutine ("disableWordCompletionTexture");
		StopCoroutine ("pronounciation");
		
		loader = null;
        mSystem.OnSetHeaderAlphabet -= OnSpellingWordSet;
        mSystem.OnUnsetHeaderAlphabet -= OnSpellingWordUnset;
        mSystem.OnWordHit -= OnWordSpellingCorrect;
        mSystem.OnInputChange -= OnInputChange;
    }
    #endregion

    #region Callback of AlphabetCoreSystem 
    /// <summary>
	/// Sets the word for spelling
	/// </summary>
    void OnSpellingWordSet()
    {
        // 5
        CCLog.SpecialMsg("Word set to: " + currentWord);

        // Assign the spelling into a list for matching
        currentWordList = new List<SpellingCharacter>();
        for (int i = 0; i < currentWord.Length; i++)
        {
            SpellingCharacter hc = new SpellingCharacter(currentWord.Substring(i, 1));
            currentWordList.Add(hc);
        }
    }

    private int RandomPick(List<int> number)
    {
        int i = Random.Range(0, totalCount);
        while (number.Contains(i))
        {
             i = Random.Range(0, totalCount);
        }
        number.Add(i);
        return i;
    }


    private void setupFlipPlanes()
    {
        //int openedPlanes = 0;
        List<int> openedPlanes = new List<int>();
        List<int> listOfNumer = new List<int>();
        flipPlaneIndexes.Clear();
        while ((flipPlaneIndexes.Count + openedPlanes.Count) < totalCount)
        {
            int index = RandomPick(listOfNumer);
            // int index = Random.Range(0, totalCount);
            if (openedPlanes.Count < numberOfPlanesOpenedAtStart)
            {
                //openedPlanes++;
                int row = index / colCount;
       
                if (!openedPlanes.Contains(index))
                {
                    openedPlanes.Add(index);
                    flipPlaneRows[row].transform.GetChild(index - (row * colCount)).gameObject.GetComponent<RotatePlaneController>().startRotate();
            
                }
            }
            else if (!flipPlaneIndexes.Contains(index) && !openedPlanes.Contains(index))
            {
                flipPlaneIndexes.Enqueue(index);
           
            }
            Debug.Log(flipPlaneIndexes.Count);
        }
        //for(int i = 0; i < numberOfPlanesOpenedAtStart; i++)
        //{
        //    flipPlane();
        //    //int index = flipPlaneIndexes.Dequeue();
        //    //int row = index / colCount;
        //    //flipPlaneRows[row].transform.GetChild(index - (row * colCount)).gameObject.GetComponent<RotatePlaneController>().startRotate();
        //    //Transform flipPlaneTransform = flipPlaneRows[row].transform.GetChild(index - (row * colCount));
        //    //rotateFlipPlane(flipPlaneTransform);
        //}
    }

    private void flipPlane()
    {
        if (flipPlaneIndexes.Count > 0)
        {
            int index = flipPlaneIndexes.Dequeue();
            int row = index / colCount;
            int number = index - (row * colCount);
            flipPlaneRows[row].transform.GetChild(index - (row * colCount)).gameObject.GetComponent<RotatePlaneController>().startRotate();
        }
    }

    private void openAllFlipPlanes()
    {
        while (flipPlaneIndexes.Count > 0)
        {
            flipPlane();
        }
    }

    private void resetFlipPlanes()
    {
        foreach (GameObject flipPlaneRow in flipPlaneRows)
        {
            for (int i = 0; i < flipPlaneRow.transform.childCount; i++)
            {
                flipPlaneRow.transform.GetChild(i).GetComponent<RotatePlaneController>().resetRotation();
            }
        }

        flipPlaneIndexes.Clear();
        timeUntilPlaneFlipsTimer = 0f;
    }

    /// <summary>
    /// Checks if the word is spelled correctly
    /// </summary>
    /// <returns>True if word is correctly spelled</returns>
	public bool returnIsWordCorrect()
    {
		return wordCorrect;
	}

    /// <summary>
	/// Unsets the word for spelling
	/// </summary>
    void OnSpellingWordUnset()
    {
		CCLog.SpecialMsg("Word unset");
        currentWord = "";
        resetFlipPlanes();
        setupFlipPlanes();

        // 3D Off Unload all displayed 3d model
        try
		{
			loader.unloadGameObject ();
		}
		catch {}
		
		markerBg.material.mainTexture = loader.loadTexture ("floor/default");
    }

    /// <summary>
	/// Method called whenever user changes letter positions
	/// </summary>
    /// <param name="input">
    /// The string of the currently spelled word
    /// </param>
    void OnInputChange(string input)
    {
        CCLog.SpecialMsg(input);

        // Store current word spelling for hint use
        currentSpelling = input;
        matchMaximumCharacters(false);

        setLabelDisplay();
    }

    /// <summary>
	/// Method to be called when word is spelled correctly
	/// </summary>
    /// <param name="spelling">
    /// The string of the correctly spelled word
    /// </param>
    void OnWordSpellingCorrect(string spelling)
    {
        if (isCheckingWord
            || (isTutorialTime
                && (tutorialController.tutorialState != 1
                || tutorialController.tutorialState == 1 && (!tutorialController.isFadeInAudioPlayedOnce() || TutorialInstructionAudioController.IsPlaying))))
        {
            return;
        }

        isCheckingWord = true;

        CCLog.SpecialMsg("Word (" + currentWord + ") spelled correctly!" + currentWord);

        if (!last_spelling.Equals(spelling))
        {
            last_spelling = spelling;

            openAllFlipPlanes();

            // display different notice 
            isHintTimer = false;
            hintTimer = 0;
            if (useHint)
            {
                CCLog.SpecialMsg("Finish with hint");
                int randomPick = Random.Range(0, completionTexturesWithHint.Length);
                WordCompletionTexture.mainTexture = completionTexturesWithHint[randomPick];
                AlphabetCoreAudioSystem.instance.playOnce(completionTexturesWithHint[randomPick].name , AlphabetCoreConstants.AUDIO_Selections.ENCOURAGEMENT);
            }
            else
            {
                CCLog.SpecialMsg("Finish without hint");
                int randomPick = Random.Range(0, completionTexturesWithOutHint.Length);
                WordCompletionTexture.mainTexture = completionTexturesWithOutHint[randomPick];
                Debug.Log(completionTexturesWithOutHint[randomPick].name);
                AlphabetCoreAudioSystem.instance.playOnce(completionTexturesWithOutHint[randomPick].name, AlphabetCoreConstants.AUDIO_Selections.ENCOURAGEMENT);
            }
            WordCompletionTexture.MakePixelPerfect();

            StartCoroutine("disableWordCompletionTexture");
            StartCoroutine("pronounciation");

            // Play Animation
            try
			{
				
				if(loader.current_obj.GetComponentInChildren<SetLastFrameOfAnimation>() == null)
				{
					loader.current_obj.GetComponentInChildren<Animation>().Play();
				} 
				else 
				{
					loader.current_obj.GetComponentInChildren<SetLastFrameOfAnimation>().Play();
				}
            }
            catch { }

            // Set variable to keep word on screen
            wordCorrect = true;

            // update the game version
            if (CCLocalStorageAPI.GET<int>("IsFullVersion") != (int)AlphabetCoreSystem.GameVersion.Free)
            {
                CCLocalStorageAPI.SET<int>("IsFullVersion", (int)AlphabetCoreSystem.GameVersion.Full);
            }
        }

        isCheckingWord = false;
    }


    public bool isWordExcluded(string text)
    {
        if (excludeList.Count >0 )
        {
            if (excludeList.Contains(text))
            {
                return true;
            } else
            {
                return false;
            }
        }
        return false;
    }

    /// <summary>
    /// IEnumerator function for pronouncing the correct word
    /// </summary>
    [System.Reflection.Obfuscation(ApplyToMembers=false)]
    private IEnumerator pronounciation ()
	{
		while (AlphabetCoreAudioSystem.IsPlaying)
		{
			yield return 0;
		}
        AlphabetCoreAudioSystem.instance.playOnce(last_spelling ,AlphabetCoreConstants.AUDIO_Selections.WORD_PRONUNCIATION);
    }
	
	[System.Reflection.Obfuscation(ApplyToMembers=false)]
	private IEnumerator disableWordCompletionTexture ()
	{
		yield return new WaitForSeconds (WORD_COMPLETION_TEXTURE_DUR_S);
		WordCompletionTexture.mainTexture = null;
	}

    [Beebyte.Obfuscator.SkipRename]
    public void OnTutorialFinishButtonClick ()
	{
        CCLog.SpecialMsg ("Finish Tutorial");
		isTutorialTime = false;
		enableUIButtons();
		WordCompletionTexture.mainTexture = null;
	}
	
	void OnTutorialSkipButtonClick ()
	{
		setNextWord ();
	}

    #endregion

    #region Event for UI
    /// <summary>
    /// Hint button function call
    /// </summary>
    [Beebyte.Obfuscator.SkipRename]
    public void OnHintButtonClick ()
	{       
        CCLog.SpecialMsg("Using hint now");

        hintTimer = 0;
        useHint = true;
        isHintTimer = true;
        tutorialController.setHintsButtonClicked();
        matchMaximumCharacters(true);
        setLabelDisplay();
    }

    /// <summary>
    /// Next button function call
    /// </summary>
    [Beebyte.Obfuscator.SkipRename]
    public virtual void OnNextButtonClick ()
	{
        // Next word is set when user skips or completes the current word
        AlphabetCoreAudioSystem.instance.stopAll();
        if (tutorialController.isOverrideWordSetting)
        {
            setNextWord(tutorialController.overrideWords[tutorialController.overrideWordIndex]);
            if (tutorialController.overrideWordIndex == tutorialController.overrideWords.Count - 1)
            {
                tutorialController.overrideWordIndex = 0;
            }
            else
            {
                tutorialController.overrideWordIndex++;
            }
        }
        else
        {
            setNextWord();
        }
		OnInputChange(currentSpelling);
    }
    #endregion

    #region Event for object
    /// <summary>
    /// 3D Object click function call
    /// </summary>
    [Beebyte.Obfuscator.SkipRename]
    public void On3dObjClick ()
	{
        // Click the marker area to play animation and audio
        if (string.IsNullOrEmpty(last_spelling) || !tutorialController.isTutorialFinished())
        {
            return;
        }

        tutorialController.set3DObjectClicked();

        try
		{
			if(!loader.current_obj.GetComponentInChildren<Animation>().isPlaying)
			{
                StartCoroutine ("pronounciation");
				loader.current_obj.GetComponentInChildren<Animation>().Play ();
			}
		}
		catch {}
	}
	#endregion

    /// <summary>
    /// Public method for force rechecking current spelling
    /// </summary>
    public void recheckSpellingForTutorial()
    {
        OnInputChange(currentSpelling);
    }
	
    public void disableLeftSideUIButtons()
    {
        backButtonCollider.enabled = false;
        backButtonUI.SetState(UIButtonColor.State.Disabled, false);

        captureShareButtonCollider.enabled = false;
        captureShareButtonUI.SetState(UIButtonColor.State.Disabled, false);
    }

	private void enableUIButtons()
	{
        backButtonCollider.enabled = true;
        backButtonUI.SetState(UIButtonColor.State.Normal, false);

        captureShareButtonCollider.enabled = true;
        captureShareButtonUI.SetState(UIButtonColor.State.Normal, false);

        foreach (BoxCollider collider in rightColliders)
        {
            collider.enabled = true;
        }

        foreach (UIButton uibutton in rightUiButtons)
        {
            uibutton.SetState(UIButtonColor.State.Normal, false);
        }
	}

    /// <summary>
    /// Set the next word to be spelled. Also reset hint timer
    /// </summary>
    protected void setNextWord()
    {
        if (IsBeginnerMode)
        {
            disableHintLetters();
            setNextWordProperties();
            numOfCharsForTips = currentWord.Length > 3 ? 2 : 1;
            setNextWordTransformAndDisplay();
        }
        else
        {
            setNextWordProperties();
            setNextWordTransformAndDisplay();
        }
    }

    /// <summary>
    /// For testing purposes
    /// </summary>
    protected void setNextWord(string word)
    {
        if (IsBeginnerMode)
        {
            disableHintLetters();
            setNextWordProperties(word);
            numOfCharsForTips = currentWord.Length > 3 ? 2 : 1;
            setNextWordTransformAndDisplay();
        }
        else
        {
            setNextWordProperties(word);
            setNextWordTransformAndDisplay();
        }
    }

    /// <summary>
    /// Set the properties of the next word to be spelled
    /// </summary>
    protected void setNextWordProperties()
    {
        // If all words have been used, reset and randomize list
        if (wordListIndex > mSystem.getWordList().Count - 1)
        {
            mSystem.randomOrderOfWordList();
            wordListIndex = 0;
        }
        
        // Need to reset system, let the logic of system work smoothly
        mSystem.resetSpelling(false);
        
        // Reset the last spelling
        last_spelling = "";

        if (isTutorialTime)
        {
            currentWord = "car";
        }
        else
        {
            currentWord = mSystem.getWordList()[wordListIndex++];
            while (isWordExcluded(currentWord))
            {
                setNextWordProperties();
            }


        }
        mSystem.setupSpelling(currentWord, AlphabetCoreSystem.HeaderAlphabetSettings.Same);
        wordCorrect = false;
        useHint = false;
        isHintTimer = false;
        WordCompletionTexture.mainTexture = null;
    }

    public Sprite[] splitTexture(string path)
    {
        Debug.Log("path : " + path);
       wordSprites = new Sprite[12];
        source = loader.loadSprite(path);
       // source[0].Resize(441,274);
        int index = 0;
        for (int i = 0; i < 4; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                Sprite newSprite = Sprite.Create(source[0], new Rect(i * 110, j * 91, 110, 91), new Vector2(0.5f, 0.5f));
                Debug.Log(index);
                wordSprites[index] = newSprite;
                index++;


            }
        }
        return wordSprites;
    }
    /// <summary>
    /// For testing purposes
    /// </summary>
    protected void setNextWordProperties(string word)
    {
        // Need to reset system, let the logic of system work smoothly
        mSystem.resetSpelling(false);
        
        // Reset the last spelling
        last_spelling = "";

        currentWord = word;
        mSystem.setupSpelling(currentWord, AlphabetCoreSystem.HeaderAlphabetSettings.Same);

        wordCorrect = false;
        useHint = false;
        isHintTimer = false;
        WordCompletionTexture.mainTexture = null;
    }

    /// <summary>
    /// Set the transform and display settings of the next word to be spelled
    /// </summary>
    protected void setNextWordTransformAndDisplay()
    {
        /*
        // 3D display and setup transfrom also stop the animation before user spell correct
        try
        {
            Transform obj = loader.loadGameObject("obj/" + currentWord).transform;

            if (!tutorialController.magicScreen.isTrackerFind)
            {
                Renderer[] components = obj.GetComponentsInChildren<Renderer>(true);
                foreach (Renderer render in components)
                {
                    render.enabled = false;
                }
            }

            Vector3 pos = obj.localPosition;
            Vector3 scale = obj.localScale;
            Vector3 rotation = obj.localEulerAngles;

            obj.parent = objParent;
            obj.localPosition = pos;
            obj.localScale = scale;
            obj.localEulerAngles = rotation;

            obj.GetComponentInChildren<Animation>().Stop();
        }
        catch {}
        */

        //markerBg.material.mainTexture = loader.loadTexture("floor/" + AlphabetCoreSystem.WordCategory(currentWord));
         Sprite[] wordSprites = loader.loadSpriteMultiple("realobj/markerBG_" + currentWord);
       // Sprite[] wordSprites = splitTexture("realobj/markerBG_" + currentWord); 
        int spriteIndex = 0;

        if (wordSprites != null && wordSprites.Length > 0)
        {
            foreach (GameObject flipPlaneRow in flipPlaneRows)
            {
                foreach (Transform child in flipPlaneRow.transform)
                {
                    //Debug.Log("parent: " + child.parent.name + ", child: " + child.name);
                    SpriteRenderer spriteRenderer = child.FindChild("Back").GetComponent<SpriteRenderer>();
                    spriteRenderer.sprite = wordSprites[spriteIndex++];
                }
            }
        }

        setLabelDisplayBoxes();

        foreach (SpellingCharacter sc in currentWordList)
        {
            sc.setIsDisplay(false);
            sc.setIsHint(false);
        }
    }

    /// <summary>
	/// Set the number of boxes to display for characters
	/// </summary>
    private void setLabelDisplayBoxes()
    {
        Transform[] childObjects = grid.GetComponentsInChildren<Transform>(true);
        foreach(Transform t in childObjects)
        {
            // Only set active/inactive for the UISprite letter box objects
            if (t.name.Contains(LETTER_BOX_NAME_PREFIX))
            {
                if (t.GetSiblingIndex() < currentWord.Length)
                {
                    t.gameObject.SetActive(true);
                }
                else
                {
                    t.gameObject.SetActive(false);
                }
            }
            // Set letters to blank
            // *NOTE*: This must come after the letter box checking as the string below matches more than above
            else if (t.name.Contains(LETTER_NAME_PREFIX))
            {
                t.GetComponent<UILabel>().text = "";
            }
        }

        grid.Reposition();
    }

    /// <summary>
	/// Update display of characters on screen. Also handle hint display
	/// </summary>
    private void setLabelDisplay()
    {
        // Check each character to be displayed
        for (int i = 0; i < currentWordList.Count; i++)
        {
            // Check first if spelling is correct for display
            // Also display if its a hint
            if (wordCorrect || currentWordList[i].getIsDisplay() || currentWordList[i].getIsHint())
            {
                showGridTextBox(i);
            }
            // Other characters just show underscore
            else
            {
                hideGridTextBox(i);
            }
        }
    }

    /// <summary>
	/// Show the hint or spelled label of the character in the current word
	/// </summary>
    /// <param name="i">
    /// The index of the letter being modified
    /// </param>
    protected void showGridTextBox(int i)
    {
        grid.GetChild(i).GetChild(0).GetComponent<UILabel>().text = currentWordList[i].getLetter().ToUpper();
        
        if (IsBeginnerMode)
        {
            if (i == 0)
            {
                tipChars1.transform.FindChild(currentWordList[i].getLetter().ToUpper()).gameObject.SetActive(false);
            }
            else if (i == 1 && numOfCharsForTips > 1)
            {
                tipChars2.transform.FindChild(currentWordList[i].getLetter().ToUpper()).gameObject.SetActive(false);
            }
        }
    }

    /// <summary>
    /// Hide the hint or spelled label of the character in the current word
    /// </summary>
    /// <param name="i">
    /// The index of the letter being modified
    /// </param>
    protected void hideGridTextBox(int i)
    {
        grid.GetChild(i).GetChild(0).GetComponent<UILabel>().text = "";

        if (IsBeginnerMode)
        {
            if (i == 0)
            {
                tipChars1.transform.FindChild(currentWordList[i].getLetter().ToUpper()).gameObject.SetActive(true);
            }
            else if (i == 1 && numOfCharsForTips > 1)
            {
                tipChars2.transform.FindChild(currentWordList[i].getLetter().ToUpper()).gameObject.SetActive(true);
            }
        }
    }

    private void matchMaximumCharacters(bool isUseHint)
    {
		// Count how many alphabet are displaying and by hint
		int displayingCount = 0;
		int hintCount = 0;
		
        int baseIndex = 0;
        bool showChar = true;
        bool hintDisplayed = false;
        do
        {
            if (showChar && currentSpelling != null && baseIndex < currentSpelling.Length
                && currentSpelling.Substring(baseIndex, 1).Equals(currentWord.Substring(baseIndex, 1)))
			{
                currentWordList[baseIndex].setIsDisplay(true);
				// Increase the displaying count which is spelling correct
				displayingCount++;
            }
            else
            {
                // Let hit show one by one and increase hint count
                if (hintCount < currentWord.Length && currentWordList[baseIndex].getIsHint())
                {
                    hintCount++;
                }
                // Show the next valid character if using hint
                else if (!hintDisplayed && isUseHint && !currentWordList[baseIndex].getIsDisplay())
                {
                    currentWordList[baseIndex].setIsHint(true);
                    hintDisplayed = true;
                    
                    // Increase the hint count as current hint are counting in!
                    hintCount++;
                    hintTimer = 0;

                }
                else if (showChar && hintCount > 0
                    && currentSpelling.Length > 0 && (currentSpelling.Length > baseIndex - hintCount)
                    && currentSpelling.Substring(baseIndex - hintCount, 1).Equals(currentWord.Substring(baseIndex, 1)))
                {
                    currentWordList[baseIndex].setIsDisplay(true);
                    displayingCount++;
                    baseIndex++;
                    continue;
                }
                else
                {
                    // Once a character is incorrect, do not show subsequent ones even if they are in correct position
                    showChar = false;
                }
                currentWordList[baseIndex].setIsDisplay(false);
            }
            baseIndex++;
        }
        while (baseIndex < currentWordList.Count);
		
		// Check spelling is finish by hint and display notice
        if(showChar)
        {
            OnWordSpellingCorrect(currentWord);
        }
    }

    /// <summary>
    /// Disable hint letters to prepare for next word
    /// </summary>
    private void disableHintLetters()
    {
        if (currentWord != null)
        {
            tipChars1.transform.FindChild(currentWordList[0].getLetter().ToUpper()).gameObject.SetActive(false);
            if (currentWord.Length > 3)
            {
                tipChars2.transform.FindChild(currentWordList[1].getLetter().ToUpper()).gameObject.SetActive(false);
            }
        }
    }

    protected class SpellingCharacter
    {
        private string letter = "";
        private bool isDisplay = false;
        private bool isHint = false;

        public SpellingCharacter(string l)
        {
            letter = l;
        }

        public void setLetter(string l)
        {
            letter = l;
        }

        public string getLetter()
        {
            return letter;
        }

        public void setIsDisplay(bool b)
        {
            isDisplay = b;
        }

        public bool getIsDisplay()
        {
            return isDisplay;
        }

        public void setIsHint(bool b)
        {
            isHint = b;
        }

        public bool getIsHint()
        {
            return isHint;
        }
    }

    private void OnMarkerAlphabetCheck(bool isFreeMarkerScaned)
    {
        
    }

    private void OnStageMarkerFound()
    {
        isStageMarkerFound = true;
		flipPlanes.transform.localScale = Vector3.one;
    }

    private void OnStageMarkerLost()
    {
        isStageMarkerFound = false;
		if (mSystem.currentITB.Count == 0) 
		{
			flipPlanes.transform.localScale = Vector3.zero;
		}

    }
}
