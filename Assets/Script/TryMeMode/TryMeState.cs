﻿using UnityEngine;

[System.Serializable]
public class TryMeState
{
    public const float MIN_FADE_TIME = 0.1f;
    public const float MAX_FADE_TIME = 3f;
    public const float MIN_ALPHA = 0.0f;
    public const float MAX_ALPHA = 1.0f;

    [Header("Tutorial Settings")]
    public AlphabetCoreConstants.CHARACTER_TAG_NAMESS tagName;
    public AlphabetCoreConstants.CHARACTER_Image_Icon imageName;
    [System.NonSerialized]
    public string clipName;
    public string message;
    public string audioName;

    /// <summary>
    /// Initializes a new instance of the <see cref="TutorialState"/> class.
    /// </summary>
    /// <param name="tagName">Tag name for character picture</param>
    /// <param name="clipName">Name of audio clip.</param>
    /// <param name="message">Message to display.</param>
    public TryMeState(AlphabetCoreConstants.CHARACTER_TAG_NAMESS tagName, AlphabetCoreConstants.CHARACTER_Image_Icon imageName, string clipName, string message, string audioName)
    {
        this.tagName = tagName;
        this.imageName = imageName;
        this.clipName = clipName;
        this.message = message;
        this.audioName = audioName;

    }
}