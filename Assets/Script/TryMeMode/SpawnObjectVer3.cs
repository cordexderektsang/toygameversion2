﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System.Linq;

public class SpawnObjectVer3 : MonoBehaviour
{
	public List <GameObject> model = new List <GameObject>();	// array to put object from outside
	private List <SpawnConstructor> modeldetails = new List <SpawnConstructor>(); // list to store objects
	private List <GameObject> CurrentModelPositionStorage = new List <GameObject>(); // list to store objects
	int positionIndex = 0;//where camera at
	string currentObjectName;
	bool isPlaying = false;
	[SerializeField] bool isSingleTime = false;

	[SerializeField] int scriptRotationPosition;
	[SerializeField] bool isRotating = false;
	bool isPreviouslyRotated = false;

	public GameObject arCam;
	public GameObject arMenu;
	public GameObject arImage;
	public GameObject TryMeMenu;
	public Camera TryMeCam;
	public UILabel ModelName;
	public GameObject Bubble;
	public UIButton btnLabel;
    public GameObject bubbleLabel;

	public bool isRotateYMore180;
	public bool isRotateYLess180;


	//editor
	public string fileLocation;
	public bool allowRemoveTRyMeStuff = false;
	public TextAsset ListofCompleteDictionary;


	ParticleSystem.EmissionModule bubbleEmissionModel;
    public float bubbleLabelShakingMaxTime = 2;
    public float bubbleLabelShakingCounter = 0;

    void Awake()
	{

        // add int all object from array to  object list 
        bubbleLabelShakingCounter = bubbleLabelShakingMaxTime;
        for (int i = 0; i < model.Count; i++) {
			SpawnConstructor objectmodel = new SpawnConstructor (model[i]);
			modeldetails.Add (objectmodel);
		}
	}
		
	void Start () 
	{
		// spawn all objects with same distance regardless about their size and add all new object int to a new list to store their position
		loopStartData(positionIndex, true);
		ModelName.text = modeldetails [positionIndex].name;
		currentObjectName = ModelName.text;
		for (int i = 1; i < modeldetails.Count; i++) {
			loopStartData(i, false);
		}

        bubbleEmissionModel = Bubble.GetComponent<ParticleSystem>().emission;
    }

	/// <summary>
	/// Loops the start data.
	/// </summary>
	/// <param name="index">list position</param>
	/// <param name="enable">set the object vision <c>true</c> enable.</param>
	void loopStartData (int index, bool enable)
	{
		GameObject currentObject = Instantiate (modeldetails [index].currentModel) as GameObject;
		currentObject.name = modeldetails [index].name;
		if (currentObject.transform.position == Vector3.zero) 
		{
			currentObject.transform.position = new Vector3 (0, -0.85f, 30f);
		}
		currentObject.SetActive (enable);
		CurrentModelPositionStorage.Add (currentObject);
	}

	void Update ()
	{
		Animation checkAnimation = CurrentModelPositionStorage [positionIndex].GetComponent<Animation> ();
		if (Input.GetMouseButton (0)) {
			Ray ray = TryMeCam.ScreenPointToRay (Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast (ray, out hit, 1000) && hit.collider.gameObject.name.Equals(currentObjectName)) {
                if (checkAnimation.enabled) {
					AnimationHandling (hit.collider.gameObject);
				} else if (!isRotating) {
					isRotateYMore180 = false;
					isRotateYLess180 = false;
					CurrentModelPositionStorage[positionIndex].transform.rotation = model[positionIndex].transform.rotation;
				}
			}
		}

		if(checkAnimation.enabled) {
			SetEmissionIfAnimating(checkAnimation);
		} else {
			SelfRotation();
		}
	}

	/// <summary>
	/// right arrow call and enable right side object.
	/// </summary>
	public void ObjectMoveRight()
	{
        DisableCurrentModel();
        if (positionIndex >= modeldetails.Count - 1) {
			//CurrentModelPositionStorage [positionIndex].SetActive (false);
			positionIndex = 0;
			//CurrentModelPositionStorage [positionIndex].SetActive (true);
			//ModelName.text = modeldetails [positionIndex].name;
			//currentObjectName = modeldetails [positionIndex].name;
		} else {
			//CurrentModelPositionStorage [positionIndex].SetActive (false);
			positionIndex++;
			//CurrentModelPositionStorage [positionIndex].SetActive (true);
			//ModelName.text = modeldetails [positionIndex].name;
			//currentObjectName = modeldetails [positionIndex].name;
		}
        EnableNextModel();
        //isSingleTime = false;
	}

	/// <summary>
	/// Left arrow call and enable left side object.
	/// </summary>
	public void ObjectMoveLeft()
	{
        DisableCurrentModel();
        if (positionIndex == 0) {
			//CurrentModelPositionStorage [positionIndex].SetActive (false);
			positionIndex = modeldetails.Count - 1;
			//CurrentModelPositionStorage [positionIndex].SetActive (true);
			//ModelName.text = modeldetails [positionIndex].name;
			//currentObjectName = modeldetails [positionIndex].name;
		} else {
			//CurrentModelPositionStorage [positionIndex].SetActive (false);
			positionIndex--;
			//CurrentModelPositionStorage [positionIndex].SetActive (true);
			//ModelName.text = modeldetails [positionIndex].name;
			//currentObjectName = modeldetails [positionIndex].name;
		}
        EnableNextModel();
        //isSingleTime = false;
    }

    private void DisableCurrentModel()
	{

		// to reset the rotation
		Animation checkAnimation = CurrentModelPositionStorage [positionIndex].GetComponent<Animation> ();
		if (!checkAnimation.enabled) 
		{
			CurrentModelPositionStorage [scriptRotationPosition].transform.localEulerAngles = model [positionIndex].transform.localEulerAngles;
			Debug.Log (CurrentModelPositionStorage [scriptRotationPosition].name + " "  + CurrentModelPositionStorage [scriptRotationPosition].transform.localEulerAngles.y);
		}
		CurrentModelPositionStorage[positionIndex].SetActive(false);
		isRotating = false;
		isRotateYMore180 = false;
		isRotateYLess180 = false;

    }

    private void EnableNextModel()
	{
        CurrentModelPositionStorage[positionIndex].SetActive(true);
        ModelName.text = modeldetails[positionIndex].name;
        currentObjectName = modeldetails[positionIndex].name;

        isSingleTime = false;
		isRotateYMore180 = false;
		isRotateYLess180 = false;

		//btnLabel.isEnabled = false;
    }

	/// <summary>
	/// Back Button on click function 
	/// </summary>
	public void BacktoTryMe()
	{
		TryMeMenu.SetActive(true);
		arCam.SetActive(false) ;
		arMenu.SetActive(false) ;
		arImage.SetActive(false);
	}

	/// <summary>
	/// AR mode Button on click function
	/// </summary>
	public void AREnable()
	{		
		TryMeMenu.SetActive(false);
		arCam.SetActive(true) ;
		arMenu.SetActive(true) ;
		arImage.SetActive(true);
	}

	/// <summary>
	/// Back to Trymemode button on click function
	/// </summary>
	public void BacktoMenu()
	{
		//Application.LoadLevel ("Menu");
        SceneManager.LoadScene("Menu");
	}

	/// <summary>
	/// Labal Display on click function
	/// </summary>
	public void DisplayAnimationReplay()
	{
		AnimationHandling (CurrentModelPositionStorage [positionIndex]);
	}

	/// <summary>
	/// Play Animation
	/// </summary>
	/// <param name="clickedObject">Clicked object.</param>
	void AnimationHandling (GameObject clickedObject)
	{
		if(isPreviouslyRotated) {
			
			CurrentModelPositionStorage [scriptRotationPosition].transform.rotation = model[positionIndex].transform.rotation;
			isPreviouslyRotated = false;
		}
		Animation ani = clickedObject.GetComponent<Animation> ();
		ani.Play();
	}

	/// <summary>
	/// handle buddle start and end
	/// </summary>
	/// <param name="CheckAni">Check ani.</param>

	void SetEmissionIfAnimating(Animation CheckAni)
	{
		if (CheckAni.isPlaying) {
            bubbleEmissionModel.enabled = false;
            //btnLabel.isEnabled = false;
        } else {
            bubbleEmissionModel.enabled = true;
           // Debug.Log(delayTimerForBubbleShake());
            if (delayTimerForBubbleShake())
            {
                iTween.ShakePosition(bubbleLabel, iTween.Hash("x", 0.05f, "y", 0.05f, "time", 0.5f, "looptype", iTween.LoopType.none, "oncompletetarget", bubbleLabel, "oncomplete", "delayTimerForBubbleShake"));
            }
            //btnLabel.isEnabled = true;
        }
	}

    bool delayTimerForBubbleShake ()
    {
        if (bubbleLabelShakingCounter < bubbleLabelShakingMaxTime)
        {
            bubbleLabelShakingCounter += Time.deltaTime;
        } else
        {
            bubbleLabelShakingCounter = 0;
            return true;
        }
        return false;
   }

	/// <summary>
	/// handle rotation start and end (when animation is off)
	/// </summary>
	void SelfRotation()
	{
		if(!isSingleTime) {
			if(isPreviouslyRotated) {
				CurrentModelPositionStorage [scriptRotationPosition].transform.rotation = model[positionIndex].transform.rotation;
			}
			CurrentModelPositionStorage[positionIndex].transform.localEulerAngles = model[positionIndex].transform.localEulerAngles;

			isSingleTime = true;
			scriptRotationPosition = positionIndex;
			isPreviouslyRotated = true;
		}
		//ParticleSystem.EmissionModule em = Bubble.GetComponent<ParticleSystem>().emission;

		// rotate 1 round
		if (Mathf.Round(model[positionIndex].transform.localEulerAngles.y - CurrentModelPositionStorage [positionIndex].transform.localEulerAngles.y) == 0 && isRotateYLess180 && isRotateYMore180) 
		{
			isRotating = false;
			bubbleEmissionModel.enabled = true;
			//btnLabel.isEnabled = true;
		}
		else 
		{
			isRotating = true;
            bubbleEmissionModel.enabled = false;
			//btnLabel.isEnabled = false;
			CurrentModelPositionStorage [positionIndex].transform.Rotate (Vector3.up * Time.deltaTime * 20);
			// checkpoint: check the model rotate y is more / less than 180
			if (CurrentModelPositionStorage [positionIndex].transform.localEulerAngles.y > 180) 
			{
				isRotateYMore180 = true;	
			}
			else if (CurrentModelPositionStorage [positionIndex].transform.localEulerAngles.y < 180) 
			{
				isRotateYLess180 = true;
			}

		}
	}

	public void SortObjectByName() 
	{
		// sort the model name order by name
		model = model.OrderBy(obj => obj.name).ToList ();
	}

	public void OnLabelClicked() 
	{
		Debug.Log ("OnLabelClicked ");
		Animation checkAnimation = CurrentModelPositionStorage [positionIndex].GetComponent<Animation> ();
		if (checkAnimation.enabled) {
			AnimationHandling (CurrentModelPositionStorage [positionIndex].gameObject);
		} else if (!isRotating) {
			isRotateYMore180 = false;
			isRotateYLess180 = false;
			CurrentModelPositionStorage[positionIndex].transform.rotation = model[positionIndex].transform.rotation;
		}

	}

	public void checkWithCompleteList ()
	{
		bool IsFindMatch = false;
		string listoftext;
		List<string> objectListName = new List<string> ();
		listoftext = ListofCompleteDictionary.text.Replace("\r", "");
		objectListName.AddRange (listoftext.Split ("\n" [0]));
		int i = 0;
		while( i < objectListName.Count)
			{
				for(int o = 0 ; o < model.Count; o++)
				{
				if(objectListName[i].ToLower().Equals(model[o].name.ToLower()))
					{
						IsFindMatch = true;
						break;
					}
				}
				if (IsFindMatch == false)
				{
					Debug.Log (objectListName[i]);
				}
				i++;
				IsFindMatch = false;
			}
			Debug.Log ("All Done");
		}
}
