﻿using UnityEngine;

public class RocketBubbleMovement : MonoBehaviour
{
	// Use this for initialization
	float bubbleScaleTimer = 0; // scale bubble timer
	float maxBubbleScaleTimer = 0.2f; //timer 
	float startBubbleScaleSize = 0.2f; //start size for bubble

	// Update is called once per frame
	void Update () 
	{
		if (bubbleScaleTimer < maxBubbleScaleTimer) {
			bubbleScaleTimer += Time.deltaTime;
		} else {
			transform.localScale = new Vector2 (startBubbleScaleSize, startBubbleScaleSize); // scale smaller
			startBubbleScaleSize -= 0.08f;
			bubbleScaleTimer = 0;
		}

		if (startBubbleScaleSize > 0) {
			transform.Translate (Vector2.right * Time.deltaTime); // move bubble
		} else {
			Destroy (gameObject); // destroy it
		}
	}
}
