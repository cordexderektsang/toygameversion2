﻿
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using System.IO;



public class TrymeSpawn : MonoBehaviour
{

    public List<TrymeSpawnConstructor> spawnObjectList; // List to Store All Object data
    public List<string> ExcludeGameObjectName; // Game Object to be exclude

    // Source Path
    private const string path = "Assets/Resources/";
    private string fileLocation = path + "dictionary/dict.txt";
    private string TextLocation = path + "obj";
    private string ChangeObjPath = "Assets/ItemChangeFile";

    private string fileName = "TrymeObjectData.txt";// name for output data
    private string dictName = "dictionary.txt";// name for output data
    private List<GameObject> storeGameObject; // list to store Gameobject for Reference

#if UNITY_EDITOR

    /// <summary>
    /// Export All Data into Text file
    /// </summary>
    public void SaveData()
    {
        if (File.Exists(Application.dataPath + "/" + fileName))
        {
            Debug.Log(fileName + " Already Exists");
            return;
        }
        StreamWriter sr = File.CreateText(Application.dataPath + "/" + fileName);
        for (int i = 0; i < spawnObjectList.Count; i++)
        {
            sr.WriteLine(spawnObjectList[i].name);
            sr.WriteLine(spawnObjectList[i].objectPosition.ToString("G4"));
            sr.WriteLine(spawnObjectList[i].objectRotation.ToString("G4"));
            sr.WriteLine(spawnObjectList[i].objectScale.ToString("G4"));

        }

        sr.Close();

    }

    public void generateDict()
    {
       /* if (File.Exists(Application.dataPath + "/" + dictName))
        {
            Debug.Log(dictName + "dict Already Exists");
            return;
        }*/
        StreamWriter sr = File.CreateText(Application.dataPath + "/" + dictName);
        for (int i = 0; i < spawnObjectList.Count; i++)
        {
            sr.WriteLine(spawnObjectList[i].name);
        }

        sr.Close();

    }

    /// <summary>
    /// Write Data to the List
    /// </summary>
    public void WriteData()
    {
        if (File.Exists(Application.dataPath + "/" + fileName))
        {
            int fileIndex = 0;
            StreamReader sr = new StreamReader(Application.dataPath + "/" + fileName);
            string Data = sr.ReadToEnd();
            sr.Close();
            storage();
            string[] lines = Data.Split("\n"[0]);
            string lineRemoveSpace = "";
          //  string nameOfTheGameObject = "";
            Vector3 positon = new Vector3(0, 0, 0);
            Vector3 rotation = new Vector3(0, 0, 0);
            Vector3 scale = new Vector3(0, 0, 0);

            //GameObject CurrentGameobject = null;
            spawnObjectList = new List<TrymeSpawnConstructor>();
            for (int i = 0; i < lines.Length; i++)
            {
                try
                {

                    if (lines[i].Length > 0)
                    {
                        if (fileIndex == 0)// display name
                        {
                            lineRemoveSpace = lines[i].TrimEnd();
                        }
                        else
                       //  if (fileIndex == 1)// name of the actually object
                       // {
                          //  nameOfTheGameObject = lines[i].TrimEnd();
                           // CurrentGameobject = loadData(nameOfTheGameObject);// find the object in folder then add it into the list
                       // }
                       // else

                         if (fileIndex == 1)// position
                        {
                            string[] positions = lines[i].Split(',');
                            positon = new Vector3(float.Parse(positions[0].Substring(1)), float.Parse(positions[1]), float.Parse(positions[2].Replace(")", "")));

                        }
                        else

                         if (fileIndex == 2)// rotation
                        {
                            string[] rotations = lines[i].Split(',');
                            rotation = new Vector3(float.Parse(rotations[0].Substring(1)), float.Parse(rotations[1]), float.Parse(rotations[2].Replace(")", "")));

                        }
                        else

                         if (fileIndex == 3) //scale
                        {
                            string[] scales = lines[i].Split(',');
                            scale = new Vector3(float.Parse(scales[0].Substring(1)), float.Parse(scales[1]), float.Parse(scales[2].Replace(")", "")));

                        }
                        fileIndex++;
                        if (fileIndex == 4)
                        {
                            //if (CurrentGameobject != null)
                            //{ 
                                TrymeSpawnConstructor CreateNewObject = new TrymeSpawnConstructor(lineRemoveSpace, positon, rotation, scale, false);
                                IsRotation(CreateNewObject);
                                CreateNewObject.name = lineRemoveSpace;
                                // finish  a cycle for one object :(name 0, object 1,position  2,rotation  3,scale 4) then restart the cycle for the rest of the object
                                spawnObjectList.Add(CreateNewObject);
                           // }
                            fileIndex = 0;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Debug.Log(ex);
                }

            }
        }
        storeGameObject = new List<GameObject>();
        checkWithCompleteList();// check list 

    }

    /// <summary>
    /// Load All New items
    /// </summary>
    public void LoadNewItems()
    {
        storage();
        Debug.Log(storeGameObject.Count);
        if (storeGameObject != null)
        {
            for (int i = 0; i < storeGameObject.Count; i++)
            {
                bool isFind = false;
                for (int o = 0; o < spawnObjectList.Count && isFind == false; o++)
                {
   
                    if (spawnObjectList[o].name.ToLower().Equals(storeGameObject[i].name.ToLower()))
                    {
                        isFind = true;
                    }
                }
                if (isFind == false && string.IsNullOrEmpty(storeGameObject[i].name))
                {
                   // Debug.Log(storeGameObject[i].name + " i " + i );
                    TrymeSpawnConstructor CreateNewObject = new TrymeSpawnConstructor(storeGameObject[i].name, new Vector3(0, 0, 0), new Vector3(0, 0, 0), new Vector3(1, 1, 1), false);
                  //  spawnObjectList.Add(CreateNewObject);
                    Debug.Log("currentObject : " + CreateNewObject.name + " has add in");
                }
            }
            SortObjectByName();
            Debug.Log("Loaded All Items , All Done");
            checkWithCompleteList();
        }
    }

    /// <summary>
    /// Load All Changed Item
    /// </summary>
    public void LoadChangeData()
    {
        if (spawnObjectList.Count > 0)
        {
            List<string> path = new List<string>();
            string[] assetsPaths = UnityEditor.AssetDatabase.GetAllAssetPaths();
            foreach (string assetPath in assetsPaths)
            {
                if (assetPath.Contains(ChangeObjPath))
                {
                    path.Add(assetPath);
                }
            }
            for (int i = 0; i < path.Count; i++)
            {
                GameObject currentObject = UnityEditor.AssetDatabase.LoadAssetAtPath<GameObject>(path[i]);
                if (currentObject != null)
                {
                    for (int o = 0; o < spawnObjectList.Count; o++)
                    {
                        if (currentObject.name.Equals(spawnObjectList[o].name))
                        {
                            spawnObjectList[o].objectPosition = currentObject.transform.position;
                            spawnObjectList[o].objectRotation = new Vector3(currentObject.transform.eulerAngles.x, currentObject.transform.eulerAngles.y, currentObject.transform.eulerAngles.z);
                            spawnObjectList[o].objectScale = currentObject.transform.localScale;
                            Debug.Log(spawnObjectList[o].name + " has Changed");
                        }
                    }
                }
            }
        }
        else
        {
            Debug.Log("Nothing Change");
        }

    }

    /// <summary>
    /// Sort the list by name
    /// </summary>
    private void SortObjectByName()
    {
        spawnObjectList = spawnObjectList.OrderBy(obj => obj.name).ToList();
    }

    /// <summary>
    /// check with Textfile to determine what is missing
    /// </summary>
    private void checkWithCompleteList()
    {
        TextAsset ListofCompleteDictionary = null;
        string[] assetsPaths = UnityEditor.AssetDatabase.GetAllAssetPaths();
        foreach (string assetPath in assetsPaths)
        {
            if (assetPath.Contains(fileLocation) && ListofCompleteDictionary == null)
            {
                ListofCompleteDictionary = (TextAsset)UnityEditor.AssetDatabase.LoadAssetAtPath(assetPath, typeof(TextAsset));

            }
        }
        if (ListofCompleteDictionary != null)
        {
            bool IsFindMatch = false;
            string listoftext;
            List<string> objectListName = new List<string>();
            listoftext = ListofCompleteDictionary.text.Replace("\r", "");
            objectListName.AddRange(listoftext.Split("\n"[0]));
            int i = 0;
            while (i < objectListName.Count)
            {
                for (int o = 0; o < spawnObjectList.Count; o++)
                {
                    if (objectListName[i].ToLower().Equals(spawnObjectList[o].name.ToLower()))
                    {
                        IsFindMatch = true;
                        break;
                    }
                }
                if (IsFindMatch == false && string.IsNullOrEmpty(objectListName[i]))
                {
                    Debug.Log(objectListName[i] + "is Missing");
                }
                i++;
                IsFindMatch = false;
            }
            Debug.Log("Dictionary Check Complete All Done");
        }
        else
        {
            Debug.Log(fileLocation + " Path not find");
        }
    }

    /// <summary>
    /// get object from path and store as reference
    /// </summary>
    private void storage()
    {
        storeGameObject = new List<GameObject>();
        List<string> path = new List<string>();
        string[] assetsPaths = UnityEditor.AssetDatabase.GetAllAssetPaths();
        foreach (string assetPath in assetsPaths)
        {
            if (assetPath.Contains(TextLocation))
            {
                path.Add(assetPath);
            }
        }
        for (int i = 0; i < path.Count; i++)
        {
            bool Exclude = false;
            GameObject currentObject = UnityEditor.AssetDatabase.LoadAssetAtPath<GameObject>(path[i]);
            if (currentObject != null)
            {
                for (int o = 0; o < ExcludeGameObjectName.Count && Exclude == false; o++)// exclude gameobject not add into the list
                {

                    if (currentObject.name.Equals(ExcludeGameObjectName[o]))
                    {
                        Exclude = true;
                    }
                }
                if (Exclude == false)
                {
                    storeGameObject.Add(currentObject);
                }
            }
        }

    }

    /// <summary>
    /// Return Game object
    /// </summary>
    /// <param name="line"></param>
    /// <returns> Game Object</returns>
    private GameObject loadData(string line)
    {
        bool findMatch = false;

        for (int i = 0; i < storeGameObject.Count; i++)
        {
            if (storeGameObject[i].name.ToLower().Equals(line.ToLower()))
            {
                findMatch = true;
                return storeGameObject[i];
            }
        }
        if (findMatch == false)
        {
            Debug.Log("Object :" + line + " Not Find");
        }
        return null;
    }

    /// <summary>
    /// set rotation
    /// </summary>
    /// <param name="ObjectName"></param>
    private void IsRotation(TrymeSpawnConstructor ObjectName)
    {
        switch (ObjectName.name)
        {
            case "boat":
                ObjectName.IsRotating = true;
                break;
            case "car":
                ObjectName.IsRotating = true;
                break;
            case "clock":
                ObjectName.IsRotating = true;
                break;
            case "jar":
                ObjectName.IsRotating = true;
                break;
            case "pear":
                ObjectName.IsRotating = true;
                break;
            case "whale":
                ObjectName.IsRotating = true;
                break;
            case "urn":
                ObjectName.IsRotating = true;
                break;
        }
    }
#endif
}




