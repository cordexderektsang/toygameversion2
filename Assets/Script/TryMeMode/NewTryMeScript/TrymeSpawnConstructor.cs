﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class TrymeSpawnConstructor
{

    [Header("Spawn Object Detail")]
    public string name;
    public Vector3 objectPosition;
    public Vector3 objectScale;
    public Vector3 objectRotation;
    public bool IsRotating;


    public TrymeSpawnConstructor(string name , Vector3 objectPosition , Vector3 objectRotation, Vector3 objectScale , bool IsRotating)
    {
        this.objectPosition = objectPosition;
        this.objectScale = objectScale;
        this.objectRotation = objectRotation;
        this.IsRotating = IsRotating;
    }
}
