﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System.Linq;
using System.Collections;

public class SpawnObjectVer4 : MonoBehaviour
{
    public Camera TryMeCam;
    public UILabel ModelName;
    public UIButton btnLabel;
    public GameObject Bubble;
    public GameObject bubbleLabel;
    public bool isRotateYMore180;
    public bool isRotateYLess180;
    public float bubbleLabelShakingMaxTime = 2;
    public float bubbleLabelShakingCounter = 0;

    private int positionIndex = 0;//currentObject
    private int scriptRotationPosition;
    private string currentObjectName;
    private bool isSingleTime = false;
    private bool isRotating = false;
    private bool isPlaying = false;
    private bool isPreviouslyRotated = false;
    private TrymeSpawn tryMeScript;
    private Animation checkAnimation;
    private ParticleSystem.EmissionModule bubbleEmissionModel;
    private float soundTimeCounter = 0;
    private float maxCounter = 1f;
    private bool didPlayed = false;
    private GameObject currentObject;
    private Quaternion currentObjectRotation;
	public UISprite display;
	private const string orangeDisplay ="display_bingo";
	private const string normalDisplay = "display";
			
    private CCManualLoadResourceObj loader;


    void Awake()
    {
        // add int all object from array to  object list 
        bubbleLabelShakingCounter = bubbleLabelShakingMaxTime;
        tryMeScript = GetComponent<TrymeSpawn>();
        loader = new CCManualLoadResourceObj();
    }


    // load and enable all model	
    void Start()
    {
        loopStartData(positionIndex, true); // enable  the vision  and load data for first object
        currentObjectName = ModelName.text; //clone and replace name       
                                            /* for (int i = 1; i < tryMeScript.spawnObjectList.Count; i++) // load the rest of the data
                                             {
                                                 loopStartData(i, false);
                                             }*/
        bubbleEmissionModel = Bubble.GetComponent<ParticleSystem>().emission; //get the bubble
    }


    void destory()
    {
        Destroy(currentObject);
        isRotating = false;
        isRotateYMore180 = false;
        isRotateYLess180 = false;
    }
    /// <summary>
    /// Loops the start data.
    /// </summary>
    /// <param name="index">list position</param>
    /// <param name="enable">set the object vision <c>true</c> enable.</param>
    void loopStartData(int index, bool enable)
    {
        try
        {

            // GameObject obj = loader.loadGameObject("obj/" + tryMeScript.spawnObjectList[index].name) as GameObject;

            // Reset position after assigning to parent

            // currentObject = Instantiate(tryMeScript.spawnObjectList[index].currentObject) as GameObject;
            if (!tryMeScript.spawnObjectList[index].name.ToLower().Equals("whale"))
            {
                currentObject = loader.loadGameObject("obj/" + tryMeScript.spawnObjectList[index].name) as GameObject;
            } else
            {
                currentObject = loader.loadGameObject("obj/" + "TRywhale") as GameObject;
            }
                if (currentObject.GetComponent<SetLastFrameOfAnimation>() != null) // disable last frame effect
            {
                currentObject.GetComponent<SetLastFrameOfAnimation>().enabled = false;
            }

            if (tryMeScript.spawnObjectList[index].IsRotating == true) /// disable animation for rotation
            {
                currentObject.GetComponent<Animation>().enabled = false;
            }

            for (int i = 0; i < currentObject.transform.childCount; i++) // disable deep Mask
            {
                GameObject childObject = currentObject.transform.GetChild(i).gameObject;
                if (childObject.name.Equals("mask"))
                {
                    DestroyObject(childObject);
                }
            }
            if (tryMeScript.spawnObjectList[index].IsRotating)
            {
                SetLastFrameOfAnimation setLast = currentObject.GetComponent<SetLastFrameOfAnimation>();
                if (setLast != null)
                {
                    setLast.enabled = true;
                }
            }
                //currentObject.transform.SetParent(emptyGameObject.transform);      
                currentObject.name = tryMeScript.spawnObjectList[index].name; ; // remove clone inside the name
            currentObject.transform.position = tryMeScript.spawnObjectList[index].objectPosition; // get the position
            currentObject.transform.eulerAngles = tryMeScript.spawnObjectList[index].objectRotation;// get the rotation
            currentObject.transform.localScale = tryMeScript.spawnObjectList[index].objectScale; // get the scale
            currentObject.SetActive(enable); // set the active
			ModelName.text = tryMeScript.spawnObjectList[positionIndex].name.ToUpper(); //get the text
            checkAnimation = currentObject.GetComponent<Animation>();
         
         

        }
        catch (System.Exception ex)
        {

        }
    }

    // enable and display bubble base on animation
    void FixedUpdate()
    {
        if (checkAnimation.enabled)
        {
            SetEmissionIfAnimating(checkAnimation);
        }
        else
        {
            SelfRotation();
        }
        startTimer();
    }

    /// <summary>
    /// when click on object or label replay animation
    /// </summary>
    public void OnclickObject()
    {
        Animation checkAnimation = currentObject.GetComponent<Animation>();
     

        //if (checkAnimation.enabled)
        //{
        //    checkAnimation.Play();
        //} 
        //else
        if ((!AlphabetCoreAudioSystem.IsPlaying && !checkAnimation.isPlaying) || (!checkAnimation.enabled && !isRotating))
        {

            StartCoroutine(word_pronounciation());

            if (!checkAnimation.enabled && !isRotating)
            {
                isRotateYMore180 = false;
                isRotateYLess180 = false;
                // currentObject.transform.rotation = tryMeScript.spawnObjectList[positionIndex].currentObject.transform.rotation;
                currentObject.transform.rotation = currentObjectRotation;

            }
            else if (checkAnimation.enabled && !checkAnimation.isPlaying)
            {
                checkAnimation.Play();
            }
        }
        //else if (!isRotating)
        //{
            

        //}

     


    }

  

    private IEnumerator word_pronounciation()
    {
        while (AlphabetCoreAudioSystem.IsPlaying )
        {
            yield return 0;
        }
        string text = tryMeScript.spawnObjectList[positionIndex].name.ToLower();
        AlphabetCoreAudioSystem.instance.playOnce(text, AlphabetCoreConstants.AUDIO_Selections.WORD_PRONUNCIATION);
    }


    private void resetTimer()
    {
        soundTimeCounter = 0;
        didPlayed = false;
    }


    private void startTimer()
    {
        if (soundTimeCounter < maxCounter)
        {
            soundTimeCounter += Time.deltaTime;
        }
        else if (!didPlayed)
        {
            StartCoroutine(word_pronounciation());
            didPlayed = true;
        }
    }
    /// <summary>
    /// right arrow call and enable right side object.
    /// </summary>
    public void ObjectMoveRight()
    {

        destory();
        if (positionIndex >= tryMeScript.spawnObjectList.Count - 1)
        {
            positionIndex = 0;

        }
        else
        {
            positionIndex++;
        }
        currentObjectRotation = currentObject.transform.rotation;
        nextItem();


        //isSingleTime = false;
    }

    /// <summary>
    /// Left arrow call and enable left side object.
    /// </summary>
    public void ObjectMoveLeft()
    {
        destory();
        if (positionIndex == 0)
        {
            positionIndex = tryMeScript.spawnObjectList.Count - 1;
        }
        else
        {
            positionIndex--;
        }
        nextItem();


    }

    private void nextItem()
    {
        loopStartData(positionIndex, true);
        resetTimer();
        currentObjectRotation = currentObject.transform.rotation;
    }





    /// <summary>
    /// Back to Trymemode button on click function
    /// </summary>
    public void BacktoMenu()
    {
        //Application.LoadLevel ("Menu");
        SceneManager.LoadScene("Menu");
    }


    /// <summary>
    /// handle buddle start and end
    /// </summary>
    /// <param name="CheckAni">Check ani.</param>
    void SetEmissionIfAnimating(Animation CheckAni)
    {
        if (CheckAni.isPlaying)
        {
			display.spriteName = orangeDisplay;
            bubbleEmissionModel.enabled = false;
            //btnLabel.isEnabled = false;
        }
        else
        {
			display.spriteName = normalDisplay	;
            bubbleEmissionModel.enabled = true;
            if (delayTimerForBubbleShake())
            {
                iTween.ShakePosition(bubbleLabel, iTween.Hash("x", 0.05f, "y", 0.05f, "time", 0.5f, "looptype", iTween.LoopType.none, "oncompletetarget", bubbleLabel, "oncomplete", "delayTimerForBubbleShake"));
            }
            //btnLabel.isEnabled = true;
        }
    }

    /// <summary>
    /// handle delay
    /// </summary>
    /// <returns></returns>

    bool delayTimerForBubbleShake()
    {
        if (bubbleLabelShakingCounter < bubbleLabelShakingMaxTime)
        {
            bubbleLabelShakingCounter += Time.deltaTime;
        }
        else
        {
            bubbleLabelShakingCounter = 0;
            return true;
        }
        return false;
    }

    /// <summary>
    /// handle rotation start and end (when animation is off)
    /// </summary>
    void SelfRotation()
    {
        if (!isSingleTime)
        {
            if (isPreviouslyRotated)
            {
                // currentObject.transform.rotation = tryMeScript.spawnObjectList[positionIndex].currentObject.transform.rotation;
                currentObject.transform.rotation = currentObjectRotation;

            }
            currentObject.transform.localEulerAngles = tryMeScript.spawnObjectList[positionIndex].objectRotation;

            isSingleTime = true;
            scriptRotationPosition = positionIndex;
            isPreviouslyRotated = true;
        }
        // rotate 1 round
        if (Mathf.Round(tryMeScript.spawnObjectList[positionIndex].objectRotation.y - currentObject.transform.localEulerAngles.y) == 0 && isRotateYLess180 && isRotateYMore180)
        {
            isRotating = false;
            bubbleEmissionModel.enabled = true;
            btnLabel.isEnabled = true;
			display.spriteName = normalDisplay;
        }
        else
        {
            isRotating = true;
            bubbleEmissionModel.enabled = false;

            currentObject.transform.Rotate(Vector3.up * Time.deltaTime * 100);
			display.spriteName = orangeDisplay;
            // checkpoint: check the model rotate y is more / less than 180

            if (currentObject.transform.localEulerAngles.y > 180)
            {
                isRotateYMore180 = true;
            }
            else if (currentObject.transform.localEulerAngles.y < 180)
            {
                isRotateYLess180 = true;
            }

        }
    }

}
