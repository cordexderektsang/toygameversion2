﻿#if UNITY_EDITOR
using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;
using System;

[CustomEditor(typeof(TrymeSpawn))]
public class TrymoeSpawntEditor : Editor {

	public override void OnInspectorGUI()
	{
		DrawDefaultInspector ();

        TrymeSpawn edit = (TrymeSpawn)target;

        if (GUILayout.Button("Save"))
        {

            edit.SaveData();
        }

        if (GUILayout.Button("generateDictData"))
        {

            edit.generateDict();
        }



        if (GUILayout.Button("TextLoad"))
        {

            edit.WriteData();
        }


        if (GUILayout.Button("LoadNewItems"))
        {

            edit.LoadNewItems();
        }


        if (GUILayout.Button("LoadChangeItems"))
        {

            edit.LoadChangeData();
        }
    }
	
}
#endif