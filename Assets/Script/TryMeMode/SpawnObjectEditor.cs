﻿#if UNITY_EDITOR
using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;
using System;

[CustomEditor(typeof(SpawnObjectVer3))]
public class SpawnObjectEditor : Editor {

	public override void OnInspectorGUI()
	{
		DrawDefaultInspector ();

		SpawnObjectVer3 spawnObject = (SpawnObjectVer3)target;
		if (GUILayout.Button ("sort")) {
			spawnObject.SortObjectByName ();
		}

		// load item in a file to a specfic list
		if (GUILayout.Button ("loading")) {
			if (!spawnObject.fileLocation.Equals ("")) {
				List<string> path = new List <string> ();
				string[] assetsPaths = AssetDatabase.GetAllAssetPaths ();

				foreach (string assetPath in assetsPaths) {
					//if(assetPath.Contains("Assets/testing/"))
					if (assetPath.Contains (spawnObject.fileLocation)) {
						path.Add (assetPath);
					}
				}

				for (int i = 0; i < path.Count; i++) {
					//Debug.Log (spawnObject.model.Contains (AssetDatabase.LoadAssetAtPath<GameObject> (path [i])));
					GameObject currentObject = AssetDatabase.LoadAssetAtPath<GameObject> (path [i]);
					GameObject searchCurrentObjectInTheList = spawnObject.model.Find (obj => obj.name == currentObject.name);
					if (!currentObject.name.Equals (searchCurrentObjectInTheList.name)) {
						spawnObject.model.Add (AssetDatabase.LoadAssetAtPath<GameObject> (path [i]));
					}
				}
		
			} else {
				Debug.Log ("please enter a vaild file path e.g Assets/testing/");
			}
			//	Debug.Log ("count " + AssetDatabase.LoadAllAssetsAtPath ("Assets").Length);
			//spawnObject.listOfNewObject = AssetDatabase.LoadAllAssetsAtPath ("Assets/testing/") as GameObject[];
		}


		// add box , mesh collider and animation
		if (GUILayout.Button ("AddTRyMeStuff")) {
			if (spawnObject.model != null) {
				for (int i = 0; i < spawnObject.model.Count; i++) {
					if (spawnObject.model [i].GetComponent<Animator> () != null) {
						DestroyImmediate (spawnObject.model [i].GetComponent<Animator> ());
					}
					Animation anim = spawnObject.model [i].GetComponent<Animation> ();
					if (anim == null) {
						anim = spawnObject.model [i].AddComponent<Animation> ();
					}
					if (spawnObject.model [i].AddComponent<BoxCollider> () != null) {
						spawnObject.model [i].AddComponent<BoxCollider> ();
					}
					if (spawnObject.model [i].AddComponent<MeshRenderer> () != null) {
						spawnObject.model [i].AddComponent<MeshRenderer> ();
					}
				}
			}
		}

		// remove all box and mesh collider
		if (GUILayout.Button ("RemoveAddTRyMeStuff")) {
			if (spawnObject.model != null && spawnObject.allowRemoveTRyMeStuff == true) {
				for (int i = 0; i < spawnObject.model.Count; i++) {
					if (spawnObject.model [i].GetComponent<BoxCollider> () != null) {
						DestroyImmediate (spawnObject.model [i].GetComponent<BoxCollider> (), true);
					}

					if (spawnObject.model [i].GetComponent<MeshRenderer> () != null) {
						DestroyImmediate (spawnObject.model [i].GetComponent<MeshRenderer> (), true);
					}

				}
			}
		}



		if (GUILayout.Button ("CheckCompleteList")) {
			spawnObject.checkWithCompleteList ();
		}
	}
		
}
#endif