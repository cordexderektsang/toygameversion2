﻿using UnityEngine;
using System.Collections;

public class SpawnConstructor
{
    public Vector3 minBound; // min bound for the next spawning object or next current object;
    public Vector3 maxBound;// max bound for the next spawning object or next current object;
    public Vector3 centerBound;// Center bound for the next spawning object or next current object;
    public GameObject currentModel;// current gameobject
    public float movingGapX;// half radius distance of an object
    public string name;// object name

    public SpawnConstructor(GameObject model)
    {
        currentModel = model;
        name = model.name.ToUpper();
      //  Renderer c = model.GetComponent<Renderer>();
        //BoxCollider c = model.GetComponent<BoxCollider> ();
       // minBound = c.bounds.min;
       // maxBound = c.bounds.max;
        //centerBound = c.bounds.center;
      //  movingGapX = Mathf.Abs(minBound.x) - Mathf.Abs(centerBound.x);
    }

    void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.gameObject.name);
    }
}
