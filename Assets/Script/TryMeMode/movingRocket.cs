﻿using UnityEngine;
using System.Collections;

public class movingRocket : MonoBehaviour
{
	bool isTurnOnce = false;// did rocket turn yet?
	public GameObject rocket; // the spawning rocket
	public GameObject rocketAlive ;// the instantiate rocket
	bool isStartFly = false;// not yet start fly
	float delayTimer = 0 ; //timer counter
	public GameObject StartButton; // start buttom
	public GameObject rocketBubble;
	bool isSpawnRocket = false;// did Rocket spawn yet?
	iTweenPath rocketPath;// the rocketpath
	int rocketAnimationCounter = 1;// animation sprite number
	float rocketSlowFrameCounter = 0;// counter
	float maxRocketSlowFrame = 0.3f; // the max timer
	float angle = 0.2f; // add in angle
	int numberOfBubble = 11;//max bubble to spawn
	int countOfBubble = 0;// counter
	bool IsUpDown = true; // move up and down
	float upDowntime = 0; // time for up and down
	GameObject BubbleAlive;//bubble

	void Start ()
	{
		rocketPath = rocket.GetComponent<iTweenPath> ();
		ResetRocket ();
		maxRocketSlowFrame = 0.3f;
	}

	void Update ()
	{
		if(rocketAlive == null) {
			return;
		}
		if (IsUpDown) {
			rocketAlive.transform.Translate (Vector2.up*0.002f);
			upDowntime += Time.deltaTime;
			if (upDowntime > 0.5) {
				IsUpDown = false;
				upDowntime = 0;
			}
		} else {
			rocketAlive.transform.Translate (Vector2.down*0.002f);
			upDowntime += Time.deltaTime;
			if (upDowntime > 0.5) {
				IsUpDown = true;
				upDowntime = 0;
			}
		}
	    // slow down the animation for rocket
		if (rocketSlowFrameCounter < maxRocketSlowFrame) {
			rocketSlowFrameCounter += Time.deltaTime;
		} else if (countOfBubble < numberOfBubble) {
			
			BubbleAlive = Instantiate (rocketBubble);//spawn rock
			BubbleAlive.transform.parent = transform;// make it as child of this game object
			BubbleAlive.transform.localScale = new Vector2 (0.22f, 0.22f); // scale smaller
			BubbleAlive.transform.position = new Vector3 (rocketAlive.transform.position.x + 0.37f, rocketAlive.transform.position.y, rocketAlive.transform.position.z);
			rocketSlowFrameCounter = 0;	
			if (isStartFly) {
			    maxRocketSlowFrame = 0.08f;
			    countOfBubble++;
			}
		}

		// rotate at node 2
		if (isStartFly && !isTurnOnce) {
			rocketAlive.transform.Rotate (new Vector3 (0, 0, angle));
			if(rocketAlive.transform.rotation.z >= 0.7f){ /// if max angle is 90 degree stop rotate
				isTurnOnce = true;
			}
		}

		// when user pressed start then start the flying timer
		if (isStartFly && delayTimer < 3f) {
			delayTimer += Time.deltaTime;
		} else if(isStartFly == true && delayTimer >= 3f) {// when done and go to menu
			Destroy (rocketAlive);
		}
    }
		
	/// <summary>
	/// user pressed start then start the rocket path
	/// </summary>
	public void fly ()
	{
		StartButton.SetActive (false);
		if (isStartFly == false) {
			iTween.MoveTo (rocketAlive, iTween.Hash ("path", iTweenPath.GetPath ("RocketPath"), "time", 3));
			isStartFly = true;
		}
	}
		
	/// <summary>
	/// Spawn and Resets the rocket.
	/// </summary>
	void ResetRocket ()
	{
		delayTimer = 0;// reset timer
		isStartFly = false;//reset the rocket position
		rocketAlive = Instantiate (rocket);//spawn rock
		rocketAlive.transform.parent = transform;// make it as child of  this game object
		rocketAlive.transform.localScale = new Vector2 (0.5f ,0.5f); // scale smaller
		rocketAlive.transform.position = new Vector3 (this.transform.position.x, this.transform.position.y, this.transform.position.z);// spawn at this position
		isSpawnRocket = true;// finish reset or spawn rocket
		StartButton.SetActive (true);/// tap for start
		countOfBubble = 0;
	}
}
