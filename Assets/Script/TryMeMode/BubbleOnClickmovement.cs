﻿using UnityEngine;
using System.Collections;

public class BubbleOnClickmovement : MonoBehaviour {

    float bubbleScaleTimer = 0; // scale bubble timer
    float maxBubbleScaleTimer = 0.02f; //timer 
    float startBubbleScaleSize = 0.2f; //start size for bubble
    public enum direction {DOWN , UP , NONE ,LEFT ,RIGHT ,CENTER };
    public  direction movement = direction.NONE;
    private bool isRotationStart = false;
    private float speed = 50;

    void Start()
    {
       /* transform.localScale = new Vector2(startBubbleScaleSize, startBubbleScaleSize); // scale smaller
        float RandomZrotation = Random.Range(0, 360);
        transform.eulerAngles = new Vector3(0, 0, 0);*/

    }

    // Update is called once per frame
    void Update()
    {
 
       if (bubbleScaleTimer < maxBubbleScaleTimer)
        {
            bubbleScaleTimer += Time.deltaTime;
        }
        else
        {
           if (!isRotationStart && movement != direction.CENTER)
            {
                StartCoroutine(Rotate(20));
                isRotationStart = true;
            }
      
          transform.localScale = new Vector2(startBubbleScaleSize, startBubbleScaleSize); // scale smaller
          startBubbleScaleSize -= 0.05f;
          bubbleScaleTimer = 0;
        }

        if (startBubbleScaleSize > 0)
        {
            if (movement == direction.UP)
            {
       
                transform.Translate(Vector2.up * Time.deltaTime); // move bubble
            }
            else if (movement == direction.DOWN)
            {
               transform.Translate(Vector2.down * Time.deltaTime); // move bubble
            } else if (movement == direction.RIGHT)
            {
                transform.Translate(Vector2.right * Time.deltaTime); // move bubble
            } else if (movement == direction.LEFT)
            {
                transform.Translate(Vector2.left * Time.deltaTime); // move bubble
            } else if (movement == direction.CENTER)
            {
				// move bubble
            }
        }
        else
        {
            Destroy(gameObject); // destroy it
        }
    }

    IEnumerator Rotate(float duration)
    {
        float startRotation = transform.eulerAngles.z;
        float endRotation = startRotation + 360.0f;
        float t = 0.0f;
        while (t <= duration)
        {
            if (t >= duration)
            {
              //  isRotationStart = true;
            }
            else
            {
                t += Time.deltaTime;
                float rotation = Mathf.Lerp(startRotation, endRotation, t / duration) % 360.0f;
				transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, rotation);
			
                yield return null;
            }
        }
    }
}
