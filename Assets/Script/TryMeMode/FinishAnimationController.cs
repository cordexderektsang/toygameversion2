﻿using UnityEngine;
using System.Collections;

public class FinishAnimationController : MonoBehaviour
{
    public GameObject MSG;
    private BaseTrymeController baseTrymeController;

    void Start()
    {
        baseTrymeController = GetComponent<BaseTrymeController>();
    }

    public void OnAnimationFinish()
    {
        MSG.SetActive(true);
    }

    public void OnMSGOKClick()
    {
        MSG.SetActive(false);
        baseTrymeController.resetAnimation();
    }
}
