﻿using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class BaseTrymeController : MonoBehaviour
{
    #region EDITOR_SETTINGS
    // Var of AR obj
    public GameObject arObjs;
    public ParticleSystem particle;
    public ARDemoTrackableEventHandler demoTrackableEventHandler; //marker
    public GameObject markerBG;

    public Animation tryMeMascotAnimation;
    [Header("Set Tutorial Pages Settings")]
    public List<TryMeState> listOfTryMeState; // a list of editoring state
    public int tryMeState = 0;// counter to track the audio state
    #endregion

    #region PRIVATE_VARIABLES
    private AlphabetCharacterController characterController;
    private AlphabetCoreSystem mSystem;
    private AudioSource clip;
    private FinishAnimationController finishAnimationController;
    private Renderer particleRenderer;

    // For letters
    private string lastAlphabet;
    private int lastAlphabetAscii;
    private bool isFirstClick = false;

    // For AR demo animation
    private bool IsPause = false;// do play pause
    private bool IsDelayTimerStartYet = false; // bool to call delay timer to ensure the next audio will play at the right time
    private float delayTime = 0;// counter for incrementation inside the delay function
    public List<ImageTargetBehaviour> currentITB;
    public GameObject imageMarkerObjects;
    #endregion

    public void Start()
    {
        mSystem = GetComponent<AlphabetCoreSystem>();
        mSystem.OnClick += OnMarkerClick;
        characterController = GetComponent<AlphabetCharacterController>();
        clip = GetComponent<AudioSource>();
        finishAnimationController = GetComponent<FinishAnimationController>();
        particleRenderer = particle.transform.GetComponent<Renderer>();
        setupAnimation();
    }

    // Use this for endup the scene
    void OnDestroy()
    {
        mSystem.OnClick -= OnMarkerClick;
    }

    public void setupAnimation()
    {
        tryMeState = 0; // reset audio state to 0
        delayTime = 0; // reset delay time
        IsDelayTimerStartYet = false; // stop delay
        characterController.setCharacterToMoveIn(); // hide the mascot during start
        TrymeAudioController.IsPlaying = false; // restart the audio to be false
        tryMeMascotAnimation.Stop(); // stop the animation and allow it to restart
    }

    public void resetAnimation()
    {
        setupAnimation();
        tryMeMascotAnimation.Play();
        isTrack(demoTrackableEventHandler.isTrack, null);
    }

    void OnMarkerClick(Transform marker, string input)
    {
        if(demoTrackableEventHandler.isTrack)
        {
            return;
        }

        OnHeaderAlphabetChange(input);
        particle.transform.parent = marker;
        particle.transform.localPosition = Vector3.zero;
        particle.transform.localScale = Vector3.one;
        var em = particle.emission;
        em.enabled = true;
        particle.transform.GetComponent<Renderer>().enabled = true;
        if (isFirstClick)
        {
            isFirstClick = false;
            particle.startSize *= marker.lossyScale.x;
            particle.gravityModifier *= marker.lossyScale.x;
        }
    }

    public void OnHeaderAlphabetChange(string input)
    {
        if (!isActiveAndEnabled || !gameObject.activeSelf)
        {
            return;
        }

        AlphabetCoreAudioSystem.instance.stopAll();

        try
        {
            // Save new to currnet
            lastAlphabet = input;
            lastAlphabetAscii = CCTools.Convert.StringToASCII(input);

            // Turn on new 3dObj
            // Auto read
            if (!string.IsNullOrEmpty(lastAlphabet))
            {
                AlphabetCoreAudioSystem.instance.stopAll();
                AlphabetCoreAudioSystem.instance.playOnce(lastAlphabet, AlphabetCoreConstants.AUDIO_Selections.LETTER_PRONUNCIATION);
            }

            //arObjs.transform.GetChild(lastAlphabetAscii - 97).gameObject.SetActive(true);
        }
        catch (System.Exception e) { print(e); }
    }

    public void Update ()
    {
        if (Input.GetMouseButtonUp(0) && !UICamera.isOverUI)//.hoveredObject.name == "UI Root"
        {
            RaycastHit hit;
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
            {
                AlphabetCoreClickableObjBehaviour _event = hit.transform.GetComponent<AlphabetCoreClickableObjBehaviour>();
                if (_event != null)
                {
                    _event.OnClick();
                }
            }
            else
            {
                try { Vuforia.CameraDevice.Instance.SetFocusMode(Vuforia.CameraDevice.FocusMode.FOCUS_MODE_TRIGGERAUTO); } catch { }
            }
        }

        ////if marker find unpause  and run the audio and animation else pause
        //if (isMarkerFind.isTrack)
        //{
        //    //if state not finish move the mascot out
        //    if(tryMeState < listOfTryMeState.Count)
        //    {
        //        characterController.setCharacterToMoveOut(listOfTryMeState[tryMeState].tagName, listOfTryMeState[tryMeState].imageName, listOfTryMeState[tryMeState].message);
        //    }
        //    else if (tryMeState >= listOfTryMeState.Count)
        //    {
        //        characterController.setCharacterToMoveIn();
        //        finishAnimationController.OnAnimationFinish();

        //    }
        //    // play audio once
        //    if (tryMeState < listOfTryMeState.Count && !TrymeAudioController.IsPlaying && !IsDelayTimerStartYet)
        //    {
        //        TrymeAudioController.instance.playOnce(listOfTryMeState[tryMeState].audioName);
        //        IsDelayTimerStartYet = true;
        //    }
          
        //    clip.UnPause();
        //    IsPause = false;
        //}
        //else
        //{ 
        //    clip.Pause();
        //    IsPause = true;
        //    characterController.setCharacterToMoveIn();// hide the mascot during start
        //}
   
        // if  audio is playing then start time 
        if (IsDelayTimerStartYet)
        {
           delay(TrymeAudioController.maxTime);
        }
    }

    public void isTrack(bool tracked, ImageTargetBehaviour itb)
    {

        if (!tracked)
        {
            markerBG.SetActive(true);
        }
        if (clip == null)
        {
            return;
        }

        //if marker find unpause and run the audio and animation else pause
        if (tracked)
        {

            markerBG.SetActive(false);
            //if state not finish move the mascot out
            if (tryMeState < listOfTryMeState.Count)
            {
                characterController.setCharacterToMoveOut(listOfTryMeState[tryMeState].tagName, listOfTryMeState[tryMeState].imageName, listOfTryMeState[tryMeState].message);
            }
            else if (tryMeState >= listOfTryMeState.Count)
            {
                characterController.setCharacterToMoveIn();
                finishAnimationController.OnAnimationFinish();

            }
            // play audio once
            if (tryMeState < listOfTryMeState.Count && !TrymeAudioController.IsPlaying && !IsDelayTimerStartYet)
            {
                TrymeAudioController.instance.playOnce(listOfTryMeState[tryMeState].audioName);
                IsDelayTimerStartYet = true;
            }

            clip.UnPause();
            IsPause = false;
            if (itb != null)
            {
                currentITB.Add(itb);
                updateMarkerObjectParent(true);
            }
        }
        else
        {
            clip.Pause();
            IsPause = true;
            characterController.setCharacterToMoveIn();// hide the mascot during start
            if (itb != null)
            {
                currentITB.Remove(itb);
                updateMarkerObjectParent(false);
            }
        }
    }

    private void updateMarkerObjectParent(bool addImageTarget)
    {
        Debug.Log("currentITB " + currentITB.Count + " " + addImageTarget);
        // scan 1 magic screen
        if (!addImageTarget)
        {
            if (currentITB.Count > 0)
            {
                demoTrackableEventHandler = currentITB[0].GetComponent<ARDemoTrackableEventHandler>();
                imageMarkerObjects.transform.SetParent(currentITB[0].transform);
                imageMarkerObjects.transform.localPosition = Vector3.zero;
                imageMarkerObjects.transform.localEulerAngles = Vector3.zero;
            }
        }
        else
        {
            if (currentITB.Count == 1)
            {
                demoTrackableEventHandler = currentITB[0].GetComponent<ARDemoTrackableEventHandler>();
                imageMarkerObjects.transform.SetParent(currentITB[0].transform);
                imageMarkerObjects.transform.localPosition = Vector3.zero;
                imageMarkerObjects.transform.localEulerAngles = Vector3.zero;
            }
        }
    }
    // start delay base on animation time
    private void delay(float MaxTimer)
    {
        if (!IsPause)
        {
            if (delayTime < MaxTimer)
            {
                delayTime += Time.deltaTime;
            }
            else
            {
                delayTime = 0;
                tryMeState++;// increment state to play next audio
                IsDelayTimerStartYet = false; //reset delay call for next audio
                TrymeAudioController.IsPlaying = false;// audio is now finish reset the state
                isTrack(demoTrackableEventHandler.isTrack, null);
            }
        }
    }

}