﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TrymeAudioController : MonoBehaviour 
{
	public List<AudioClip> clips_list = new List<AudioClip> ();


    public List<AudioClip> intro_clips_list = new List<AudioClip>();

    private static TrymeAudioController _obj;
	
	public static bool IsPlaying = false;

    public static float maxTime = 0;

    // dot instance to call class level method/function 
    public static TrymeAudioController instance
	{
		get
		{
			if(_obj == null)
			{
				_obj = GameObject.Find ("ARDemoController").GetComponent<TrymeAudioController> ();
			}
			
			return _obj;
		}
		set
		{
			_obj = value;
		}
	}
	
	public void playOnce (string name)
	{
		IsPlaying = true;

		AudioClip _clip = null;
        _clip = clips_list.Find(delegate (AudioClip clip) { return clip.name == name; });
        //_clip = clips_list.FindAll(delegate (AudioClip clip) {return clip.name == name;}) [0];
		GetComponent<AudioSource>().PlayOneShot (_clip);
        maxTime = _clip.length;
	}


	public void stopAll ()
	{
        GetComponent<AudioSource>().Stop();
        IsPlaying = false;
	}
}