﻿using UnityEngine;

public class CloubMovement : MonoBehaviour
{
    // TODO: (to Derek) set constants for float size
	public GameObject left;
	public GameObject right;
	bool isScale = false;

	// Update is called once per frame
	void Update ()
    {
		// cloud moving right
		if (gameObject.transform.position.x < right.transform.position.x) {
			if (!isScale) {
				gameObject.transform.localScale = new Vector3 (Random.Range (0.5f, 0.6f), Random.Range (0.2f, 0.3f)); //random scale, rangeForScale);
				isScale = true;
			}
			gameObject.transform.Translate (Vector2.right * Time.deltaTime*0.2f); //move right
		}
		else {
			//if (isScale) { //right
				//gameObject.transform.localScale = new Vector2 (Random.Range (0.5f, 0.6f), Random.Range (0.2f, 0.3f));
			isScale = false;
			//}
			gameObject.transform.position = new Vector3 (left.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z); //move left
		}

	}

}
