﻿using UnityEngine;
using System.Collections;

public class scrollNotification : MonoBehaviour {
    public string hitObjectName;
    public scrollViewFinalVersion scrollSet;
    public TouchControllor toucher;
    // Update is called once per frame
    void Update ()
    {
        if (toucher.hitGameObject() != null)
        {
        
            if (toucher.hitGameObject().name.Equals(hitObjectName))
            {
                scrollSet.isVerticalScroll = true;
            }
            else
            {
                scrollSet.isVerticalScroll = false;
            }
        }
	
	}
}
