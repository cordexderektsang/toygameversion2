﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ButtonController : MonoBehaviour {
	public List<UIButton> buttons;
//	public List<GameObject> shakeButtons;

	private UIButton clickedButton;

	// Update is called once per frame
	void Update () {
		for (int i = 0; i < buttons.Count; i++) 
		{
			
			if (buttons[i].state == UIButtonColor.State.Pressed && clickedButton == null) 
			{
				// shake animation
//				GameObject shakeBtn = shakeButtons.Find (gameObject => gameObject.Equals (buttons [i]));
//				if (shakeBtn != null) {
//					iTween.ShakeScale (shakeBtn, iTween.Hash ("x", 0.03f, "y", 0.03f, "time", 0.5f, "looptype", iTween.LoopType.none));
//					tweenColor.to = Color.grey;
//				}
			
				clickedButton = buttons [i];
				// to disable button
				setEnabled (false);

			}
			else if (clickedButton != null) 
			{
				if (clickedButton.state == UIButtonColor.State.Normal) 
				{
					clickedButton = null;
					// to enable all button
					setEnabled (true);
				}
			}
		}
	}

	private void setEnabled(bool isEnable) 
	{
		for (int i = 0; i < buttons.Count; i++) 
		{
			if (!buttons [i].Equals(clickedButton))
			{
				buttons[i].enabled = isEnable;
			}
		}
	}
}
