﻿using System.Collections;
using UnityEngine;
using System.Collections.Generic;
using Vuforia;
using UnityEngine.SceneManagement;

public class AlphabetGameMode1UISystem : MonoBehaviour 
{
    private enum BUTTONS
    {
        NONE, STROKE, WORDS, PHONICS
    }

	// Var of AR obj
	public GameObject arParentObj, arObjs;

	public Renderer blackboardRenderer;
	public Texture2D blankBlackboardTextureOriginal;
	public Texture2D blankBlackboardTexture;
	public Color[] txt_color;
	public /*TextMesh*/CCAlphabetStrokeAnimation letterTxt, letterStroke;
	public ParticleSystem particle;
	
	// Var of core system
	private AlphabetCoreSystem mSystem;

    public GameObject characterObject;
    private Animator characterObjectAnimator;

    private BUTTONS currentButtonPressed = BUTTONS.NONE;
    public GameObject buttonRoot;

    public Material[] strokeButtonMaterials;
    private Animator strokeButtonAnimator;
    private SkinnedMeshRenderer strokeButtonMeshRenderer;

    public Material[] wordsButtonMaterials;
    private Animator wordsButtonAnimator;
    private SkinnedMeshRenderer wordsButtonMeshRenderer;

    public Material[] phonicsButtonMaterials;
    private Animator phonicsButtonAnimator;
    private SkinnedMeshRenderer phonicsButtonMeshRenderer;

    public GameObject backButton;
    public BoxCollider backButtonCollider;
    private UIButton backButtonUI;
    public GameObject captureShareButton;
    private BoxCollider captureShareButtonCollider;
    private UIButton captureShareButtonUI;

    private bool is3dObjectPlaying;
    private bool is3dObjectFinishedPlaying;

    // Var of tutorial system
    private GameMode1TutorialController tutorialController;
    //private AlphabetTutorialBehaviour tutorialSystem;
	private bool isTutorialTime;
	
	// Var of load exist file
	private CCManualLoadResourceObj loader;

    // Var of this script
    private string currentSpelling;
    private string lastAlphabet;
	private int lastAlphabetAscii;
    private bool isFirstClick = true;
	private GameMode1CoreonTrackFunction gameModeTrackableScript;
	private bool isLetterMode = true;


    /*
     * ascii 97-122  =  a-z
     * ascii a-z  - 32 = A-Z
     * int ascii = CCTools.Convert.StringToASCII (input);
     * string str = CCTools.Convert.ASCIIToString (ascii);
     * 
     * Flow
     * init dict 
     * add word to list "a-z"
     * keep check first Alpabet change (have a callback)
     * show effect
     */
    #region Unity Life Cycle
    // Use this for initialization
    //void Start () 
    public void initialize()
    {
		//SceneManager.LoadScene("GameMode2", LoadSceneMode.Additive);
		gameModeTrackableScript = GetComponent<GameMode1CoreonTrackFunction>();
		mSystem = GetComponent <AlphabetCoreSystem> ();
		mSystem.initDict ();
		loader = new CCManualLoadResourceObj ();
		// mSystem.OnHeaderAlphabetChange += OnHeaderAlphabetChange;
		mSystem.OnClick += OnMarkerClick;
        mSystem.OnInputChange += OnInputChange;
        lastAlphabet = "";
		setActiveToArObject (false);
		
		AppController.ManualReleaseMemory ();

        characterObjectAnimator = characterObject.GetComponent<Animator>();

        GameObject strokeButton = buttonRoot.transform.FindChild("StrokeButton").gameObject;
        strokeButtonAnimator = strokeButton.GetComponent<Animator>();
        strokeButtonMeshRenderer = strokeButton.GetComponentInChildren<SkinnedMeshRenderer>();
        //strokeButtonMeshRenderer.material = strokeButtonMeshRenderer.materials[0];

        GameObject wordsButton = buttonRoot.transform.FindChild("WordsButton").gameObject;
        wordsButtonAnimator = wordsButton.GetComponent<Animator>();
        wordsButtonMeshRenderer = wordsButton.GetComponentInChildren<SkinnedMeshRenderer>();
        //wordsButtonMeshRenderer.material = wordsButtonMeshRenderer.materials[0];

        GameObject phonicsButton = buttonRoot.transform.FindChild("PhonicsButton").gameObject;
        phonicsButtonAnimator = phonicsButton.GetComponent<Animator>();
        phonicsButtonMeshRenderer = phonicsButton.GetComponentInChildren<SkinnedMeshRenderer>();
        //phonicsButtonMeshRenderer.material = phonicsButtonMeshRenderer.materials[0];

        tutorialController = GetComponent<GameMode1TutorialController>();
        //tutorialSystem = GetComponent <AlphabetTutorialBehaviour> ();
		//isTutorialTime = tutorialSystem.NeedTutorial;
        isTutorialTime = tutorialController.isNeedTutorial;
        //if (isTutorialTime) {
        //	tutorialSystem.onTutorialFinish += OnTutorialFinish;
        //	tutorialSystem.onTutorialSkipButtonClicked += OnTutorialFinish;
        //	mSystem.onStageMarkerFound += OnStageMarkerFound;
        //} 

        backButtonCollider = backButton.GetComponent<BoxCollider>();
        backButtonUI = backButton.GetComponent<UIButton>();

        captureShareButtonCollider = captureShareButton.GetComponent<BoxCollider>();
        captureShareButtonUI = captureShareButton.GetComponent<UIButton>();

        is3dObjectPlaying = false;
        is3dObjectFinishedPlaying = true;
    }
	
	// Use this for endup the scene
	void OnDestroy ()
	{
		letterStroke = null;
		loader = null;
		mSystem.OnClick -= OnMarkerClick;
        mSystem.OnInputChange -= OnInputChange;
        //mSystem.onStageMarkerFound = null;
        //tutorialSystem.onTutorialFinish = null;
        // mSystem.OnHeaderAlphabetChange -= OnHeaderAlphabetChange;
    }



    void Update ()
    {
		// Check if sound is finished playing to allow further clicking
        if (is3dObjectPlaying)
        {
            if (!is3dObjectFinishedPlaying && !AlphabetCoreAudioSystem.IsPlaying)
            {
                is3dObjectPlaying = false;
                is3dObjectFinishedPlaying = true;
                if (characterObjectAnimator.GetBool("isTimoSpeaking"))
                {
                    characterObjectAnimator.SetBool("isTimoSpeaking", false);
                }
                setButtonProperties(false);
            }
        }
        //disable interaction with other button durring capture
        if (AlphabetCoreSystem.capturing == true)
        {
            backButtonCollider.enabled = false;
            captureShareButtonCollider.enabled = false;
        }
        else if (backButtonCollider.enabled == false /*&& tutorialController.isTutorialFinished()*/)
        {
            backButtonCollider.enabled = true;
            captureShareButtonCollider.enabled = true;
        }
    }
	#endregion
	
	#region Private method
	//private void sendToTutorialSystem (int stageID, float delayTime = 1.5f)
	//{
	//	if (!isTutorialTime) 
	//		return;
		
	//	if (stageID == 0 && !tutorialSystem.isButtonClicked)
	//	{
	//		canStage0EventSendOut = false;
	//		tutorialSystem.onTutorialOkButtonClicked += OnTutorialOkButtonClick;
	//		return;
	//	}
		
	//	tutorialSystem.QueueCompleteStageEvent (stageID, delayTime);
	//}
	
	private void setActiveToArObject (bool value)
	{
		try
		{
			arParentObj.SetActive (value);
			letterTxt.resetToDefault ();
			Debug.Log("setActiveToArObject " + value);
		}
		catch {}	
	}

    public void resetBlackboard()
    {
       /* AlphabetCoreAudioSystem.instance.stopAll();

        // Disable blackboard objects
        setActiveToArObject(false);

        // Turn off alphabet character
        arObjs.transform.GetChild(lastAlphabetAscii - 97).gameObject.SetActive(false);

        // Reset blackboard texture
        blackboardRenderer.material.mainTexture = blankBlackboardTextureOriginal;

        ParticleSystem.EmissionModule em = particle.emission;
        em.enabled = false;

        isFirstClick = true;*/
    }
    #endregion

    public void disableLeftSideUIButtons()
    {
        backButtonCollider.enabled = false;
        backButtonUI.SetState(UIButtonColor.State.Disabled, false);

        captureShareButtonCollider.enabled = false;
        captureShareButtonUI.SetState(UIButtonColor.State.Disabled, false);
    }

    private void enableUIButtons()
    {
        backButtonCollider.enabled = true;
        backButtonUI.SetState(UIButtonColor.State.Normal, false);

        captureShareButtonCollider.enabled = true;
        captureShareButtonUI.SetState(UIButtonColor.State.Normal, false);
    }

    //public void enableTutorialNextButton()
    //{
    //    tutorialNextButtonCollider.enabled = true;
    //    tutorialNextButtonUI.SetState(UIButtonColor.State.Normal, false);
    //}

    #region Callback of AlphabetCoreSystem 
    //private bool canStage0EventSendOut = true;

    //void OnStageMarkerFound ()
    //{
    //	sendToTutorialSystem (0);
    //}

    //void OnTutorialOkButtonClick ()
    //{
    //	if (!canStage0EventSendOut && tutorialSystem.isTrackerFind)
    //	{
    //		canStage0EventSendOut = true;
    //		sendToTutorialSystem (0);
    //		tutorialSystem.onTutorialOkButtonClicked -= OnTutorialOkButtonClick;
    //	}
    //	else if (!tutorialSystem.isTrackerFind)
    //	{
    //		canStage0EventSendOut = false;
    //	}
    //}

    [Beebyte.Obfuscator.SkipRename]
    public void OnTutorialFinish ()
	{
		CCLog.SpecialMsg ("OnTutorialFinish");
		isTutorialTime = false;
        enableUIButtons();
        //tutorialSystem.onTutorialFinish = null;
    }


	void OnMarkerClick (Transform marker, string input)
	{
        if (isTutorialTime
            && (tutorialController.tutorialState != 1
            || tutorialController.tutorialState == 1 && (!tutorialController.isFadeInAudioPlayedOnce() || TutorialInstructionAudioController.IsPlaying)))
        {
            return;
        }

      
		OnHeaderAlphabetChange (input);
		marker.FindChild (input).GetComponent<Animation> ().Play ();
        //particle.transform.parent = marker;
		//particle.transform.localPosition = Vector3.zero;
		//particle.transform.localScale = Vector3.one;
		// particle.enableEmission = true;
		//var em = particle.emission;
		//em.enabled = true;
		//particle.transform.GetComponent<Renderer> ().enabled = true;
		if (isFirstClick)
		{
			isFirstClick = false;
			//particle.startSize *= marker.lossyScale.x;
			//particle.gravityModifier *= marker.lossyScale.x;
			
		//	blackboardRenderer.material.mainTexture = blankBlackboardTexture;
		}

        tutorialController.OnNextLetterClicked();
	}




    public void OnInputChange(string input)
    {
        currentSpelling = input;
        tutorialController.OnLetterScan();
    }

    public void OnHeaderAlphabetChange(string input)
    {

        if (!isActiveAndEnabled || !gameObject.activeSelf)
        {
            return;
        }
		gameModeTrackableScript.ResetAllMode();
        CCLog.SpecialMsg("Header Change to : " + input);

        AlphabetCoreAudioSystem.instance.stopAll();

        if (characterObjectAnimator.isInitialized && characterObjectAnimator.GetBool("isTimoSpeaking"))
        {
            characterObjectAnimator.SetBool("isTimoSpeaking", false);
        }

        // Turn on Ar Obj
        if (string.IsNullOrEmpty(input))
        {
            setActiveToArObject(false);
            return;
        }
        else if (!arParentObj.activeSelf)
        {
            setActiveToArObject(true);

        }

        // Reset audio list for playing multi clip
        AlphabetCoreAudioSystem.instance.resetLetterList();

        // As each part may work itself so use 2 try and catch 
        try
        {
            // Turn off showing 3dObj
            arObjs.transform.GetChild(lastAlphabetAscii - 97).gameObject.SetActive(false);
        }
        catch { }

        try
        {
            // Save new to currnet
            lastAlphabet = input;
            lastAlphabetAscii = CCTools.Convert.StringToASCII(input);

            // Set alphabet stroke
            letterStroke.setTextureToMaterial(loader.loadTexture("png/stroke/stroke " + lastAlphabet));

            // Set color and alphabet also animation
            letterTxt.resetToDefault();
            letterTxt.setColorToMaterial(txt_color[(lastAlphabetAscii - 97) % txt_color.Length]);
            letterTxt.setTextureToMaterial(loader.loadTexture("png/letter/" + lastAlphabet)/*txt_texture[(last_alphabet_ascii-97)]*/);

            //letter_txt.play (); // if auto play jsut uncomment it

            // Turn on new 3dObj
            // Auto read
            if (!string.IsNullOrEmpty(lastAlphabet))
            {
                //is3dObjectPlaying = true;
                //is3dObjectFinishedPlaying = false;
                AlphabetCoreAudioSystem.instance.stopAll();
                AlphabetCoreAudioSystem.instance.playOnce(lastAlphabet, AlphabetCoreConstants.AUDIO_Selections.LETTER_PRONUNCIATION);
            }

            arObjs.transform.GetChild(lastAlphabetAscii - 97).gameObject.SetActive(true);
        }
        catch (System.Exception e) { print(e); }
    }

    public bool hasSpelling()
    {
        if (string.IsNullOrEmpty(currentSpelling))
        {
            return false;
        }

        return true;
    }
    #endregion

    #region Callback of Obj click event
    public void On3dPhonicsClick ()
	{
        //if (string.IsNullOrEmpty(lastAlphabet))
        //{
        //    return;
        //}
       //if (check3dObjClick())
       //{
            is3dObjectPlaying = true;
            is3dObjectFinishedPlaying = false;
            AlphabetCoreAudioSystem.instance.stopAll();
            AlphabetCoreAudioSystem.instance.playOnce(lastAlphabet + " phonics" , AlphabetCoreConstants.AUDIO_Selections.LETTER_PHONICS);

            //phonicsButtonAnimator.SetBool("isButtonPressed", true);
         //   currentButtonPressed = BUTTONS.PHONICS;
          //  setButtonProperties(true);
            characterObjectAnimator.SetBool("isTimoSpeaking", true);
       //}
	}
	
	public void On3dLetterClick ()
	{
        //if (string.IsNullOrEmpty(lastAlphabet))
        //{
        //    return;
        //}
       //if (check3dObjClick())
       //{
            is3dObjectPlaying = true;
            is3dObjectFinishedPlaying = false;
            //gameModeAudioController.playOnce(AlphabetCoreConstants.AUDIO_CONTROLLER_TYPES.STROKES, lastAlphabet + " stroke");
            AlphabetCoreAudioSystem.instance.stopAll();
            AlphabetCoreAudioSystem.instance.playOnce(lastAlphabet + " stroke" , AlphabetCoreConstants.AUDIO_Selections.LETTER_STROKE);
            //strokeButtonAnimator.SetBool("isButtonPressed", true);
          //  currentButtonPressed = BUTTONS.STROKE;
          //  setButtonProperties(true);
            letterTxt.play();
        //}
    }

    public void On3dObjClick ()
	{
        //if (string.IsNullOrEmpty(lastAlphabet))
        //{
        //    return;
        //}
		Debug.Log(check3dObjClick());
       // if (check3dObjClick())
        //{
            is3dObjectPlaying = true;
            is3dObjectFinishedPlaying = false;
            AlphabetCoreAudioSystem.instance.stopAll();
			Debug.Log (" On3dObjClick ");
            AlphabetCoreAudioSystem.instance.playOnce(lastAlphabet, AlphabetCoreConstants.AUDIO_Selections.LETTER_PRONUNCIATION, false, true);
            //wordsButtonAnimator.SetBool("isButtonPressed", true);
           // currentButtonPressed = BUTTONS.WORDS;
           // setButtonProperties(true);
            arObjs.transform.GetChild(lastAlphabetAscii - 97).gameObject.GetComponent<Animation>().Play();
       //}
	}
	#endregion

    // Set boolean to transition button state to pressed
    private void setButtonProperties(bool isClick)
    {
        if (isClick)
        {
            switch (currentButtonPressed)
            {
                case BUTTONS.STROKE:
                    strokeButtonMeshRenderer.material = strokeButtonMaterials[1];
                    strokeButtonAnimator.SetBool("isButtonPressed", true);
                    break;

                case BUTTONS.WORDS:
                    wordsButtonMeshRenderer.material = wordsButtonMaterials[1];
                    wordsButtonAnimator.SetBool("isButtonPressed", true);
                    break;

                case BUTTONS.PHONICS:
                    phonicsButtonMeshRenderer.material = phonicsButtonMaterials[1];
                    phonicsButtonAnimator.SetBool("isButtonPressed", true);
                    break;
            }
        }
        else
        {
            switch (currentButtonPressed)
            {
                case BUTTONS.STROKE:
                    strokeButtonMeshRenderer.material = strokeButtonMaterials[0];
                    strokeButtonAnimator.SetBool("isButtonPressed", false);
                    //wordsButtonAnimator.SetTime(0);
                    //phonicsButtonAnimator.SetTime(0);
                    break;

                case BUTTONS.WORDS:
                    wordsButtonMeshRenderer.material = wordsButtonMaterials[0];
                    wordsButtonAnimator.SetBool("isButtonPressed", false);
                    //strokeButtonAnimator.SetTime(0);
                    //phonicsButtonAnimator.SetTime(0);
                    break;

                case BUTTONS.PHONICS:
                    phonicsButtonMeshRenderer.material = phonicsButtonMaterials[0];
                    phonicsButtonAnimator.SetBool("isButtonPressed", false);
                    //wordsButtonAnimator.SetTime(0);
                    //strokeButtonAnimator.SetTime(0);
                    break;
            }

            currentButtonPressed = BUTTONS.NONE;
        }
    }

    public bool check3dObjClick()
    {
		//Debug.Log (is3dObjectPlaying + " :  is empty" + string.IsNullOrEmpty(lastAlphabet) + "is play : " + AlphabetCoreAudioSystem.IsPlaying);
        if (string.IsNullOrEmpty(lastAlphabet) || is3dObjectPlaying)
        {
            return false;
        }
        return true;
    }
}