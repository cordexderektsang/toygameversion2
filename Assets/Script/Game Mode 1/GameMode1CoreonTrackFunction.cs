﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class GameMode1CoreonTrackFunction : MonoBehaviour 
{
	//game mode 1
	public GameObject arParent;
	public List <GameObject> planetParent;
	public List <ArBuddy> arBuddy;
	public List <Platform> platform;
	public GameObject blueLayout;
	public AlphabetGameMode1UISystemver2 gameMode1;
	public AlphabetCoreAudioSystem alphabetCoreAudioSystem;
	public Animator rotation;
	public List<TextData> textData ;
	public delegate void functionCall ();
	public GameObject spaceShip; 
	public UITexture aimTexture;
	public Color32 colourForRayhit;
	public ParticleSystem particle;

	private GameObject referenceCurrentRayHitLetter;
	private GameObject referenceCurrentDisplayLetter;
	public Color setColorForParticleWhenSelected;
	public Color setColorForParticleWhenUnSelected;
	private const string Timo_phonics = "Timo_phonics";
	private const string Letters = "3D Letters";
	private const string Stroke = "Stroke";
	private const string Text = "Text";
	private const string Introduction = "Introduction";
	private BuddyModeDataReference timoPhonics;
	private string currentGameMode = "";
	public TextMesh textDisplay;
	public TextMesh textDisplayForWord;
	public GameObject arrowDisplay;
	private string nextLine;
	private string introduction;

	private bool isReset = false;
	private float index = 0;
	private float maxIndex = 1.5f;
	private bool isArrowRequired = false;





	public void Start ()
	{
		var em = particle.emission;
		em.enabled = false;
		nextLine = System.Environment.NewLine;
		introduction = textDisplay.text;
	}

	//for reset
	public void TimerCount()
	{
		if (isReset)
		{
			if (index < maxIndex)
			{
				index += Time.deltaTime;
				//Debug.Log(index);
			}
			else {
				if (isArrowRequired)
				{
					arrowDisplay.SetActive(true);
					isArrowRequired = false;
			
				}

		
				textDisplay.gameObject.SetActive(true);
				isReset = false;
				index = 0;
			}
		}

	}




	public void Update()
	{
		TimerCount();

	}
	/// <summary>
	/// Resets all mode to introduction when change marker
	/// </summary>
	public void ResetAllMode ()
	{
		index = 0;
		textDisplay.gameObject.SetActive(false);
		foreach (ArBuddy mode in arBuddy) {
			mode.buddyModeObject.SetActive (false);
			//	mode.effect.SetActive (false);
			mode.buddyModeObject.transform.parent = arParent.transform;
			mode.buddyModeObject.transform.localPosition = Vector3.zero;
			mode.buddyModeObject.transform.localEulerAngles = Vector3.zero;
			mode.buddyModeObject.transform.localScale = new Vector3(1, 1, 1);
			mode.dragonBall.startColor = setColorForParticleWhenUnSelected;
			mode.dragonBall2.startColor = setColorForParticleWhenUnSelected;

		}
		textDisplay.gameObject.transform.localPosition = new Vector3(0.02519531f, 0.0045f, -0.007499999f);
		textDisplay.gameObject.transform.localEulerAngles = new Vector3(65.2051f, 180, 8.770321e-07f); ;
		textDisplay.text = introduction;
		RestPlatformFacing();
		isArrowRequired = true;
		isReset = true;

		//arrowDisplay.SetActive (true);
		//textDisplay.gameObject.transform.localPosition = new Vector3 (0.02519531f,0.0045f,-0.007499999f);
		//textDisplay.gameObject.transform.localEulerAngles = new Vector3 (65.2051f,180,8.770321e-07f);;
		//textDisplay.text = introduction;
		currentGameMode = "";


	}

	/// <summary>
	/// Turns the off all mode.
	/// </summary>
	public void TurnOffAllMode ()
	{
		foreach (ArBuddy mode in arBuddy) {
			mode.buddyModeObject.SetActive (false);
			//	mode.effect.SetActive (false);
			mode.dragonBall.startColor = setColorForParticleWhenUnSelected;
			mode.dragonBall2.startColor = setColorForParticleWhenUnSelected;
		}
		foreach (Platform data in platform)
		{
			data.isFaceUpToUser = false;
		}
		//arrowDisplay.SetActive (true);
		textDisplay.gameObject.transform.localPosition = new Vector3 (0.02519531f,0.0045f,-0.007499999f);
		textDisplay.gameObject.transform.localEulerAngles = new Vector3 (65.2051f,180,8.770321e-07f);;
		textDisplay.text = introduction;
		//currentGameMode = "";


	}
	/// <summary>
	/// put arParent parent with Letter 
	/// </summary>
	/// <param name="targetTranformParent">Target tranform parent.</param>
	public void OnArParentPositionUpdate (GameObject targetTranformParent)
	{	


		if (arParent != null) {
	
			arParent.transform.parent = targetTranformParent.transform;
			arParent.transform.localPosition = new Vector3 (0, 0.6f, 0);
			arParent.transform.localScale = new Vector3 (1, 1, 1);
			arParent.transform.localEulerAngles = new Vector3 (0,180,0);
	
			//targetTranformParent.SetActive (true);
			EnableRenderForArParnet ();
			spaceShip.SetActive (true);
		}
			
	}



	/// <summary>
	/// Enables the target mode with one object then executre fucntion when rotate finish
	/// change particle color
	/// return currnet mode object 
	/// </summary>
	/// <returns>The target mode object.</returns>
	/// <param name="name">target mode name.</param>
	/// <param name="callback">function.</param>
	public GameObject EnableTargetModeObject (string name, functionCall callback)
	{
		GameObject currentLetteModeObject = null;
		SetAnimatorForTimoPhonics (false);
		foreach (ArBuddy gameModeOne in arBuddy)
		{
			if (gameModeOne.buddyModeObject.name.Equals (name)) {
				currentLetteModeObject = gameModeOne.buddyModeObject;
				gameModeOne.dragonBall.gameObject.SetActive (false);
				gameModeOne.dragonBall2.gameObject.SetActive (false);
				gameModeOne.dragonBall.startColor = setColorForParticleWhenSelected;
				gameModeOne.dragonBall2.startColor = setColorForParticleWhenSelected;
				gameModeOne.dragonBall.gameObject.SetActive (true);
				gameModeOne.dragonBall2.gameObject.SetActive (true);
				StartCoroutine (waitForAnimationFinish (gameModeOne.buddyModeObject, callback));
			} else {

				gameModeOne.buddyModeObject.SetActive (false);
				gameModeOne.dragonBall.gameObject.SetActive (false);
				gameModeOne.dragonBall2.gameObject.SetActive (false);
				gameModeOne.dragonBall.startColor = setColorForParticleWhenUnSelected;
				gameModeOne.dragonBall2.startColor = setColorForParticleWhenUnSelected;
				gameModeOne.dragonBall.gameObject.SetActive (true);
				gameModeOne.dragonBall2.gameObject.SetActive (true);
			}

		}
		return currentLetteModeObject;

	}


	/// <summary>
	/// Sets the reference current display letter.
	/// </summary>
	/// <param name="displayTarget">Display target.</param>
	public void SetReferenceCurrentDisplayLetter (GameObject displayTarget)
	{
		referenceCurrentDisplayLetter = displayTarget;
	}

	// turn on and off animator player to prevent animator is not playable
	public void SetAnimatorForTimoPhonics(bool value)
	{

		if (timoPhonics == null) 
		{
			foreach (ArBuddy gameModeOne in arBuddy) 
			{
				if (gameModeOne.buddyModeObject.name.Equals (Timo_phonics)) 
				{
					timoPhonics = gameModeOne.buddyModeObject.GetComponent<BuddyModeDataReference> ();
				} 

			}
		} 
		timoPhonics.Target_Timo_phonics_only (value);	

	}

	/// <summary>
	/// Enables the target mode with two objects , set up current platform  position and parent then executre fucntion when rotate finish.
	/// change particle color
	/// return currnet mode object 
	/// </summary>
	/// <returns>The two target mode object.</returns>
	/// <param name="name">Name.</param>
	/// <param name="nameSecond">Name second.</param>
	/// <param name="platform">Platform.</param>
	/// <param name="callback">Callback.</param>
	public void EnableTwoTargetModeObject (string name ,string nameSecond , GameObject platform, functionCall callback)
	{
		SetAnimatorForTimoPhonics (false);
		//TurnOffAllMode ();
		foreach (ArBuddy gameModeOne in arBuddy)
		{
			if (gameModeOne.buddyModeObject.name.Equals (name) || gameModeOne.buddyModeObject.name.Equals (nameSecond)) {
				gameModeOne.dragonBall.gameObject.SetActive (false);
				gameModeOne.dragonBall2.gameObject.SetActive (false);
				gameModeOne.dragonBall.startColor = setColorForParticleWhenSelected;
				gameModeOne.dragonBall2.startColor = setColorForParticleWhenSelected;
				gameModeOne.dragonBall.gameObject.SetActive (true);
				gameModeOne.dragonBall2.gameObject.SetActive (true);
				gameModeOne.buddyModeObject.transform.parent = platform.transform;
				gameModeOne.buddyModeDataReference.SetUpPosition (platform.name);
				StartCoroutine (waitForAnimationFinish (gameModeOne.buddyModeObject, callback));
			} else {

				gameModeOne.dragonBall.startColor = setColorForParticleWhenUnSelected;
				gameModeOne.dragonBall2.startColor = setColorForParticleWhenUnSelected;
				gameModeOne.buddyModeObject.SetActive(false);
			}
		}



	}


	/// <summary>
	/// is there any different from previous mode (no differen on click call)
	///  on ray hit phonic Call
	/// put tim on the next platform
	/// enable vision for tim
	/// allow rotation animation
	/// set up text position
	/// </summary>
	public void OnEnablePhonics ()
	{
		//Debug.Log ("OnEnablePhonics on click : " + gameMode1.check3dObjClick ());
		if (gameMode1.check3dObjClick ()) {
			Debug.Log ("OnEnablePhonics");
			index = 0;
			arrowDisplay.SetActive (false);
			if (IsCurrentModeDifferentToPreviousMode (Timo_phonics)) {
				GameObject modeObject = EnableTargetModeObject (Timo_phonics, gameMode1.On3dPhonicsClick);
				textDisplay.gameObject.SetActive(false);
				foreach (Platform data in platform) {

					if (!data.isFaceUpToUser) {
						AnimationRotation ();
						//GameObject modeObject = EnableTargetModeObject (Timo_phonics, gameMode1.On3dPhonicsClick);
						Debug.Log ("enable phonics");
						modeObject.transform.parent = data.platform.transform;
						modeObject.GetComponent<BuddyModeDataReference> ().SetUpPosition (data.platform.name);
						TextData thisModeText = textForCurrentMode (Timo_phonics);
						textDisplay.gameObject.transform.localPosition = new Vector3 (0.0247f, 0.00871f, -0.0153f);
						textDisplay.gameObject.transform.localEulerAngles = new Vector3 (65.2051f, 180, 0);
						//Debug.Log (referenceCurrentDisplayLetter);
						string currentLetter = referenceCurrentDisplayLetter.transform.GetChild (0).name.ToUpper ();
						textDisplay.text = currentLetter + " " + thisModeText.textDataEnglish + nextLine + currentLetter + " " + thisModeText.textDataChinese;
						data.isFaceUpToUser = true;

					} else {
						data.isFaceUpToUser = false;
					}
				}
			} else {
				//Debug.Log ("on click phonics");
				gameMode1.On3dPhonicsClick ();
			}
	
		}
	}

	/// <summary>
	/// is there any different from previous mode (no differen on click call)
	///  on ray hit 3d Letter
	/// put 3d letter on the next platform
	/// enable vision for 3d letter
	/// allow rotation animation
	/// set up text position
	/// wait till get current word from audio
	/// </summary>
		public void OnEnable3DLetter ()
		{
			Debug.Log ("on3DLetter on click : " + gameMode1.check3dObjClick ());
			if(gameMode1.check3dObjClick()){

				Debug.Log ("on3DLetter");
				arrowDisplay.SetActive (false);
			if (IsCurrentModeDifferentToPreviousMode (Letters)) {
				GameObject modeObject = EnableTargetModeObject (Letters, gameMode1.On3dObjClick);
				textDisplay.gameObject.SetActive(false);
				foreach (Platform data in platform) {

					if (!data.isFaceUpToUser) {
						AnimationRotation ();
						modeObject.transform.parent = data.platform.transform;
						modeObject.GetComponent<BuddyModeDataReference> ().SetUpPosition (data.platform.name);
						data.isFaceUpToUser = true;
						TextData thisModeText = textForCurrentMode (Letters);
						textDisplay.gameObject.transform.localEulerAngles = new Vector3 (65.2051f,180,0);
						string currentLetter = referenceCurrentDisplayLetter.transform.GetChild (0).name.ToUpper();;
						textDisplay.transform.localPosition = new Vector3 (0.0059f, 0.0092f,-0.0157f);
						textDisplay.text = currentLetter + " " + thisModeText.textDataEnglish + nextLine + currentLetter + " " + thisModeText.textDataChinese;

					} else {
						data.isFaceUpToUser = false;
					}

				}
			} else {
				Debug.Log ("on click object");
				gameMode1.On3dObjClick ();

			}

			StartCoroutine (waitForClipNamePick());
			}
		}


	/// <summary>
	/// Waits for clip name pick from audio.
	/// set text to audio name
	/// </summary>
	/// <returns>The for clip name pick.</returns>
	private IEnumerator waitForClipNamePick (){
		while (true){
			string diplayLetter = "";
			yield return new WaitForSeconds (0.1f);
			if (!string.IsNullOrEmpty (alphabetCoreAudioSystem.pickedClipName)) {
			//	Debug.Log (" text word " + alphabetCoreAudioSystem.pickedClipName);
				char[] charater = alphabetCoreAudioSystem.pickedClipName.ToCharArray();
				for(int i= 1; i < charater.Length ; i ++ )
				{
					diplayLetter += charater [i];
				}
				textDisplayForWord.text = diplayLetter;
				textDisplayForWord.gameObject.SetActive (true);
				alphabetCoreAudioSystem.pickedClipName = "";
				break;
			} 
		
		}

	}


	/// <summary>
	/// is there any different from previous mode (no differen on click call)
	///  on ray hit for stroke and text
	/// put stroke and text on the next platform
	/// enable vision for stroke and text
	/// allow rotation animation
	/// set up text position
	/// </summary>
	public void OnEnableTextAndStroke () 
	{
		Debug.Log ("OnEnableTextAndStroke on click : " + gameMode1.check3dObjClick ());
		if (gameMode1.check3dObjClick ()) {

			Debug.Log ("OnEnableTextAndStroke");
			arrowDisplay.SetActive (false);
			if (IsCurrentModeDifferentToPreviousMode (Stroke)) {
				textDisplay.gameObject.SetActive(false);
				foreach (Platform data in platform) {
					Debug.Log(data.platform.gameObject.name + " : "+ data.isFaceUpToUser);
					if (!data.isFaceUpToUser) {
						EnableTwoTargetModeObject (Stroke, Text, data.platform, gameMode1.On3dLetterClick);
						AnimationRotation ();
						Debug.Log ("enable text");
						data.isFaceUpToUser = true;
						TextData thisModeText = textForCurrentMode (Stroke);
						textDisplay.gameObject.transform.localPosition = new Vector3 (0.0247f, 0.012f, -0.0128f);
						textDisplay.gameObject.transform.localEulerAngles = new Vector3 (65f, 180, 0);
						textDisplay.text = thisModeText.textDataEnglish + nextLine + thisModeText.textDataChinese;
	

					} else {
						data.isFaceUpToUser = false;
					}

				}
			} else {
				Debug.Log ("on click Letter");
				gameMode1.On3dLetterClick ();

			}
		}

	}


	/// <summary>
	/// Waits for roation animation finish.then play audio and rest of animation
	/// </summary>
	/// <returns>The for animation finish.</returns>
	/// <param name="targetObject">Target object.</param>
	/// <param name="callback">Callback.</param>
	public IEnumerator waitForAnimationFinish (GameObject targetObject,functionCall callback){
		while (true) {
			yield return new WaitForSeconds (1.5f);
			targetObject.SetActive (true);
			if(targetObject.name.Equals (Timo_phonics))
			{
				yield return new WaitForSeconds (0.01f);
				Debug.Log (targetObject.name);
				SetAnimatorForTimoPhonics (true);
			
			}
			textDisplay.gameObject.SetActive (true);
			callback ();
			break;
		}

	}

	/// <summary>
	/// Animations the rotation.
	/// </summary>
	public void AnimationRotation ()
	{
		textDisplay.gameObject.SetActive (false);
		textDisplayForWord.gameObject.SetActive (false);
		bool isDownward = rotation.GetBool ("Downward");
		Debug.Log("isDownward : " + isDownward);
		if (isDownward) {
			rotation.SetBool ("Downward", false);
			rotation.SetBool ("Upward", true);

		} else {

			rotation.SetBool ("Downward", true);
			rotation.SetBool ("Upward", false);
		
		}
	}

	/// <summary>
	/// Determines whether this instance is current mode different to previous mode the specified mode.
	/// </summary>
	/// <returns><c>true</c> if this instance is current mode different to previous mode the specified mode; otherwise, <c>false</c>.</returns>
	/// <param name="mode">Mode.</param>
	public bool IsCurrentModeDifferentToPreviousMode (string mode)
	{
		if (string.IsNullOrEmpty (currentGameMode)) {
			currentGameMode = mode;
			return true;
		} else if(!currentGameMode.Equals(mode)) {
			currentGameMode = mode;
			return true;
		}
		return false;
	}

	/// <summary>
	/// Texts for current mode.
	/// </summary>
	/// <returns>The for current mode.</returns>
	/// <param name="mode">Mode.</param>
	public TextData textForCurrentMode (string mode)
	{

		foreach (TextData data in textData)
		{
			if (mode.Equals (data.mode)) 
			{
				return data;
			}

		}
		return null;
	}

	/// <summary>
	/// Returns the current mode.
	/// </summary>
	/// <returns>The current mode.</returns>
	public string ReturnCurrentMode () 
	{
		return currentGameMode;
	}


	/// <summary>
	/// Returns the current on ray hit letter.
	/// </summary>
	/// <returns>The current on ray hit letter.</returns>
	public GameObject returnCurrentOnRayHitLetter ()
	{
		return referenceCurrentRayHitLetter;
	}

	public void setCurrentOnTrackLetter(GameObject currentLetter , bool requireBlueLayOut = false)
	{
		if (blueLayout != null  && requireBlueLayOut)
		{
			referenceCurrentRayHitLetter = currentLetter; 
			blueLayout.transform.parent = currentLetter.transform;
			blueLayout.transform.localPosition = new Vector3 (0, 0, 0);
			blueLayout.transform.localScale = new Vector3 (0.1f, 0.1f, 0.1f);
			blueLayout.transform.localEulerAngles = Vector3.zero;
			particle.transform.parent = currentLetter.transform;
			particle.transform.localPosition = Vector3.zero;
			particle.transform.localScale = Vector3.one;
			blueLayout.SetActive (true);
			EnableRenderForBlueOutlay ();
		}
		aimTexture.color = colourForRayhit;
		if(!requireBlueLayOut){
			var em = particle.emission;
			if (em.enabled) {
				//Debug.Log ("off 1");
				em.enabled = false;
				blueLayout.SetActive (false);
			}
		}

	}

	/// <summary>
	/// Resets the current letter.
	/// </summary>
	public void resetCurrentLetter ()
	{
		//Debug.Log ("reset");
		//referenceCurrentLetter = null;
		//DisableRenderForBlueOutlay ();\
		var em = particle.emission;
		if (em.enabled) {
//			Debug.Log ("reset 2");
			em.enabled = false;
			blueLayout.SetActive (false);
		}
		aimTexture.color = Color.white;

	}


	public void OnOffSpaceShip (bool status)
	{

		spaceShip.SetActive (status);

	}

	public void RestPlatformFacing()
	{

		foreach (Platform data in platform)
		{

			if (data.platform.name.Equals("platform_A"))
			{

				data.isFaceUpToUser = true;
			}
			else {
				data.isFaceUpToUser = false;

			}

		}


		bool isDownward = rotation.GetBool("Downward");
		if (isDownward)
		{
			rotation.SetBool("Downward", false);
			rotation.SetBool("Upward", true);

		}
		/*else {

			rotation.SetBool("Downward", true);
			rotation.SetBool("Upward", false);

		}*/
	

	}




	/// <summary>
	/// Enables the render for blue outlay.
	/// </summary>
	public  void EnableRenderForBlueOutlay ()
	{
		//Debug.Log ("enable render");
		Renderer[] rendererComponents = blueLayout.GetComponentsInChildren<Renderer>(true);

		// Enable rendering:
		foreach (Renderer component in rendererComponents)
		{
			component.enabled = true;
		}
	
		var em = particle.emission;
		if (!em.enabled) {
			//Debug.Log ("layout on");
			em.enabled = true;
		}
		//Debug.Log ("render on" + em.enabled);
	}

	/// <summary>
	/// Enables the render for ar parnet.
	/// </summary>
	public  void EnableRenderForArParnet()
	{
		//Debug.Log ("enable renderAR");
		Renderer[] rendererComponents = arParent.GetComponentsInChildren<Renderer>(true);
		Collider[] colliderComponents = arParent.GetComponentsInChildren<Collider> (true);
		// Enable rendering:
		foreach (Renderer component in rendererComponents)
		{
			component.enabled = true;
		}
		foreach (Collider component in colliderComponents)
		{
			component.enabled = true;
		}

	}

	/// <summary>
	/// Disables the render for ar parnet.
	/// </summary>
	public  void DisableRenderForArParnet ()
	{
		//Debug.Log ("Disble both");
		Renderer[] rendererComponents = arParent.GetComponentsInChildren<Renderer>(true);
		Collider[] colliderComponents = arParent.GetComponentsInChildren<Collider> (true);
		// Enable rendering:
		foreach (Renderer component in rendererComponents)
		{
			component.enabled = false;
		}
		foreach (Collider component in colliderComponents)
		{
			component.enabled = false;
		}
	}

	/// <summary>
	/// Disables the render for blue outlay.
	/// </summary>
	public void DisableRenderForBlueOutlay ()
	{
	//	Debug.Log ("Disble layout");
		Renderer[] rendererComponents = blueLayout.GetComponentsInChildren<Renderer>(true);
		// Enable rendering:
		foreach (Renderer component in rendererComponents)
		{
			component.enabled = false;
		}
		var em = particle.emission;
		if (em.enabled) {
			//Debug.Log ("layout off");
			em.enabled = false;
		}
	}
}

[System.Serializable]
public class Platform {

	public GameObject platform;
	public bool isFaceUpToUser;

}

[System.Serializable]
public class ArBuddy {

	public GameObject buddyModeObject;
	public GameObject effect;
	public ParticleSystem dragonBall;
	public ParticleSystem dragonBall2;
	public BuddyModeDataReference buddyModeDataReference;

}


[System.Serializable]
public class TextData {

	public string textDataChinese;
	public string textDataEnglish;
	public string mode;

}


