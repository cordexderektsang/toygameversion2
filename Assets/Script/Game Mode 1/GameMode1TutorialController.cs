﻿using UnityEngine;
using System.Collections.Generic;

public class GameMode1TutorialController : BaseTutorialController
{
    // Var of core system
    private AlphabetCoreSystem mSystem;

    private bool isTutorialStatePassed = false;

    // Var of instruction system

    // instruction 1: bool
    private bool isLetterScanned = false;

    // instruction 5: checking var
    private bool letterClicked = false;
    private bool phonicsClicked = false;
    private bool objClicked = false;
    private bool isClickAnotherLetter = false;

    private bool isInstructionStatePassed = false;
    private float endInstructionTimer = 0.0f;

    // ui system
    private AlphabetGameMode1UISystemver2 gameModeSystem;

    void OnDestroy()
    {
        mSystem.OnClick -= OnMarkerClick;
    }

    #region SUPERCLASS_FUNCTION
    public override void setupVariablesAndConstants()
    {
        gameModeNumber = 1;
        mSystem = GetComponent<AlphabetCoreSystem>();
        gameModeSystem = GetComponent<AlphabetGameMode1UISystemver2>();
        gameModeSystem.initialize();
    }

    public override void setupGameModeTutorial()
    {
        
    }

    public override void setupGameModeInstructions()
    {
        mSystem.OnClick += OnMarkerClick;
    }

    // tutorial state update
    public override void checkTutorialState()
    {
        // Don't process any logic until voice finishes talking
        if (TutorialInstructionAudioController.IsPlaying)
        {
            return;
        }

        if (tutorialState == 0)
        {
            if (magicScreen.isTrackerFind || magicScreen2.isTrackerFind)
            {
                playTutorialInstructionAudio(ENCOURAGEMENT + (tutorialState + 1));
                nextTutorialAnimation();
            }
        }
        else if (tutorialState == 1)
        {
            if (isTutorialStatePassed)
            {
                isTutorialStatePassed = false;
                playTutorialInstructionAudio(ENCOURAGEMENT + (tutorialState + 1));
                nextTutorialAnimation();
                gameModeSystem.disableLeftSideUIButtons();
            }
        }
        else if (tutorialState == 2)
        {
            characterSystem.setCharacterToMoveIn();
            checkEnableTutorialNextButton();
            onTutorialNextButtonClick();
        }
    }

    public override void onInstructionStart()
    {
        if (isInstructionStart)
        {
            return;
        }

        // Setup timer but dont set max time yet to prevent clock from running
        //CCTimerSuperClass.SetupTimer(0, 0, maxTimeReachCount, timeIncrement, glassCeiling);
        timerSystem.setTimerActive(true);
        setNextInstructionState(2);
        isInstructionStart = true;
    }

    // instruction state update
    public override void checkInstructionState()
    {
        // instruction 1: scan one marker
        if (instructionState == 0)
        {
            if (isLetterScanned)
            {
                setNextInstructionState();
            }
        }
        // instruction 2: word marker clicked
        else if (instructionState == 1)
        {
            if (isInstructionStatePassed)
            {
                isInstructionStatePassed = false;
                setNextInstructionState();
            }
        }
        // instruction 3: scan magic screen
        else if (instructionState == 2)
        {
            if (magicScreen.isTrackerFind || magicScreen2.isTrackerFind)
            {
                setNextInstructionState();
            }
        }
        // instruction 4: check to press all 3 blackboard things: object, letter and phonics
        else if (instructionState == 3)
        {
            if (objClicked && letterClicked && phonicsClicked)
            {
                setNextInstructionState();
            }
        }
        // instruction 5: no action
        else if (instructionState == 4)
        {
            if (isClickAnotherLetter)
            {
                isClickAnotherLetter = false;
                setNextInstructionState(2);
            }
            else if (!gameModeSystem.hasSpelling() && (!magicScreen.isTrackerFind && !magicScreen2.isTrackerFind))
            {
                endInstructionTimer += Time.deltaTime;

                if (endInstructionTimer >= endInstructionResetTime)
                {
                    gameModeSystem.resetBlackboard();
                    ResetClickBooleans();
                    resetAllInstructions();
                }
            }
            else if (endInstructionTimer > 0)
            {
                endInstructionTimer = 0.0f;
            }
        }
    }

    public override void onTutorialEnd()
    {
        if (isTutorialDone)
        {
            return;
        }

        isClickFinishButton = true;
    }
    #endregion

    #region CONDITION
    // instruction condition

    [Beebyte.Obfuscator.SkipRename]
    public void OnLetterScan()
    {
        //Debug.Log("OnLetterScan ");
        // instruction 1 letter scanned;
        if (instructionState == 0 && !isLetterScanned)
        {
            isLetterScanned = true;
        }
    }

    // phonics clicked
    [Beebyte.Obfuscator.SkipRename]
    public void On3dPhonicsClick()
    {
        //Debug.Log("On3dPhonicsClick ");
        resetInstructionState();
        // instruction 4 Phonics or Letter or 3dObj Clicked;
        if (instructionState == 3 && !phonicsClicked)
        {
            if (!phonicsClicked)
            {
                phonicsClicked = true;
            }
        }
    }

    /// <summary>
    // 3d letter clicked
    /// </summary>
    [Beebyte.Obfuscator.SkipRename]
    public void On3dLetterClick()
    {
        //Debug.Log("On3dLetterClick ");
        resetInstructionState();
        // instruction 4 Phonics or Letter or 3dObj Clicked;
        if (instructionState == 3)
        {
            if (!letterClicked)
            {
                letterClicked = true;
            }
        }
    }

    /// <summary>
    // 3d obj on the blackboard clicked
    /// </summary>
    [Beebyte.Obfuscator.SkipRename]
    public void On3dObjClick()
    {
        //Debug.Log("On3dObjClick ");
        resetInstructionState();
        // instruction 4 Phonics or Letter or 3dObj Clicked;
        if (instructionState == 3)
        {
            if (!objClicked)
            {
                objClicked = true;
            }
        }
    }

    /// <summary>
    // on words marker clicked
    /// </summary>
    [Beebyte.Obfuscator.SkipRename]
    void OnMarkerClick(Transform marker, string input)
    {
        if (!isTutorialDone
            && (tutorialState != 1
            || tutorialState == 1 && (!isFadeInAudioPlayedOnce() || TutorialInstructionAudioController.IsPlaying)))
        {
            return;
        }

        //Debug.Log("OnMarkerClick " +  input);
        // instruction 2: word Marker Clicked;
        if (instructionState == 1 && !isInstructionStatePassed)
        {
            isInstructionStatePassed = true;
        }
        // tutorial 2: Marker Clicked;
        else if (tutorialState == 1 && !isTutorialStatePassed)
        {
            isTutorialStatePassed = true;
        }

        ResetClickBooleans();
    }
    #endregion

    /// <summary>
    // Reset all clicked boolean variables
    /// </summary>
    private void ResetClickBooleans()
    {
        isLetterScanned = false;

        letterClicked = false;
        objClicked = false;
        phonicsClicked = false;
    }

    /// <summary>
    /// set current page fade out and next page fade in
    /// </summary>
    private void nextTutorialAnimation()
    {
        isFadeOut = true;
    }

    public void OnNextLetterClicked()
    {
        if (instructionState == 4 && !isClickAnotherLetter)
        {
            isClickAnotherLetter = true;
        }
    }
}
