﻿using UnityEngine;
using System.Collections;

public class BuddyModeDataReference : MonoBehaviour 
{
	public Vector3 positionPlatformA;
	public Vector3 rotationPlatformA;
	public Vector3 scalePlatformA;
	public Vector3 positionPlatformB;
	public Vector3 rotationPlatformB;
	public Vector3 scalePlatformB;
	public const string PlatformAName = "platform_A";
	public const string PlatformBName = "platform_B";
	public Animator animatorForTimo;
	// Use this for initialization
	private void Start ()
	{
		if (this.GetComponent<Animator> () != null) {

			animatorForTimo = GetComponent<Animator> ();
		}
	}

	public void SetUpPosition (string platformName)
	{
		if (platformName.Equals (PlatformAName)) 
		{
			this.gameObject.transform.localScale = new Vector3 (scalePlatformA.x,scalePlatformA.y,scalePlatformA.z);
			this.gameObject.transform.localPosition = new Vector3 (positionPlatformA.x,positionPlatformA.y,positionPlatformA.z);
			this.gameObject.transform.localEulerAngles = new Vector3 (rotationPlatformA.x,rotationPlatformA.y,rotationPlatformA.z);

		} else if (platformName.Equals (PlatformBName))
		{

			this.gameObject.transform.localScale = new Vector3 (scalePlatformB.x,scalePlatformB.y,scalePlatformB.z);
			this.gameObject.transform.localPosition = new Vector3 (positionPlatformB.x,positionPlatformB.y,positionPlatformB.z);
			this.gameObject.transform.localEulerAngles = new Vector3 (rotationPlatformB.x,rotationPlatformB.y,rotationPlatformB.z);
		}


	}

	public void Target_Timo_phonics_only (bool value) 
	{
		if(animatorForTimo != null)
		{
			Debug.Log ("enable timo animator");
			animatorForTimo.enabled = value;
		}

	}

}
