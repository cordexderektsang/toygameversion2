using UnityEngine;
using System.Collections;

public class AlphabetIDMarkerBehaviour : AlphabetCoreClickableObjBehaviour 
{
	private BoxCollider _collider;
	
	private void Start () {
		
		_collider = GetComponent <BoxCollider> ();
	}
	
	public override void OnClick ()
	{
		setSelfEnable (false);
	}
	
	public void setSelfEnable (bool value = true) 
	{
		_collider.enabled = value;
	}
}
