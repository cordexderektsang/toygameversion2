﻿using System.Collections;
using UnityEngine;
using System.Collections.Generic;
using Vuforia;
using UnityEngine.SceneManagement;

public class AlphabetGameMode1UISystemver2 : MonoBehaviour 
{
    private enum BUTTONS
    {
        NONE, STROKE, WORDS, PHONICS
    }

	// Var of AR obj
	public GameObject arParentObj, arObjs;

	public Renderer blackboardRenderer;
	public Texture2D blankBlackboardTextureOriginal;
	public Texture2D blankBlackboardTexture;
	public Color[] txt_color;
	public /*TextMesh*/CCAlphabetStrokeAnimation letterTxt, letterStroke;
	public ParticleSystem particle;
	
	// Var of core system
	public AlphabetCoreSystem mSystem;

    public GameObject characterObject;
    private Animator characterObjectAnimator;

    private BUTTONS currentButtonPressed = BUTTONS.NONE;
    public GameObject buttonRoot;

    public Material[] strokeButtonMaterials;
    private Animator strokeButtonAnimator;
    private SkinnedMeshRenderer strokeButtonMeshRenderer;

    public Material[] wordsButtonMaterials;
    private Animator wordsButtonAnimator;
    private SkinnedMeshRenderer wordsButtonMeshRenderer;

    public Material[] phonicsButtonMaterials;
    private Animator phonicsButtonAnimator;
    private SkinnedMeshRenderer phonicsButtonMeshRenderer;

    public GameObject backButton;
    public BoxCollider backButtonCollider;
    private UIButton backButtonUI;
    public GameObject captureShareButton;
    private BoxCollider captureShareButtonCollider;
    private UIButton captureShareButtonUI;

    private bool is3dObjectPlaying;
    private bool is3dObjectFinishedPlaying;

    // Var of tutorial system
   // private GameMode1TutorialController tutorialController;
    //private AlphabetTutorialBehaviour tutorialSystem;
	//private bool isTutorialTime;
	
	// Var of load exist file
	private CCManualLoadResourceObj loader;

    // Var of this script
    private string currentSpelling;
    private string lastAlphabet;
	private int lastAlphabetAscii;
    //private bool isFirstClick = true;
	private GameMode1CoreonTrackFunction gameModeTrackableScript;
	public static List<AlphabetCoreTrackableEventHandler> TrackableMarkers = new List<AlphabetCoreTrackableEventHandler>();
	private bool isLetterMode = false;
	private bool isWordMode = false;
	private string currnetLetter = "";


	// game mode 2
	public List<Texture> listofTextureName = new List<Texture>();
	public UITexture broad;
	public ShakepositionEffect Bubble;
	//public ParticleSystem particle;

	public string last_spelling = "", last_voice = "";
	//private string currentSpelling = "";
	// Command on version 1.3.7
	// private bool isScrolling = false,
	//			 isWordHit = false; //Beta version 1.1
	/*, isSetupFinish = false;*/ //Beta version 1.0
	private int label_list_index = 0;
	//private UIScrollView _scrollview;
	//private UIPanel _scroll_panel;
	//private CCManualLoadResourceObj loader;
	private bool isTurnOffByManual = false;
	private bool wordCorrect = false;
	private bool isCheckingWord = false;
	private Transform objectSpawnonWhichLetter;
	private GameObject previousletterIDBody;
	private GameObject previousletterIDControl;

	// Var of tutorial system
	//private GameMode2TutorialController tutorialController;
	//	private AlphabetTutorialBehaviour tutorialSystem;
	//private bool isTutorialTime;

	public Transform objParent;
	public GameObject notice_obj, refresh_button;
	public GameObject[] dict_selection;
	public UILabel prefeb, dictLabel;
	public Texture[] encouragementTexture;
	//public UITexture WordCompletionTexture;
	//public UIGrid _container;
	public UIWrapContent _container;
	private List<UILabel> label_list = new List<UILabel>();

	//public GameObject backButton;
	public GameObject dictionary;
	public string inputReference = "";
	//private BoxCollider backButtonCollider;
	private BoxCollider dictionaryCollider;
	private UIButton dictionaryButtonUI;

	//private UIButton backButtonUI;
	//public GameObject captureShareButton;
	//private BoxCollider captureShareButtonCollider;
	//private UIButton captureShareButtonUI;
	//private GameMode2CoreonTrackFunction gameMode2CoreonTrackFunction;
	//private TipsForOnshot tipsForOnShot;

	public Renderer markerBg;

	public List<string> letterDisplayMode = new List<string>();

	public static readonly string COLOR_CODE_BLUE = "[6aa2db]";
	public static readonly string COLOR_CODE_END = "[-]";


	/*
     * ascii 97-122  =  a-z
     * ascii a-z  - 32 = A-Z
     * int ascii = CCTools.Convert.StringToASCII (input);
     * string str = CCTools.Convert.ASCIIToString (ascii);
     * 
     * Flow
     * init dict 
     * add word to list "a-z"
     * keep check first Alpabet change (have a callback)
     * show effect
     */
	#region Unity Life Cycle
	// Use this for initialization
	//void Start () 
	public void Start()
	{
		initialize();

	}
    public void initialize()
    {
		//SceneManager.LoadScene("GameMode2", LoadSceneMode.Additive);

		TrackableMarkers = new List<AlphabetCoreTrackableEventHandler>();
		gameModeTrackableScript = GetComponent<GameMode1CoreonTrackFunction>();
		mSystem = GetComponent <AlphabetCoreSystem> ();
		//mSystem.initDict ();
		mSystem.initDict("dictionary/dict", 6);
		loader = new CCManualLoadResourceObj ();
		// mSystem.OnHeaderAlphabetChange += OnHeaderAlphabetChange;
		mSystem.OnClick += OnMarkerClick;
        mSystem.OnInputChange += OnInputChange;
		mSystem.OnWordHit += OnWordHitPass;

        lastAlphabet = "";
		setActiveToArObject (false);
		
		AppController.ManualReleaseMemory ();

        characterObjectAnimator = characterObject.GetComponent<Animator>();

        GameObject strokeButton = buttonRoot.transform.FindChild("StrokeButton").gameObject;
        strokeButtonAnimator = strokeButton.GetComponent<Animator>();
        strokeButtonMeshRenderer = strokeButton.GetComponentInChildren<SkinnedMeshRenderer>();
        //strokeButtonMeshRenderer.material = strokeButtonMeshRenderer.materials[0];

        GameObject wordsButton = buttonRoot.transform.FindChild("WordsButton").gameObject;
        wordsButtonAnimator = wordsButton.GetComponent<Animator>();
        wordsButtonMeshRenderer = wordsButton.GetComponentInChildren<SkinnedMeshRenderer>();
        //wordsButtonMeshRenderer.material = wordsButtonMeshRenderer.materials[0];

        GameObject phonicsButton = buttonRoot.transform.FindChild("PhonicsButton").gameObject;
        phonicsButtonAnimator = phonicsButton.GetComponent<Animator>();
        phonicsButtonMeshRenderer = phonicsButton.GetComponentInChildren<SkinnedMeshRenderer>();
        //phonicsButtonMeshRenderer.material = phonicsButtonMeshRenderer.materials[0];

        //tutorialController = GetComponent<GameMode1TutorialController>();
        //tutorialSystem = GetComponent <AlphabetTutorialBehaviour> ();
		//isTutorialTime = tutorialSystem.NeedTutorial;
       // isTutorialTime = tutorialController.isNeedTutorial;
        //if (isTutorialTime) {
        //	tutorialSystem.onTutorialFinish += OnTutorialFinish;
        //	tutorialSystem.onTutorialSkipButtonClicked += OnTutorialFinish;
        //	mSystem.onStageMarkerFound += OnStageMarkerFound;
        //} 

        backButtonCollider = backButton.GetComponent<BoxCollider>();
        backButtonUI = backButton.GetComponent<UIButton>();

        captureShareButtonCollider = captureShareButton.GetComponent<BoxCollider>();
        captureShareButtonUI = captureShareButton.GetComponent<UIButton>();

        is3dObjectPlaying = false;
        is3dObjectFinishedPlaying = true;
    }
	
	// Use this for endup the scene
	void OnDestroy ()
	{
		letterStroke = null;
		loader = null;
		mSystem.OnClick -= OnMarkerClick;
        mSystem.OnInputChange -= OnInputChange;
		mSystem.OnWordHit -= OnWordHitPass;
        //mSystem.onStageMarkerFound = null;
        //tutorialSystem.onTutorialFinish = null;
        // mSystem.OnHeaderAlphabetChange -= OnHeaderAlphabetChange;
    }

	public void resetDisplayColorAnddisEnableDisplayObjecr(bool status)
	{
		Debug.Log("turnofff parent");
		//broad.mainTexture = listofTextureName[0];
		//if (!status)
		//{
			arParentObj.transform.parent = null;

		//}
		Renderer[] rendererComponents = arParentObj.GetComponentsInChildren<Renderer>(true);
		Collider[] colliderComponents = arParentObj.GetComponentsInChildren<Collider>(true);

		// Disable rendering:

		foreach (Renderer component in rendererComponents)
		{

			component.enabled = status;
		}

		// Disable colliders:
		foreach (Collider component in colliderComponents)
		{
			component.enabled = status;
		}
		//loader.unloadGameObject ();
	}

	void LetterMode()
	{
		currnetLetter = TrackableMarkers[TrackableMarkers.Count - 1].id;
		OnMarkerClick(TrackableMarkers[TrackableMarkers.Count - 1].transform, currnetLetter);
		dict_selection[0].SetActive(false);
		isLetterMode = true;

	}
	void SetUpRealTimeDic()
	{

		if (TrackableMarkers.Count == 1 )
		{
			if (string.IsNullOrEmpty(currnetLetter))
			{
				LetterMode();
				CCLog.SpecialMsg("TrackableMarker is One both currnet null");
			} else
			if (!currnetLetter.Equals(TrackableMarkers[TrackableMarkers.Count - 1].id))
			{
				LetterMode();
				CCLog.SpecialMsg("TrackableMarker is One both currnet null");
			}

		}
		else if (TrackableMarkers.Count >= 2 )
		{
			if (isLetterMode)
			{

				isLetterMode = false;
				setActiveToArObject(false);
				gameModeTrackableScript.ResetAllMode();
				CCLog.SpecialMsg("TrackableMarker is Two both false");
			}
		}
	

	}

    void Update ()
    {
		// Check if sound is finished playing to allow further clicking
		SetUpRealTimeDic();
        if (is3dObjectPlaying)
        {
            if (!is3dObjectFinishedPlaying && !AlphabetCoreAudioSystem.IsPlaying)
            {
                is3dObjectPlaying = false;
                is3dObjectFinishedPlaying = true;
                if (characterObjectAnimator.GetBool("isTimoSpeaking"))
                {
                    characterObjectAnimator.SetBool("isTimoSpeaking", false);
                }
                setButtonProperties(false);
            }
        }
        //disable interaction with other button durring capture
        if (AlphabetCoreSystem.capturing == true)
        {
            backButtonCollider.enabled = false;
            captureShareButtonCollider.enabled = false;
        }
        else if (backButtonCollider.enabled == false /*&& tutorialController.isTutorialFinished()*/)
        {
            backButtonCollider.enabled = true;
            captureShareButtonCollider.enabled = true;
        }
    }
	#endregion
	
	#region Private method
	//private void sendToTutorialSystem (int stageID, float delayTime = 1.5f)
	//{
	//	if (!isTutorialTime) 
	//		return;
		
	//	if (stageID == 0 && !tutorialSystem.isButtonClicked)
	//	{
	//		canStage0EventSendOut = false;
	//		tutorialSystem.onTutorialOkButtonClicked += OnTutorialOkButtonClick;
	//		return;
	//	}
		
	//	tutorialSystem.QueueCompleteStageEvent (stageID, delayTime);
	//}
	
	private void setActiveToArObject (bool value)
	{
		try
		{
			//arParentObj.SetActive (value);
			resetDisplayColorAnddisEnableDisplayObjecr(false);
			letterTxt.resetToDefault ();
			//Debug.Log("setActiveToArObject " + value);
		}
		catch {}	
	}

    public void resetBlackboard()
    {
       /* AlphabetCoreAudioSystem.instance.stopAll();

        // Disable blackboard objects
        setActiveToArObject(false);

        // Turn off alphabet character
        arObjs.transform.GetChild(lastAlphabetAscii - 97).gameObject.SetActive(false);

        // Reset blackboard texture
        blackboardRenderer.material.mainTexture = blankBlackboardTextureOriginal;

        ParticleSystem.EmissionModule em = particle.emission;
        em.enabled = false;

        isFirstClick = true;*/
    }
    #endregion

    public void disableLeftSideUIButtons()
    {
        backButtonCollider.enabled = false;
        backButtonUI.SetState(UIButtonColor.State.Disabled, false);

        captureShareButtonCollider.enabled = false;
        captureShareButtonUI.SetState(UIButtonColor.State.Disabled, false);
    }

    private void enableUIButtons()
    {
        backButtonCollider.enabled = true;
        backButtonUI.SetState(UIButtonColor.State.Normal, false);

        captureShareButtonCollider.enabled = true;
        captureShareButtonUI.SetState(UIButtonColor.State.Normal, false);
    }

    //public void enableTutorialNextButton()
    //{
    //    tutorialNextButtonCollider.enabled = true;
    //    tutorialNextButtonUI.SetState(UIButtonColor.State.Normal, false);
    //}

    #region Callback of AlphabetCoreSystem 
    //private bool canStage0EventSendOut = true;

    //void OnStageMarkerFound ()
    //{
    //	sendToTutorialSystem (0);
    //}

    //void OnTutorialOkButtonClick ()
    //{
    //	if (!canStage0EventSendOut && tutorialSystem.isTrackerFind)
    //	{
    //		canStage0EventSendOut = true;
    //		sendToTutorialSystem (0);
    //		tutorialSystem.onTutorialOkButtonClicked -= OnTutorialOkButtonClick;
    //	}
    //	else if (!tutorialSystem.isTrackerFind)
    //	{
    //		canStage0EventSendOut = false;
    //	}
    //}

    [Beebyte.Obfuscator.SkipRename]
    public void OnTutorialFinish ()
	{
		CCLog.SpecialMsg ("OnTutorialFinish");
	//	isTutorialTime = false;
        enableUIButtons();
        //tutorialSystem.onTutorialFinish = null;
    }


	void OnMarkerClick (Transform marker, string input)
	{
       /* if (isTutorialTime
            && (tutorialController.tutorialState != 1
            || tutorialController.tutorialState == 1 && (!tutorialController.isFadeInAudioPlayedOnce() || TutorialInstructionAudioController.IsPlaying)))
        {
            return;
        }*/


		if (TrackableMarkers.Count == 1)
		{
			CCLog.SpecialMsg(marker.name + " Clicked, Id : " + input + " nubmer of Marker in Fields: " + TrackableMarkers.Count);
			gameModeTrackableScript.OnArParentPositionUpdate(marker.gameObject);
			//sendToTutorialSystem (1, arObjs.transform.GetChild (CCTools.Convert.StringToASCII (input)-97).gameObject.GetComponentInChildren <Animation> (true).clip.length);
			gameModeTrackableScript.SetReferenceCurrentDisplayLetter(marker.gameObject);
			arParentObj.SetActive(true);
			OnHeaderAlphabetChange(input);
		}
		else {
			CCLog.SpecialMsg( " Clicked " );
			//OnHeaderAlphabetChange(input);
			marker.FindChild(input).GetComponent<Animation>().Play();
		}
        //particle.transform.parent = marker;
		//particle.transform.localPosition = Vector3.zero;
		//particle.transform.localScale = Vector3.one;
		// particle.enableEmission = true;
		//var em = particle.emission;
		//em.enabled = true;
		//particle.transform.GetComponent<Renderer> ().enabled = true;
		//if (isFirstClick)
		//{
			//isFirstClick = false;
			//particle.startSize *= marker.lossyScale.x;
			//particle.gravityModifier *= marker.lossyScale.x;
			
		//	blackboardRenderer.material.mainTexture = blankBlackboardTexture;
		//}

        //tutorialController.OnNextLetterClicked();
	}




    public void OnInputChange(string input)
    {
        currentSpelling = input;
       // tutorialController.OnLetterScan();
		// Consult in 1.4.0 //
		//Debug.Log("input.Length: " + input.Length);
		//Debug.Log("mSystem.isHeaderSet (): " + mSystem.isHeaderSet());
		//Debug.Log("mSystem.getCurrentHeader (): " + mSystem.getCurrentHeader());
		//Debug.Log("input [0].ToString (): " + input[0].ToString());
		if (input.Length > 0 && mSystem.isHeaderSet() && !(mSystem.getCurrentHeader().Equals(input[0].ToString()))/*&& last_input.Length > 0 && (!(last_input.StartsWith (input)) || (last_input [0] != input [0]))*/)
		{
			dict_selection[0].SetActive(false);
			broad.mainTexture = listofTextureName[0];
			mSystem.releaseHeader();
			mSystem.setupHeader("" + input[0]);
		}

		if (isCheckingWord)
		{
			Debug.Log("clear");
			dictLabel.text = "";
			dict_selection[0].SetActive(false);
			broad.mainTexture = listofTextureName[0];
			objParent.gameObject.SetActive(false);
			resetDisplayColorAnddisEnableDisplayObjecr(false);
			isCheckingWord = false;
		}

    }

    public void OnHeaderAlphabetChange(string input)
    {

        if (!isActiveAndEnabled || !gameObject.activeSelf)
        {
            return;
        }
		if (!string.IsNullOrEmpty(inputReference))
		{
			if (!inputReference.Equals(input))
			{
				gameModeTrackableScript.ResetAllMode();
				Debug.Log("Clear");
			}

		}
		CCLog.SpecialMsg("Header Change to : " + input );

		inputReference = input;
		//gameModeTrackableScript.ResetAllMode();


        AlphabetCoreAudioSystem.instance.stopAll();

        if (characterObjectAnimator.isInitialized && characterObjectAnimator.GetBool("isTimoSpeaking"))
        {
            characterObjectAnimator.SetBool("isTimoSpeaking", false);
        }

        // Turn on Ar Obj
        if (string.IsNullOrEmpty(input))
        {
            //setActiveToArObject(false);
			objParent.gameObject.SetActive(false);
            return;
        }
        else if (!arParentObj.activeSelf)
        {
			objParent.gameObject.SetActive(true);
           // setActiveToArObject(true);

        }


        // Reset audio list for playing multi clip
        AlphabetCoreAudioSystem.instance.resetLetterList();

        // As each part may work itself so use 2 try and catch 
        try
        {
            // Turn off showing 3dObj
            arObjs.transform.GetChild(lastAlphabetAscii - 97).gameObject.SetActive(false);
        }
        catch { }

        try
        {
            // Save new to currnet
            lastAlphabet = input;
            lastAlphabetAscii = CCTools.Convert.StringToASCII(input);

            // Set alphabet stroke
            letterStroke.setTextureToMaterial(loader.loadTexture("png/stroke/stroke " + lastAlphabet));

            // Set color and alphabet also animation
            letterTxt.resetToDefault();
            letterTxt.setColorToMaterial(txt_color[(lastAlphabetAscii - 97) % txt_color.Length]);
            letterTxt.setTextureToMaterial(loader.loadTexture("png/letter/" + lastAlphabet)/*txt_texture[(last_alphabet_ascii-97)]*/);

            //letter_txt.play (); // if auto play jsut uncomment it

            // Turn on new 3dObj
            // Auto read
            if (!string.IsNullOrEmpty(lastAlphabet))
            {
                //is3dObjectPlaying = true;
                //is3dObjectFinishedPlaying = false;
                AlphabetCoreAudioSystem.instance.stopAll();
                AlphabetCoreAudioSystem.instance.playOnce(lastAlphabet, AlphabetCoreConstants.AUDIO_Selections.LETTER_PRONUNCIATION);
            }

            arObjs.transform.GetChild(lastAlphabetAscii - 97).gameObject.SetActive(true);
        }
        catch (System.Exception e) { print(e); }
    }

    public bool hasSpelling()
    {
        if (string.IsNullOrEmpty(currentSpelling))
        {
            return false;
        }

        return true;
    }
    #endregion

    #region Callback of Obj click event
    public void On3dPhonicsClick ()
	{
        //if (string.IsNullOrEmpty(lastAlphabet))
        //{
        //    return;
        //}
       //if (check3dObjClick())
       //{
            is3dObjectPlaying = true;
            is3dObjectFinishedPlaying = false;
            AlphabetCoreAudioSystem.instance.stopAll();
            AlphabetCoreAudioSystem.instance.playOnce(lastAlphabet + " phonics" , AlphabetCoreConstants.AUDIO_Selections.LETTER_PHONICS);

            //phonicsButtonAnimator.SetBool("isButtonPressed", true);
         //   currentButtonPressed = BUTTONS.PHONICS;
          //  setButtonProperties(true);
            characterObjectAnimator.SetBool("isTimoSpeaking", true);
       //}
	}
	
	public void On3dLetterClick ()
	{
        //if (string.IsNullOrEmpty(lastAlphabet))
        //{
        //    return;
        //}
       //if (check3dObjClick())
       //{
            is3dObjectPlaying = true;
            is3dObjectFinishedPlaying = false;
            //gameModeAudioController.playOnce(AlphabetCoreConstants.AUDIO_CONTROLLER_TYPES.STROKES, lastAlphabet + " stroke");
            AlphabetCoreAudioSystem.instance.stopAll();
            AlphabetCoreAudioSystem.instance.playOnce(lastAlphabet + " stroke" , AlphabetCoreConstants.AUDIO_Selections.LETTER_STROKE);
            //strokeButtonAnimator.SetBool("isButtonPressed", true);
          //  currentButtonPressed = BUTTONS.STROKE;
          //  setButtonProperties(true);
            letterTxt.play();
        //}
    }
	public void On3dObjModelClick()
	{
		// Consult the beta version by Cordex
		// Click the marker area to play animation and audio
		if (string.IsNullOrEmpty(last_spelling) )
		{
			return;
		}

		try
		{
			broad.mainTexture = listofTextureName[1];
			Bubble.turnShakingOn();
			//			if (isTutorialTime)
			//			{

			//				if(tutorialController.currentState == 1 && !loader.current_obj.GetComponentInChildren<Animation>().isPlaying /*&& !canStage1EventSendOut*/)
			//				{
			////					canStage1EventSendOut = true;
			//					// StartCoroutine ("pronounciation");
			//					loader.current_obj.GetComponentInChildren<Animation> ().Play ();
			////					sendToTutorialSystem (1, loader.current_obj.GetComponentInChildren <Animation> ().clip.length);
			//				}
			//			}
			//			else 
			Debug.Log("On3dObjClick");
			if (loader.current_obj != null)
			{
				if (!loader.current_obj.GetComponentInChildren<Animation>().isPlaying)
				{
					//AlphabetCoreAudioSystem.instance.stopAll ();
					//AlphabetCoreAudioSystem.instance.playOnce (last_spelling);
					StartCoroutine(pronounciation(last_spelling));
					loader.current_obj.GetComponentInChildren<Animation>().Play();
				}
			}
			/*
			if(!loader.current_obj.GetComponent<Animation>().isPlaying)
			{
				//AlphabetCoreAudioSystem.instance.stopAll ();
				//AlphabetCoreAudioSystem.instance.playOnce (last_spelling);
				StartCoroutine ("pronounciation");
				loader.current_obj.GetComponent<Animation>().Play ();
			}
			*/

		}
		catch (System.Exception ex)
		{
			Debug.Log(ex);
		}
		/////////////////////////////////////////
	}

	[System.Reflection.Obfuscation(ApplyToMembers = false)]
	private IEnumerator pronounciation(string last_spelling)
	{
		while (true)
		{
			if (AlphabetCoreAudioSystem.IsPlaying)
			{
				yield return null;
			}
			else {
				//WordCompletionTexture.gameObject.SetActive(false);
				AlphabetCoreAudioSystem.instance.playOnce(last_spelling, AlphabetCoreConstants.AUDIO_Selections.WORD_PRONUNCIATION);
				break;

			}
		}

	}

	public void On3dObjClick ()
	{
        //if (string.IsNullOrEmpty(lastAlphabet))
        //{
        //    return;
        //}
		Debug.Log(check3dObjClick());
       // if (check3dObjClick())
        //{
            is3dObjectPlaying = true;
            is3dObjectFinishedPlaying = false;
            AlphabetCoreAudioSystem.instance.stopAll();
			Debug.Log (" On3dObjClick ");
            AlphabetCoreAudioSystem.instance.playOnce(lastAlphabet, AlphabetCoreConstants.AUDIO_Selections.LETTER_PRONUNCIATION, false, true);
            //wordsButtonAnimator.SetBool("isButtonPressed", true);
           // currentButtonPressed = BUTTONS.WORDS;
           // setButtonProperties(true);
            arObjs.transform.GetChild(lastAlphabetAscii - 97).gameObject.GetComponent<Animation>().Play();
       //}
	}
	#endregion

    // Set boolean to transition button state to pressed
    private void setButtonProperties(bool isClick)
    {
        if (isClick)
        {
            switch (currentButtonPressed)
            {
                case BUTTONS.STROKE:
                    strokeButtonMeshRenderer.material = strokeButtonMaterials[1];
                    strokeButtonAnimator.SetBool("isButtonPressed", true);
                    break;

                case BUTTONS.WORDS:
                    wordsButtonMeshRenderer.material = wordsButtonMaterials[1];
                    wordsButtonAnimator.SetBool("isButtonPressed", true);
                    break;

                case BUTTONS.PHONICS:
                    phonicsButtonMeshRenderer.material = phonicsButtonMaterials[1];
                    phonicsButtonAnimator.SetBool("isButtonPressed", true);
                    break;
            }
        }
        else
        {
            switch (currentButtonPressed)
            {
                case BUTTONS.STROKE:
                    strokeButtonMeshRenderer.material = strokeButtonMaterials[0];
                    strokeButtonAnimator.SetBool("isButtonPressed", false);
                    //wordsButtonAnimator.SetTime(0);
                    //phonicsButtonAnimator.SetTime(0);
                    break;

                case BUTTONS.WORDS:
                    wordsButtonMeshRenderer.material = wordsButtonMaterials[0];
                    wordsButtonAnimator.SetBool("isButtonPressed", false);
                    //strokeButtonAnimator.SetTime(0);
                    //phonicsButtonAnimator.SetTime(0);
                    break;

                case BUTTONS.PHONICS:
                    phonicsButtonMeshRenderer.material = phonicsButtonMaterials[0];
                    phonicsButtonAnimator.SetBool("isButtonPressed", false);
                    //wordsButtonAnimator.SetTime(0);
                    //strokeButtonAnimator.SetTime(0);
                    break;
            }

            currentButtonPressed = BUTTONS.NONE;
        }
    }

    public bool check3dObjClick()
    {
		//Debug.Log (is3dObjectPlaying + " :  is empty" + string.IsNullOrEmpty(lastAlphabet) + "is play : " + AlphabetCoreAudioSystem.IsPlaying);
		Debug.Log("lastAlphabet  : " + lastAlphabet +  " is3dObjectPlaying : "  + is3dObjectPlaying);
        if (string.IsNullOrEmpty(lastAlphabet) || is3dObjectPlaying)
        {
            return false;
        }
        return true;
    }

	public void OnWordHitPass(string spelling)
	{
		mSystem.setupHeader(mSystem.getFirstAlphabetId());
	/*	if (isCheckingWord
			|| (isTutorialTime
				&& (tutorialController.tutorialState != 1
				|| tutorialController.tutorialState == 1 && (!tutorialController.isFadeInAudioPlayedOnce() || TutorialInstructionAudioController.IsPlaying))))
		{
			return;
		}*/

		isCheckingWord = true;

		// Spell once
		CCLog.SpecialMsg("Hit : " + spelling + " Last : " + last_spelling);
		//CCPlayerStatusManager.ChangePlayerStatus (AlphabetCoreConstants.PLAYER_STATUS.WORD);

		// isWordHit = true;

		// Beta version 1.4 //
		dict_selection [0].SetActive (true);
		dictLabel.text = spelling;
		broad.mainTexture = listofTextureName[1];
		 Bubble.turnShakingOn();
		//label_list_index = label_list.IndexOf ((label_list.FindAll(delegate (UILabel label) {return label.text.Replace (COLOR_CODE_BLUE, "").Replace (COLOR_CODE_END, "") == spelling;}) [0]));
		//(label_list [label_list_index]).transform.parent = dict_selection [0].transform;
		//(label_list [label_list_index]).transform.localPosition = new Vector3 (0, 10, 0);
		//////////////////////

		/*
		label_list_index = label_list.IndexOf ((label_list.FindAll(delegate (UILabel label) {return label.text.Replace (COLOR_CODE_BLUE, "").Replace (COLOR_CODE_END, "") == spelling;}) [0]));
		startSpring ();
		*/

		//refresh_button.SetActive(true);
		//refresh_button.GetComponent<UISprite>().ResizeCollider();
		string tmp_voice = "";
		for (int i = 0; i < spelling.Length; i++)
		{
			tmp_voice += spelling[i] + " ";
		}
		tmp_voice += spelling;

		// Consult the beta version by Cordex  //
		// Control the audio
		tmp_voice = spelling;
		//////////////////////////////////////

		/*
		if (isTutorialTime)
		{
//			if (tutorialSystem.tutorialOkButtonClickCounter > 0)
//			{
			if(last_voice != tmp_voice)
			{
                AlphabetCoreAudioSystem.instance.stopAll();
                //AlphabetCoreAudioSystem.instance.playOnce (AlphabetCoreSystem.CorrectPronounciation (spelling));
                AlphabetCoreAudioSystem.instance.playOnce(spelling);
                last_voice = tmp_voice;
            }
//			}
		}
        */

	//	if (!isTutorialTime /*&& !last_voice.Equals(tmp_voice)*/)
	//	{
			AlphabetCoreAudioSystem.instance.stopAll();
			//AlphabetCoreAudioSystem.instance.playOnce (AlphabetCoreSystem.CorrectPronounciation (spelling));
			AlphabetCoreAudioSystem.instance.playOnce(spelling, AlphabetCoreConstants.AUDIO_Selections.WORD_PRONUNCIATION);
			last_voice = tmp_voice;
		//	}

		// Consult the beta version by Cordex  //
		// Control the animation
		//if (last_spelling == spelling)
		// if (!last_spelling.Equals(spelling))
		//{
		// last_spelling = spelling;
		//////////////////////////////////////
		Debug.Log(last_spelling + " : " + spelling);
		if (!last_spelling.Equals(spelling))
		{
			try
			{
				last_spelling = spelling;
				loader.unloadGameObject();
			}
			catch
			{
			}

			Debug.Log(AlphabetCoreSystem.WordCategory(spelling));
			//markerBg.material.mainTexture = loader.loadTexture("floor/" + AlphabetCoreSystem.WordCategory(spelling));


			try
			{
				Transform obj = loader.loadGameObject("obj/" + spelling).transform;
				/*if (isTutorialTime)
                {
                    tutorialController.setLoad3dObject(spelling);
                    //obj.GetComponentInChildren<Animation>().Stop();
                }

                if (!tutorialController.magicScreen.isTrackerFind && !tutorialController.magicScreen2.isTrackerFind)
                {
                    Renderer[] components = obj.GetComponentsInChildren<Renderer>();
                    foreach (Renderer render in components)
                    {
                        render.enabled = false;
                    }
                }*/

				// Reset position after assigning to parent
				Vector3 pos = obj.localPosition;
				Vector3 scale = obj.localScale;
				Vector3 rotation = obj.localEulerAngles;
				obj.parent = markerBg.transform;
				//objParent.gameObject.SetActive (true);
				char[] lastLetter = spelling.ToCharArray();

				objectSpawnonWhichLetter = GetLastLetterFromTheWord(lastLetter, mSystem.return_ListOfTrackableEventHandle()).transform;
				Debug.Log("objectSpawnonWhichLetter " + objectSpawnonWhichLetter);
				//	Transform currentLastLetter =  mSystem.return_ListOfTrackableEventHandle()[mSystem.return_ListOfTrackableEventHandle().Count-1].transform;
				if (objectSpawnonWhichLetter != null)
				{
					//objectSpawnonWhichLetter.GetComponent<BoxCollider>().enabled = false;
					//	Destroy(objectSpawnonWhichLetter.GetComponent<Rigidbody>());
					//GameObject letterIDBody =objectSpawnonWhichLetter.GetChild(0).gameObject;
					//GameObject letterIDControl =objectSpawnonWhichLetter.GetChild(1).gameObject;
					//objectSpawnonWhichLetter.gameObject.SetActive(false);
					objParent.parent = objectSpawnonWhichLetter.transform.parent;
					//letterIDBody.SetActive(false);
					//letterIDControl.SetActive(false);
					//previousletterIDBody = letterIDBody;
					//previousletterIDControl = letterIDControl;
					objParent.localPosition = new Vector3(0, 0, 1.4f);
					objParent.localScale = new Vector3(40, 40, 40);
					objParent.localEulerAngles = Vector3.zero;
					obj.localPosition = pos;
					obj.localScale = scale;
					//obj.localScale = new Vector3(0.01f,0.01f,0.01f);;
					obj.localEulerAngles = rotation;
					var em = particle.emission;
					em.enabled = false;
					//AlphabetCoreTrackableEventHandler currentTrackEvent = TipsForOnshot.CurrentOnHitTarget.GetComponent<AlphabetCoreTrackableEventHandler>();
					//setRender(currentTrackEvent.isTrack,objParent.gameObject);
					//currentTrackEvent.ResetActiveTrackableEvent();
					AlphabetCoreTrackableEventHandler currentTrackEvent = objectSpawnonWhichLetter.transform.parent.gameObject.GetComponent<AlphabetCoreTrackableEventHandler>();
					// currentTrackEvent = TipsForOnshot.CurrentOnHitTarget.GetComponent<AlphabetCoreTrackableEventHandler>();
					currentTrackEvent.ResetActiveTrackableEvent();
					Debug.Log(obj.gameObject.name + "      =====play Animation ");
					obj.gameObject.GetComponent<Animation>().Play();

				}


			}
			catch (System.Exception ex) { Debug.Log(ex); }

			wordCorrect = true;
			TipsForOnshot.IsCorrect = true;
			//playEncouragement(spelling);
			//}

			//isCheckingWord = false;
		}
		//resetDisplayColorAnddisEnableDisplayObjecr(true);
		objParent.gameObject.SetActive(true);
	}

	private GameObject GetLastLetterFromTheWord(char[] lastLetter, List<AlphabetCoreTrackableEventHandler> trackableEvent)
	{
		//	foreach(char letter in  lastLetter) 
		//{
		int length = trackableEvent.Count;
		int center = 0;
		if (length % 2 == 0)
		{
			center = length / 2 - 1;
		}
		else {

			center = (length / 2);
		}

		return trackableEvent[center].transform.GetChild(0).gameObject;

		//foreach (AlphabetCoreTrackableEventHandler track in trackableEvent) 
		//{
		//Debug.Log (track.transform.GetChild(0).gameObject.name + " : " + letter);
		//if (track.transform.GetChild(0).gameObject.name.Equals (letter.ToString())) {
		//	return track.gameObject;

		//}

		//}
		//}
		return null;
	}

}