﻿public class ScaleEffect : UIButton {
	public bool isPressed = false;
	//UIButtonColor color;

	void Start ()
	{
		//isPressed = false;
		//color = GetComponent<UIButtonColor> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (state == State.Pressed && !isPressed)
        {
			iTween.ShakeScale (gameObject, iTween.Hash ("x", 0.03f, "y", 0.03f, "time", 0.5f, "looptype", iTween.LoopType.none));
			isPressed = true;
		} 

		if(state == State.Normal) {
			isPressed = false;
		}
	}
}
