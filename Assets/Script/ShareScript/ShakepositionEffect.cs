﻿using UnityEngine;
using System.Collections;

public class ShakepositionEffect: UIButton {

	bool isPressed = false;
    bool onShakeNeeded = false;
    float i = 0;


    void Start ()
	{
		isPressed = false;
	}
		
	void Update ()
	{
		if (state == State.Pressed  && isPressed == false)
        {
			iTween.ShakePosition(gameObject,iTween.Hash("x", 0.01f, "y", 0.01f, "time", 0.2f, "looptype", iTween.LoopType.none));
			isPressed = true;
		}

		if(state == State.Normal) {
			isPressed = false;
		}
        if (onShakeNeeded)
        {
            StartCoroutine("OnCallShake");
        }

    }

    private IEnumerator OnCallShake()
    {

        while (i < 10 && onShakeNeeded)
        {
            yield return new WaitForSeconds(0.1f);
            iTween.ShakePosition(gameObject, iTween.Hash("x", 0.02f, "y", 0.02f, "time", 0.2f, "looptype", iTween.LoopType.none));
            i += Time.deltaTime;
            if (i > 9) {
                onShakeNeeded = false;
            }
        }
        
    }



    public void turnShakingOn()
    {
        onShakeNeeded = true;
        i = 0;
    }
}
