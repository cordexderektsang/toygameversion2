﻿using UnityEngine;
using System.Collections;

public class shakeTextBox : MonoBehaviour {

    public GameObject currentShakeTarget;
	// Use this for initialization
	public void OnPresEffect()
    {
        iTween.ShakePosition(currentShakeTarget, iTween.Hash("x", 0.05f, "y", 0.05f, "time", 0.5f, "looptype", iTween.LoopType.none));
    }
}
