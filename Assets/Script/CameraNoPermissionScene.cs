﻿using UnityEngine;
using System.Collections;

public class CameraNoPermissionScene : MonoBehaviour {

	public void OnBtnExitClicked()
	{
		Debug.Log ("exit");
		Application.Quit ();
	}

	public void BackToMenu()
	{
		UnityEngine.SceneManagement.SceneManager.LoadScene ("Menu");
	}
}
