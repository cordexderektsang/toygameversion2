﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class TryMeBagSetLastFrameOfAnimation : MonoBehaviour {
	private Animation anim;
	private List<string> stateList;


	void Start() 
	{
        if (SceneManager.GetActiveScene().name.Equals("TryMeMode")) {
            anim = GetComponent<Animation>();

            // get the clip name 
            stateList = new List<string>();
            foreach (AnimationState states in anim)
            {
                stateList.Add(states.name);
            }

            Debug.Log("stateList " + stateList[0] + " " + stateList[1]);
            // play last frame only
            anim.Play(stateList[1]);
        }
	}
    void OnEnable()
    {
        if (SceneManager.GetActiveScene().name.Equals("TryMeMode"))
        {
            anim = GetComponent<Animation>();

            // get the clip name 
            stateList = new List<string>();
            foreach (AnimationState states in anim)
            {
                stateList.Add(states.name);
            }

            Debug.Log("stateList " + stateList[0] + " " + stateList[1]);
            // play last frame only
            anim.Play(stateList[0]);
        }
    }

    /// <summary>
        // to play the animation.
        /// </summary>
    public void Play() 
	{
		// from first frame to last frame
		anim.Play(stateList[0]);
	}
}
