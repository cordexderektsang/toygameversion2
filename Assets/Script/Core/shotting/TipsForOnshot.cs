﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TipsForOnshot : MonoBehaviour {
	// game mode 2
	AlphabetGameMode2UISystem alphabetGameMode2UISystem ;
	GameMode2CoreonTrackFunction gameMode2CoreonTrackFunction;
	public bool intialStartLetterForWord = false;
	public List <tipFor3dWord> letterTipData;
	public List <UITexture> letterTipDataForUi;
	public string previousWord = "";
	public GameObject letterBuddyPlaneParent;
	public GameObject uiletterBuddyPlaneParent;
	public GameObject instantiateLetterBuddyPlaneForUi;
	public GameObject instantiateLetterBuddyPlane;
	public List<Texture> letterBuddyTexture;
	public List<string> referenceForCurrentListOfWord = new List<string>();
	public MeshRenderer backGroundRender;
	public List<Color> backGroundCurrentColour = new List<Color> ();
	// use For Transform tip in betweeen  UiRoot and current Letter;
	public Transform uiObject;
	public bool isBlackScreen = false;
	public static Transform CurrentOnHitTarget;
	public Texture2D tex ;
	public GameObject frameTarget;
	public GameObject frameTargetFree;
	public List<GameObject> listOfUIOnOff;
	public string previousCorrectWord = "";
	public shotForGameModeTwo shotForGamemodetwo;



	// set the maximum word length for game mode 2
	public int maximumWordLength = 5; 
	public float gapForEach3Dtip = 0.5f;
	private AlphabetCoreSystem mSystem;
	private string currentTipReference = "";
	private int index = 0;
	private string onHitRayResult = "";
	private Material backGroundMaterial;
	private Texture2D arCamTexture;

	// use For Transform tip in betweeen  UiRoot and current Letter;

	private Vector3 positionForUI = new Vector3 (0 ,0, 0);
	private Vector3 scaleForWorld= new Vector3 (2, 2, 2);
	private Vector3 scaleForUI= new Vector3 (10	, 10, 2);
	private int widthForUiTip = 502;
	private int heightForUiTip = 502;

	// check BlackScreen 5 fix point coordinateion
	private Vector2 PointOne;
	private Vector2 PointTwo;
	private Vector2 PointThree;
	private Vector2 PointFour;
	private Vector2 PointFive;
	private List <Vector2> listOfPointForCheckingBlackScreen = new List<Vector2>();
	private Coroutine blackScreenFunction;
	private	int numberOfBlackPixel;
	private Coroutine autoResetCoroutine = null;

	public static bool IsCorrect = false;
	private string onRayHitLetter = "";
	private int initalGapForUiTips = 50;
	// black screen variable
	private bool isEnableForBlackScreen = false;
	private float countTimerForBlackScreen = 0;
	private float maxTimerForBlackScreen = 0.5f;
	private float counterFornoAction = 0;
	private float maxTimerForNoAction = 10;
	private Coroutine coroutineForReScan = null;




	#region Start Up
	private void Start ()
	{
		IsCorrect = false;
		Input.gyro.enabled = true;
		alphabetGameMode2UISystem = GetComponent<AlphabetGameMode2UISystem> ();
		gameMode2CoreonTrackFunction = GetComponent<GameMode2CoreonTrackFunction> ();
		mSystem = GetComponent<AlphabetCoreSystem> ();
		GenerateNecessaryPlainForDisplay3dWordTipsAndUI ();
		backGroundMaterial = backGroundRender.material;
		arCamTexture = (Texture2D) backGroundMaterial.GetTexture ("_MainTex");
		CurrentOnHitTarget = null;
		// add in five fix point for texture to check black screen
		//PointOne = new Vector2 (0,0);
		//PointTwo = new Vector2 (arCamTexture.width-1,0);
	//	PointThree = new Vector2 (arCamTexture.width-1,arCamTexture.height-1);
		//PointFour = new Vector2 (0,arCamTexture.height-1);
		PointFive = new Vector2 (arCamTexture.width/2,arCamTexture.height/2);
		//listOfPointForCheckingBlackScreen.Add (PointOne);
		//listOfPointForCheckingBlackScreen.Add (PointTwo);
		//listOfPointForCheckingBlackScreen.Add (PointThree);
		//listOfPointForCheckingBlackScreen.Add (PointFour);
		listOfPointForCheckingBlackScreen.Add (PointFive);
		numberOfBlackPixel = listOfPointForCheckingBlackScreen.Count;

	}



	/// <summary>
	/// inital generate all plain for 3d world tips use;
	/// turn off the first 3d letter tip
	/// </summary>
	private void GenerateNecessaryPlainForDisplay3dWordTipsAndUI ()	
	{
		int i = 0;
		float gapFor3D = 0;
		float gapForUI = 0;
		while (i < 5)
		{
			// for Word tip
			//Material material = new Material(Shader.Find("Transparent/VertexLit with Z"));
			Material material = new Material(Shader.Find("Legacy Shaders/Transparent/Bumped Diffuse"));
			GameObject plain = Instantiate(instantiateLetterBuddyPlane);
			plain.GetComponent<MeshRenderer>().material = material;
			tipFor3dWord tipdata = new tipFor3dWord (plain,material,letterBuddyPlaneParent);
			if (i == 0) {
				plain.SetActive (false);
			}
			letterTipData.Add (tipdata);
			plain.transform.parent = letterBuddyPlaneParent.transform;
			plain.transform.localPosition = new Vector3 (gapFor3D,0,0);

			gapFor3D += gapForEach3Dtip;

			/// for UI Tip
			GameObject uiTip = Instantiate(instantiateLetterBuddyPlaneForUi);
			UITexture thisTexture = uiTip.GetComponent<UITexture> ();
			letterTipDataForUi.Add (thisTexture);
			uiTip.transform.parent = uiletterBuddyPlaneParent.transform;
			uiTip.transform.localPosition = new Vector3 (gapForUI,0,0);
			uiTip.transform.eulerAngles = new Vector3 (0,0,0);
			gapForUI += widthForUiTip;
			i ++;
		}
	}
	#endregion
		
	#region Main Flow Function Call By ray hit
	/// <summary>
	/// step 1
	/// when first time hit with ray
	/// set up the start up letter for tips
	/// reset collusion object for incoming new ray hit
	/// </summary>
	/// <param name="objectHit">Object hit.</param>
	public void SetUpWordSelection (Transform objectHit)
	{
		Debug.Log ("intialStartLetterForWord" + intialStartLetterForWord + "IsCorrect" + IsCorrect);
		if (!intialStartLetterForWord && !IsCorrect) {

			ResetWordTip ();

			isBlackScreen = false;
			gameMode2CoreonTrackFunction.maxWordCount = 0;
			SetStatusForTipParentObject (true);
			if (objectHit.transform.parent != null) {
				Debug.Log ("set up first shot word hit : " + objectHit.name);
				letterBuddyPlaneParent.transform.parent = objectHit.transform.parent.transform;
				letterBuddyPlaneParent.transform.localScale = scaleForWorld;
				letterBuddyPlaneParent.transform.localPosition = Vector3.zero;
				letterBuddyPlaneParent.transform.localEulerAngles = Vector3.zero;
				CurrentOnHitTarget = objectHit.transform.parent;
				SwitchBetwenUITipsAndWorldTips ();
				List< string>referenceList  = mSystem.setupHeader (objectHit.name);
				referenceForCurrentListOfWord = new List<string> ();
					foreach (string data in referenceList) {
					referenceForCurrentListOfWord.Add (data);
					}
				onRayHitLetter = objectHit.name;
				onHitRayResult = objectHit.name;
				alphabetGameMode2UISystem.dictLabel.text = onHitRayResult;
				mSystem.isChangeAllowed = false;
			}
			Display3dWordAndUITip (GetNextTip ());
			if (blackScreenFunction == null) {
				blackScreenFunction = StartCoroutine (ReturnWhetherScreenDarkorNot ());
			}
			GetCloseHeaderFromList (objectHit.name);
			intialStartLetterForWord = true;
			shotForGameModeTwo.CollusionObject = null;
			if (mSystem.result_spelling.Length > 1) {;
				MatchLetterWithWord (mSystem.result_spelling);
			}
		}

	}


	/// <summary>
	/// step 2
	/// if ray hit letter update current tip match with incoming letter
	/// </summary>
	public void MatchLetterWithWord (string objectHit)
	{
		Debug.Log ("MatchLetterWithWord" + intialStartLetterForWord);
		if(intialStartLetterForWord){
			string temp = objectHit;
			Debug.Log ("matching  : " +temp);
			if (GetCloseHeaderFromList (temp)) {
				onHitRayResult = temp;
				Debug.Log ("label  : " +onHitRayResult);

			}
		}
	}

	/// <summary>
	/// step 2
	/// if ray hit letter update current tip match with incoming letter
	/// </summary>
	/*public void MatchLetterWithWord (Transform objectHit)
	{
		if(intialStartLetterForWord){
			string temp = onHitRayResult.Insert(onHitRayResult.Length ,objectHit.name);
			Debug.Log ("matching  : " +temp);
			if (GetCloseHeaderFromList (temp)) {
				onHitRayResult = temp;
				Debug.Log ("label  : " +onHitRayResult);
 
			}
		}
		intialStartLetterForWord = true;
	}*/
	#endregion	


	#region Return Value Function Call by Main Flow 

	/// <summary>
	/// step 3
	/// Gets the close word from list.
	/// </summary>
	/// <param name="currentSpelling">Current spelling.</param>
	private bool GetCloseHeaderFromList (string currentSpelling)
	{
		Debug.Log (referenceForCurrentListOfWord.Count);
		if (referenceForCurrentListOfWord.Count > 0) 
		{
			foreach (string word in referenceForCurrentListOfWord) 
			{
				if(DoWordContainLetter(currentSpelling,word))
				{
					string HitLetterReference = ReturnRemainHitLetterForAWord (currentSpelling,word);
					alphabetGameMode2UISystem.dictLabel.text = HitLetterReference;
					if (HitLetterReference.Equals (word)) 
					{
						Debug.Log ("hit2");
						alphabetGameMode2UISystem.OnWordHitPass (word);
						StopCheckBlackScreen ();
						SetStatusForTipParentObject (false);
						alphabetGameMode2UISystem.On3dObjClick ();
						IsCorrect = true;
					}
				
						Display3dWordAndUITip (word);

					return true;
				}
			}
		}
		return false;
	}


	/// <summary>
	/// step 3
	/// Gets the close word from list.
	/// </summary>
	/// <param name="currentSpelling">Current spelling.</param>
	private string returnCloseStringFromList (string currentSpelling)
	{
		if (referenceForCurrentListOfWord.Count > 0) 
		{
			foreach (string word in referenceForCurrentListOfWord) 
			{
				if(DoWordContainLetter(currentSpelling,word))
				{
					string HitLetterReference = ReturnRemainHitLetterForAWord (currentSpelling,word);
					Debug.Log (" GetCloseLetter for matching : " + HitLetterReference);
					alphabetGameMode2UISystem.dictLabel.text = HitLetterReference;

					//Display3dWordAndUITip(word);
					return HitLetterReference;
				}
			}
		}
		return "";
	}
	/// <summary>
	/// step 4
	/// Doess the word contain letter.
	/// compare currentspelling with tip pool
	/// if currentspelling have letter in order with target word return true 
	/// </summary>
	/// <returns><c>true</c>, if word contain letter was done, <c>false</c> otherwise.</returns>
	/// <param name="currentSpelling">Current spelling.</param>
	/// <param name="word">Word.</param>
	private bool DoWordContainLetter (string currentSpelling , string word) 
	{
		if (string.IsNullOrEmpty (currentSpelling) || string.IsNullOrEmpty (word)) 
		{
			return false;
		}
		//Debug.Log (word + " : " + currentSpelling);
		char [] _currentSpelling = currentSpelling.ToCharArray ();
		char [] _word = word.ToCharArray ();
		List<char> listOfWord = new List<char>();
		foreach (char letter in _word) 
		{
			listOfWord.Add (letter);

		}

	//	int index = 0;
		int numberOfMatch = 0;
		int _currentSpellingLenght = _currentSpelling.Length;
		int _wordLenght = _word.Length;
		if(_currentSpellingLenght <=_wordLenght){
			for (int i = 0; i < _currentSpellingLenght; i++) {
		//	Debug.Log (_currentSpelling [i] + " : " + _word [i]);
				if (_currentSpelling [i].Equals (_word [i])) {

				numberOfMatch++;
				}
			}
		}
		/*while (index < _currentSpellingLenght) {
			bool isFind = false;
			foreach( char letter in listOfWord) {
			//foreach (char letter in _word) {
				if (letter.Equals (_currentSpelling [index]) && !isFind) {
					numberOfMatch++;
					listOfWord.Remove (letter);
					isFind = true;

				}

			}*/
		//	index++;
		///}
		if (numberOfMatch == _currentSpellingLenght ) {

			return true;
		}
		return false;
	}
		



	public void SetStatusForTipParentObject (bool status) 
	{
		letterBuddyPlaneParent.SetActive (status);
		uiletterBuddyPlaneParent.SetActive (status);
	}

	/// <summary>
	/// call Function
	/// Display3ds the word tip.
	/// </summary>
	private void Display3dWordAndUITip (string word)
	{
		Debug.Log (" tip Display  " + word);
		char[] letter = word.ToCharArray ();
		for(int i = 1 ; i < letter.Length; i ++ ){
			letterTipData[i].SetMaterial(ReturnTargetLetterBuddyTexture (letter[i].ToString()));
			letterTipData[i].SetColor();
			letterTipData [i].Enable ();
		}
		if(letter.Length < letterTipData.Count)
		{
			for(int j = letter.Length; j < letterTipData.Count; j ++)
			{
				letterTipData [j].Disable ();
			}
		}

		for(int p = 0; p < letter.Length; p ++ )
		{
			letterTipDataForUi[p].mainTexture = ReturnTargetLetterBuddyTexture (letter[p].ToString());
			letterTipDataForUi[p].width = widthForUiTip;
			letterTipDataForUi[p].height = heightForUiTip;
			letterTipDataForUi [p].alpha = 1;

		}
		if(letter.Length < letterTipDataForUi.Count)
		{
			for(int j = letter.Length; j < letterTipDataForUi.Count; j ++)
			{
				letterTipDataForUi [j].alpha = 0;
			}
		}

		if (letterBuddyPlaneParent.transform.parent != null) {
			letterBuddyPlaneParent.transform.parent.GetComponent<AlphabetCoreTrackableEventHandler> ().ResetActiveTrackableEvent ();
		}
		float positionForUITip = -(letter.Length * initalGapForUiTips);
		uiletterBuddyPlaneParent.transform.localPosition = new Vector2(positionForUITip,uiletterBuddyPlaneParent.transform.localPosition.y);
	
	}


	/// <summary>
	/// call Function
	/// return letter buddy texture 
	/// </summary>
	/// <returns>The target letter buddy texture.</returns>
	/// <param name="letter">Letter.</param>
	private Texture ReturnTargetLetterBuddyTexture (string letter) {
		foreach (Texture texture in letterBuddyTexture) 
		{
			if(texture.name.Contains(letter.ToLower()))
			{
				return texture;
			}

		}
		return null;
	}

	/// <summary>
	/// call Function
	/// Returns the On Hit letter for A word.
	/// </summary>
	/// <returns>The remain missing letter for A word.</returns>
	/// <param name="currentSpelling">Current spelling.</param>
	/// <param name="tipForNow">current Tip for display.</param>

	private string ReturnRemainHitLetterForAWord (string currentSpelling ,string tipForNow) 
	{
		string hitLetter = "";

		if (!string.IsNullOrEmpty(currentSpelling)) 
		{
			Debug.Log ("current Spelling : " + currentSpelling + " : tipFor now : " + tipForNow);
 			string space = "_";
			char[] stringSpaceToChar = space.ToCharArray ();
			char[] _currentSpellingArray = currentSpelling.ToCharArray ();
			char[] _tipForNow = tipForNow.ToCharArray (); 

			foreach (char _currentTipLetter in _tipForNow)
			{
				hitLetter += space;
			}

			foreach (char _currentSpellLetter in _currentSpellingArray) 
			{
				bool isMatch = false;
				for(int i =0; i < _tipForNow.Length && !isMatch; i ++)
				{
					if (_currentSpellLetter.Equals (_tipForNow [i])) 
					{
						char[] _hitLetterToChar = hitLetter.ToCharArray ();
						if (_hitLetterToChar [i].Equals (stringSpaceToChar[0])) {
							hitLetter = hitLetter.Remove (i, 1);
							hitLetter = hitLetter.Insert (i, _tipForNow [i].ToString ());
							isMatch = true;
						}
							
					} 
				}

			}
		}
		Debug.Log (" hit letter " + hitLetter);
		return hitLetter;

	}


	/// <summary>
	/// auto wait For Letter to Reset and check is it correct.
	/// </summary>
	/// <returns>The whether screen darkor not.</returns>
	private IEnumerator WaitForWhileTillResetCurrentScannedWord () 
	{

		while (true) 
		{
			yield return new WaitForSeconds (4f);
			gameMode2CoreonTrackFunction.GetCurrentScaningLetter ();
			break;
	
		}


	}
		
	/// <summary>
	/// Returns  whether screen dark or not.
	/// </summary>
	/// <returns>The whether screen darkor not.</returns>
	private IEnumerator ReturnWhetherScreenDarkorNot () 
	{

		while (true) 
		{
			yield return new WaitForEndOfFrame ();
			/*int indexForCheckBlackPixel = 0;
			//arCamTexture = (Texture2D)backGroundMaterial.GetTexture ("_MainTex");
			 tex = new Texture2D(2,2);
			// Read screen contents into the texture
	
			backGroundCurrentColour = new List<Color> ();
			foreach (Vector2 position in listOfPointForCheckingBlackScreen) {
				tex.ReadPixels(new Rect((int)position.x, (int)position.x, 1, 1), 0, 0);
				tex.Apply();	
	
				Color getcolour = tex.GetPixel (0, 0);
				backGroundCurrentColour.Add (getcolour);
				Debug.Log (getcolour.r + " : " +getcolour.b + " : "+ getcolour.g);
				if (getcolour.r < 0.25f && getcolour.b < 0.25f && getcolour.g < 0.25f) 
				{
					indexForCheckBlackPixel++;
				}
			}*/

			if (!isAnyTrack() &&  IsFlat ()) {

				isBlackScreen = true;

			} else {

				isBlackScreen = false;
	

			}
			SwitchBetwenUITipsAndWorldTips ();
		}
				

	}

	/// <summary>
	/// Starts the auto reset. if previous word is right then as long as the next scan letter is different to what we have now reset
	/// </summary>
	/// <param name="tmr">Tmr.</param>
	public void StartAutoReset (string tmr)
	{
		
		if (tmr.Equals(previousCorrectWord) || !IsCorrect) {
			return;
		}
		ResetWordTip (true);

	}



	/// <summary>
	/// Determines is gycro flat.
	/// </summary>
	/// <returns><c>true</c> if this instance is flat; otherwise, <c>false</c>.</returns>
	public bool IsFlat ()
	{

		//if (Input.gyro.attitude.x < 0.004f && Input.gyro.attitude.x > -0.004f) {
		if (Input.gyro.attitude.x < 0.03f && Input.gyro.attitude.x > -0.03) {
//			Debug.Log ("enter screen mode");
			return isBlackScreenShowUp ();
		} else {
			//Debug.Log ("reset screen mode");
			countTimerForBlackScreen = 0;
			return false;
		}


	}


	// determine is black Screen
	public bool isBlackScreenShowUp ()
	{
		if (countTimerForBlackScreen < maxTimerForBlackScreen) 
		{
			countTimerForBlackScreen += Time.deltaTime;
		} else {

			return true;
		}
		return false;

	}



	public bool isAnyTrack ()
	{
		foreach (Transform trackevent in frameTargetFree.transform) {
			if (trackevent.gameObject.GetComponent<AlphabetCoreTrackableEventHandler> ().isTrack) {
				return true;
			}
		}
		foreach (Transform trackevent in frameTarget.transform) {
			if (trackevent.gameObject.GetComponent<AlphabetCoreTrackableEventHandler> ().isTrack) {
				return true;
			}
		}
		return false;
	}





	/// <summary>
	/// Switchs the betwen user interface tips and world tips.
	/// </summary>
	private void SwitchBetwenUITipsAndWorldTips ()
	{
		if (isBlackScreen) {
			letterBuddyPlaneParent.SetActive (false);
			uiletterBuddyPlaneParent.SetActive (true);
			TurnOnOffUIForUIAndWorldTips (false);
			shotForGamemodetwo.isShotAlready = false;
			shotForGamemodetwo.ChangeGunTexture (0);
		
		} else {

			letterBuddyPlaneParent.SetActive (true);
			uiletterBuddyPlaneParent.SetActive (false);
			TurnOnOffUIForUIAndWorldTips (true);


		}

	}

	private void TurnOnOffUIForUIAndWorldTips (bool status)
	{
		foreach (GameObject ui in  listOfUIOnOff)
		{
			ui.SetActive (status);
		} 


	}

	/// <summary>
	/// Stops the check black screen.
	/// </summary>
	public void StopCheckBlackScreen () {

		if (blackScreenFunction != null) 
		{
			StopCoroutine (blackScreenFunction);
			blackScreenFunction = null;
		}
	}



	/// <summary>
	/// call Function
	/// Gets the next tip.
	/// </summary>
	/// <returns>The next tip.</returns>
	public string  GetNextTip ()
	{
		string nextTip = GetNextHeaderFromList ();
//		Debug.Log ("nextTip : " + nextTip);
		return nextTip;

	}

	/// <summary>
	/// call Function
	/// Gets the previous tip.
	/// </summary>
	/// <returns>The previous tip.</returns>
	public string  GetPreviousTip ()
	{
		string previousTip = GetPreviousHeaderFromList ();
		//Debug.Log ("previousTip : " + previousTip);
		return previousTip;

	}
	#endregion	
		
	#region On Click Button

	/// <summary>
	/// on Click Button 
	/// Removes one letter from matching word.
	/// get the close tip
	/// find the first index that does not contain "_"
	/// remove the first index 
	/// replace the rest with "_"
	/// if the word lenght less than one reset
	/// reassign text to label
	/// </summary>
	public void RemoveOneLetterFromMatchingWord () 
	{
		//Debug.Log ("Click REmove");
	//	if (IsCorrect) {
	//		ResetWordTip ();
		//} else 
	/*	if (!string.IsNullOrEmpty (onHitRayResult) && !IsCorrect) {
			int removeOneCharacter = 0;
			string s = returnCloseStringFromList (onHitRayResult); 
			char[] letter = s.ToCharArray ();
			int lengthForWord = letter.Length;
			bool isFindFirstLetterIndex = false;

			for (int i = letter.Length - 1; i > 0 && !isFindFirstLetterIndex; i--) {
				//	Debug.Log (letter[i].ToString() + " :  " + letter[i].ToString().Equals("_"));
				if (!letter [i].ToString ().Equals ("_")) {
					removeOneCharacter = i;
					isFindFirstLetterIndex = true;
				}
			}
			//	Debug.Log ("index : " + removeOneCharacter);
			if (removeOneCharacter > 0) {
				s = s.Remove (removeOneCharacter);
				string replaceUnderLine = s.Replace ("_", "");
				//Debug.Log ("replaceUnderLine : " + replaceUnderLine);
				onHitRayResult = replaceUnderLine;
			} else {
				onHitRayResult = "";
			}

			Debug.Log ("word : " + onHitRayResult);

			if (string.IsNullOrEmpty (onHitRayResult)) {
				ResetWordTip (true);
				//Debug.Log ("empty ");
				alphabetGameMode2UISystem.dictLabel.text = "";
			} else {
				while (s.Length < lengthForWord) {
					if (s.Length >= 1) {
						s += "_";

					} 

				}
				//Debug.Log ("word ss : " + s);
				alphabetGameMode2UISystem.dictLabel.text = s;
				IsCorrect = false;
			}

		} else {*/

			ResetWordTip (true);
		//}

	}


	/// <summary>
	/// on Click Button 
	/// Removes Word Allow new ray hit to set new Letter header
	/// </summary>
	public void ResetWordTip (bool reactiveOnTrackedMaker = false) 
	{
		Debug.Log ("reset");
		intialStartLetterForWord = false;
		onHitRayResult = "";
		referenceForCurrentListOfWord = new List<string> ();
		Display3dWordAndUITip ("");
		alphabetGameMode2UISystem.dictLabel.text = "";
		alphabetGameMode2UISystem.resetDisplayColorAnddisEnableDisplayObjecr ();
		alphabetGameMode2UISystem.resetIscheckWordBoolean ();
		IsCorrect = false;
		gameMode2CoreonTrackFunction.ResetCurrentLetter ();
		if (coroutineForReScan == null) {
			coroutineForReScan = StartCoroutine (WaitForWhileTillResetCurrentScannedWord ());
		} else {
			StopCoroutine (coroutineForReScan);
			StartCoroutine (WaitForWhileTillResetCurrentScannedWord ());
		}
		mSystem.isChangeAllowed = true;
		if (reactiveOnTrackedMaker) {
			mSystem.OnAlphabeReactiveCurrnetOnTrackedMarker ();
		}

	}
		

	/// <summary>
	/// on Click Button 
	///Gets the next Tip from list.
	/// </summary>
	/// <returns>The next header from list.</returns>
	public string GetNextHeaderFromList ()
	{
		if (referenceForCurrentListOfWord.Count > 0) 
		{
			if (index < referenceForCurrentListOfWord.Count - 1) 
			{
				index++;
				return referenceForCurrentListOfWord [index];
			} else {
				index = 0;
				return referenceForCurrentListOfWord [index];
			}

		}

		return "";
	}

	/// <summary>
	// on Click Button 
	/// Gets the previous Tip from list.
	/// </summary>
	/// <returns>The previous header from list.</returns>
	public string GetPreviousHeaderFromList ()
	{
		if (referenceForCurrentListOfWord.Count > 0) 
		{
			if (index > 0) {
				index--;
				return referenceForCurrentListOfWord [index]; 
			} else {
				index = referenceForCurrentListOfWord.Count-1;
				return referenceForCurrentListOfWord [index];

			}

		}

		return "";
	}

	/// <summary>
	/// on Click Button
	/// display next tip
	/// </summary>
	public void NextTip ()
	{
		Display3dWordAndUITip (GetNextTip());
	}

	/// <summary>
	/// on Click Button
	/// display previous tip
	/// </summary>
	public void PreviousTip ()
	{
		Display3dWordAndUITip (GetPreviousTip ());
	}
	#endregion


}

[System.Serializable]
public class tipFor3dWord {


	public GameObject letterTipObject;
	public Material letterMaterial;
	public GameObject letterTipObjectParent;

	/// <summary>
	/// Set3ds the tip data.
	/// </summary>
	/// <param name="_letterTipObject">Letter tip object.</param>
	/// <param name="_letterMaterial">Letter material.</param>
	/// <param name="_letterTipObjectParent">Letter tip object parent.</param>
	public tipFor3dWord (GameObject _letterTipObject , Material _letterMaterial, GameObject _letterTipObjectParent)
	{
		letterTipObject = _letterTipObject;
		letterMaterial = _letterMaterial;
		letterTipObjectParent = _letterTipObjectParent;
	}

	public void Enable (){
		letterTipObject.SetActive (true);
	}

	public void Disable (){
		letterTipObject.SetActive (false);
	}

	public void SetMaterial (Texture texure){
		letterMaterial.SetTexture ("_MainTex", texure);
	}

	public void SetColor(){
		Color setcolor = new Color (1, 1, 1, 0.2f);
		letterMaterial.SetColor ("_Color", setcolor);
	}

	public void SetColor(Color setcolor ){
		letterMaterial.SetColor ("_Color", setcolor);
	}

	public Texture ReturnCurrentAttachTexture (){
		return letterMaterial.GetTexture ("_MainTex");
	}
	public bool IsActive () {
		if (letterTipObject.activeSelf) {
			return true;
		}
		return false;
	}

}

