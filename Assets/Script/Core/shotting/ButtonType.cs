﻿using UnityEngine;
using System.Collections;

public class ButtonType : MonoBehaviour {

	public ButtonInteraction.buttonType typeOfButton = ButtonInteraction.buttonType.None;
	public ButtonInteraction buttonInteraction;


	private void Start()
	{
		buttonInteraction = GameObject.Find("GameMode1Controller").GetComponent<ButtonInteraction>();
	}

	public void OnCallAction()
	{

		buttonInteraction.OnClickCallForGameModeChange(this.gameObject.transform);

	}
}
