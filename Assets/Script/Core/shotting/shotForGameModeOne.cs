﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class shotForGameModeOne : MonoBehaviour {

	//game mode 1 
	[Header("Game mode One Core Script  for game mode 1")]
	public GameMode1CoreonTrackFunction gameMode1CoreonTrackFunction;
	public GameObject projectile; // bullet type
	public GameObject arCam;//arCam
	public GameObject initalRayCastEndPointObject; // ray bullet type object shot object toward end distance
	public GameObject targetAimTexture; // the aim texture for ray bullet to shot from
	public GameObject particle;
	public Animator gunAnimation;
	public static GameObject CollusionObject = null;
	public static bool BulletReachEndPoint = false;// on press shot Coroutine reference
	public GameObject bulletShotStartPoint;//arCam
	public List<Texture> bubbleGunTexture;


	private bool isShotAlready = false;
	private GameObject endRayPoint;//raycat end Point
	private RaycastHit hit; //hit ray
	private float distance = 3000; // distance for ray
	private GameObject previousDisabledLetterBuddy;// a reference to track previous tracked letter buddy to that we can enable it back
	/// <summary>
	/// three game mode for game mode 1
	/// </summary>
	private const string btn_pencil = "btn_pencil";
	private const string btn_phonics = "btn_phonics";
	private const string btn_word = "btn_word";
	private const string btn_platform = "platform";
	private string currentSelectedMode ="";
	private Material bubbleGunMaterial;
	private float counterFornoAction = 0;
	private float maxTimerForNoAction = 10;





	/// <summary>
	/// inital end ray point by shotting ray bullet from aim texture toward target distance to ensure there are strange long  ray line for hit collision 
	/// </summary>
	/// 
	private void Start ()
	{
		endRayPoint = Instantiate(initalRayCastEndPointObject) as GameObject;
		endRayPoint.transform.parent = targetAimTexture.transform;
		endRayPoint.transform.localPosition = new Vector3 (0, 0,0);
		endRayPoint.transform.localScale = new Vector3 (1f,1f,1f);
		endRayPoint.transform.localEulerAngles = Vector3.zero;
		endRayPoint.transform.localPosition = Vector3.forward * 10000;
		CollusionObject = null;
		BulletReachEndPoint = false;
		bubbleGunMaterial = gunAnimation.gameObject.GetComponent<MeshRenderer>().material;
	} 
		
	/// <summary>
	/// keep shotting ray cast to check is there any mark on hit 
	/// </summary>
	void Update ()
	{
		RayCastForOutLine ();
		isAnyAction ();
	}

	/// <summary>
	/// ray cast for showing blue layout if hit letter either just change aim texture colourß;
	/// </summary>
	private void RayCastForOutLine ()
	{

		Vector3 raycastDir = endRayPoint.transform.position - arCam.transform.position;
		bool isHit = Physics.Raycast (arCam.transform.position, raycastDir, out hit, distance);
		Debug.DrawRay (arCam.transform.position, raycastDir,Color.green);
		if (isHit) 
		{
			Transform objectHit = hit.transform;
		
			if (objectHit.GetComponent<AlphabetCoreTrackableEventHandler> () != null && objectHit.gameObject.name.Contains ("FrameMarker") && objectHit.GetComponent<AlphabetCoreTrackableEventHandler> ().isTrack) {
				gameMode1CoreonTrackFunction.setCurrentOnTrackLetter (objectHit.gameObject, true);
			} else {
				gameMode1CoreonTrackFunction.setCurrentOnTrackLetter (objectHit.gameObject, false);
			}

		} else {
				gameMode1CoreonTrackFunction.resetCurrentLetter ();
		}

	}



	/// <summary>
	/// on Click Shot
	/// set it to gun parent then reset it to gun position
	/// then set it to arCam  so that we are using position related to arCam rather than gun
	/// reset the scale and rotation 
	/// fire the bullet
	/// </summary>
	public void OnPressShotting () 
	{
		if (!isShotAlready)
		{
		//	Debug.Log ("on press shot");
			gunAnimation.ResetTrigger ( "isShot" );
			gunAnimation.SetBool ("isShot", true);
			CollusionObject = null;
			GameObject bullet = Instantiate (projectile) as GameObject;
			bullet.transform.parent = bulletShotStartPoint.transform; 
			bullet.transform.localPosition = new Vector3 (0, 0, 3);	
			bullet.transform.localEulerAngles = Vector3.zero;	
			bullet.transform.parent = null;
			bullet.transform.localScale = new Vector3 (0.2f, 0.2f,0.2f);
			bullet.GetComponent<Rigidbody>().AddForce(transform.up *1000);

			isShotAlready = true;
			StartCoroutine (MovingTowardTarget (bullet));
			ChangeGunTexture (bubbleGunTexture.Count-1);
		}

	}



	/// <summary>
	/// 
	/// move bullet toward center point
	/// start moving upward then once it reach half distance than move downward 
	///  using xSubstractConstant and ySubstractConstant to control movement
	/// </summary>
	/// <returns>The toward target.</returns>
	/// <param name="bullet">Bullet.</param>
	private IEnumerator MovingTowardTarget (GameObject bullet) 
	{
	//	Debug.Log ("start move bullet to target end point");

		//int index = bubbleGunTexture.Count-2;
		StartCoroutine (GunTextureChange());
		while (true) {
			/*if(index > 0){
				yield return new WaitForSeconds (0.3f);
				ChangeGunTexture (index);
				index--;
		
			}*/
			yield return new WaitForEndOfFrame ();
		
			if (BulletReachEndPoint) {
			//	Debug.Log (" force stop due to bullet either destory or detect item");
				gunAnimation.SetBool ("isShot", false);
				BulletCollusionReaction ();

				//isShotAlready = false;
				BulletReachEndPoint = false;
				//ChangeGunTexture ();
				break;
			} 
			else if (bullet == null || CollusionObject != null) {

		//		Debug.Log ("stop due to bullet either destory or detect item");
				gunAnimation.SetBool ("isShot", false);
				BulletCollusionReaction ();
				//isShotAlready = false;
				//ChangeGunTexture ();;
				break;

			}
		}

	}
		
	/// <summary>
	/// Guns the texture change.
	/// </summary>
	/// <returns>The texture change.</returns>
	public IEnumerator GunTextureChange  ()
	{
		Debug.Log ("gun change TExture");
		int index = bubbleGunTexture.Count-1;
		while (true) {
			if (index > -1) {
				yield return new WaitForSeconds (0.5f);
				ChangeGunTexture (index);
				index--;

			} else {
				isShotAlready = false;
				break;

			}
		}

	}
	/// <summary>
	/// return bool is hit space ship or not
	/// </summary>
	/// <returns><c>true</c>, if hit space was ised, <c>false</c> otherwise.</returns>
	/// <param name="objectHit">Object hit.</param>
	private bool isHitSpace (Transform objectHit)
	{


		if (objectHit.gameObject.layer == 10) {

			//Debug.Log (" hit space ship" + objectHit.name + " layer : " + objectHit.gameObject.layer );
			return true;

		} else {
			//Debug.Log ("  not hit space ship" + objectHit.name + " layer : " + objectHit.gameObject.layer );
			return false;
		}

	}

	/// <summary>
	/// ray cast to hit letter buddy and letter interaction
	/// </summary>
	private void BulletCollusionReaction ()
	{

		if (CollusionObject == null) {
			return;
		}
		
		Transform objectHit = CollusionObject.transform;
//		Debug.Log (" hit target : " + objectHit.transform.gameObject.name + " layer " +  objectHit.gameObject.layer);

		if (!isHitSpace(objectHit)) {

			gameMode1CoreonTrackFunction.SetReferenceCurrentDisplayLetter (CollusionObject.gameObject);
			gameMode1CoreonTrackFunction.ResetAllMode ();
			//particle.transform.parent = objectHit.transform;
			//particle.transform.localPosition = new Vector3 (0, 0, 0);
			//particle.transform.localScale = new Vector3 (2, 2, 2);
			//particle.transform.localEulerAngles = Vector3.zero;
			//particle.gameObject.SetActive (true);

			//	CollusionObject.gameObject.SetActive(false);
			//	CollusionObject.gameObject.SetActive(true);

				StartCoroutine (onCallHitObject (objectHit, 0, 0f, true));
			} else {

				StartCoroutine (onCallHitObject (objectHit, 0f, 0f));
			}

	}



	/// <summary>
	/// on call ray hit trasfer ar parent to current marker, disable letter buddy if letter is different
	/// on call ray hit change game mode and on particle
	/// </summary>
	/// <returns>The call hit object.</returns>
	/// <param name="objectHit">Object hit.</param>
	/// <param name="timeObjectApear">Time object apear.</param>
	/// <param name="timeForParticleOff">Time for particle off.</param>
	private IEnumerator onCallHitObject (Transform objectHit , float timeObjectApear, float timeForParticleOff  , bool isDifferent = false){

		yield return new WaitForSeconds (timeObjectApear);
		if (isDifferent) {
			currentSelectedMode = "";
			OnClickCallForFrameMarker (objectHit);
			yield return new WaitForSeconds (timeForParticleOff);
			//particle.gameObject.SetActive (false);
		} else {
			OnClickCallForGameModeChange (objectHit);
		}

	}

	/// <summary>
	/// ray cast hit interaction
	/// attach ar parent to letter buddy
	/// on Click Letter function
	/// disable  Letter Buddy so that only show ar Parent
	/// </summary>
	/// <param name="objectHit">Object hit.</param>
	private void OnClickCallForFrameMarker (Transform objectHit)
	{
		Debug.Log ("set up spare ship toward target : " + objectHit.gameObject.name);
		if(objectHit.GetComponent<AlphabetCoreTrackableEventHandler>() != null)
		{
			AlphabetCoreTrackableEventHandler clickFunction = objectHit.GetComponent<AlphabetCoreTrackableEventHandler> ();
			if (gameMode1CoreonTrackFunction != null) 
			{
				gameMode1CoreonTrackFunction.OnArParentPositionUpdate (objectHit.gameObject);
			}
			if (clickFunction.isClickable) 
			{
				clickFunction.OnClick ();
			}
			OnEnableAndDisableLetterBuddy (objectHit);
		}

	}

/// <summary>
/// Is there any action.
/// </summary>
	public void isAnyAction ()
	{

		if (!Input.GetMouseButton (0)) {
			if (counterFornoAction < maxTimerForNoAction) {
				counterFornoAction += Time.deltaTime;
			} else {
				gameMode1CoreonTrackFunction.OnOffSpaceShip (false);
				if (previousDisabledLetterBuddy != null) {
					previousDisabledLetterBuddy.SetActive (true);
					previousDisabledLetterBuddy.transform.parent.GetComponent<BoxCollider> ().enabled = true;
					previousDisabledLetterBuddy = null;
				}
			}
		} else {

			counterFornoAction = 0;
		}

	}

	/// <summary>
	/// disable and enable letter buddy base on tracker
	/// </summary>
	/// <param name="objectHit">Object hit.</param>
	private void OnEnableAndDisableLetterBuddy (Transform objectHit)
	{
		if (previousDisabledLetterBuddy == null) {
			previousDisabledLetterBuddy = objectHit.GetChild (0).gameObject;
			previousDisabledLetterBuddy.SetActive (false);
			previousDisabledLetterBuddy.transform.parent.GetComponent<BoxCollider> ().enabled = false;
			DestroyImmediate (previousDisabledLetterBuddy.transform.parent.GetComponent<Rigidbody> ());
			Debug.Log (" first Time turn off the following letter : " + previousDisabledLetterBuddy );
		} else if(!previousDisabledLetterBuddy.name.Equals(objectHit.GetChild (0).gameObject.name))
		{
			previousDisabledLetterBuddy.SetActive (true);
			previousDisabledLetterBuddy.transform.parent.GetComponent<BoxCollider> ().enabled = true;
			Rigidbody rig = previousDisabledLetterBuddy.transform.parent.gameObject.AddComponent<Rigidbody> ();
			rig.constraints = RigidbodyConstraints.FreezeAll;
			Debug.Log ("turn on previous letter " + previousDisabledLetterBuddy );
			previousDisabledLetterBuddy = objectHit.GetChild (0).gameObject;
			previousDisabledLetterBuddy.transform.parent.GetComponent<BoxCollider> ().enabled = false;
			DestroyImmediate (previousDisabledLetterBuddy.transform.parent.GetComponent<Rigidbody> ());
			previousDisabledLetterBuddy.SetActive (false);
			Debug.Log ("turn off following letter : " + previousDisabledLetterBuddy );
		}

	}


	/// <summary>
	/// change game mode base one ray cast hit object
	/// </summary>
	/// <param name="objectHit">Object hit.</param>
	private void OnClickCallForGameModeChange(Transform objectHit)
	{
		/*if(objectHit.GetComponent<OnObjClickSendEvent>() != null)
		{

			OnObjClickSendEvent clickFunction = objectHit.GetComponent<OnObjClickSendEvent> ();
			EventDelegate.Execute (clickFunction.onClick);

		}*/
		Debug.Log (objectHit.gameObject.name + "current mode is : " + currentSelectedMode);
		// if there is no selection from any button or is not hit platform
		if (string.IsNullOrEmpty (currentSelectedMode) || !objectHit.gameObject.name.Contains(btn_platform)) {
			switch (objectHit.gameObject.name) {

			case btn_pencil:
				gameMode1CoreonTrackFunction.OnEnableTextAndStroke ();
				currentSelectedMode = btn_pencil;
				break;
			case btn_phonics: 
				gameMode1CoreonTrackFunction.OnEnablePhonics ();
				currentSelectedMode = btn_phonics;
				break;
			case btn_word: 
				gameMode1CoreonTrackFunction.OnEnable3DLetter ();
				currentSelectedMode = btn_word;
				break;	

			}
		} else {
			// hit platform but not buttom
			switch (currentSelectedMode) {

			case btn_pencil:
				gameMode1CoreonTrackFunction.OnEnableTextAndStroke ();
				//currentSelectedMode = btn_pencil;
				break;
			case btn_phonics: 
				gameMode1CoreonTrackFunction.OnEnablePhonics ();
				//currentSelectedMode = btn_phonics;
				break;
			case btn_word: 
				gameMode1CoreonTrackFunction.OnEnable3DLetter ();
				//currentSelectedMode = btn_word;
				break;	

			}


		}

	}

	/// <summary>
	/// Changes the gun texture.
	/// </summary>
	/// <param name="index">Index.</param>
	public void ChangeGunTexture (int index = 0)
	{
		bubbleGunMaterial.SetTexture ("_MainTex",bubbleGunTexture[index]);
	}




}
