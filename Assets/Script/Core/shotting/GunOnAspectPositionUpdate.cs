﻿using UnityEngine;
using System.Collections;

public class GunOnAspectPositionUpdate : MonoBehaviour 
{
	public Vector3 sixteenNine;
	public Vector3 threeTwo;
	public Vector3 fourThree;
	// Use this for initialization
	void Start () 
	{
		if (Camera.main.aspect >= 1.7)
		{
			Debug.Log("16:9");
			this.gameObject.transform.localPosition = sixteenNine;
		}
		else if (Camera.main.aspect >= 1.5)
		{
			Debug.Log("3:2");
			this.gameObject.transform.localPosition = threeTwo;
		}
		else
		{
			Debug.Log("4:3");
			this.gameObject.transform.localPosition = fourThree;
		}

	}
	

}
