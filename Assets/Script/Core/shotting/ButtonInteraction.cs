﻿using UnityEngine;
using System.Collections;

public class ButtonInteraction : MonoBehaviour {


	public enum buttonType {btn_pencil,btn_phonics,btn_word,platform,None };
	public buttonType butttonID;

	public GameMode1CoreonTrackFunction gameMode1CoreonTrackFunction;
	private const string btn_pencil = "btn_pencil";
	private const string btn_phonics = "btn_phonics";
	private const string btn_word = "btn_word";
	private const string btn_platform = "platform";
	private string currentSelectedMode = "";

	public void OnClickCallForGameModeChange(Transform objectHit)
	{
		/*if(objectHit.GetComponent<OnObjClickSendEvent>() != null)
		{

			OnObjClickSendEvent clickFunction = objectHit.GetComponent<OnObjClickSendEvent> ();
			EventDelegate.Execute (clickFunction.onClick);

		}*/
		Debug.Log(objectHit.gameObject.name + "current mode is : " + currentSelectedMode);
		// if there is no selection from any button or is not hit platform
		if (string.IsNullOrEmpty(currentSelectedMode) || !objectHit.gameObject.name.Contains(btn_platform))
		{
			switch (objectHit.gameObject.name)
			{

				case btn_pencil:
					gameMode1CoreonTrackFunction.OnEnableTextAndStroke();
					currentSelectedMode = btn_pencil;
					break;
				case btn_phonics:
					gameMode1CoreonTrackFunction.OnEnablePhonics();
					currentSelectedMode = btn_phonics;
					break;
				case btn_word:
					gameMode1CoreonTrackFunction.OnEnable3DLetter();
					currentSelectedMode = btn_word;
					break;

			}
		}
		else {
			// hit platform but not buttom
			switch (currentSelectedMode)
			{

				case btn_pencil:
					gameMode1CoreonTrackFunction.OnEnableTextAndStroke();
					//currentSelectedMode = btn_pencil;
					break;
				case btn_phonics:
					gameMode1CoreonTrackFunction.OnEnablePhonics();
					//currentSelectedMode = btn_phonics;
					break;
				case btn_word:
					gameMode1CoreonTrackFunction.OnEnable3DLetter();
					//currentSelectedMode = btn_word;
					break;

			}


		}

	}
}
