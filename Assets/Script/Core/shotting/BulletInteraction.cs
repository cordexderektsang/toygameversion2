﻿using UnityEngine;
using System.Collections;

public class BulletInteraction : MonoBehaviour {
	private Coroutine timerForDestory;
	private bool isHit = false;

	private void Start ()
	{
		timerForDestory = StartCoroutine (StartTimer());
	}

	/// <summary>
	/// if too long not collusion destory it then
	/// </summary>
	/// <returns>The timer.</returns>
	private IEnumerator StartTimer ()
	{	
		yield return new WaitForSeconds (1f);
		Debug.Log("dead");
		shotForGameModeOne.BulletReachEndPoint = true;
		shotForGameModeOne.CollusionObject = null;
		DestroyImmediate (this.gameObject);
	
	}

	/// <summary>
	/// what did bullet hit for first target
	/// </summary>
	/// <param name="col">Col.</param>
	void OnCollisionEnter (Collision col)
	{
		//if (!isHit) {
			Debug.Log (col.gameObject.name);
			shotForGameModeOne.CollusionObject = col.gameObject;
			isHit = true;
			Destroy (gameObject,0.2f);
		//}
	
	}
		
}
