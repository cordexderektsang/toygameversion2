﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class shotForGameModeTwo : MonoBehaviour {

	[Header("Game mode Two Core Script  for game mode 2")]
	public GameMode2CoreonTrackFunction gameMode2CoreonTrackFunction;
	public AlphabetGameMode2UISystem alphabetGameMode2UISystem; 
	public GyroController gyroController;
	public TipsForOnshot tipsForOnshot;

	public GameObject projectile; // bullet type
	public GameObject arCam;//arCam
	public GameObject bulletShotStartPoint;//arCam
	public GameObject initalRayCastEndPointObject; // ray bullet type object shot object toward end distance
	public GameObject targetAimTexture; // the aim texture for ray bullet to shot from
	public Animator gunAnimation;
	public GameObject particle;
	public Vector3 positionToShotForward;
	public List<Texture> bubbleGunTexture;
	public static GameObject CollusionObject = null;
	public static bool BulletReachEndPoint = false;// on press shot Coroutine reference

	public bool isShotAlready = false;

	private GameObject target;//raycat end Point
	private RaycastHit hit; //hit ray
	private float distance = 3000; // distance for ray
	private Transform rayHitOutLayObject;
	private bool isStartUpReady = false;
	private GameObject endRayPoint;
	private Material bubbleGunMaterial;


	/// <summary>
	/// inital end ray point by shotting ray bullet from aim texture toward target distance to ensure there are strange long  ray line for hit collision 
	/// </summary>
	/// 
	public void Start ()
	{

		endRayPoint = Instantiate(initalRayCastEndPointObject) as GameObject;
		endRayPoint.transform.parent = targetAimTexture.transform;
		endRayPoint.transform.localPosition = new Vector3 (0, 0,0);
		endRayPoint.transform.localScale = new Vector3 (1f,1f,1f);
		endRayPoint.transform.localEulerAngles = Vector3.zero;
		endRayPoint.transform.localPosition = Vector3.forward * 10000;
		endRayPoint.transform.localPosition = new Vector3 (220, 0, endRayPoint.transform.localPosition.z);
		target = endRayPoint;
		CollusionObject = null;
		BulletReachEndPoint = false;
		positionToShotForward = endRayPoint.transform.localPosition;
		isStartUpReady = true;
		bubbleGunMaterial = gunAnimation.gameObject.GetComponent<MeshRenderer>().material;
	
	}

	/// <summary>
	/// ray cast for blue layout
	/// on press shot bullet and interaction
	/// </summary>
	void Update ()
	{
		rayCastForOutLine ();
		//tipsForOnshot.isAnyAction ();
	}

	#region  shotting Function
	/// <summary>
	/// on Click Shot
	/// set it to gun parent then reset it to gun position
	/// then set it to arCam  so that we are using position related to arCam rather than gun
	/// reset the scale and rotation 
	/// fire the bullet
	/// </summary>
	public void OnPressShotting () 
	{
		if (!isShotAlready && isStartUpReady && !UICamera.isOverUI)
		{
			Debug.Log ("shot");
			gyroController.Reset ();
			tipsForOnshot.intialStartLetterForWord = false;
			gunAnimation.ResetTrigger ( "isShot" );
			gunAnimation.SetBool ("isShot", true);
			GameObject bullet = Instantiate(projectile) as GameObject;

			//bullet.transform.localEulerAngles = Vector3.zero;
			bullet.transform.parent = bulletShotStartPoint.transform; 
			bullet.transform.localEulerAngles = Vector3.zero;
			bullet.transform.localPosition = new Vector3 (0, 0, 3);
			bullet.transform.parent = null;
			bullet.transform.localScale = new Vector3 (0.5f, 0.5f,0.5f);
			bullet.GetComponent<Rigidbody>().AddForce(transform.forward *1000);
			StartCoroutine (GunTextureChange());
			StartCoroutine(movingTowardTarget(bullet));
			isShotAlready = true;
		}

	}


	/// <summary>
	/// Movings bullet the toward target.
	/// </summary>
	/// <returns>The toward target.</returns>
	/// <param name="bullet">Bullet.</param>
	public IEnumerator movingTowardTarget (GameObject bullet) 
	{
		Debug.Log ("move");
	
		//Vector3 targetPosition = target.transform.localPosition;
		//int index = bubbleGunTexture.Count-2;
		while (true) {
			/*if(index > 0){
				yield return new WaitForSeconds (0.3f);
				ChangeGunTexture (index);
				index--;

			}*/
			yield return new WaitForEndOfFrame ();
			if (BulletReachEndPoint) {
				Debug.Log (" force stop due to bullet either destory or detect item");
				gunAnimation.SetBool ("isShot", false);
				RayCastForReaction ();
				//isShotAlready = false;
				BulletReachEndPoint = false;
				//ChangeGunTexture ();
				break;
			} else 		if(bullet == null || CollusionObject != null ) {
				Debug.Log ("stop due to bullet either destory or detect item");
				gunAnimation.SetBool ("isShot",false);
				RayCastForReaction ();
				//isShotAlready = false;
				//ChangeGunTexture ();
				break;

			}
				
		}

	}


	/// <summary>
	/// Guns the texture change.
	/// </summary>
	/// <returns>The texture change.</returns>
	public IEnumerator GunTextureChange  ()
	{
		Debug.Log ("gun change TExture");
		int index = bubbleGunTexture.Count-1;
		while (true) {
			if (index > -1) {
				yield return new WaitForSeconds (0.5f);
				ChangeGunTexture (index);
				index--;

			} else {
				isShotAlready = false;
				break;

			}
		}

	}

	#endregion

	#region ray cast Function
	/// <summary>
	/// ray cast to hit letter buddy and letter interaction
	/// </summary>
	public void RayCastForReaction ()
	{
		if (CollusionObject == null) {

			Debug.Log ("hit nothing" );
			return;

		}
		Debug.Log ("hit " + CollusionObject.transform + " layer "+ CollusionObject.gameObject.layer );
		Transform objectHit = CollusionObject.transform;
		/*Vector3 raycastDir = target.transform.position - arCam.transform.position;
		bool isHit = Physics.Raycast (arCam.transform.position, raycastDir, out hit, distance);
		if (isHit) {
			Transform objectHit = hit.transform;
			Debug.Log (hit.transform.gameObject.name);*/

			//alphabetGameMode2UISystem.On3dObjClick ();
			if(objectHit.gameObject.name.Equals("polySurface2"))
			{
				
				alphabetGameMode2UISystem.On3dObjClick ();
				CollusionObject = null;

			} else 
			if (objectHit.gameObject.layer == 14) {
				TipsForOnshot.IsCorrect = false;
				//particle.gameObject.SetActive (true);
				//particle.transform.parent = objectHit.transform;
				//particle.transform.localPosition = new Vector3 (0, 0, 0);
				//particle.transform.localScale = new Vector3 (2, 2, 2);
				//particle.transform.localEulerAngles = Vector3.zero;

				//hit.transform.gameObject.SetActive (false);//special handle for particle to run
				//hit.transform.gameObject.SetActive (true);
				tipsForOnshot.SetUpWordSelection (objectHit);
				//tipsForOnshot.MatchLetterWithWord (objectHit);

			}
		//}

	}


	/// <summary>
	/// ray cast for showing blue layout and change aim color texture;
	/// </summary>
	private void rayCastForOutLine ()
	{

		Vector3 raycastDir = target.transform.position - arCam.transform.position;
		//bool isHit = Physics.Raycast (arCam.transform.position, raycastDir, out hit, distance);
		Debug.DrawRay (arCam.transform.position, raycastDir,Color.green);

		bool isHit = Physics.BoxCast(arCam.transform.position, new Vector3(0.1f,0.1f,0.1f), raycastDir ,out hit, Quaternion.LookRotation(raycastDir) );
		if (isHit) 
		{
			Transform objectHit = hit.transform;
			// if hit framemarker  then set blue lay parent to this framemarker
			if (objectHit.transform.parent.GetComponent<AlphabetCoreTrackableEventHandler> () != null && objectHit.transform.parent.GetComponent<AlphabetCoreTrackableEventHandler> ().isTrack ) {

				if (gameMode2CoreonTrackFunction != null) {
				rayHitOutLayObject = objectHit;
					gameMode2CoreonTrackFunction.SetCurrentOnTrackLetter (objectHit.transform.parent.gameObject);
				}
			} else if(objectHit.gameObject.name.Equals("polySurface2")){
				gameMode2CoreonTrackFunction.ResetCurrentLetter ();
				gameMode2CoreonTrackFunction.ChangeAimTextureColor (false);
			}else {
				gameMode2CoreonTrackFunction.ResetCurrentLetter ();
				rayHitOutLayObject = null;
			}

		} else {
			// turn off blue render if framemarker is not on track
			if (gameMode2CoreonTrackFunction != null) 
			{
				gameMode2CoreonTrackFunction.ResetCurrentLetter ();
				rayHitOutLayObject = null;
			} 
		}

	}

	/// <summary>
	/// Changes the gun texture.
	/// </summary>
	/// <param name="index">Index.</param>
	public void ChangeGunTexture (int index = 0)
	{
		//Debug.Log (" change Texture " + index);
		bubbleGunMaterial.SetTexture ("_MainTex",bubbleGunTexture[index]);
	}

	#endregion
}
