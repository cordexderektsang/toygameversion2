﻿using System.Text;
using UnityEngine;
using System.Collections.Generic;

public class AlphabetCharacterController : MonoBehaviour
{
    public UIRoot UIRoot;
    public GameObject character; // Character gameObject

    private AlphabetCoreConstants.CHARACTER_STATES targetCharacterState; // reverse the animation cycle
    private AlphabetCoreConstants.CHARACTER_STATES characterState;

    private float screenRatio;
    private float screenWidth;
    private float screenHeight;

    private UISprite characterSprite;

    private float characterWidth;
    private float characterHeight;

    private Vector3 defaultSpawnLocationHidden;
    private Vector3 defaultSpawnLocationShown;

    private AlphabetCoreConstants.CHARACTER_DIRECTIONS direction = AlphabetCoreConstants.CHARACTER_DIRECTIONS.TOP; // Where does the character appear

    [Header("(Speed in pixels)")]
    [Range(1.0f, 100.0f)]
    public float movementSpeed = 1.0f; //movement speed (in pixels) for character (1.0f);

    [Range(0.0f, 1.0f)]
    public float displayCharacterAspectX = 0.5f; // how far should the character appear in the X coordinate. 0 is left (0.5f);
    [Range(0.0f, 1.0f)]
    public float displayCharacterAspectY = 0.5f; // how far should the character appear in the Y coordinate. 0 is top (0.25f);

    private UILabel informationDisplay;
    private GameObject wordDisplayTextBox;
    private UILabel wordDisplay;
    private AlphabetCoreSystem coreSystemDictionary;
    //private UISprite imageSprite; // Character's image UISprite to display icon

    void Start()
    {
        informationDisplay = character.GetComponentInChildren<UILabel>();
        wordDisplayTextBox = character.transform.FindChild("TextBoxForWordDisplay").gameObject;
        wordDisplay = wordDisplayTextBox.GetComponentInChildren<UILabel>();
        characterSprite = character.GetComponent<UISprite>();
        coreSystemDictionary = GetComponent<AlphabetCoreSystem>();
        //imageSprite = character.GetComponentsInChildren<UISprite>()[2];
        setDefaultLocations();
        targetCharacterState = AlphabetCoreConstants.CHARACTER_STATES.IN;
        characterState = AlphabetCoreConstants.CHARACTER_STATES.IN;
        wordDisplayTextBox.SetActive(false);
        setCharacterLocation();
    }

    // Update is called once per frame
    void Update()
    {
        if (!characterState.Equals(targetCharacterState))
        {
            moveToPoint();
        }
    }

    private void setDefaultLocations()
    {
        // Only set for TOP for now
        screenRatio = (float)UIRoot.activeHeight / Screen.height;
        screenWidth = Mathf.Ceil(Screen.width * screenRatio);
        screenHeight = Mathf.Ceil(Screen.height * screenRatio);

        CCLog.SpecialMsg("screenRatio: " + screenRatio);
        CCLog.SpecialMsg("screenWidth: " + screenWidth);
        CCLog.SpecialMsg("screenHeight: " + screenHeight);

        characterWidth = characterSprite.width;
        characterHeight = characterSprite.height;

        CCLog.SpecialMsg("characterWidth: " + characterWidth);
        CCLog.SpecialMsg("characterHeight: " + characterHeight);

        float displayXPosition = ((screenWidth / 2) * displayCharacterAspectX) - (screenWidth / 2) + (characterWidth / 2);
        float displayYPosition = (screenHeight / 2) + (characterHeight / 2);

        defaultSpawnLocationHidden = new Vector3(displayXPosition, displayYPosition);

        displayYPosition = (screenHeight / 2) + (characterHeight / 2) - (screenHeight * displayCharacterAspectY);

        defaultSpawnLocationShown = new Vector3(displayXPosition, displayYPosition);
    }

    /// <summary>
    /// Set the character position after getting bounds location
    /// </summary>
    private void setCharacterLocation()
    {
        CCLog.SpecialMsg("before set, localPosition: " + character.transform.localPosition);
        character.transform.localPosition = defaultSpawnLocationHidden;
        CCLog.SpecialMsg("after set, localPosition: " + character.transform.localPosition);
    }

    /// <summary>
	/// Set location for character based on selection
	/// </summary>
    /// <returns>Vector3 of the new character position. Invalid position settings return a zero vector</returns>
	private Vector3 getCharacterLocationPosition(AlphabetCoreConstants.CHARACTER_DIRECTIONS direction, GameObject character, Vector3 minBound, Vector3 maxBound, Vector3 characterMinBound)
    {
        //switch (direction)
        //{
        //    case AlphabetCoreConstants.CHARACTER_DIRECTIONS.LEFT_BOTTOM:
        //        return new Vector3(minBound.x + characterMinBound.x, minBound.y - characterMinBound.y, minBound.z);
        //    case AlphabetCoreConstants.CHARACTER_DIRECTIONS.RIGHT_TOP:
        //        return new Vector3(maxBound.x - characterMinBound.x, maxBound.y + characterMinBound.y, maxBound.z);
        //    case AlphabetCoreConstants.CHARACTER_DIRECTIONS.RIGHT_BOTTOM:
        //        return new Vector3(maxBound.x - characterMinBound.x, minBound.y - characterMinBound.y, maxBound.z);
        //    case AlphabetCoreConstants.CHARACTER_DIRECTIONS.LEFT_TOP:
        //        return new Vector3(minBound.x + characterMinBound.x, maxBound.y + characterMinBound.y, maxBound.z);
        //    case AlphabetCoreConstants.CHARACTER_DIRECTIONS.TOP:
        //        //return new Vector3((minBound.x + characterMinBound.x) / 2.5f, maxBound.y * 1.5f, maxBound.z);
        //        return new Vector3(defaultSpawnLocationIn.transform.position.x, defaultSpawnLocationIn.transform.position.y , defaultSpawnLocationIn.transform.position.z);
        //    default:
        //        return new Vector3(0, 0, 0);
        //}

        // Only do TOP direction for now
        switch (direction)
        {
            case AlphabetCoreConstants.CHARACTER_DIRECTIONS.TOP:
            //return new Vector3(defaultSpawnLocationIn.transform.position.x, defaultSpawnLocationIn.transform.position.y, defaultSpawnLocationIn.transform.position.z);
            default:
                return new Vector3(0, 0, 0);
        }
    }

    /// <summary>
    /// Sets a new direction for the character to come out
    /// </summary>
    /// <param name="newDirection">New direction.</param>
    //public void changeDirection(AlphabetCoreConstants.CHARACTER_DIRECTIONS newDirection)
    //   {
    //       direction = newDirection;
    //       setCharacterLocation();
    //   }

    /// <summary>
    /// Set a new direction for the character to come out randomly
    /// </summary>
    //public void randomChangeDirection()
    //{
    //    int randomPick = Random.Range(0, 5);
    //    switch (randomPick)
    //    {
    //        case 0:
    //            direction = AlphabetCoreConstants.CHARACTER_DIRECTIONS.LEFT_BOTTOM;
    //            setCharacterLocation();
    //            break;
    //        case 1:
    //            direction = AlphabetCoreConstants.CHARACTER_DIRECTIONS.RIGHT_TOP;
    //            setCharacterLocation();
    //            break;
    //        case 2:
    //            direction = AlphabetCoreConstants.CHARACTER_DIRECTIONS.RIGHT_BOTTOM;
    //            setCharacterLocation();
    //            break;
    //        case 3:
    //            direction = AlphabetCoreConstants.CHARACTER_DIRECTIONS.LEFT_TOP;
    //            setCharacterLocation();
    //            break;
    //        case 4:
    //            direction = AlphabetCoreConstants.CHARACTER_DIRECTIONS.TOP;
    //            setCharacterLocation();
    //            break;
    //    }
    //}

    /// <summary>
    /// Move the character out to the screen area. Returns the state of the character after movement.
    /// </summary>
    /// <returns>Current character state</returns>
    private AlphabetCoreConstants.CHARACTER_STATES moveCharacterOut()
    {
        switch (direction)
        {
            //case AlphabetCoreConstants.CHARACTER_DIRECTIONS.LEFT_BOTTOM:
            //case AlphabetCoreConstants.CHARACTER_DIRECTIONS.LEFT_TOP:
            //    if (character.transform.position.x <= distanceX)
            //    {
            //        character.transform.position = new Vector3(character.transform.position.x + directionMovement, character.transform.position.y, character.transform.position.z);
            //        return AlphabetCoreConstants.CHARACTER_STATES.MOVING;
            //    }
            //    break;
            //case AlphabetCoreConstants.CHARACTER_DIRECTIONS.RIGHT_BOTTOM:
            //case AlphabetCoreConstants.CHARACTER_DIRECTIONS.RIGHT_TOP:
            //    if (character.transform.position.x >= distanceX)
            //    {
            //        character.transform.position = new Vector3(character.transform.position.x - directionMovement, character.transform.position.y, character.transform.position.z);
            //        return AlphabetCoreConstants.CHARACTER_STATES.MOVING;
            //    }
            //    break;
            case AlphabetCoreConstants.CHARACTER_DIRECTIONS.TOP:
                if (character.transform.localPosition.y > defaultSpawnLocationShown.y)
                {
                    float newYPosition = character.transform.localPosition.y - movementSpeed;
                    if (newYPosition <= defaultSpawnLocationShown.y)
                    {
                        character.transform.localPosition = defaultSpawnLocationShown;
                    }
                    else
                    {
                        character.transform.localPosition = new Vector3(character.transform.localPosition.x, newYPosition);
                        return AlphabetCoreConstants.CHARACTER_STATES.MOVING;
                    }
                }
                break;
        }

        return AlphabetCoreConstants.CHARACTER_STATES.OUT;
    }

    /// <summary>
    /// Move the character in away from the screen area. Returns the state of the character after movement.
    /// </summary>
    /// <returns></returns>
    private AlphabetCoreConstants.CHARACTER_STATES MoveCharacterIn()
    {
        switch (direction)
        {
            //case AlphabetCoreConstants.CHARACTER_DIRECTIONS.LEFT_BOTTOM:
            //case AlphabetCoreConstants.CHARACTER_DIRECTIONS.LEFT_TOP:
            //    if (character.transform.position.x > oldPositionX)
            //    {
            //        character.transform.position = new Vector3(character.transform.position.x - directionMovement, character.transform.position.y, character.transform.position.z);
            //        return AlphabetCoreConstants.CHARACTER_STATES.MOVING;
            //    }
            //    break;
            //case AlphabetCoreConstants.CHARACTER_DIRECTIONS.RIGHT_BOTTOM:
            //case AlphabetCoreConstants.CHARACTER_DIRECTIONS.RIGHT_TOP:
            //    if (character.transform.position.x < oldPositionX)
            //    {
            //        character.transform.position = new Vector3(character.transform.position.x + directionMovement, character.transform.position.y, character.transform.position.z);
            //        return AlphabetCoreConstants.CHARACTER_STATES.MOVING;
            //    }
            //    break;
            case AlphabetCoreConstants.CHARACTER_DIRECTIONS.TOP:
                if (character.transform.localPosition.y < defaultSpawnLocationHidden.y)
                {
                    float newYPosition = character.transform.localPosition.y + movementSpeed;
                    if (newYPosition >= defaultSpawnLocationHidden.y)
                    {
                        character.transform.localPosition = defaultSpawnLocationHidden;
                    }
                    else
                    {
                        character.transform.localPosition = new Vector3(character.transform.localPosition.x, newYPosition);
                        return AlphabetCoreConstants.CHARACTER_STATES.MOVING;
                    }
                }
                break;
        }

        return AlphabetCoreConstants.CHARACTER_STATES.IN;
    }

    /// <summary>
    /// move character toward new point or backward to old point 
    /// </summary>
    private void moveToPoint()
    {
        if (targetCharacterState.Equals(AlphabetCoreConstants.CHARACTER_STATES.OUT))
        {
            characterState = moveCharacterOut();
        }
        else if (targetCharacterState.Equals(AlphabetCoreConstants.CHARACTER_STATES.IN))
        {
            characterState = MoveCharacterIn();
        }
    }

    /// <summary>
    /// Set the emotion (picture) and message displayed
    /// </summary>
    /// <param name="tagName">TagName of emotion to change to</param>
    /// <param name="message">Message to display in textbox</param>
    ///  <param name="randomWord">random suggest Word to display in textbox</param>
    //public static void ChangeEmotion(UISprite characterSprite, UILabel informationDisplay, string inTagName, string message)
    public void setEmotion(AlphabetCoreConstants.CHARACTER_TAG_NAMESS tagName, string message, bool requireSuggestedWord = false)
    {
        int width = characterSprite.width;
        int height = characterSprite.height;

        StringBuilder tagNameStringBuilder = new StringBuilder();
        tagNameStringBuilder.Append(tagName.ToString().Substring(0, 1)).Append(tagName.ToString().Substring(1).ToLower());

        characterSprite.spriteName = tagNameStringBuilder.ToString();
        characterSprite.SetDimensions(width, height);

        informationDisplay.text = message;
        if (requireSuggestedWord) {
            wordDisplayTextBox.SetActive(true);
            wordDisplay.text = coreSystemDictionary.returnASingleRandomWord();
       } else
        {
            wordDisplayTextBox.SetActive(false);
        }
    }



    /// <summary>
    /// set the icon next
    /// </summary>
    //public void setIcon(AlphabetCoreConstants.CHARACTER_Image_Icon imageName)
    //{
    //    int width = imageSprite.width;
    //    int height = imageSprite.height;
    //    StringBuilder tagNameStringBuilder = new StringBuilder();
    //    tagNameStringBuilder.Append(imageName.ToString().Substring(0, 1)).Append(imageName.ToString().Substring(1).ToLower());
    //    imageSprite.spriteName = tagNameStringBuilder.ToString();
    //    imageSprite.SetDimensions(width, height);
    //}

    /// <summary>
    /// Sets the character to move out. Also changes the picture and the message
    /// </summary
    /// <param name="tagName">TagName of emotion to change to</param>
    /// <param name="message">Message to display in textbox</param>
    /// <param name="wordSuggestion">random suggest Word to display in textbox</param>
    public void setCharacterToMoveOut(AlphabetCoreConstants.CHARACTER_TAG_NAMESS tagName, AlphabetCoreConstants.CHARACTER_Image_Icon imageName, string message, bool requireSuggestedWord = false)
    {
        setEmotion(tagName, message , requireSuggestedWord);
        //setIcon(imageName);
        setCharacterToMoveOut();
    }

    /// <summary>
    /// Sets the character to move out
    /// </summary
    public void setCharacterToMoveOut()
    {
        targetCharacterState = AlphabetCoreConstants.CHARACTER_STATES.OUT;
    }

    /// <summary>
    /// Sets the character to move away from the screen
    /// </summary
    public void setCharacterToMoveIn()
    {
        if (wordDisplayTextBox != null)
        {
            wordDisplayTextBox.SetActive(false);
        }
        targetCharacterState = AlphabetCoreConstants.CHARACTER_STATES.IN;
    }

    /// <summary>
    /// Get the current character state
    /// </summary>
    /// <returns>Current character state</returns>
    public AlphabetCoreConstants.CHARACTER_STATES getCharacterState()
    {
        return characterState;
    }

    /// <summary>
    /// Get the target character state it should change to
    /// </summary>
    /// <returns>Target character state</returns>
    public AlphabetCoreConstants.CHARACTER_STATES getTargetCharacterState()
    {
        return targetCharacterState;
    }
}
