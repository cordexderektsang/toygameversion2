﻿using UnityEngine;

public class TimerController
{
    private AlphabetCoreConstants.TIMER_MAX_INCREASE_TYPES _timerMaxIncreaseType;
    public AlphabetCoreConstants.TIMER_MAX_INCREASE_TYPES timerMaxIncreaseType
    {
        get
        {
            return _timerMaxIncreaseType;
        }
        private set
        {
            _timerMaxIncreaseType = value;
        }
    }

    private float _time = 0;
    public float time
    {
        get
        {
            return _time;
        }
        private set
        {
            _time = value;
        }
    }

    private float _maxTime = 0;
	public float maxTime
    {
        get
        {
            return _maxTime;
        }
        private set
        {
            _maxTime = value;
        }
    }

    private float _slope = 0;
    public float slope
    {
        get
        {
            return _slope;
        }
        private set
        {
            _slope = value;
        }
    }

    private float _power = 0;
    public float power
    {
        get
        {
            return _power;
        }
        private set
        {
            _power = value;
        }
    }

    //private float _yIntercept = 0;
    //public float yIntercept
    //{
    //    get
    //    {
    //        return _yIntercept;
    //    }
    //    private set
    //    {
    //        _yIntercept = value;
    //    }
    //}

    //private int _maxTimeReachCount = 0;
    //public int maxTimeReachCount
    //{
    //    get
    //    {
    //        return _maxTimeReachCount;
    //    }
    //    private set
    //    {
    //        _maxTimeReachCount = value;
    //    }
    //}

    //private float _timeIncrement = 0.5f;
    //public float timeIncrement
    //{
    //    get
    //    {
    //        return _timeIncrement;
    //    }
    //    private set
    //    {
    //        _timeIncrement = value;
    //    }
    //}

    private float _absoluteMaxTime = 0;
    public float absoluteMaxTime
    {
        get
        {
            return _absoluteMaxTime;
        }
        private set
        {
            _absoluteMaxTime = value;
        }
    }

    //private int _timeReachCount = 0;
    //public int timeReachCount
    //{
    //    get
    //    {
    //        return _timeReachCount;
    //    }
    //    private set
    //    {
    //        _timeReachCount = value;
    //    }
    //}

    private bool isTimerReachedMax = false;
	private bool isTimerEnabled = false;

    public TimerController()
    {

    }

    public TimerController(InstructionState iState)
    {
        setupTimerForInstructionState(iState);
    }

    //public TimerController(float time, float maxTime, int maxTimeReachCount, float timeIncrement, float glassCeil)
    //{
    //    setupTimer(time, maxTime, maxTimeReachCount, timeIncrement, glassCeil);
    //}

    /// <summary>
    /// Setup the timer variables
    /// </summary>
    /// <param name="time">Initial time</param>
    /// <param name="iState">InstructionState object to parse</param>
    public void setupTimerForInstructionState(InstructionState iState)
    {
        timerMaxIncreaseType = iState.timerMaxIncreaseType;
        switch (timerMaxIncreaseType)
        {
            case AlphabetCoreConstants.TIMER_MAX_INCREASE_TYPES.LINEAR:
                setupInstructionTimerLinear(iState);
                break;
            case AlphabetCoreConstants.TIMER_MAX_INCREASE_TYPES.EXPONENTIAL:
                setupInstructionTimerPower(iState);
                break;
            default:
                setupInstructionTimerBasic(iState);
                break;
        }
    }

    private void setupInstructionTimerBasic(InstructionState iState)
    {
        time = 0;
        maxTime = iState.waitTime;
    }

    private void setupInstructionTimerCore(InstructionState iState)
    {
        //yIntercept = iState.yIntercept;
        absoluteMaxTime = iState.absoluteMaxTime;
        setupInstructionTimerBasic(iState);
    }

    private void setupInstructionTimerLinear(InstructionState iState)
    {
        slope = iState.slope;
        setupInstructionTimerCore(iState);
    }

    private void setupInstructionTimerPower(InstructionState iState)
    {
        power = iState.power;
        setupInstructionTimerCore(iState);
    }

    /// <summary>
    /// Ensure that after adding time, the glass ceiling is not broken
    /// </summary>
    private void checkInstructionTimerGlassCeiling()
    {
        if (maxTime > absoluteMaxTime)
        {
            maxTime = absoluteMaxTime;
        }
    }

    //public void setupTimer(float time, float maxTime, int maxTimeReachCount, float timeIncrement, float glassCeil)
    //{
    //    this.time = time;
    //    this.maxTime = maxTime;
    //    this.maxTimeReachCount = maxTimeReachCount;
    //    this.timeIncrement = timeIncrement;
    //    this.glassCeil = glassCeil;
    //}

    /// <summary>
    /// Ensure the timer is setup before it runs
    /// </summary>
    /// <returns><c>true</c> if timer is setup properly, <c>false</c> otherwise.</returns>
    private bool checkIfTimerIsSetup()
    {
        if (maxTime > 0)
        {
            return true;
        }
        else {
            return false;
        }
    }

    /// <summary>
    /// Check if the time can be increased after timer duration iteration
    /// </summary>
 //   public void checkMaxTimeIncrement ()
 //   {
	//	if (timeIncrement < glassCeil)
 //       {
	//		maxTime += timeIncrement;
	//		timeReachCount++;
	//		if(timeReachCount > maxTimeReachCount)
 //           {
	//			timeIncrement = timeIncrement + timeIncrement;
 //               timeReachCount = 0;
	//		}
	//	}
	//}

    /// <summary>
    /// Check if the max time can be increased after timer iteration
    /// </summary>
    public void checkInstructionTimerIncrement()
    {
        if (maxTime < absoluteMaxTime)
        {
            switch (timerMaxIncreaseType)
            {
                case AlphabetCoreConstants.TIMER_MAX_INCREASE_TYPES.LINEAR:
                    maxTime += slope;
                    checkInstructionTimerGlassCeiling();
                    break;
                case AlphabetCoreConstants.TIMER_MAX_INCREASE_TYPES.EXPONENTIAL:
                    maxTime = Mathf.Pow(maxTime, power);
                    checkInstructionTimerGlassCeiling();
                    break;
                default:
                    break;
            }
        }
    }

    /// <summary>
	/// Return the status of the current timer whether it is at maximum time
	/// </summary>
	/// <returns><c>true</c> if the timer reached maximum time, <c>false</c> otherwise.</returns>
	public bool isMaxTimeReached()
    {
        return isTimerReachedMax;
    }

    /// <summary>
    /// Enables/disables the timer
    /// </summary>
    /// <param name="enabled">If set to <c>true</c>, timer will run.</param>
    public void setTimerActive (bool enabled)
    {
        isTimerEnabled = enabled;
    }

	/// <summary>
	/// Resets the time value to zero to restart timer
	/// </summary>
	public void resetTimer ()
	{
        time = 0;
        isTimerReachedMax = false;
	}

	/// <summary>
	/// Timer operation
	/// </summary>
	public void runTimer()
	{
        if (checkIfTimerIsSetup() && isTimerEnabled && !isTimerReachedMax)
        {
            if (time < maxTime)
            {
                time += Time.deltaTime;
            }
            else
            {
                isTimerReachedMax = true;
            }
        }
	}
}
