﻿#if UNITY_EDITOR
using UnityEditor;

public static class InstructionStateCustomList
{
    static bool isOverrideTutorial = false;
    static bool isOverrideWord = false;

    public static void Show(SerializedProperty list, bool showListSize = true)
    {
        bool isDone;

        //SerializedProperty list = so.FindProperty(propertyName);
        //if (showListSize)
        //{
        //    EditorGUILayout.PropertyField(list.FindPropertyRelative("Array.size"));
        //}
        //Debug.Log("list.name: " + list.name);
        if (list == null)
        {
            return;
        }
        switch (list.name)
        {
            case AlphabetCoreConstants.MAGIC_SCREEN:
            case AlphabetCoreConstants.MAGIC_SCREEN2:
            case AlphabetCoreConstants.TUTORIAL_STATES:
            case AlphabetCoreConstants.END_INSTRUCTION_RESET_TIME:
                EditorGUILayout.PropertyField(list, true);
                break;
            case AlphabetCoreConstants.IS_OVERRIDE_TUTORIAL_SETTING:
                isOverrideTutorial = list.boolValue;
                EditorGUILayout.PropertyField(list);
                break;
            case AlphabetCoreConstants.IS_NEED_TUTORIAL:
                if (isOverrideTutorial)
                {
                    EditorGUILayout.PropertyField(list);
                }
                break;
            case AlphabetCoreConstants.IS_OVERRIDE_WORD_SETTING:
                isOverrideWord = list.boolValue;
                EditorGUILayout.PropertyField(list);
                break;
            case AlphabetCoreConstants.OVERRIDE_WORDS:
                if (isOverrideWord)
                {
                    EditorGUILayout.PropertyField(list, true);
                }
                break;
            case AlphabetCoreConstants.INSTRUCTION_STATES:
                EditorGUILayout.PropertyField(list);
                if (list.isExpanded)
                {
                    EditorGUI.indentLevel += 1;
                    EditorGUILayout.PropertyField(list.FindPropertyRelative("Array.size"));
                    for (int i = 0; i < list.arraySize; i++)
                    {
                        isDone = false;

                        SerializedProperty sp = list.GetArrayElementAtIndex(i);
                        EditorGUILayout.PropertyField(sp);

                        //Debug.Log("sp.name (1): " + sp.name);

                        if (sp.isExpanded)
                        {
                            EditorGUI.indentLevel += 1;
                            AlphabetCoreConstants.TIMER_MAX_INCREASE_TYPES timerMaxIncreaseType = AlphabetCoreConstants.TIMER_MAX_INCREASE_TYPES.NONE;

                            while (!isDone && sp.Next(true))
                            {
                                //Debug.Log("sp.name (2): " + sp.name);
                                switch (sp.name)
                                {
                                    case "Array":
                                    case "size":
                                    case "data":
                                        continue;
                                    case AlphabetCoreConstants.TIMER_MAX_INCREASE_TYPE:
                                        timerMaxIncreaseType = (AlphabetCoreConstants.TIMER_MAX_INCREASE_TYPES)sp.enumValueIndex;
                                        EditorGUILayout.PropertyField(sp);
                                        break;
                                    case AlphabetCoreConstants.SLOPE:
                                        if (timerMaxIncreaseType.Equals(AlphabetCoreConstants.TIMER_MAX_INCREASE_TYPES.LINEAR))
                                        {
                                            EditorGUILayout.PropertyField(sp);
                                        }
                                        break;
                                    case AlphabetCoreConstants.POWER:
                                        if (timerMaxIncreaseType.Equals(AlphabetCoreConstants.TIMER_MAX_INCREASE_TYPES.EXPONENTIAL))
                                        {
                                            EditorGUILayout.PropertyField(sp);
                                        }
                                        break;
                                    case AlphabetCoreConstants.Y_INTERCEPT:
                                        //if (!timerMaxIncreaseType.Equals(AlphabetCoreConstants.TIMER_MAX_INCREASE_TYPES.NONE))
                                        //{
                                        //    EditorGUILayout.PropertyField(sp);
                                        //}
                                        break;
                                    case AlphabetCoreConstants.ABSOLUTE_MAX_TIME:
                                        if (!timerMaxIncreaseType.Equals(AlphabetCoreConstants.TIMER_MAX_INCREASE_TYPES.NONE))
                                        {
                                            EditorGUILayout.PropertyField(sp);
                                        }
                                        break;
                                    default:
                                        EditorGUILayout.PropertyField(sp);
                                        break;
                                }

                                if (sp.name.Equals(AlphabetCoreConstants.INSTRUCTION_STATE_LAST_VARIABLE))
                                {
                                    isDone = true;
                                }
                            }
                            EditorGUI.indentLevel -= 1;
                        }
                    }
                    EditorGUI.indentLevel -= 1;
                }
                break;
        }
    }
}
#endif