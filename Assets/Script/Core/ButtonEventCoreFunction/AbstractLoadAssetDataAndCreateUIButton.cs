﻿using UnityEngine;
using System.Collections.Generic;




/*
 * Setup and Extract AssertBundle then return UIAltas or Sprite
 * Ngui delegate Add in function for UI Button
 * common variable for MyCartManager nad MyCollectionManager
 * common function for MyCartManager nad MyCollectionManager
 * by Derek 
 */

public abstract class AbstractLoadAssetDataAndCreateUIButton : MonoBehaviour
{


 

/// <summary>
/// Adds the function and parameter to ngui button.
/// </summary>
/// <param name="Script">Script.</param>
/// <param name="methodName">Method name.</param>
/// <param name="button">Button.</param>
/// <param name="Parameter">Parameter.</param>
	public  static void AddParameterFunction(MonoBehaviour Script, string methodName, UIButton button, params object[] Parameter)
    {
		if (Script != null && button != null && methodName != null) {
				EventDelegate btnFunctionToBeAdd = new EventDelegate (Script, methodName); // add a function from a script;
            /*if (target != null )
            {
                //Object value = target as Object;
                btnFunctionToBeAdd.parameters[0] = new EventDelegate.Parameter(target);
           
            }*/
            if (Parameter != null)
            {//if there are parameter
                for (int i = 0; i < Parameter.Length; i++)
                {
                    btnFunctionToBeAdd.parameters[i].value = Parameter[i];// add all parameter
                }
            }
            if (button.onClick.Count == 0)
            {
                EventDelegate.Set(button.onClick, btnFunctionToBeAdd);//set the first script function
            }
            else
            {
                EventDelegate.Add(button.onClick, btnFunctionToBeAdd);  // add another script function 
               
            }
        }

    }

}
