﻿using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class AudioClipList
{
    //public AlphabetCoreConstants.AUDIO_CONTROLLER_TYPES audioControllerType;
    public string audioControllerType;
    public List<AudioClip> audioClipList = new List<AudioClip>();
}
