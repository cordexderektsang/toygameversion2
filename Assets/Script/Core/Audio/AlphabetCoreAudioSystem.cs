﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.SceneManagement;

public class AlphabetCoreAudioSystem : MonoBehaviour 
{
	public List<AudioClip> clips_list = new List<AudioClip> ();
    
    public List<AudioClip> AudioUS_clips_lists = new List<AudioClip>();
    public List<AudioClip> AudioUK_clips_lists = new List<AudioClip>();
    public List<AudioClip> Encourage_clips_lists = new List<AudioClip>();

    public List<AudioClip> LetterUS_clips_lists = new List<AudioClip>();
    public List<AudioClip> StrokeUS_clips_lists = new List<AudioClip>();
    public List<AudioClip> PhonicsUS_clips_lists = new List<AudioClip>();

    public List<AudioClip> LetterUK_clips_lists = new List<AudioClip>();
    public List<AudioClip> StrokeUK_clips_lists = new List<AudioClip>();
    public List<AudioClip> PhonicsUK_clips_lists = new List<AudioClip>();

    public AudioClip MustPlay;

    public static string languageOption;// language option
    public static bool IsPlaying = false;
	public string pickedClipName;
	private int index = 0;
    //private Hashtable table;

    private List<AudioClip> tempList;

    //editor path
    private const string AUDIO_PATH = "Assets/AlphaAssets/Audio";
    private const string AUDIO_VOICE_PATH = AUDIO_PATH + "/voice";
    private const string AUDIO_ALPHABET_PATH = AUDIO_PATH + "/alphabet";
    private const string AUDIO_PHONICS_PATH = AUDIO_PATH + "/phonics";
    private const string AUDIO_STROKES_PATH = AUDIO_PATH + "/strokes";

    private string voiceUSSoundPath = AUDIO_VOICE_PATH + "/US_AUDIO";
    private string voiceUKSoundPath = AUDIO_VOICE_PATH + "/UK_AUDIO";
    private string encourageSoundPath = AUDIO_PATH + "/encourage";
    private string alphabetUSSoundPath = AUDIO_ALPHABET_PATH + "/alphabet_US";
    private string alphabetUKSoundPath = AUDIO_ALPHABET_PATH + "/alphabet_UK";
    private string phonicsUSSoundPath = AUDIO_PHONICS_PATH + "/phonics_US";
    private string phonicsUKSoundPath = AUDIO_PHONICS_PATH + "/phonics_UK";
    private string strokesUSSoundPath = AUDIO_STROKES_PATH + "/strokes_US";
    private string strokesUKSoundPath = AUDIO_STROKES_PATH + "/strokes_UK";
	
	private static AlphabetCoreAudioSystem _obj;
    private int letterAudioCount = -1;
    private bool isLetterChange = true;

	// dot instance to call class level method/function 
    void Start ()
    {
        //table = new Hashtable();
        //foreach (AudioClip clip in AudioUS_clips_lists)
        //{
        //    table.Add(clip.name, 0);
        //}
        languageOption = SettingPanelBehaviour.LanguageOption;
    }
	public static AlphabetCoreAudioSystem instance
	{
		get
		{
			if(_obj == null)
			{
				_obj = GameObject.Find ("AudioController").GetComponent<AlphabetCoreAudioSystem> ();
			}
			
			return _obj;
		}
		set
		{
			_obj = value;
		}
	}
	
	public void playOnce (string name, AlphabetCoreConstants.AUDIO_Selections audio, bool isForDemo = false, bool isMultiClip = false)
	{
		IsPlaying = true;
		try {
            AudioClip _clip = null;
            if (!isMultiClip) 
			{
				// hard code demo use
				if (isForDemo && (name[0] == 'a' || name[0] == 'A'))
                {
					//_clip = specialAudio.getAudioFromQueue (name);
				} 
				else // Normal use
				{
		
                   _clip =  ClipSelection(name ,audio);
	
                    // _clip = clips_list.FindAll(delegate (AudioClip clip) { return clip.name == name; })[0];
                }
				
				GetComponent<AudioSource>().PlayOneShot (_clip);
				StartCoroutine ("countTime", _clip.length);
			}
			else // test multi clip
			{
                //StartCoroutine ("playMulitClip", specialAudio.getMultiClip (clips_list.FindAll(delegate (AudioClip clip) {return clip.name == name;}) [0]))

               StartCoroutine("playMultiClip", getMultiClip(ClipSelection(name, AlphabetCoreConstants.AUDIO_Selections.LETTER_PRONUNCIATION)));
            }
		} catch (System.Exception e){
            Debug.Log(e);
			CCLog.SpecialMsg ("Speech From Web : " + name);
			
			if (name.Length < 3) return;
			
			if(Application.internetReachability != NetworkReachability.NotReachable) // Demo use
			{
				StartCoroutine ("text2Speech", name);
			}
			else
			{
				IsPlaying = true;
			}
		}
	}

    AudioClip ClipSelection (string name , AlphabetCoreConstants.AUDIO_Selections audio)
    {
        AudioClip _clip = null;
        if (audio == AlphabetCoreConstants.AUDIO_Selections.WORD_PRONUNCIATION)
        {
            switch (languageOption)
            {
			case "UK_AUDIO":
		
                    _clip = AudioUK_clips_lists.Find(delegate (AudioClip clip) { return clip.name.Equals(name); });//UK
                    if (_clip == null)
                    {
                        _clip = AudioUS_clips_lists.Find(delegate (AudioClip clip) { return clip.name.Equals(name); });//US
                    }
                    break;

                case "US_AUDIO":
		
                    _clip = AudioUS_clips_lists.Find(delegate (AudioClip clip) { return clip.name.Equals(name); });
                    break;

				default:
					_clip = AudioUS_clips_lists.Find(delegate (AudioClip clip) { return clip.name.Equals(name); });
				break;
            }
        }
        else if (audio == AlphabetCoreConstants.AUDIO_Selections.ENCOURAGEMENT)
        {
            _clip = Encourage_clips_lists.Find(delegate (AudioClip clip) { return clip.name.Equals(name); });//Encouragement
        }
        else  if (audio == AlphabetCoreConstants.AUDIO_Selections.SOUND)
        {
            _clip = clips_list.Find(delegate (AudioClip clip) { return clip.name.Equals(name); });// normal clip
        }
        else if (audio == AlphabetCoreConstants.AUDIO_Selections.LETTER_PHONICS)
        {
            //_clip = PhonicsUS_clips_lists.Find(delegate (AudioClip clip) { return clip.name.Equals(name); });// PHONICS clip
            switch (languageOption)
            {
                case "UK_AUDIO":
                    _clip = PhonicsUK_clips_lists.Find(delegate (AudioClip clip) { return clip.name.Equals(name); });//UK
                    if (_clip == null)
                    {
                        _clip = PhonicsUS_clips_lists.Find(delegate (AudioClip clip) { return clip.name.Equals(name); });//US
                    }
                    break;

                case "US_AUDIO":
                    _clip = PhonicsUS_clips_lists.Find(delegate (AudioClip clip) { return clip.name.Equals(name); });
                    break;

				default:
					_clip = PhonicsUS_clips_lists.Find(delegate (AudioClip clip) { return clip.name.Equals(name); });
				break;
            }
        }
        else if (audio == AlphabetCoreConstants.AUDIO_Selections.LETTER_PRONUNCIATION)// Letter clip
        {
            switch (languageOption)
            {

                case "UK_AUDIO":
                    _clip = LetterUK_clips_lists.Find(delegate (AudioClip clip) { return clip.name.Equals(name); });//UK
                    if (_clip == null)
                    {
                        _clip = LetterUS_clips_lists.Find(delegate (AudioClip clip) { return clip.name.Equals(name); });//US
                    }
                    break;

                case "US_AUDIO":
                    _clip = LetterUS_clips_lists.Find(delegate (AudioClip clip) { return clip.name.Equals(name); });
                    break;

				default:
					_clip = LetterUS_clips_lists.Find(delegate (AudioClip clip) { return clip.name.Equals(name); });
				break;
            }

        }
        else if (audio == AlphabetCoreConstants.AUDIO_Selections.LETTER_STROKE)// stroke clip
        {
            switch (languageOption)
            {
                case "UK_AUDIO":
                    _clip = StrokeUK_clips_lists.Find(delegate (AudioClip clip) { return clip.name.Equals(name); });//UK
                    if (_clip == null)
                    {
                        _clip = StrokeUS_clips_lists.Find(delegate (AudioClip clip) { return clip.name.Equals(name); });//US
                    }
                    break;

                case "US_AUDIO":
                    _clip = StrokeUS_clips_lists.Find(delegate (AudioClip clip) { return clip.name.Equals(name); });
                    break;

				default:
					_clip = StrokeUS_clips_lists.Find(delegate (AudioClip clip) { return clip.name.Equals(name); });
				break;
            }
        }
        //UK_AUDIO
        //US_AUDIO
        return _clip;
    }

    public AudioClip[] getMultiClip(AudioClip clip)
    {

        return new AudioClip[] { clip, MustPlay, getClipFromTable(clip.name) };
    }

    private AudioClip getClipFromTable(string refClipHeader)
    {
        // First time playing the letter
        if (isLetterChange)
        {
            isLetterChange = false;
			index = 0;
            tempList = new List<AudioClip>();
            if (languageOption.Equals("UK_AUDIO"))
            {
                tempList = AudioUK_clips_lists.FindAll(delegate (AudioClip clip) { return clip.name.StartsWith(refClipHeader); });
            }
            //else if (languageOption.Equals("US_AUDIO"))
            //{
            //    tempList = AudioUS_clips_lists.FindAll(delegate (AudioClip clip) { return clip.name.StartsWith(refClipHeader); });
            //}
            List<AudioClip> tempList2 = AudioUS_clips_lists.FindAll(delegate (AudioClip clip) { return clip.name.StartsWith(refClipHeader); });

            foreach (AudioClip clip in tempList2)
            {
                if (tempList.Find(delegate (AudioClip clip2) { return clip2.name.Equals(clip.name); }) == null)
                {
                    tempList.Add(clip);
                }
            }

            foreach (AudioClip clip in tempList)
            {
                CCLog.SpecialMsg("clip is: " + clip.name);
            }
        }
		if (index < tempList.Count -1) {
			index++;
		} else {
			index = 0;
		}
		//int randomPickValue = Random.Range (0, tempList.Count);
		pickedClipName = tempList [index].name;
		//Debug.Log (pickedClipName + " index " +index + " name " +  tempList [index].name);
	
	
		return tempList[index];

        //if (tempList.Count == 1)
        //{
        //    return tempList[0];
        //}
        //else
        //{
            //int index = 0;
            //int min = (int)table[tempList[index].name];
            //for (int i = 0; i < tempList.Count; i++)
            //{
            //    int count = (int)table[tempList[i].name];
            //    if (count < min)
            //    {
            //        min = count;
            //        index = i;
            //    }
            //}

            //table[tempList[index].name] = (int)table[tempList[index].name] + 1;
            //return tempList[index];
        //}
    }

    public void resetLetterList()
    {
        letterAudioCount = -1;
        isLetterChange = true;
    }

    public void stopAll ()
	{
		StopCoroutine ("playMultiClip");
		StopCoroutine ("text2Speech");
		StopCoroutine ("countTime");
		GetComponent<AudioSource>().enabled = false;
		GetComponent<AudioSource>().enabled = true;
		IsPlaying = false;
	}
	
	[System.Reflection.Obfuscation(ApplyToMembers=false)]
	private IEnumerator playMultiClip (AudioClip[] clips) 
	{
		for (int i = 0; i < clips.Length; i++)
		{
			GetComponent<AudioSource>().PlayOneShot (clips [i]);
			yield return new WaitForSeconds (clips [i].length);
			yield return new WaitForSeconds (.05f);
		}
		
		IsPlaying = false;
	}
	
	[System.Reflection.Obfuscation(ApplyToMembers=false)]
	private IEnumerator countTime (float t)
	{
		yield return new WaitForSeconds (t);
		IsPlaying = false;
	}
	
	// Use web Voice
	[System.Reflection.Obfuscation(ApplyToMembers=false)]
	private IEnumerator text2Speech (string text)
	{
		string url = 
		"https://s.yimg.com/tn/dict/ox/mp3/v1/" + WWW.EscapeURL(text) +"@_us_1.mp3";
		//"http://tts-api.com/tts.mp3?q=" + WWW.EscapeURL(text);
		//"http://translate.google.com/translate_tts?tl=en&q=" +  WWW.EscapeURL(text);
		
		
		WWW www = new WWW (url);
		yield return www;
		GetComponent<AudioSource>().clip = www.GetAudioClip (false, false, AudioType.MPEG);
		GetComponent<AudioSource>().Play ();
		
		yield return StartCoroutine ("countTime", GetComponent<AudioSource>().clip.length);
		IsPlaying = false;
	}

    public float getClipLength(string name)
    {
        AudioClip _clip = clips_list.Find(delegate (AudioClip clip) { return clip.name.Equals(name); });
        CCLog.SpecialMsg("clip length: " + _clip.length);
        return _clip.length;
    }

    /*
#if UNITY_EDITOR
    /// <summary>
    /// sort by order use by edittor 
    /// </summary>
    public void SortAudioByNameForGame2And3()
    {
        AudioUS_clips_lists = AudioUS_clips_lists.OrderBy(obj => obj.name).ToList();
        AudioUK_clips_lists = AudioUK_clips_lists.OrderBy(obj => obj.name).ToList();
        Encourage_clips_lists = Encourage_clips_lists.OrderBy(obj => obj.name).ToList();
    }

    /// <summary>
    /// sort by order use by edittor 
    /// </summary>
    public void SortAudioByNameforGame1()
    {
        LetterUS_clips_lists = LetterUS_clips_lists.OrderBy(obj => obj.name).ToList();
        PhonicsUS_clips_lists = PhonicsUS_clips_lists.OrderBy(obj => obj.name).ToList();
        PhonicsUK_clips_lists = PhonicsUK_clips_lists.OrderBy(obj => obj.name).ToList();
        StrokeUS_clips_lists = StrokeUS_clips_lists.OrderBy(obj => obj.name).ToList();
        AudioUS_clips_lists = AudioUS_clips_lists.OrderBy(obj => obj.name).ToList();
        AudioUK_clips_lists = AudioUK_clips_lists.OrderBy(obj => obj.name).ToList();
    }

    /// <summary>
    /// Load all List of Audio Data by edittor
    /// </summary>
    public void LoadData()
    {
        AudioUS_clips_lists = new List<AudioClip>();
        AudioUK_clips_lists = new List<AudioClip>();
        Encourage_clips_lists = new List<AudioClip>();
        LetterUS_clips_lists = new List<AudioClip>();
        StrokeUS_clips_lists = new List<AudioClip>();
        PhonicsUS_clips_lists = new List<AudioClip>();
        LetterUK_clips_lists = new List<AudioClip>();
        StrokeUK_clips_lists = new List<AudioClip>();
        PhonicsUK_clips_lists = new List<AudioClip>();

        string[] assetsPaths = UnityEditor.AssetDatabase.GetAllAssetPaths();
        if (SceneManager.GetActiveScene().name.Equals("GameMode2") || SceneManager.GetActiveScene().name.Equals("GameMode3"))
        {

            foreach (string assetPath in assetsPaths)
            {
                if (assetPath.Contains(voiceUSSoundPath))
                {

                    AudioClip audioClip = (AudioClip)UnityEditor.AssetDatabase.LoadAssetAtPath(assetPath, typeof(AudioClip));
                    if (audioClip != null)
                    {
                        AudioUS_clips_lists.Add(audioClip);
                    }

                }
                else
                if (assetPath.Contains(voiceUKSoundPath))
                {

                    AudioClip audioClip = (AudioClip)UnityEditor.AssetDatabase.LoadAssetAtPath(assetPath, typeof(AudioClip));
                    if (audioClip != null)
                    {
                        AudioUK_clips_lists.Add(audioClip);
                    }


                }
                else
                if (assetPath.Contains(encourageSoundPath))
                {

                    AudioClip audioClip = (AudioClip)UnityEditor.AssetDatabase.LoadAssetAtPath(assetPath, typeof(AudioClip));
                    if (audioClip != null)
                    {
                        Encourage_clips_lists.Add(audioClip);
                    }
                }
            }
            SortAudioByNameForGame2And3();
        }
        else if (SceneManager.GetActiveScene().name.Equals("GameMode1") || SceneManager.GetActiveScene().name.Equals("ARDemo"))
        {
            foreach (string assetPath in assetsPaths)
            {
                if (assetPath.Contains(voiceUSSoundPath))
                {

                    AudioClip audioClip = (AudioClip)UnityEditor.AssetDatabase.LoadAssetAtPath(assetPath, typeof(AudioClip));
                    if (audioClip != null)
                    {
                        AudioUS_clips_lists.Add(audioClip);
                    }

                }
                else
               if (assetPath.Contains(voiceUKSoundPath))
                {

                    AudioClip audioClip = (AudioClip)UnityEditor.AssetDatabase.LoadAssetAtPath(assetPath, typeof(AudioClip));
                    if (audioClip != null)
                    {
                        AudioUK_clips_lists.Add(audioClip);
                    }


                }
                if (assetPath.Contains(alphabetUSSoundPath))
                {

                    AudioClip audioClip = (AudioClip)UnityEditor.AssetDatabase.LoadAssetAtPath(assetPath, typeof(AudioClip));
                    if (audioClip != null)
                    {
                        LetterUS_clips_lists.Add(audioClip);
                    }

                }
                else
                if (assetPath.Contains(phonicsUSSoundPath))
                {

                    AudioClip audioClip = (AudioClip)UnityEditor.AssetDatabase.LoadAssetAtPath(assetPath, typeof(AudioClip));
                    if (audioClip != null)
                    {
                        PhonicsUS_clips_lists.Add(audioClip);
                    }


                }
                else
                if (assetPath.Contains(phonicsUKSoundPath))
                {

                    AudioClip audioClip = (AudioClip)UnityEditor.AssetDatabase.LoadAssetAtPath(assetPath, typeof(AudioClip));
                    if (audioClip != null)
                    {
                        PhonicsUK_clips_lists.Add(audioClip);
                    }
                }
                else
                if (assetPath.Contains(strokesUSSoundPath))
                {

                    AudioClip audioClip = (AudioClip)UnityEditor.AssetDatabase.LoadAssetAtPath(assetPath, typeof(AudioClip));
                    if (audioClip != null)
                    {
                        StrokeUS_clips_lists.Add(audioClip);
                    }
                }
                else
                if (assetPath.Contains(alphabetUKSoundPath))
                {

                    AudioClip audioClip = (AudioClip)UnityEditor.AssetDatabase.LoadAssetAtPath(assetPath, typeof(AudioClip));
                    if (audioClip != null)
                    {
                        LetterUK_clips_lists.Add(audioClip);
                    }
                }
                else
                if (assetPath.Contains(strokesUKSoundPath))
                {

                    AudioClip audioClip = (AudioClip)UnityEditor.AssetDatabase.LoadAssetAtPath(assetPath, typeof(AudioClip));
                    if (audioClip != null)
                    {
                        StrokeUK_clips_lists.Add(audioClip);
                    }
                }

            }

            SortAudioByNameforGame1();
        }
    }
#endif
    */
}
//https://www.facebook.com/dialog/feed?app_id=145634995501895&display=popup&caption=By%20Cordex&picture=http://dreamatico.com/data_images/apple/apple-1.jpg&redirect_uri=https://developers.facebook.com/tools/explorer