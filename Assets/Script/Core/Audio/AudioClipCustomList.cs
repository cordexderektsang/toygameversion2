﻿#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEditor;

public static class AudioClipCustomList
{
    private static List<AlphabetCoreConstants.AUDIO_CONTROLLER_TYPES> usedControllerTypes;

    public static void Show(SerializedProperty list, bool showListSize = true)
    {
        if (usedControllerTypes == null)
        {
            usedControllerTypes = new List<AlphabetCoreConstants.AUDIO_CONTROLLER_TYPES>();
        }

        bool isDone;

        switch (list.name)
        {
            //case AlphabetCoreConstants.AUDIO_CLIP_LIST:
            //    EditorGUILayout.PropertyField(list, true);
            //    break;
            case AlphabetCoreConstants.AUDIO_CONTROLLERS:
                EditorGUILayout.PropertyField(list);
                if (list.isExpanded)
                {
                    EditorGUI.indentLevel += 1;
                    EditorGUILayout.PropertyField(list.FindPropertyRelative("Array.size"));
                    for (int i = 0; i < list.arraySize; i++)
                    {
                        isDone = false;

                        SerializedProperty sp = list.GetArrayElementAtIndex(i);
                        EditorGUILayout.PropertyField(sp);

                        //Debug.Log("sp.name (1): " + sp.name);

                        if (sp.isExpanded)
                        {
                            EditorGUI.indentLevel += 1;

                            while (!isDone && sp.Next(true))
                            {
                                //Debug.Log("sp.name (2): " + sp.name);
                                switch (sp.name)
                                {
                                    case "Array":
                                    case "size":
                                    case "data":
                                        continue;
                                    case AlphabetCoreConstants.AUDIO_CONTROLLER_TYPE:
                                        // Only show unselected controller types

                                        break;
                                    case AlphabetCoreConstants.AUDIO_CLIP_LIST:
                                        EditorGUILayout.PropertyField(sp, true);
                                        break;
                                    default:
                                        EditorGUILayout.PropertyField(sp);
                                        break;
                                }

                                if (sp.name.Equals(AlphabetCoreConstants.AUDIO_CLIP_LAST_VARIABLE))
                                {
                                    isDone = true;
                                }
                            }

                            //AlphabetCoreConstants.TIMER_MAX_INCREASE_TYPES timerMaxIncreaseType = AlphabetCoreConstants.TIMER_MAX_INCREASE_TYPES.NONE;

                            //while (!isDone && sp.Next(true))
                            //{
                            //    //Debug.Log("sp.name (2): " + sp.name);
                            //    switch (sp.name)
                            //    {
                            //        case "Array":
                            //        case "size":
                            //        case "data":
                            //            continue;
                            //        case AlphabetCoreConstants.TIMER_MAX_INCREASE_TYPE:
                            //            timerMaxIncreaseType = (AlphabetCoreConstants.TIMER_MAX_INCREASE_TYPES)sp.enumValueIndex;
                            //            EditorGUILayout.PropertyField(sp);
                            //            break;
                            //        case AlphabetCoreConstants.SLOPE:
                            //            if (timerMaxIncreaseType.Equals(AlphabetCoreConstants.TIMER_MAX_INCREASE_TYPES.LINEAR))
                            //            {
                            //                EditorGUILayout.PropertyField(sp);
                            //            }
                            //            break;
                            //        case AlphabetCoreConstants.POWER:
                            //            if (timerMaxIncreaseType.Equals(AlphabetCoreConstants.TIMER_MAX_INCREASE_TYPES.EXPONENTIAL))
                            //            {
                            //                EditorGUILayout.PropertyField(sp);
                            //            }
                            //            break;
                            //        case AlphabetCoreConstants.Y_INTERCEPT:
                            //            //if (!timerMaxIncreaseType.Equals(AlphabetCoreConstants.TIMER_MAX_INCREASE_TYPES.NONE))
                            //            //{
                            //            //    EditorGUILayout.PropertyField(sp);
                            //            //}
                            //            break;
                            //        case AlphabetCoreConstants.ABSOLUTE_MAX_TIME:
                            //            if (!timerMaxIncreaseType.Equals(AlphabetCoreConstants.TIMER_MAX_INCREASE_TYPES.NONE))
                            //            {
                            //                EditorGUILayout.PropertyField(sp);
                            //            }
                            //            break;
                            //        default:
                            //            EditorGUILayout.PropertyField(sp);
                            //            break;
                            //    }

                            //    if (sp.name.Equals(AlphabetCoreConstants.INSTRUCTION_STATE_LAST_VARIABLE))
                            //    {
                            //        isDone = true;
                            //    }
                            //}

                            EditorGUI.indentLevel -= 1;
                        }
                    }
                    EditorGUI.indentLevel -= 1;
                }
                break;
        }
    }
}
#endif