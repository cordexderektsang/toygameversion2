﻿using UnityEngine;
using System.Collections.Generic;

public class GameAudioController
{
    public AudioSource audioSource;
    public List<AudioClip> audioClipList = new List<AudioClip>();
    public AudioClip clip;
    public bool isPlaying = false;
}
