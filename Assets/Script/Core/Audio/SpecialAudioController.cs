﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpecialAudioController : MonoBehaviour {
	// Testing Multi Clip audio //
	public AudioClip MustPlay;
	public List<AudioClip> ClipList;
    public List<AudioClip> UKClipList;
    private Hashtable table;
	
	void Start () {
		table = new Hashtable ();
		foreach (AudioClip clip in ClipList) {
			table.Add (clip.name, 0);
		}
	}
	
	public AudioClip[] getMultiClip (AudioClip clip) {
		return new AudioClip[] {clip, MustPlay, getClipFromTable (clip.name)};
	}
	
	private AudioClip getClipFromTable (string refClipHeader)
	{
		List<AudioClip> tempList = ClipList.FindAll(delegate (AudioClip clip) {return clip.name.StartsWith (refClipHeader);});
		
		int index = 0;
		int min = (int)table[tempList [index].name];
		for (int i = 0; i < tempList.Count; i++)
		{
			int count = (int)table[tempList [i].name];
			if (count < min) {
				min = count;
				index = i;
			}
		}
		
		table[tempList [index].name] = (int)table[tempList [index].name] + 1;
		return tempList [index];
	}
	////////////////////////
	
	// Hard code demo use //
	public AudioClip[] demoUseVoices;
	private int counter = 0;
	
	public AudioClip getAudioFromQueue (string name) {
		
		if (counter >= demoUseVoices.Length) {
			
			counter = 0;
		}
		
		return demoUseVoices [counter++];
	}
	////////////////////////
}
