﻿#if UNITY_EDITOR
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AlphabetCoreAudioCustomList
{
    //editor path
    private const string AUDIO_PATH = "Assets/AlphaAssets/Audio";
    private const string AUDIO_VOICE_PATH = AUDIO_PATH + "/voice";
    private const string AUDIO_ALPHABET_PATH = AUDIO_PATH + "/alphabet";
    private const string AUDIO_PHONICS_PATH = AUDIO_PATH + "/phonics";
    private const string AUDIO_STROKES_PATH = AUDIO_PATH + "/strokes";

    private static string VoiceUSSoundPath = AUDIO_VOICE_PATH + "/US_AUDIO";
    private static string VoiceUKSoundPath = AUDIO_VOICE_PATH + "/UK_AUDIO";
    private static string EncourageSoundPath = AUDIO_PATH + "/encourage";
    private static string AlphabetUSSoundPath = AUDIO_ALPHABET_PATH + "/alphabet_US";
    private static string AlphabetUKSoundPath = AUDIO_ALPHABET_PATH + "/alphabet_UK";
    private static string PhonicsUSSoundPath = AUDIO_PHONICS_PATH + "/phonics_US";
    private static string PhonicsUKSoundPath = AUDIO_PHONICS_PATH + "/phonics_UK";
    private static string StrokesUSSoundPath = AUDIO_STROKES_PATH + "/strokes_US";
    private static string StrokesUKSoundPath = AUDIO_STROKES_PATH + "/strokes_UK";

    /// <summary>
    /// sort by order use by edittor 
    /// </summary>
    private static void SortAudioByNameForGameMode2And3(AlphabetCoreAudioSystem alphabetCoreAudioSystem)
    {
        SortVoiceClips(alphabetCoreAudioSystem);
        alphabetCoreAudioSystem.Encourage_clips_lists = alphabetCoreAudioSystem.Encourage_clips_lists.OrderBy(obj => obj.name).ToList();
    }

    /// <summary>
    /// sort by order use by edittor 
    /// </summary>
    private static void SortAudioByNameforGameMode1(AlphabetCoreAudioSystem alphabetCoreAudioSystem)
    {
        SortLetterClips(alphabetCoreAudioSystem);
        alphabetCoreAudioSystem.PhonicsUS_clips_lists = alphabetCoreAudioSystem.PhonicsUS_clips_lists.OrderBy(obj => obj.name).ToList();
        alphabetCoreAudioSystem.PhonicsUK_clips_lists = alphabetCoreAudioSystem.PhonicsUK_clips_lists.OrderBy(obj => obj.name).ToList();
        alphabetCoreAudioSystem.StrokeUS_clips_lists = alphabetCoreAudioSystem.StrokeUS_clips_lists.OrderBy(obj => obj.name).ToList();
        alphabetCoreAudioSystem.StrokeUK_clips_lists = alphabetCoreAudioSystem.StrokeUK_clips_lists.OrderBy(obj => obj.name).ToList();
        SortVoiceClips(alphabetCoreAudioSystem);
    }

    /// <summary>
    /// sort by order use by edittor 
    /// </summary>
    private static void SortAudioByNameforARDemo(AlphabetCoreAudioSystem alphabetCoreAudioSystem)
    {
        SortLetterClips(alphabetCoreAudioSystem);
    }

    private static void SortLetterClips(AlphabetCoreAudioSystem alphabetCoreAudioSystem)
    {
        alphabetCoreAudioSystem.LetterUS_clips_lists = alphabetCoreAudioSystem.LetterUS_clips_lists.OrderBy(obj => obj.name).ToList();
        alphabetCoreAudioSystem.LetterUK_clips_lists = alphabetCoreAudioSystem.LetterUK_clips_lists.OrderBy(obj => obj.name).ToList();
    }

    private static void SortVoiceClips(AlphabetCoreAudioSystem alphabetCoreAudioSystem)
    {
        alphabetCoreAudioSystem.AudioUS_clips_lists = alphabetCoreAudioSystem.AudioUS_clips_lists.OrderBy(obj => obj.name).ToList();
        alphabetCoreAudioSystem.AudioUK_clips_lists = alphabetCoreAudioSystem.AudioUK_clips_lists.OrderBy(obj => obj.name).ToList();
    }

    /// <summary>
    /// Helper function to check for audio clip file to load into list
    /// </summary>
    /// <param name="assetPath">Path to asset to load</param>
    /// <param name="audioClipList">List to add clip to</param>
    private static void LoadClipIntoList(string assetPath, List<AudioClip> audioClipList)
    {
        AudioClip audioClip = (AudioClip)UnityEditor.AssetDatabase.LoadAssetAtPath(assetPath, typeof(AudioClip));
        if (audioClip != null)
        {
            audioClipList.Add(audioClip);
        }
    }

    /// <summary>
    /// Load all List of Audio Data by edittor
    /// </summary>
    public static void LoadData(AlphabetCoreAudioSystem alphabetCoreAudioSystem)
    {
        alphabetCoreAudioSystem.AudioUS_clips_lists.Clear();
        alphabetCoreAudioSystem.AudioUK_clips_lists.Clear();
        alphabetCoreAudioSystem.Encourage_clips_lists.Clear();
        alphabetCoreAudioSystem.LetterUS_clips_lists.Clear();
        alphabetCoreAudioSystem.LetterUK_clips_lists.Clear();
        alphabetCoreAudioSystem.StrokeUS_clips_lists.Clear();
        alphabetCoreAudioSystem.StrokeUK_clips_lists.Clear();
        alphabetCoreAudioSystem.PhonicsUS_clips_lists.Clear();
        alphabetCoreAudioSystem.PhonicsUK_clips_lists.Clear();

        string[] assetsPaths = UnityEditor.AssetDatabase.GetAllAssetPaths();

        if (SceneManager.GetActiveScene().name.Equals("GameMode2") || SceneManager.GetActiveScene().name.Equals("GameMode3"))
        {
            foreach (string assetPath in assetsPaths)
            {

                if (assetPath.Contains(VoiceUSSoundPath))
                {
                    LoadClipIntoList(assetPath, alphabetCoreAudioSystem.AudioUS_clips_lists);
                }
                else if (assetPath.Contains(VoiceUKSoundPath))
                {
                    LoadClipIntoList(assetPath, alphabetCoreAudioSystem.AudioUK_clips_lists);
                }
                else if (assetPath.Contains(EncourageSoundPath))
                {
                    LoadClipIntoList(assetPath, alphabetCoreAudioSystem.Encourage_clips_lists);
                }
                //if (assetPath.Contains(VoiceUSSoundPath))
                //{
                //    AudioClip audioClip = (AudioClip)UnityEditor.AssetDatabase.LoadAssetAtPath(assetPath, typeof(AudioClip));
                //    if (audioClip != null)
                //    {
                //        alphabetCoreAudioSystem.AudioUS_clips_lists.Add(audioClip);
                //    }
                //}
                //else if (assetPath.Contains(VoiceUKSoundPath))
                //{
                //    AudioClip audioClip = (AudioClip)UnityEditor.AssetDatabase.LoadAssetAtPath(assetPath, typeof(AudioClip));
                //    if (audioClip != null)
                //    {
                //        alphabetCoreAudioSystem.AudioUK_clips_lists.Add(audioClip);
                //    }
                //}
                //else if (assetPath.Contains(EncourageSoundPath))
                //{
                //    AudioClip audioClip = (AudioClip)UnityEditor.AssetDatabase.LoadAssetAtPath(assetPath, typeof(AudioClip));
                //    if (audioClip != null)
                //    {
                //        alphabetCoreAudioSystem.Encourage_clips_lists.Add(audioClip);
                //    }
                //}
            }

            SortAudioByNameForGameMode2And3(alphabetCoreAudioSystem);
        }
        else if (SceneManager.GetActiveScene().name.Equals("ARDemo"))
        {
            foreach (string assetPath in assetsPaths)
            {
                if (assetPath.Contains(AlphabetUSSoundPath))
                {
                    LoadClipIntoList(assetPath, alphabetCoreAudioSystem.LetterUS_clips_lists);
                }
                else if (assetPath.Contains(AlphabetUKSoundPath))
                {
                    LoadClipIntoList(assetPath, alphabetCoreAudioSystem.LetterUK_clips_lists);
                }
                //if (assetPath.Contains(AlphabetUSSoundPath))
                //{
                //    AudioClip audioClip = (AudioClip)UnityEditor.AssetDatabase.LoadAssetAtPath(assetPath, typeof(AudioClip));
                //    if (audioClip != null)
                //    {
                //        alphabetCoreAudioSystem.LetterUS_clips_lists.Add(audioClip);
                //    }
                //}
                //else if (assetPath.Contains(AlphabetUKSoundPath))
                //{
                //    AudioClip audioClip = (AudioClip)UnityEditor.AssetDatabase.LoadAssetAtPath(assetPath, typeof(AudioClip));
                //    if (audioClip != null)
                //    {
                //        alphabetCoreAudioSystem.LetterUK_clips_lists.Add(audioClip);
                //    }
                //}
            }

            SortAudioByNameforARDemo(alphabetCoreAudioSystem);
        }
        else if (SceneManager.GetActiveScene().name.Equals("GameMode1"))
        {
            foreach (string assetPath in assetsPaths)
            {
                if (assetPath.Contains(VoiceUSSoundPath))
                {
                    LoadClipIntoList(assetPath, alphabetCoreAudioSystem.AudioUS_clips_lists);
                }
                else if (assetPath.Contains(VoiceUKSoundPath))
                {
                    LoadClipIntoList(assetPath, alphabetCoreAudioSystem.AudioUK_clips_lists);
                }
                else if (assetPath.Contains(AlphabetUSSoundPath))
                {
                    LoadClipIntoList(assetPath, alphabetCoreAudioSystem.LetterUS_clips_lists);
                }
                else if (assetPath.Contains(AlphabetUKSoundPath))
                {
                    LoadClipIntoList(assetPath, alphabetCoreAudioSystem.LetterUK_clips_lists);
                }
                else if (assetPath.Contains(PhonicsUSSoundPath))
                {
                    LoadClipIntoList(assetPath, alphabetCoreAudioSystem.PhonicsUS_clips_lists);
                }
                else if (assetPath.Contains(PhonicsUKSoundPath))
                {
                    LoadClipIntoList(assetPath, alphabetCoreAudioSystem.PhonicsUK_clips_lists);
                }
                else if (assetPath.Contains(StrokesUSSoundPath))
                {
                    LoadClipIntoList(assetPath, alphabetCoreAudioSystem.StrokeUS_clips_lists);
                }
                else if (assetPath.Contains(StrokesUKSoundPath))
                {
                    LoadClipIntoList(assetPath, alphabetCoreAudioSystem.StrokeUK_clips_lists);
                }
                //if (assetPath.Contains(VoiceUSSoundPath))
                //{
                //    AudioClip audioClip = (AudioClip)UnityEditor.AssetDatabase.LoadAssetAtPath(assetPath, typeof(AudioClip));
                //    if (audioClip != null)
                //    {
                //        alphabetCoreAudioSystem.AudioUS_clips_lists.Add(audioClip);
                //    }
                //}
                //else if (assetPath.Contains(VoiceUKSoundPath))
                //{
                //    AudioClip audioClip = (AudioClip)UnityEditor.AssetDatabase.LoadAssetAtPath(assetPath, typeof(AudioClip));
                //    if (audioClip != null)
                //    {
                //        alphabetCoreAudioSystem.AudioUK_clips_lists.Add(audioClip);
                //    }
                //}
                //else if (assetPath.Contains(AlphabetUSSoundPath))
                //{
                //    AudioClip audioClip = (AudioClip)UnityEditor.AssetDatabase.LoadAssetAtPath(assetPath, typeof(AudioClip));
                //    if (audioClip != null)
                //    {
                //        alphabetCoreAudioSystem.LetterUS_clips_lists.Add(audioClip);
                //    }
                //}
                //else if (assetPath.Contains(AlphabetUKSoundPath))
                //{
                //    AudioClip audioClip = (AudioClip)UnityEditor.AssetDatabase.LoadAssetAtPath(assetPath, typeof(AudioClip));
                //    if (audioClip != null)
                //    {
                //        alphabetCoreAudioSystem.LetterUK_clips_lists.Add(audioClip);
                //    }
                //}
                //else if (assetPath.Contains(PhonicsUSSoundPath))
                //{
                //    AudioClip audioClip = (AudioClip)UnityEditor.AssetDatabase.LoadAssetAtPath(assetPath, typeof(AudioClip));
                //    if (audioClip != null)
                //    {
                //        alphabetCoreAudioSystem.PhonicsUS_clips_lists.Add(audioClip);
                //    }
                //}
                //else if (assetPath.Contains(PhonicsUKSoundPath))
                //{
                //    AudioClip audioClip = (AudioClip)UnityEditor.AssetDatabase.LoadAssetAtPath(assetPath, typeof(AudioClip));
                //    if (audioClip != null)
                //    {
                //        alphabetCoreAudioSystem.PhonicsUK_clips_lists.Add(audioClip);
                //    }
                //}
                //else if (assetPath.Contains(StrokesUSSoundPath))
                //{
                //    AudioClip audioClip = (AudioClip)UnityEditor.AssetDatabase.LoadAssetAtPath(assetPath, typeof(AudioClip));
                //    if (audioClip != null)
                //    {
                //        alphabetCoreAudioSystem.StrokeUS_clips_lists.Add(audioClip);
                //    }
                //}
                //else if (assetPath.Contains(StrokesUKSoundPath))
                //{
                //    AudioClip audioClip = (AudioClip)UnityEditor.AssetDatabase.LoadAssetAtPath(assetPath, typeof(AudioClip));
                //    if (audioClip != null)
                //    {
                //        alphabetCoreAudioSystem.StrokeUK_clips_lists.Add(audioClip);
                //    }
                //}
            }

            SortAudioByNameforGameMode1(alphabetCoreAudioSystem);
        }
    }
}
#endif