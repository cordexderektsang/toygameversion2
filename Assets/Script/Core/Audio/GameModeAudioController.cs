﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameModeAudioController : MonoBehaviour
{
    [Header("Root Gameobject for Audio Controllers")]
    public GameObject AudioControllerRoot;

    public List<AudioClipList> audioControllers;

    //private Dictionary<AlphabetCoreConstants.AUDIO_CONTROLLER_TYPES, List<AudioClip>> audioClipLists;
    //private Dictionary<AlphabetCoreConstants.AUDIO_CONTROLLER_TYPES, GameAudioController> audioControllerPropertiesDictionary;
    private Dictionary<string, GameAudioController> audioControllerPropertiesDictionary;

    void Start()
    {
        //audioClipLists = new Dictionary<AlphabetCoreConstants.AUDIO_CONTROLLER_TYPES, List<AudioClip>>();
        //audioControllerPropertiesDictionary = new Dictionary<AlphabetCoreConstants.AUDIO_CONTROLLER_TYPES, GameAudioController>();
        audioControllerPropertiesDictionary = new Dictionary<string, GameAudioController>();
        for (int i = 0; i < audioControllers.Count; i++)
        {
            if (audioControllerPropertiesDictionary.ContainsKey(audioControllers[i].audioControllerType))
            {
                Debug.LogWarning("Only one of each kind of audio controller type is allowed. Subsequent same type controllers will be skipped. (Skipped element index: " + i + ")");
                continue;
            }
            //audioClipListDictionary.Add(audioControllers[i].audioControllerType, audioControllers[i]);

            // Create GameObject child
            GameObject audioControllerGameObject = new GameObject();

            // Set parent
            audioControllerGameObject.transform.parent = AudioControllerRoot.transform;

            // Create audio source
            AudioSource audioSource = audioControllerGameObject.AddComponent<AudioSource>();
            audioSource.playOnAwake = false;

            // Create audio controller properties to store data
            GameAudioController audioControllerProperties = new GameAudioController();
            audioControllerProperties.audioSource = audioSource;
            audioControllerProperties.audioClipList = audioControllers[i].audioClipList;

            // Save reference to different audio sources
            audioControllerPropertiesDictionary.Add(audioControllers[i].audioControllerType, audioControllerProperties);

            // Save down list of audio in dictionary for easier searching
            //audioClipLists.Add(audioControllers[i].audioControllerType, audioControllers[i].audioClipList);
        }
    }

    // TODO: multiclip
    //public void Start()
    //{
    //    audioSource = GetComponent<AudioSource>();
    //}

    public void playOnce(string audioControllerType, string clipName)
    {
        GameAudioController audioControllerProperties;

        if (audioControllerPropertiesDictionary.TryGetValue(audioControllerType, out audioControllerProperties))
        {
            // Controller is found, proceed to play audio
            audioControllerProperties.isPlaying = true;
            AudioClip clip = audioControllerProperties.audioClipList.Find(delegate (AudioClip _clip) { return _clip.name.Equals(clipName); });
            audioControllerProperties.clip = clip;
            audioControllerProperties.audioSource.PlayOneShot(clip);
            StartCoroutine("countPlayTime", audioControllerProperties);
        }

        //IsPlaying = true;
        //AudioClip clip = audioClipList.Find(delegate (AudioClip _clip) { return _clip.name.Equals(clipName); });

        //audioSource.PlayOneShot(clip);
        //StartCoroutine("countTime", clip.length);
    }

    public void stopAll()
    {
        StopCoroutine("playMultiClip");
        StopCoroutine("text2Speech");
        StopCoroutine("countPlayTime");

        foreach (GameAudioController gameAudioController in audioControllerPropertiesDictionary.Values)
        {
            gameAudioController.audioSource.Stop();
            gameAudioController.isPlaying = false;
        }
    }

    //[System.Reflection.Obfuscation(ApplyToMembers = false)]
    //private IEnumerator playMultiClip(AudioClip[] clips)
    //{
    //    for (int i = 0; i < clips.Length; i++)
    //    {
    //        GetComponent<AudioSource>().PlayOneShot(clips[i]);
    //        yield return new WaitForSeconds(clips[i].length);
    //        yield return new WaitForSeconds(.05f);
    //    }

    //    IsPlaying = false;
    //}

    //[System.Reflection.Obfuscation(ApplyToMembers = false)]
    //private IEnumerator countTime(float t)
    //{
    //    yield return new WaitForSeconds(t);
    //    IsPlaying = false;
    //}

    public bool isAudioControllerPlaying(string audioControllerType)
    {
        GameAudioController audioControllerProperties;

        if (audioControllerPropertiesDictionary.TryGetValue(audioControllerType, out audioControllerProperties))
        {
            return audioControllerProperties.isPlaying;
        }

        return false;
    }

    [System.Reflection.Obfuscation(ApplyToMembers = false)]
    private IEnumerator countPlayTime(GameAudioController audioControllerProperties)
    {
        yield return new WaitForSeconds(audioControllerProperties.clip.length);
        CCLog.SpecialMsg("Stop countPlayTime for: " + audioControllerProperties.clip.name);
        audioControllerProperties.isPlaying = false;
    }

    //// Use web Voice
    //[System.Reflection.Obfuscation(ApplyToMembers = false)]
    //private IEnumerator text2Speech(string text)
    //{
    //    string url =
    //    "https://s.yimg.com/tn/dict/ox/mp3/v1/" + WWW.EscapeURL(text) + "@_us_1.mp3";
    //    //"http://tts-api.com/tts.mp3?q=" + WWW.EscapeURL(text);
    //    //"http://translate.google.com/translate_tts?tl=en&q=" +  WWW.EscapeURL(text);


    //    WWW www = new WWW(url);
    //    yield return www;
    //    GetComponent<AudioSource>().clip = www.GetAudioClip(false, false, AudioType.MPEG);
    //    GetComponent<AudioSource>().Play();

    //    yield return StartCoroutine("countTime", GetComponent<AudioSource>().clip.length);
    //    IsPlaying = false;
    //}
}
