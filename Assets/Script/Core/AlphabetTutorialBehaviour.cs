﻿	using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AlphabetTutorialBehaviour : MonoBehaviour 
{
	public const string kNonFristLunchTutorial = "kNonFristLunchTutorial";
	
	/// <summary>
	/// Gets a value from local storage.
	/// </summary>
	public bool NeedTutorial 
	{
		get {
			return SettingPanelBehaviour.IsTutorial;
		}
	}
	
	public bool isButtonClicked = false;
	
	/// <summary>
	/// Gets a value indicating whether this Stage Marker is find.
	/// </summary>
	/// <value>
	/// <c>true</c> if is tracker find; otherwise, <c>false</c>.
	/// </value>
	public bool isTrackerFind
	{
		get {
			return stageMarker.isTrackerFind;
		}
	}
	
	/// <summary>
	/// On tutorial skip button clicked.
	/// </summary>
	public delegate void OnTutorialSkipButtonClicked ();
	public OnTutorialSkipButtonClicked onTutorialSkipButtonClicked;
	
	/// <summary>
	/// On tutorial ok button clicked.
	/// </summary>
	public delegate void OnTutorialOkButtonClicked ();
	public OnTutorialOkButtonClicked onTutorialOkButtonClicked;
	
	/// <summary>
	/// On tutorial finish delegate.
	/// </summary>
	public delegate void OnTutorialFinish ();
	public OnTutorialFinish onTutorialFinish;
	
	/// <summary>
	/// The stage marker obj.
	/// </summary>
	public StageTrackableEventHandler stageMarker;
	
	/// <summary>
	/// The tutorial pages.
	/// </summary>
	public GameObject[] tutorialPages;
	
	/// <summary>
	/// The tutorial finish object.
	/// </summary>
	public GameObject tutorialFinishObject;
	
	/// <summary>
	/// The page pass audio.
	/// </summary>
	public AudioClip [] pagePassAudio;
	
	/// <summary>
	/// The last stage ID.
	/// </summary>
	public int lastStageID = -1;
	
	/// <summary>
	/// The tutorial ok button click counter.
	/// </summary>
	public int tutorialOkButtonClickCounter = 0;
	
	/// <summary>
	/// isReady to go to next page of tutorial.
	/// </summary>
	private bool isReady = true;
	
	/// <summary>
	/// The event fire before button click then need to resend the stage id.
	/// </summary>
	private bool needResendStageID = false;
	
	/// <summary>
	/// The stage id to resend.
	/// </summary>
	private int resendStageID = -1;
	
	/// <summary>
	/// The resend stage delay time.
	/// </summary>
	private float resendStageDelayTime = 0;
	
	/// <summary>
	/// The is not first lunch.
	/// </summary>
	private bool NonFristLunchTutorial = false;
	
	/// <summary>
	/// The current scene ID.
	/// </summary>
	private int currentSceneID = 1;
	
	/// <summary>
	/// Manual to show first tutorial page if need.
	/// </summary>
	void Start ()
	{
		string sceneName = UnityEngine.SceneManagement.SceneManager.GetActiveScene ().name;
		
		if (sceneName.Contains ("2"))
		{
			currentSceneID = 2;
		}
		else if (sceneName.Contains ("3"))
		{
			currentSceneID = 3;
		}
		
//		if (NeedTutorial)
//		{
//			UIGrid grid = tutorialPages [0].GetComponentInChildren <UIGrid> ();
//			
//			if (CCLocalStorageAPI.GET <int> (kNonFristLunchTutorial + currentSceneID) == 1)
//			{
//				NonFristLunchTutorial = true;
//				UIButton[] buttons = grid.GetComponentsInChildren <UIButton> (true);
//				foreach (UIButton btn in buttons)
//				{
//					if (!btn.gameObject.activeSelf)
//					{
//						btn.gameObject.SetActive (true);
//					}
//				}
//			}
//			else
//			{
//				NonFristLunchTutorial = false;
//			}
//			
//			tutorialPages [0].SetActive (true);
//			grid.repositionNow = true;
//			
//			// gameObject.GetComponentsInChildren<Vuforia.ITrackableEventHandler> ();
//		}
	}
	
	/// <summary>
	/// Stop all pointer to function.
	/// </summary>
	void OnDestroy () 
	{
		StopCoroutine ("showNextPageTutorial");
	}
	
	/// <summary>
	/// Resets the queued stage.
	/// </summary>
	private void resetQueuedStage ()
	{
		needResendStageID = false;
		resendStageID = -1;
		resendStageDelayTime = 0;
	}
	
	/// <summary>
	/// Finish and reset tutorial system.
	/// </summary>
	private void finishAndResetTutorial (bool withMsg = false)
	{				
		if (withMsg && tutorialFinishObject != null)
		{
			CCLog.SpecialMsg ("Show Finish Msg");
			tutorialFinishObject.SetActive (true);
			return;
		}
		else if (/*!withMsg &&*/ onTutorialFinish != null)
		{
			CCLog.SpecialMsg ("Tutorial Finish");
			onTutorialFinish ();
		}
		
		lastStageID = -1;
		resetQueuedStage ();
		tutorialPages [0].SetActive (false);
		
		if (!NonFristLunchTutorial)
		{
			CCLocalStorageAPI.SET <int> (kNonFristLunchTutorial + currentSceneID, 1);
		}
	}
	
	/// <summary>
	/// A call back from other script calling when the page of tutorial is finish
	/// </summary>
	/// <param name='stageID'>
	/// Which Stage ID is finish.
	/// </param>
	private void OnStageComplete (int stageID, float delayTime = 1.5f)
	{
		// (stageID - lastStageID != 1) to prevent not next stage is calling
		if (!NeedTutorial || (stageID - lastStageID != 1) || !isReady)
		{
			return;
		}

		isButtonClicked = false;
		isReady = false;
		
		lastStageID = stageID;
		
		// Check if the last page
		if (stageID + 1 < tutorialPages.Length)
		{
			StartCoroutine (showNextPageTutorial(stageID + 1, delayTime));
		}

		/*
		if (lastStageID + 2 >= tutorialPages.Length)
		{
			CCLog.SpecialMsg ("Tutorial Finish");
			finishAndResetTutorial (true);
		}
		*/
	}
	
	/// <summary>
	/// Queues the complete stage event.
	/// </summary>
	/// <param name='stageID'>
	/// Stage ID.
	/// </param>
	public void QueueCompleteStageEvent (int stageID, float delayTime = 1.5f)
	{
		if (!isButtonClicked)
		{
			needResendStageID = true;
			resendStageID = stageID;
			resendStageDelayTime = delayTime;
		}
		else
		{
			OnStageComplete (stageID, delayTime);
		}
	}
	
	/// <summary>
	/// Shows the special page of tutorial after 1.5 seconds
	/// </summary>
	/// <param name='stageID'>
	/// Stage ID.
	/// </param>
	[System.Reflection.Obfuscation(ApplyToMembers=false)]
	private IEnumerator showNextPageTutorial (int stageID, float delayTime)
	{
		if (pagePassAudio != null && stageID == 1 )
		{
			
			NGUITools.PlaySound(pagePassAudio[0], 15, 1);
		}
		if (pagePassAudio != null && stageID == 2 )
		{

			NGUITools.PlaySound(pagePassAudio[1], 15, 1);
		}
		
		yield return new WaitForSeconds (delayTime + .5f);
		
		isReady = true;
		tutorialPages [stageID].SetActive (true);
	}
	
	/// <summary>
	/// Raises the tutorial ok button click event.
	/// </summary>
	public void OnTutorialOkButtonClick ()
	{
		tutorialOkButtonClickCounter++;
		isButtonClicked = true;

		NGUITools.FindInParents <UIPanel> (UICamera.hoveredObject.transform.parent.gameObject).gameObject.SetActive (false);

		if (onTutorialOkButtonClicked != null)
		{
			onTutorialOkButtonClicked ();
		}
		
		if (needResendStageID && isReady)
		{
			OnStageComplete (resendStageID, resendStageDelayTime);
			resetQueuedStage ();
		}
	}
	
	/// <summary>
	/// Raises the tutorial finish button click event.
	/// </summary>
	public void OnTutorialFinishButtonClick ()
	{
		finishAndResetTutorial (true);
	}
	
	/// <summary>
	/// Raises the tutorial finish click event.
	/// </summary>
	public void OnTutorialFinishWithMsg ()
	{

	
		tutorialFinishObject.SetActive (false);
		finishAndResetTutorial ();
	}
	
	/// <summary>
	/// Raises the tutorial skip button click event.
	/// </summary>
	public void OnTutorialSkipButtonClick ()
	{	
		finishAndResetTutorial ();
		
		if (onTutorialSkipButtonClicked != null)
		{
			onTutorialSkipButtonClicked ();
		}
	}
}
