﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AlphabetLoadingPageBehaviour : MonoBehaviour
{
    /// <summary>
    /// The mode tutorial pages.
    /// </summary>
    public GameObject[] modePages;

    /// <summary>
    /// The tip tutorial pages.
    /// </summary>
    public GameObject[] tipPages;


    /// <summary>
    /// TryMemode Loading pages
    /// </summary>
    public GameObject[] TryMePages;


    /// <summary>
    /// TryMemode Loading pages
    /// </summary>
    public GameObject[] DemoPages;

    /// <summary>
    /// The mode page appear rate.
    /// </summary>
    [Range(0, 10)]
    public int modePageAppearRate = 6;

    /// <summary>
    /// The tip page appear rate.
    /// </summary>
    private int tipPageAppearRate;

    /// <summary>
    /// The queue list for stacking randomization list.
    /// </summary>
    private List<GameObject> queueList;

    void Start()
    {
       if(SettingPanelBehaviour.LanguageOption.Equals(""))
        {
            CCLocalStorageAPI.SET<string>("audioOptionAsString", "US_AUDIO");
            Debug.Log("firstStart");
        }
    }

    /// <summary>
    /// Starts ans show the loading page randomly.
    /// </summary>
    /// <param name='pageID'>
    /// The Page ID.
    /// </param>
    [System.Reflection.Obfuscation(ApplyToMembers = false)]
    public IEnumerator startLoadingPageRandomly(int pageID)
    {
        /// if not trymemode
        if (pageID < 3)
        {
            queueList = new List<GameObject>(modePages.Length + tipPages.Length);

            for (int i = 0; i < modePageAppearRate; i++)
            {
                queueList.Add(modePages[pageID]);
            }

            tipPageAppearRate = 10 - modePageAppearRate;

            for (int i = 0; i < tipPageAppearRate; i++)
            {
                queueList.Add(tipPages[i % tipPages.Length]);
            }

            yield return new WaitForEndOfFrame();

            queueList.RandomizeIList();

            queueList[0].SetActive(true);
        }
        else if (pageID == 3)
        {
            TryMePages[0].SetActive(true);
        }
        else
        {
            DemoPages[0].SetActive(true);
        }

    }
}
