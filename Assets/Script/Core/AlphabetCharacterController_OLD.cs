﻿using System.Text;
using UnityEngine;

public class AlphabetCharacterController_OLD : MonoBehaviour
{
    //public const int MOVING_STATE = 0;
    //public const int OUT_STATE = 1;
    //public const int IN_STATE = 2;

    public GameObject character; // Character gameObject
    public GameObject defaultSpawnLocationIn;
    public GameObject defaultSpawnLocationOut;

    public AlphabetCoreConstants.CHARACTER_DIRECTIONS direction = AlphabetCoreConstants.CHARACTER_DIRECTIONS.TOP; // Where does the character appear

    [Range(0.01f, 1.0f)]
	public float directionMovement = 0.1f; //movement for character;(0.01f);

    [Range(0.01f, 1.0f)]
    public float displayCharacterAspectX = 0.25f; // how far should the character appear (0.25f);
    [Range(0.01f, 1.0f)]
    public float displayCharacterAspectY = 0.5f; // how far should the character appear (0.25f);

	private BoxCollider rootBoxCollider; // BoxCollider to help calculate root size
    private UILabel informationDisplay;
    private UISprite characterSprite; // Character's UISprite
    private UISprite imageSprite; // Character's image UISprite to display icon

    Vector3 minBound; // min bound for the root object;
	Vector3 maxBound; // max bound for the root object;

	Vector3 centerBound; // Center bound for the root object;
	Vector3 characterMinBound; // min bound for the Character;
	Vector3 characterMaxBound; // max bound for the Character;

    private AlphabetCoreConstants.CHARACTER_STATES targetCharacterState; // reverse the animation cycle
    private AlphabetCoreConstants.CHARACTER_STATES characterState;

    float distanceX; // distance aspect X
    float distanceY; // distance aspect Y
    float oldPositionX; // old position X
	float oldPositionY; // old position Y

	void Start ()
	{
        rootBoxCollider = character.GetComponentsInChildren<BoxCollider>()[1]; // Know that there is only one collider in the child of Character object
        informationDisplay = character.GetComponentInChildren<UILabel>();
        characterSprite = character.GetComponent<UISprite>();
        imageSprite = character.GetComponentsInChildren<UISprite>()[2];;
        setBounds();
        targetCharacterState = AlphabetCoreConstants.CHARACTER_STATES.IN;
        characterState = AlphabetCoreConstants.CHARACTER_STATES.IN;

        setCharacterLocation ();
	}
	
	// Update is called once per frame
	void Update () 
	{
        if (!characterState.Equals(targetCharacterState))
        {
            moveToPoint();
        }
	}

    /// <summary>
    /// Set bounds for character
    /// </summary>
    private void setBounds()
    {
        minBound = rootBoxCollider.bounds.min;
        maxBound = rootBoxCollider.bounds.max;
        centerBound = rootBoxCollider.bounds.center;
        rootBoxCollider.gameObject.SetActive(false);

        BoxCollider characterBound = character.GetComponent<BoxCollider>();
        characterMinBound = characterBound.bounds.min;
        characterMaxBound = characterBound.bounds.max;
    }

    /// <summary>
    /// Set the character position after getting bounds location
    /// </summary>
    private void setCharacterLocation()
    {
        //character.transform.position = getCharacterLocationPosition(direction, character, minBound, maxBound, characterMinBound);
        character.transform.position = new Vector3(defaultSpawnLocationIn.transform.position.x, defaultSpawnLocationIn.transform.position.y, defaultSpawnLocationIn.transform.position.z);
        oldPositionX = character.transform.position.x * 1.2f;
        oldPositionY = character.transform.position.y; // go up position for top only
		distanceX = character.transform.position.x - (character.transform.position.x * displayCharacterAspectX);
        //distanceY = character.transform.position.y - (character.transform.position.y * displayCharacterAspectY); // go down position for top only
        distanceY = defaultSpawnLocationOut.transform.position.y;
    }

    /// <summary>
	/// Set location for character based on selection
	/// </summary>
    /// <returns>Vector3 of the new character position. Invalid position settings return a zero vector</returns>
	private Vector3 getCharacterLocationPosition(AlphabetCoreConstants.CHARACTER_DIRECTIONS direction, GameObject character, Vector3 minBound, Vector3 maxBound, Vector3 characterMinBound)
    {
        switch (direction)
        {
            case AlphabetCoreConstants.CHARACTER_DIRECTIONS.LEFT_BOTTOM:
                return new Vector3(minBound.x + characterMinBound.x, minBound.y - characterMinBound.y, minBound.z);
            case AlphabetCoreConstants.CHARACTER_DIRECTIONS.RIGHT_TOP:
                return new Vector3(maxBound.x - characterMinBound.x, maxBound.y + characterMinBound.y, maxBound.z);
            case AlphabetCoreConstants.CHARACTER_DIRECTIONS.RIGHT_BOTTOM:
                return new Vector3(maxBound.x - characterMinBound.x, minBound.y - characterMinBound.y, maxBound.z);
            case AlphabetCoreConstants.CHARACTER_DIRECTIONS.LEFT_TOP:
                return new Vector3(minBound.x + characterMinBound.x, maxBound.y + characterMinBound.y, maxBound.z);
            case AlphabetCoreConstants.CHARACTER_DIRECTIONS.TOP:
                //return new Vector3((minBound.x + characterMinBound.x) / 2.5f, maxBound.y * 1.5f, maxBound.z);
                return new Vector3(defaultSpawnLocationIn.transform.position.x, defaultSpawnLocationIn.transform.position.y , defaultSpawnLocationIn.transform.position.z);
            default:
                return new Vector3(0, 0, 0);
        }
    }

    /// <summary>
	/// Sets a new direction for the character to come out
	/// </summary>
	/// <param name="newDirection">New direction.</param>
	public void changeDirection(AlphabetCoreConstants.CHARACTER_DIRECTIONS newDirection)
    {
        direction = newDirection;
        setCharacterLocation();
    }

    /// <summary>
    /// Set a new direction for the character to come out randomly
    /// </summary>
    public void randomChangeDirection()
    {
        int randomPick = Random.Range(0, 5);
        switch (randomPick)
        {
            case 0:
                direction = AlphabetCoreConstants.CHARACTER_DIRECTIONS.LEFT_BOTTOM;
                setCharacterLocation();
                break;
            case 1:
                direction = AlphabetCoreConstants.CHARACTER_DIRECTIONS.RIGHT_TOP;
                setCharacterLocation();
                break;
            case 2:
                direction = AlphabetCoreConstants.CHARACTER_DIRECTIONS.RIGHT_BOTTOM;
                setCharacterLocation();
                break;
            case 3:
                direction = AlphabetCoreConstants.CHARACTER_DIRECTIONS.LEFT_TOP;
                setCharacterLocation();
                break;
            case 4:
                direction = AlphabetCoreConstants.CHARACTER_DIRECTIONS.TOP;
                setCharacterLocation();
                break;
        }
    }

    /// <summary>
    /// Move the character out to the screen area. Returns the state of the character after movement.
    /// </summary>
    /// <param name="character">Character object</param>
    /// <param name="direction">Direction of movement</param>
    /// <param name="distanceX">Distance check for X coordinate</param>
    /// <param name="distanceY">Distance check for Y coordinate</param>
    /// <param name="directionMovement">Total movement</param>
    /// <returns></returns>
    private static AlphabetCoreConstants.CHARACTER_STATES MoveCharacterOut(GameObject character, AlphabetCoreConstants.CHARACTER_DIRECTIONS direction, float distanceX, float distanceY, float directionMovement)
    {
        switch (direction)
        {
            case AlphabetCoreConstants.CHARACTER_DIRECTIONS.LEFT_BOTTOM:
            case AlphabetCoreConstants.CHARACTER_DIRECTIONS.LEFT_TOP:
                if (character.transform.position.x <= distanceX)
                {
                    character.transform.position = new Vector3(character.transform.position.x + directionMovement, character.transform.position.y, character.transform.position.z);
                    return AlphabetCoreConstants.CHARACTER_STATES.MOVING;
                }
                break;
            case AlphabetCoreConstants.CHARACTER_DIRECTIONS.RIGHT_BOTTOM:
            case AlphabetCoreConstants.CHARACTER_DIRECTIONS.RIGHT_TOP:
                if (character.transform.position.x >= distanceX)
                {
                    character.transform.position = new Vector3(character.transform.position.x - directionMovement, character.transform.position.y, character.transform.position.z);
                    return AlphabetCoreConstants.CHARACTER_STATES.MOVING;
                }
                break;
            case AlphabetCoreConstants.CHARACTER_DIRECTIONS.TOP:
                if (character.transform.position.y > distanceY)
                {
                    character.transform.position = new Vector3(character.transform.position.x, character.transform.position.y - directionMovement, character.transform.position.z);
                    return AlphabetCoreConstants.CHARACTER_STATES.MOVING;
                }
                break;
        }

        return AlphabetCoreConstants.CHARACTER_STATES.OUT;
    }

    /// <summary>
    /// Move the character in away from the screen area. Returns the state of the character after movement.
    /// </summary>
    /// <param name="character">Character object</param>
    /// <param name="direction">Direction of movement</param>
    /// <param name="oldPositionX">Distance check for X coordinate</param>
    /// <param name="oldPositionY">Distance check for Y coordinate</param>
    /// <param name="directionMovement">Total movement</param>
    /// <returns></returns>
    private static AlphabetCoreConstants.CHARACTER_STATES MoveCharacterIn(GameObject character, AlphabetCoreConstants.CHARACTER_DIRECTIONS direction, float oldPositionX, float oldPositionY, float directionMovement)
    {
        switch (direction)
        {
            case AlphabetCoreConstants.CHARACTER_DIRECTIONS.LEFT_BOTTOM:
            case AlphabetCoreConstants.CHARACTER_DIRECTIONS.LEFT_TOP:
                if (character.transform.position.x > oldPositionX)
                {
                    character.transform.position = new Vector3(character.transform.position.x - directionMovement, character.transform.position.y, character.transform.position.z);
                    return AlphabetCoreConstants.CHARACTER_STATES.MOVING;
                }
                break;
            case AlphabetCoreConstants.CHARACTER_DIRECTIONS.RIGHT_BOTTOM:
            case AlphabetCoreConstants.CHARACTER_DIRECTIONS.RIGHT_TOP:
                if (character.transform.position.x < oldPositionX)
                {
                    character.transform.position = new Vector3(character.transform.position.x + directionMovement, character.transform.position.y, character.transform.position.z);
                    return AlphabetCoreConstants.CHARACTER_STATES.MOVING;
                }
                break;
            case AlphabetCoreConstants.CHARACTER_DIRECTIONS.TOP:
                if (character.transform.position.y < oldPositionY)
                {
                    character.transform.position = new Vector3(character.transform.position.x, character.transform.position.y + directionMovement, character.transform.position.z);
                    return AlphabetCoreConstants.CHARACTER_STATES.MOVING;
                }
                break;
        }

        return AlphabetCoreConstants.CHARACTER_STATES.IN;
    }

    /// <summary>
    /// move character toward new point or backward to old point 
    /// </summary>
    private void moveToPoint ()
	{
		if (targetCharacterState.Equals(AlphabetCoreConstants.CHARACTER_STATES.OUT))
        {
            characterState = MoveCharacterOut(character, direction, distanceX, distanceY, directionMovement);
		}
        else if (targetCharacterState.Equals(AlphabetCoreConstants.CHARACTER_STATES.IN))
        {
            characterState = MoveCharacterIn(character, direction, oldPositionX, oldPositionY, directionMovement);
		}
	}

    /// <summary>
    /// Set the emotion (picture) and message displayed
    /// </summary>
    /// <param name="tagName">TagName of emotion to change to</param>
    /// <param name="message">Message to display in textbox</param>
    //public static void ChangeEmotion(UISprite characterSprite, UILabel informationDisplay, string inTagName, string message)
    public void setEmotion(AlphabetCoreConstants.CHARACTER_TAG_NAMESS tagName, string message)
    {
        int width = characterSprite.width;
        int height = characterSprite.height;

        StringBuilder tagNameStringBuilder = new StringBuilder();
        tagNameStringBuilder.Append(tagName.ToString().Substring(0, 1)).Append(tagName.ToString().Substring(1).ToLower());

        characterSprite.spriteName = tagNameStringBuilder.ToString();
        characterSprite.SetDimensions(width, height);

        informationDisplay.text = message;
	}


    /// <summary>
    /// set the icon next
    /// </summary>
    public void setIcon(AlphabetCoreConstants.CHARACTER_Image_Icon imageName)

    {
        int width = imageSprite.width;
        int height = imageSprite.height;
        StringBuilder tagNameStringBuilder = new StringBuilder();
        tagNameStringBuilder.Append(imageName.ToString().Substring(0, 1)).Append(imageName.ToString().Substring(1).ToLower());
        imageSprite.spriteName = tagNameStringBuilder.ToString();
        imageSprite.SetDimensions(width, height);
    }

    /// <summary>
    /// Sets the character to move out. Also changes the picture and the message
    /// </summary
    /// <param name="tagName">TagName of emotion to change to</param>
    /// <param name="message">Message to display in textbox</param>
    public void moveCharacterOut(AlphabetCoreConstants.CHARACTER_TAG_NAMESS tagName, AlphabetCoreConstants.CHARACTER_Image_Icon imageName, string message)
    {
        setEmotion(tagName, message);
        setIcon(imageName);
        moveCharacterOut();
    }

    /// <summary>
    /// Sets the character to move out
    /// </summary
    public void moveCharacterOut()
    {
        targetCharacterState = AlphabetCoreConstants.CHARACTER_STATES.OUT;
    }

    /// <summary>
    /// Sets the character to move away from the screen
    /// </summary
    public void moveCharacterIn()
    {
        targetCharacterState = AlphabetCoreConstants.CHARACTER_STATES.IN;
    }

    /// <summary>
    /// Get the current character state
    /// </summary>
    /// <returns>Current character state</returns>
    public AlphabetCoreConstants.CHARACTER_STATES getCharacterState()
    {
        return characterState;
    }

    /// <summary>
    /// Get the target character state it should change to
    /// </summary>
    /// <returns>Target character state</returns>
    public AlphabetCoreConstants.CHARACTER_STATES getTargetCharacterState()
    {
        return targetCharacterState;
    }
}
