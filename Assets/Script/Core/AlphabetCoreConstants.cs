﻿public class AlphabetCoreConstants
{
	public const string GAME_MODE = "game_mode";
    public const string TUTORIAL = "tutorial";
    public const string INSTRUCTION = "instruction";
    public const string ENCOURAGEMENT = "encouragement";

    // For custom editor use, do not change or remove unless changing BaseTutorialController
    public const string MAGIC_SCREEN = "magicScreen";
    public const string MAGIC_SCREEN2 = "magicScreen2";
    public const string TUTORIAL_STATES = "tutorialStates";

    public const string INSTRUCTION_STATES = "instructionStates";
    public const string TIMER_MAX_INCREASE_TYPE = "timerMaxIncreaseType";
    public const string SLOPE = "slope";
    public const string POWER = "power";
    public const string Y_INTERCEPT = "yIntercept";
    public const string ABSOLUTE_MAX_TIME = "absoluteMaxTime";
    public const string INSTRUCTION_STATE_LAST_VARIABLE = "absoluteMaxTime";

    public const string END_INSTRUCTION_RESET_TIME = "endInstructionResetTime";
    public const string IS_OVERRIDE_TUTORIAL_SETTING = "isOverrideTutorialSetting";
    public const string IS_NEED_TUTORIAL = "isNeedTutorial";
    public const string IS_OVERRIDE_WORD_SETTING  = "isOverrideWordSetting";
    public const string OVERRIDE_WORDS = "overrideWords";

    public const string AUDIO_CONTROLLERS = "audioControllers";
    public const string AUDIO_CONTROLLER_TYPE = "audioControllerType";
    public const string AUDIO_CLIP_LIST = "audioClipList";
    public const string AUDIO_CLIP_LAST_VARIABLE = "audioClipList";
    // End custom editor use

    public enum AUDIO_OPTIONS
    {
        US_AUDIO, UK_AUDIO //enum for Audio option 
    };

    public enum AUDIO_Selections
    {
        WORD_PRONUNCIATION, LETTER_STROKE, LETTER_PHONICS, LETTER_PRONUNCIATION, ENCOURAGEMENT, SOUND //enum for Audio Selection
    };

    public enum AUDIO_CONTROLLER_TYPES
    {
        GENERIC, RANDOM, BACKGROUND, GAME, INSTRUCTION, PHONICS, STROKES
    }

    public enum CHARACTER_STATES
    {
        MOVING, OUT, IN // enum for character states
    };

    public enum CHARACTER_DIRECTIONS
    {
        LEFT_BOTTOM, RIGHT_TOP, LEFT_TOP, RIGHT_BOTTOM, TOP //enum for option to choose where character will rotate out
    };

    public enum CHARACTER_TAG_NAMESS
    {
        TIMOJI1, TIMOJI2, TIMOJI3, TIMOJI4, TIMOJI5, TIMOJI6, TIMOJI7, TIMOJI8
    }

    public enum CHARACTER_Image_Icon
    {
        TIPS, LETTER, NEXT, TAP, MAGICSCREEN, NONE
    }

    public enum TIMER_MAX_INCREASE_TYPES
    {
        NONE, LINEAR, EXPONENTIAL
    }
}
