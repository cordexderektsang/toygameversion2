﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Vuforia;

public class AlphabetCoreSystem : MonoBehaviour
{
    public static bool capturing = false;
    private static int randomBgPickValue = 0;
    public List<ImageTargetBehaviour> currentITB;
	public bool isChangeAllowed = true;
    //public GameObject imageMarkerObjects;


	[Header("Game mode Two Core Script  for game mode 2")]
	private GameMode2CoreonTrackFunction gameMode2CoreonTrackFunction;


    void Start ()
    {
        capturing = false;
        randomBgPickValue = 0;
        currentITB = new List<ImageTargetBehaviour>();
		gameMode2CoreonTrackFunction = GetComponent<GameMode2CoreonTrackFunction> ();
    }
    #region Static Dataic
    public static string CorrectPronounciation (string spelling)
	{
		string output = spelling;
		switch (spelling)
		{
		case "yoyo": 
			output = "yoyo";
			break;
		case "xray":
			output = "x-ray";
			break;
		default:
			output = spelling;
			break;
		}
		
		return output;
	}
	
	public static string WordCategory (string word)
	{
		string output = "";

        switch (word)
		{
			case "apple": 
			case "lemon":
			case "milk":
			case "onion":
            case "jam":
            case "meat":
                output = "markerBG_fridge"; 
				break;

            case "arm":       
                output = "markerBG_dot";
                break;

            case "drum":
            case "organ":
            case "opera":
            case "judge":
            case "king":
            case "queen":
                output = "markerBG_theater";
				break;
			
			case "boat":
            case "fish":
            case "whale":
            case "yacht":
            case "quay":
             case"ship":
			case"isle":
                output = "markerBG_water";
				break;
			
			case "car":
			case "van":
				output = "markerBG_road"; 
				break;
			
			case "earth": 
				output = "markerBG_space";
				break;
			
			case "fork": 
			case "knife":
            case "yam":
            case "yolk":
            case "jar":
                output = "markerBG_kitchen"; 
				break;
			
			case "gate":
            case "key":
            case "chair":
            case "desk":
			case "woman":
			case "man":
            case "baby":
            case "wall":
                output = "markerBG_house"; 
				break;
			
			case "hat":
			case "shoes":
			case "watch":
			case "zip":
            case "dress":
            case "quilt":
            case "doll":
            case "veil":
                output = "markerBG_cloakroom"; 
				break;
			
			case "ice": 
				output = "markerBG_frozen";
				break;
			
			case "juice":
            case "ball":
                output = "markerBG_summer";
				break;
			
			case "ant":
			case "nut":
            case "frog":
            case "snail":
            case "hill":
            case "oak":
                output = "markerBG_leaf";
				break;
			
			case "pen":
            case "ink":
                output = "markerBG_paper";
				break;
			
			case "train": 
				output = "markerBG_rail";
				break;
			
			case "arrow":
			case "yoyo": 
			case "fire":
			case "iron":
            case "water":
                output = "markerBG_wood";
				break;
			
			case "xray":         
            case "teeth":
                output = "markerBG_hospital";
				break;

            case "lamp":
            case "clock":
            case "video":
            case "oven":
            case "radio":
                output = "markerBG_electric";
                break;

            case "dog":
            case "goat":
            case "cat":
            case "yak":
            case "duck":
            case "golf":
            case "goose":
            case "zebra":
			case "horse":
			case "fox":
			case "pig":
            case "axe":
            case "flag":
            case "boy":
            case "jump":
            case "net":
            case "road":
            case "rock":
            case "zoo":
                output = "markerBG_grass";
                break;

            case "panda":
            case "quail":
            case "owl":
            case "lion":
            case "nest":
            case "bear":
            case "wolf":
            case "tiger":
            case "snake":
            case "mouse":
                output = "markerBG_jungle";
                break;

            case "ring":
            case "jewel":
            case "gold":
            case "urn":
                output = "markerBG_redcloth";
                break;

            case "ear":
            case "eyes":
            case "gas":
            case "hair":
            case "hand":
            case "heart":
            case "iris":
            case "knee":
            case "leg":
            case "match":
            case "nail":
            case "mouth":
            case "neck":
            case "nose":
            case "thumb":
            case "toe":
            case "oil":
            case "piano":
            case "ruler":
            case "soap":
            case "under":
                output = "markerBG_dot";
                break;

            case "egg":
            case "cake":
            case "pear":
            case "cup":
            case "soup":
            case "jug":
            case "table":
            case "vase":
                output = "markerBG_tablecloth";
                break;

            case "eagle":
            case "kite":
            case "world":
            case "zero":
                output = "markerBG_sky";
                break;

            default: 
			output = "default";
                if (randomBgPickValue == 0)
                {
                    output = "markerBG_wood";
                    randomBgPickValue = 1;
                } else
                {
                    output = "markerBG_dot";
                    randomBgPickValue = 0;
                    break;
                }
				//"markerBG_guide";
				break;
		}

        return output;
	}
	#endregion
	
	#region Dict
	public delegate void OnWordHitPass (string spelling);
	/// <summary>
	/// Delegate on the word hit (spelling correctly).
	/// </summary>
	public OnWordHitPass OnWordHit;

    private List<string> words_list;
    private List<string> words_list_free;
    /*
	// Demo Use this for initialization
	void Start () 
	{
		initDict ("dict", 10);
	}
	*/

    /// <summary>
    /// New blank word list for custom made dict, 
    /// ** Game Mode 1 may use it, make sure the word list is not null **
    /// Defalut load a-z to wordlist
    /// Return -> The new word list.
    /// </summary>
    public List<string> initDict ()
	{
		return initDict ("alphabet", 1);
	}

    // capture status
    [System.Reflection.Obfuscation(ApplyToMembers = false)]
    public void Capturing()
    {
        capturing = true;
    }
    /// <summary>
    /// Init specific file to word list.
    /// ** Game Mode 2, 3 may use it **
    /// Return -> The new word list.
    /// param name='file_path' -> File path.
    /// param name='word_length_limit' -> Limit of the word length.
    /// </summary>
    public List<string> initDict (string file_path, int word_length_limit)
	{
        // Hard code 2 speed up init ** debug **
        words_list = new List<string>
            // (new string[] {"air","apple","boat","car"}); // Hard Code
            // (System.IO.File.ReadAllText(file_path).Split(new string[] { System.Environment.NewLine }, System.StringSplitOptions.None))); // Not From Resources File
            (CCManualLoadResourceObj.LoadText(file_path).Replace("\r", "").Split(new string[] { "\n" }, System.StringSplitOptions.RemoveEmptyEntries)); // From Resources
        CCLog.SpecialMsg("Dict load : " + words_list.Count + " words");
        words_list = words_list.FindAll(delegate (string str) { return str.Length <= word_length_limit; });


        words_list_free = new List<string>
                    // (new string[] {"air","apple","boat","car"}); // Hard Code
                    // (System.IO.File.ReadAllText(file_path).Split(new string[] { System.Environment.NewLine }, System.StringSplitOptions.None))); // Not From Resources File
                    (CCManualLoadResourceObj.LoadText(file_path + "_free").Replace("\r", "").Split(new string[] { "\n" }, System.StringSplitOptions.RemoveEmptyEntries)); // From Resources
        CCLog.SpecialMsg("Dict load : " + words_list_free.Count + " words");
        words_list_free = words_list_free.FindAll(delegate (string str) { return str.Length <= word_length_limit; });

        return getWordList ();
	}
	
	/// <summary>
	/// Add word to current word list. 
	/// ** See addWords2WordList () if add much word **
	/// ** make sure the word list is not null **
	/// Return -> The new word list.
	/// param name='word' -> Word to add to list.
	/// </summary>
	public List<string> addWord2WordList (string word)
	{
		words_list.Add (word);
		return getWordList ();
	}
	
	/// <summary>
	/// Removes the word from current word list. 
	/// ** make sure the word list is not null **
	/// Return -> he new word list.
	/// param name='word' -> Word to remove from list.
	/// </summary>
	public List<string> removeWordFromWordList (string word)
	{
		if(words_list.Contains (word))
			words_list.Remove (word);
		return getWordList ();
	}
	
	/// <summary>
	/// Randomize the order of word list. 
	/// ** Game Mode 3 may use it, make sure the word list is not null **
	/// Return -> The order of word list.
	/// </summary>
	public List<string> randomOrderOfWordList ()
    {
        words_list.RandomizeIList();
        words_list_free.RandomizeIList();
        return getWordList ();
	}
	
	/// <summary>
	/// Adds list of words to current word list. 
	/// ** make sure the word list is not null **
	/// Return -> The new word list.
	/// param name='word' -> Words to add to list.
	/// </summary>
	public List<string> addWords2WordList (List<string> words)
	{
		words_list.AddRange (words);
		return getWordList ();
	}


    /// <summary>
    /// return a single suggest random word 
    /// </summary>
    /// <returns></returns>
    public string returnASingleRandomWord()
    {
        List<string> temp = getWordList();
        int lenght = temp.Count;
        int randomIndex = Random.Range(0, lenght);
        return temp[randomIndex];
    }


  public List<string> returnFullListForDebug()
    {

        return words_list;
    }

    /// <summary>
    /// Get the current word list.
    /// ** make sure the word list is not null **
    /// Return -> The current word list.
    /// if is free version, return free words list
    /// else return paid words list
    /// </summary>
    public List<string> getWordList ()
	{
        Debug.Log(CCLocalStorageAPI.GET<int>("IsFullVersion") + "Full :" + (int)GameVersion.Full);
        if(CCLocalStorageAPI.GET<int>("IsFullVersion") != (int)GameVersion.Full)
        {
            Debug.Log("free");
            return words_list_free;
        }
        else
        {
            Debug.Log("full");
            return words_list;
        }
    }


    /// <summary>
    /// Header Alphabet setting.
    /// To filter out word from word list to use list
    /// </summary>
    public enum GameVersion
    {
        Free = 0,
        MarkerFull,
        Full
    }
    #endregion Dict

    #region Header
    /// <summary>
    /// Header Alphabet setting.
    /// To filter out word from word list to use list
    /// </summary>
    public enum HeaderAlphabetSettings 
	{
		StartWith,
		Contains,
		Same,
		All,
	}
	
	public delegate void OnHeaderAlphabetSet ();
	/// <summary>
	/// Delegate on first alphabet set.
	/// </summary>
	public OnHeaderAlphabetSet OnSetHeaderAlphabet;
	
	public delegate void OnHeaderAlphabetUnset ();
	/// <summary>
	/// Delegate on header alphabet unset.
	/// </summary>
	public OnHeaderAlphabetUnset OnUnsetHeaderAlphabet;

    public delegate void OnHeaderChange(string first_alphabet);
    /// <summary>
    /// Delegate on header change.
    /// </summary>
    public OnHeaderChange OnHeaderAlphabetChange;

    public delegate void OnMarkerCheck(bool isFreeMarkerScaned);
    /// <summary>
    /// Delegate on marker check.
    /// </summary>
    public OnMarkerCheck OnMarkerAlphabetCheck;


	public List<string> header_char_word_list;
	public string header_char = "";
	private bool isLetterMode = true;
	
	/// <summary>
	/// Setups the header alphabet with default header setting of StartWith
	/// Return -> The list of words in specific setting of header.
	/// param name='id' -> Alphabet in string format e.g. A = "a".
	/// </summary>
	public List<string> setupHeader (string id)
	{
		Debug.Log ("set up Header id + " + id);
		return setupSpelling (id, HeaderAlphabetSettings.StartWith);
	}

	public List<string> setupHeaderWithOldId()
	{
		Debug.Log("set up Header id + " + header_char);
		return setupSpelling(header_char, HeaderAlphabetSettings.StartWith);
	}


	/// <summary>
	/// Setups the header alphabet with setting
	/// Return -> The list of words in specific setting of header.
	/// param name='id' -> Alphabet in string format e.g. A = "a".
	/// param name='setting' -> Setting is a enum of HeaderAlphabetSettings
	/// </summary>
	public List<string> setupSpelling (string word, HeaderAlphabetSettings setting)
	{
		//Debug.Log(setting.ToString());
	/*if (!isChangeAllowed) {
			return getHeaderList ();
		}*/

		header_char = word;
        if (setting != HeaderAlphabetSettings.All)
		{
			header_char_word_list =
			   words_list.FindAll(delegate (string str)
			   {
				   if (setting == HeaderAlphabetSettings.Same)
					   return str == header_char;
				   else if (setting == HeaderAlphabetSettings.StartWith)
					   return str.StartsWith(header_char);
				   else return str.Contains(header_char);
			   });

           /* if (CCLocalStorageAPI.GET<int>("IsFullVersion") != (int)GameVersion.Free)
            {
				Debug.Log(setting.ToString() + " : " + word);
                header_char_word_list =
                words_list.FindAll(delegate (string str)
                {
                    if (setting == HeaderAlphabetSettings.Same)
                        return str == header_char;
                    else if (setting == HeaderAlphabetSettings.StartWith)
                        return str.StartsWith(header_char);
                    else return str.Contains(header_char);
                });
            } 
            else
            {
                header_char_word_list =
                words_list_free.FindAll(delegate (string str)
                {
                    if (setting == HeaderAlphabetSettings.Same)
                        return str == header_char;
                    else if (setting == HeaderAlphabetSettings.StartWith)
                        return str.StartsWith(header_char);
                    else return str.Contains(header_char);
                });
            }*/
		} 
		else header_char_word_list = getWordList ();
		
		if(OnSetHeaderAlphabet != null) OnSetHeaderAlphabet ();
		return getHeaderList ();
	}
	
	/// <summary>
	/// Releases the header of AlphabetCoreSystem. a callback will be alert if set.
	/// </summary>
	public void releaseHeader ()
	{
		//Debug.Log ("releaser Header : " + isChangeAllowed);
		/*if (!isChangeAllowed) {
			return;
		}*/
//		Debug.Log ("clear");
		header_char = "";
		if(header_char_word_list != null)
		{
			header_char_word_list.Clear ();
			header_char_word_list = null;
		}
		if(OnUnsetHeaderAlphabet != null) OnUnsetHeaderAlphabet ();
	}
	
	/// <summary>
	/// Resets the spelling.
	/// ** Game Mode 3  may use it. it same to releaseHeader () **
	/// </summary>
	public void resetSpelling (bool withStack)
	{
		if (withStack)
		{
			resetCurrentSpellingAndStack ();
		}
		else
		{
			resetCurrentSpellingOnly ();
		}
		releaseHeader ();
	}
	
	/// <summary>
	/// Is the header alphabet set.
	/// Return -> The header set or not.
	/// </summary>
	public bool isHeaderSet ()
	{
		return !string.IsNullOrEmpty (header_char);
	}
	
	/// <summary>
	/// Gets the current header.
	/// </summary>
	/// <returns>
	/// The current header char.
	/// </returns>
	public string getCurrentHeader ()
	{
		return header_char;
	}
	
	/// <summary>
	/// Gets the curernt header list.
	/// ** Header list = Filter of word list **
	/// Return -> The current header list.
	/// </summary>
	public List<string> getHeaderList ()
	{
		if(header_char_word_list == null || header_char_word_list.Count < 1)
		{
			header_char = null;
			header_char_word_list = null;
		}
		return header_char_word_list;
	}


	
	/// <summary>
	/// Resets the header to spelling first alphaet.
	/// ** Do the same things if calls ResetHeader and setupHeader methods/functions **
	/// ** Game Mode 2 may be use it **
	/// </summary>
	public void resetHeader2FirstWord ()
	{
		// Add on Beta Version
		// Add one more function to fix new gamemode 2

		if(_list == null || _list.Count < 1 || _list.Capacity < 1) return;
		_list.Sort 
		(
			delegate (AlphabetCoreTrackableEventHandler a, AlphabetCoreTrackableEventHandler b) 
			{ 
				return (a.transform.position.x < b.transform.position.x) ? -1 : (a.transform.position.x == b.transform.position.x) ? 0 : 1;
			}
		);
		
		releaseHeader ();
		setupHeader (getFirstAlphabetId ());
	}
	
	/*
	/// <summary>
	/// Checks the spelling between user input and core system words.
	/// ** if uncomment the if loop, middle spelling will be count in ** 
	/// Return -> The correct spelling from left to right.
	/// </summary>
	public string checkSpelling ()
	{
		if(header_char_word_list == null || header_char_word_list.Count < 1 || _list == null || _list.Count < 1) 
			return "";
		
		string input = result_spelling;
		string output = "";
		foreach(string word in header_char_word_list)
		{	
			if(word.StartsWith (input))
			{
				output = input;
			}
			else if (word [0] == input [0]) 
			{
				int index = 1;
				for (; index < word.Length; index++)
				{
					if (word [index] != input [index]) break;
				}
				output = word.Substring (0, index);
			}
			
			//else if(word.Contains (input))
			//{
			//	int index_of_word = word.IndexOf (input);
			//	return word.Substring (index_of_word, index_of_word + input.Length);
			//}
			
			else
			{
				output = "";
			}
		}
		return output;
	}
	*/
	#endregion Header
	
	#region POC
	/*
	// Version 1
	// Basic
	public static string CurrentSpelling = "";
	
	public void OnAlphabetChange (bool value, string id)
	{
		if (value)
		{
			CurrentSpelling += id;
		}
		else
		{
			if(CurrentSpelling.Contains(id))
			{
				int tmp = CurrentSpelling.LastIndexOf(id);
				CurrentSpelling = CurrentSpelling.Substring(0, tmp) + CurrentSpelling.Substring (tmp + 1, CurrentSpelling.Length - tmp - 1);
			}
		}
		
		CCLog.SpecialMsg ("Current " + CurrentSpelling);
	}
	
	// Version 2
	// Check Pos By AR Input
	public static int Index = 0;
	
	public class AlphabetMarker
	{
		public string id;
		public float pos;
		
		public AlphabetMarker (string id, float pos)
		{
			this.id = id;
			this.pos = pos;
		}
	}
	
	public static Hashtable CurrentAlphabetTable = new Hashtable ();
	
	public void OnAlphabetChange (bool value, string id, float xPos, ref int index)
	{
		if(value)
		{
			index = Index++;
			CurrentAlphabetTable.Add (index, new AlphabetMarker (id, xPos));
		}
		else if(CurrentAlphabetTable.ContainsKey (index))
		{
			CurrentAlphabetTable [index] = null;
			CurrentAlphabetTable.Remove(index);
		}
		
		List<AlphabetMarker> _list = new List<AlphabetMarker> (CurrentAlphabetTable.Count);
		foreach (AlphabetMarker _marker in CurrentAlphabetTable.Values)
		{
			_list.Add(_marker);
		}
		_list.Sort 
		(
			delegate (AlphabetMarker a, AlphabetMarker b) 
			{ 
				return (a.pos < b.pos) ? -1 : (a.pos == b.pos) ? 0 : 1;
			}
		);
		string result_spelling = "";
		foreach (AlphabetMarker marker in _list)
		{
			result_spelling += marker.id;
		}
		CCLog.SpecialMsg (result_spelling);
	}
	*/
	#endregion
	
	#region Core
	/// <summary>
	/// On stage marker found.
	/// </summary>
//	public delegate void OnStageMarkerFound ();
	//public OnStageMarkerFound onStageMarkerFound;
	
	/// <summary>
	/// On stage marker lost.
	/// </summary>
//	public delegate void OnStageMarkerLost ();
//	public OnStageMarkerLost onStageMarkerLost;
	
	/// <summary>
	/// Raises the stage marker find/lost event.
	/// </summary>
	/// <param name='isFound'>
	/// Is found.
	/// </param>
/*	public void OnStageMarkerEvent (bool isFound, ImageTargetBehaviour itb)
	{
        if (isFound)
		{
			if (onStageMarkerFound != null)
			{
				onStageMarkerFound ();
			}

            currentITB.Add(itb);
            updateMarkerObjectParent(true);
        }
        else if (onStageMarkerLost != null)
		{
            Debug.Log("onStageMarkerLost");
			currentITB.Remove(itb);
			onStageMarkerLost ();
			updateMarkerObjectParent(false);
        }
        else
        {
            Debug.Log("onStageMarkerLost remove");
            currentITB.Remove(itb);
            updateMarkerObjectParent(false);
        }
    }
	
	private void updateMarkerObjectParent(bool addImageTarget)
    {
        Debug.Log("currentITB " + currentITB.Count + " " + addImageTarget);
        // scan 1 magic screen
        if (!addImageTarget)
        {
            if (currentITB.Count > 0)
            {
                imageMarkerObjects.transform.SetParent(currentITB[0].transform);
                imageMarkerObjects.transform.localPosition = Vector3.zero;
                imageMarkerObjects.transform.localEulerAngles = Vector3.zero;
            }
        }
        else
        {
            if (currentITB.Count == 1)
            {
                imageMarkerObjects.transform.SetParent(currentITB[0].transform);
                imageMarkerObjects.transform.localPosition = Vector3.zero;
                imageMarkerObjects.transform.localEulerAngles = Vector3.zero;
            }
        }
    }*/
	/// <summary>
	/// On identifier marker click. 
	/// Created on beta version 1.4  2016/1/13
	/// </summary>
	public delegate void OnIDMarkerClick (Transform marker, string alphabet);
	/// <summary>
	/// Delegate on a id marker click.
	/// </summary>
	public OnIDMarkerClick OnClick;
	
	/// <summary>
	/// Raises the marker click event.
	/// </summary>
	/// <param name='alphabet'>
	/// The alphabet id.
	/// </param>
	public void OnMarkerClick (Transform marker, string alphabet)
	{
		if (OnClick != null)
		{
			OnClick (marker, alphabet);
		}
	}
	
	// Version 3
	// Improve Version 2 & Add Tracing Pos
	public delegate void OnInput (string input);
	/// <summary>
	/// Delegate on user input alphabet change.
	/// </summary>
	public OnInput OnInputChange;
	
	public delegate void OnEachInput (bool value, string input);
	/// <summary>
	/// Delegate on user input alphabet change each time.
	/// </summary>
	public OnEachInput OnEachInputChange;
	
	public bool needTracing = true;
	/// <summary>
	/// The user spelling.
	/// </summary>
	public string result_spelling = "";
	public List<AlphabetCoreTrackableEventHandler> _list = new List<AlphabetCoreTrackableEventHandler> ();


	public List<AlphabetCoreTrackableEventHandler>  return_ListOfTrackableEventHandle ()
	{
		return _list;
	}
	
	private void resetCurrentSpellingAndStack ()
	{
		resetCurrentSpellingOnly ();
		_list.Clear ();
		_list = new List<AlphabetCoreTrackableEventHandler> ();
	}
	
	private void resetCurrentSpellingOnly ()
	{
		result_spelling = "";
	}


	public string ReturnCurrnetLetter ()
	{
		return result_spelling;
	}
	
	/// <summary>
	/// Gets the first alphabet identifier.
	/// Return -> The first alphabet identifier.
	/// </summary>
	public string getFirstAlphabetId ()
	{
		return ((AlphabetCoreTrackableEventHandler)_list[0]).id;
	}

	/// <summary>
	/// reactive current on tracked marked then put in the first id  to get word list
	/// </summary>
	public void OnAlphabeReactiveCurrnetOnTrackedMarker ()
	{
		if (_list.Count > 0) {

			_list.Sort 
			(
				delegate (AlphabetCoreTrackableEventHandler a, AlphabetCoreTrackableEventHandler b) 
				{ 
					if(!GyroController.requireVerse){
						return (a.transform.position.x < b.transform.position.x) ? -1 : (a.transform.position.x == b.transform.position.x) ? 0 : 1;
					} else {
						return (a.transform.position.x > b.transform.position.x) ? -1 : (a.transform.position.x == b.transform.position.x) ? 0 : 1;
					}
				}
			);
			setupHeader (_list [0].id);
			string tmp = "";
			foreach (AlphabetCoreTrackableEventHandler marker in _list) {
				tmp += marker.id;
			}
			//game mode 2

			//if(result_spelling != tmp)
			if (!result_spelling.Equals (tmp)) {
				if (OnHeaderAlphabetChange != null) {	
					if (string.IsNullOrEmpty (result_spelling) && !string.IsNullOrEmpty (tmp)) {
						OnHeaderAlphabetChange ("" + tmp [0]);
					} else if (!string.IsNullOrEmpty (result_spelling) && string.IsNullOrEmpty (tmp)) {
						OnHeaderAlphabetChange ("");
					} else if ("" + result_spelling [0] != "" + tmp [0]) {
						OnHeaderAlphabetChange ("" + tmp [0]);
					}
				}

				result_spelling = tmp;
				if (OnInputChange != null) {
					Debug.Log ("react" + result_spelling);
					OnInputChange (result_spelling);
				}

				if (header_char_word_list != null && header_char_word_list.Contains (tmp)) {
					CCLog.SpecialMsg ("Hit : " + tmp);
				} else {
					CCLog.SpecialMsg ("Current Spelling : " + tmp);

				}
			}
		}

	}
		
	/// <summary>
	/// Don't call this!! 
	/// ** The core input method to System **
	/// </summary>
	public void OnAlphabetChange (bool value, AlphabetCoreTrackableEventHandler ch)
	{
	//	Debug.Log("Change MarkerID : " +ch.id + "value " + value);

			if (value) {
				_list.Add (ch);
				lock ("lock") {
				//Debug.Log(isHeaderSet());
				if (!isHeaderSet () ) {
					Debug.Log("setup MarkerID : " + ch.id);
						setupHeader (ch.id);
					}

				}
			} else {

			 if (_list.Contains (ch)) {
			//	Debug.Log("remove MarkerID : " + ch.id);
					_list.Remove (ch);
				}
					/*
				if(_list.Count < 1 && OnHeaderAlphabetChange != null) 
				{
					OnHeaderAlphabetChange ("");
				}
				*/
				//}
			}

		
		if(OnEachInputChange != null) OnEachInputChange (value, ch.id);

		if(needTracing) return;
		
		_list.Sort 
		(
			delegate (AlphabetCoreTrackableEventHandler a, AlphabetCoreTrackableEventHandler b) 
			{ 
				if(!GyroController.requireVerse){
					return (a.transform.position.x < b.transform.position.x) ? -1 : (a.transform.position.x == b.transform.position.x) ? 0 : 1;
				} else {
					return (a.transform.position.x > b.transform.position.x) ? -1 : (a.transform.position.x == b.transform.position.x) ? 0 : 1;
				}
			}
		);		
		
		string tmp = "";
		foreach (AlphabetCoreTrackableEventHandler marker in _list)
		{
			tmp += marker.id;
		}
		//game mode 2
	
        //if(result_spelling != tmp)
        if (!result_spelling.Equals(tmp))
        {
			if(OnHeaderAlphabetChange != null)
			{	
				if(string.IsNullOrEmpty(result_spelling) && !string.IsNullOrEmpty(tmp))
				{
					OnHeaderAlphabetChange (""+tmp[0]);
				}
				else if(!string.IsNullOrEmpty(result_spelling) && string.IsNullOrEmpty(tmp))
				{
					OnHeaderAlphabetChange ("");
				}
				else if(""+result_spelling[0] != ""+tmp[0])
				{
					OnHeaderAlphabetChange (""+tmp[0]);
				}
			}
			
			result_spelling = tmp;
			if(OnInputChange != null) 
			{
				Debug.Log ("marker change" + result_spelling);
				OnInputChange (result_spelling);
			}
			
			if(header_char_word_list != null && header_char_word_list.Contains (tmp))
			{
				CCLog.SpecialMsg ("Hit : " + tmp);
			}
			else 
			{
				CCLog.SpecialMsg ("Current Spelling : " + tmp);

			}
		}
	}

	public void ReactiveCurrentSameLetter ()
	{
		if (header_char_word_list != null && header_char_word_list.Contains (result_spelling))
		{
			if(OnWordHit != null) OnWordHit (result_spelling);
		}

	}
	
	void Update ()
	{
		/*
		// Change to AppController
		
		if (Input.GetMouseButtonUp (0) && !UICamera.isOverUI)//.hoveredObject.name == "UI Root"
		{
			RaycastHit hit;
			if(Physics.Raycast (Camera.main.ScreenPointToRay (Input.mousePosition), out hit))
			{
				AlphabetCoreObjClickEvent _event = hit.transform.GetComponent <AlphabetCoreObjClickEvent> ();
				if (_event != null)
				{
					_event.OnClick ();
				}
			}
			else 
			{
				Vuforia.CameraDevice.Instance.SetFocusMode (Vuforia.CameraDevice.FocusMode.FOCUS_MODE_TRIGGERAUTO);
			} 
		}
		*/
		
		if (needTracing /*&& _list.Count > 0*/)
		{
			_list.Sort 
			(
				/*delegate (AlphabetCoreTrackableEventHandler a, AlphabetCoreTrackableEventHandler b) 
				{ 
					return (a.transform.position.x < b.transform.position.x) ? -1 : (a.transform.position.x == b.transform.position.x) ? 0 : 1;
				}*/

				delegate (AlphabetCoreTrackableEventHandler a, AlphabetCoreTrackableEventHandler b) 
				{ 
					if(!GyroController.requireVerse){
					return (a.transform.position.x < b.transform.position.x) ? -1 : (a.transform.position.x == b.transform.position.x) ? 0 : 1;
					} else {
						return (a.transform.position.x > b.transform.position.x) ? -1 : (a.transform.position.x == b.transform.position.x) ? 0 : 1;
					}
				}
			);

			string tmp = "";
            bool isFreeMarkerScaned = false;
            foreach (AlphabetCoreTrackableEventHandler marker in _list)
			{
				tmp += marker.id;

                // to check the free marker or not
                /*if (marker.isFree)
                {
                    isFreeMarkerScaned = true;
                }*/

            }
			//Debug.Log("tmp word " + tmp);
			/*if (gameMode2CoreonTrackFunction != null) 
			{
				gameMode2CoreonTrackFunction.OnHitWordCorrentObjectSet (_list, tmp);
			}*/
		
            if (result_spelling != tmp)
            {
			//	Debug.Log(" tmp : " + tmp + " result_spelling " + result_spelling);
                if (OnHeaderAlphabetChange != null)
				{	
					if(string.IsNullOrEmpty(result_spelling) && !string.IsNullOrEmpty(tmp))
					{
						OnHeaderAlphabetChange (""+tmp[0]);
					}
					else if(!string.IsNullOrEmpty(result_spelling) && string.IsNullOrEmpty(tmp))
					{
						OnHeaderAlphabetChange ("");
					}
					else if(""+result_spelling[0] != ""+tmp[0])
					{
						OnHeaderAlphabetChange (""+tmp[0]);
					}
				}
				
				result_spelling = tmp;
				
				if(OnInputChange != null) 
				{
					//Debug.Log ("result_spelling : " + result_spelling);
		
					OnInputChange (result_spelling);
				}

                if(OnMarkerAlphabetCheck != null && result_spelling.Length > 0)
                {
					OnMarkerAlphabetCheck(false);
                    /*if (isFreeMarkerScaned)
                    {
                        if (CCLocalStorageAPI.GET<int>("IsFullVersion") != (int)GameVersion.Free)
                        {
                            CCLocalStorageAPI.SET<int>("IsFullVersion", (int)GameVersion.Free);
                            OnMarkerAlphabetCheck(isFreeMarkerScaned);
                        }
                    }
                    // no free marker scaned && is not free version
                    else
                    {
                        if (CCLocalStorageAPI.GET<int>("IsFullVersion") == (int)GameVersion.Free)
                        {
                            CCLocalStorageAPI.SET<int>("IsFullVersion", (int)GameVersion.MarkerFull);
                            OnMarkerAlphabetCheck(isFreeMarkerScaned);
                        }
                    }*/
                    
                }
			
			//	Debug.Log(header_char_word_list.Contains(result_spelling) + "  : " + result_spelling);
                if (header_char_word_list != null && header_char_word_list.Contains (result_spelling))
				{
				CCLog.SpecialMsg("Current hit Spelling : " + result_spelling);
					if(OnWordHit != null) OnWordHit (result_spelling);
				}
				else 
				{
					//CCLog.SpecialMsg ("Current Spelling : " + result_spelling);
					//Debug.Log ("Current Spelling : " + result_spelling);
					//CCPlayerStatusManager.ChangePlayerStatus (AlphabetCoreConstants.PLAYER_STATUS.SPELLING);
				/*	if (gameMode2CoreonTrackFunction != null) 
					{
						gameMode2CoreonTrackFunction.destoryCurrentDisplayModel ();
					}*/
				}


            }
		}
	}
	#endregion
}
