using UnityEngine;
using System.Collections;

public class AppController : MonoBehaviour 
{
	#if UNITY_ANDROID
	private bool isBackPress = false;
	private float lastClickTime;
#endif

    private int touchState = 0;
    private AutoFocusByTimer focus;
    private Touch[] touches = new Touch[3];
    private int whichFingerEndedTouch = -1;

    public GameObject SettingObj;
	
	// Use this for initialization
	void Start () 
	{
		Handheld.StopActivityIndicator ();
		Resources.UnloadUnusedAssets ();
		System.GC.Collect();
        focus = GetComponent<AutoFocusByTimer>();

    }

    //private void checkTouchCount(int checkCount)
    private void checkTouchCount()
    {
        if (Input.touchCount != touchState)
        {
            touchState = Input.touchCount;
        }
    }

    //private int checkTouchPhaseEnded(int checkCount)
    private int checkTouchPhaseEnded()
    {
        for (int i = 0; i < touchState; i++)
        {
            if (Input.GetTouch(i).phase == TouchPhase.Ended || Input.GetTouch(i).phase == TouchPhase.Canceled)
            {
                return i;
            }
        }

        return -1;
    }

	void mouseClick (){
		if (Input.GetMouseButtonDown (0)) {
			Vector2 touchPosition = Input.mousePosition;
			RaycastHit hit;
			//if(Physics.Raycast (Camera.main.ScreenPointToRay (Input.mousePosition), out hit))
			if (Physics.Raycast(Camera.main.ScreenPointToRay(touchPosition), out hit))
			{
				AlphabetCoreClickableObjBehaviour _event = hit.transform.GetComponent <AlphabetCoreClickableObjBehaviour> ();
				if (_event != null)
				{
					_event.OnClick ();
				}
				ButtonType _Button = hit.transform.GetComponent<ButtonType>();
				if (_Button != null)
				{
					_Button.OnCallAction();
				}
				DisablePlane _displayObjectPlaform = hit.transform.GetComponent<DisablePlane>();
				if (_displayObjectPlaform != null)
				{
					_displayObjectPlaform.OnCallModelAnimation();
				}
			}else if(!UICamera.isOverUI)
			{
			try {Vuforia.CameraDevice.Instance.SetFocusMode (Vuforia.CameraDevice.FocusMode.FOCUS_MODE_TRIGGERAUTO);
					focus.ResetTimerForFocus();
				} catch {}
			} 
		}

	}

	// Update is called once per frame
	void Update () 
	{
		mouseClick ();
        //checkTouchCount();
       // whichFingerEndedTouch = checkTouchPhaseEnded();

        //CCScreenLogger.LogStatic("touchState: " + touchState, 0);
        //CCScreenLogger.LogStatic("whichFingerEndedTouch: " + whichFingerEndedTouch, 1);

        //if (Input.GetMouseButtonUp (0) && !UICamera.isOverUI)//.hoveredObject.name == "UI Root"
		/* if (whichFingerEndedTouch != -1 && !UICamera.isOverUI)
        {
            //CCScreenLogger.LogStatic("touches[whichFingerEndedTouch].position: " + Input.GetTouch(whichFingerEndedTouch).position, 2);

            Vector2 touchPosition = Input.GetTouch(whichFingerEndedTouch).position;
			RaycastHit hit;
            //if(Physics.Raycast (Camera.main.ScreenPointToRay (Input.mousePosition), out hit))
            if (Physics.Raycast(Camera.main.ScreenPointToRay(touchPosition), out hit))
            {
                AlphabetCoreClickableObjBehaviour _event = hit.transform.GetComponent <AlphabetCoreClickableObjBehaviour> ();
				if (_event != null)
				{
					_event.OnClick ();
				}
			}
			else 
			{
				try {Vuforia.CameraDevice.Instance.SetFocusMode (Vuforia.CameraDevice.FocusMode.FOCUS_MODE_TRIGGERAUTO);
                    focus.ResetTimerForFocus();
                } catch {}
			} 
		}*/
		
		#if UNITY_ANDROID
		if (Input.GetKeyUp (KeyCode.Escape))
		{
			if (isBackPress && Time.time - lastClickTime < 4)
			{
				CCLog.SpecialMsg ("Quit");
				Application.Quit ();
			}
			else
			{
				lastClickTime = Time.time;
				isBackPress = true;
				CCLog.SpecialMsg ("To exit, please press the return key again.");
				#if !UNITY_EDITOR && UNITY_ANDROID
					var activity = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
					activity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
					{
						new AndroidJavaClass("android.widget.Toast").CallStatic<AndroidJavaObject>("makeText",activity,"To exit, please press the return key again.", 1).Call("show");
					}));
				#endif
			}
		}
		#endif
	}
	
	[Beebyte.Obfuscator.SkipRename]
	public void OnSettingButtonClick ()
	{
		if (SettingObj == null)
			return;
		
		SettingObj.SetActive (true);
	}
	
	[Beebyte.Obfuscator.SkipRename]
	public void OnCloseSettingButtonCLick ()
	{
		if (SettingObj == null)
			return;
		
		SettingObj.SetActive (false);
	}
	
	[Beebyte.Obfuscator.SkipRename]
	public void OnUIBackButtonClick ()
	{
		UnityEngine.SceneManagement.SceneManager.LoadScene ("Menu", UnityEngine.SceneManagement.LoadSceneMode.Single);

	}
		
	[Beebyte.Obfuscator.SkipRename]
	public void OnUICuptureClick ()
	{
        /*
		StartCoroutine
		(
			CCCaptureAndShare.CaptureNewScreenshot 
			(
				(Texture2D screenCapt) => 
				{
					CCCaptureAndShare.SaveToAlbum (screenCapt, "ARlphabet");
				}
			)
		);
		*/
        Application.CaptureScreenshot("Screenshot.png");
        AlphabetCoreAudioSystem.instance.playOnce ("_camera",AlphabetCoreConstants.AUDIO_Selections.SOUND);
        StartCoroutine("capture");
        //Invoke ("captureNow", .1f);
    }

    [Beebyte.Obfuscator.SkipRename]
    private IEnumerator capture()
    {
#if !UNITY_EDITOR && UNITY_ANDROID
        AndroidJavaObject activitys = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
        activitys.Call("runOnUiThread", new AndroidJavaRunnable(() =>
        {
            new AndroidJavaClass("android.widget.Toast").CallStatic<AndroidJavaObject>("makeText", activitys, "Loading", 5).Call("show");
        }));
#endif
        yield return new WaitForSeconds(0);
        CCCaptureAndShare.CaptureAndSaveToAlbum(
            "ARAlphabet",
            (int result) => {
#if !UNITY_EDITOR && UNITY_ANDROID
				    var activity = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
				    activity.Call("runOnUiThread", new AndroidJavaRunnable(() =>			                                                           {
					    new AndroidJavaClass("android.widget.Toast").CallStatic<AndroidJavaObject>("makeText", activity, "Photo saved to album", 1).Call("show");
				    }));
#endif
            }
        );
        AlphabetCoreSystem.capturing = false;
    }

    //private void captureNow ()
	//{
	//	CCCaptureAndShare.CaptureAndSaveToAlbum ("ARlphabet");
	//	#if !UNITY_EDITOR
	//		var activity = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
	//		activity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
	//		{
	//			new AndroidJavaClass("android.widget.Toast").CallStatic<AndroidJavaObject>("makeText",activity,"Save.", 1).Call("show");
	//		}));
	//	#endif	
	//}
	
	public static void ManualReleaseMemory ()
	{
		Resources.UnloadUnusedAssets ();
		System.GC.Collect();
	}
}
