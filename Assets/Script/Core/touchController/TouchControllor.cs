﻿using UnityEngine;
using System.Collections.Generic;

public class TouchControllor : MonoBehaviour {
    public int nbTouches;
    public Touch touch;
    public GameObject hitObject;
    public Camera nguicam;

    /// <summary>
    /// input mouse
    /// </summary>
    public bool isMouseDown = false;
    public bool isButtonPressed = false;
    private Vector3 mouseClickPositionContinues;
    private Vector3 mouseClickDownPosition;
    private Vector3 mouseClickUpPosition;

     public void Start()
    {
        nbTouches = 0;
    }
    public int returnNumberOfTouch()
    {
        return nbTouches;
    }

    public  Touch currentTouch()
    {
        return touch;
    }

    public  GameObject hitGameObject()
    {
        return hitObject;
    }

    public  bool ReturnisButtonPressed()
    {
        return isButtonPressed;
    }

 


    // Update is called once per frame
    void Update () {
        nbTouches = Input.touchCount;
        if (nbTouches > 0)
        {
        
           touch = Input.GetTouch(nbTouches-1);
            TouchPhase phase = touch.phase;
     
                RaycastHit hit;
                Ray ray = nguicam.ScreenPointToRay(touch.position);
                if (Physics.Raycast(ray, out hit))
                { 
                    if (hit.transform.gameObject != null ) {
                     //   Debug.Log("find : " + hit.transform.gameObject);
                        hitObject = hit.transform.gameObject;
                    }    else
                    {

                       // Debug.Log("notright empty");
                        hitObject = null;
                    }          
                } else
                {
                
                     //   Debug.Log("hit nothing empty");
                        hitObject = null;
                
                }
        }
        /*else if (nbTouches == 0)
        {
            if (Input.GetMouseButtonDown(0))
            {
                isButtonPressed = true;
                mouseClickDownPosition = Input.mousePosition;
            }
            else
            if (Input.GetMouseButtonUp(0))
            {
                isButtonPressed = false;
                mouseClickUpPosition = Input.mousePosition;
            }
            else if (Input.GetMouseButton(0))
            {
                mouseClickPositionContinues = Input.mousePosition;
            }

            RaycastHit hit;
            Ray ray = nguicam.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.gameObject != null)
                {
                    Debug.Log("find : " + hit.transform.gameObject);
                    hitObject = hit.transform.gameObject;
                }
                else
                {

                    Debug.Log("notright empty");
                    hitObject = null;
                }
            }
            else
            {

                Debug.Log("hit nothing empty");
                hitObject = null;

            }
        }*/

    }
}
