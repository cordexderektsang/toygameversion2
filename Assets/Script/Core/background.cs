﻿using UnityEngine;
using System.Collections;

public class background : MonoBehaviour {



    void OnApplicationFocus(bool hasFocus)
    {
       Debug.Log("Focus : " + hasFocus);
        if (hasFocus)
        {
           // Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }
    }

    void OnApplicationPause(bool pauseStatus)
    {
        Debug.Log("pauseStatus : " + pauseStatus);
        if (pauseStatus)
        {
            Time.timeScale = 0;
        } else
        {
            Time.timeScale = 1;
        }
    }
}
