﻿using UnityEngine;
using System.Collections;

public class AlphabetCoreClickableObjBehaviour : MonoBehaviour 
{
	[System.Reflection.Obfuscation(ApplyToMembers=false)]
	public virtual void OnClick ()
	{
		CCLog.SpecialMsg ("OnClick");
	}
}
