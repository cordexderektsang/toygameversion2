﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Matrix : MonoBehaviour
{
    private float zLimitBoundary = 0.5f;
    public List<numberOfItem> listOfDistance = new List<numberOfItem>();
    public string result = "";

    [System.Serializable]
    public class numberOfItem
    {
       public float distance;
        public int numberOfchild;
        public List<AlphabetCoreTrackableEventHandler> list = new List<AlphabetCoreTrackableEventHandler>();
    }
    
   public void distanceCheck(List<AlphabetCoreTrackableEventHandler> data)
    {
        listOfDistance = new List<numberOfItem>();
        Debug.Log(" after : " + data.Count);
        foreach (AlphabetCoreTrackableEventHandler infor in data)
        {
            int zValue = (int)infor.transform.localPosition.z;
            Debug.Log(infor.id + " : " + zValue);
           /* if (!returnSame(infor.transform.localPosition.z, infor))
            {
                numberOfItem item = new numberOfItem();
                item.distance = infor.transform.localPosition.z;
                item.numberOfchild = 1;
                item.list.Add(infor);
                listOfDistance.Add(item);
            }*/
        }
    }

    /// <summary>
    /// return bool about the current input zValue 
    /// </summary>
    /// <param name="zValue"></param>
    /// <returns></returns>
    private bool returnSame(float zValue , AlphabetCoreTrackableEventHandler infor)
    {
        foreach (numberOfItem data in listOfDistance)
        {
            if (Mathf.Abs(zValue - data.distance) < zLimitBoundary)
            {
                data.numberOfchild++;
                data.list.Add(infor);
                return true;
            }
        }
        return false;
    }



    /// <summary>
    /// return the most used z value
    /// </summary>
    /// <returns></returns>
    public List<AlphabetCoreTrackableEventHandler> returnList()
    {
        int items = -1;
        List<AlphabetCoreTrackableEventHandler> listData = new List<AlphabetCoreTrackableEventHandler>();
        foreach (numberOfItem data in listOfDistance)
        {
            if (data.numberOfchild > items)
            {
                items = data.numberOfchild;
                listData = data.list;
            }
        }
        return listData;
    }



    public List<AlphabetCoreTrackableEventHandler> newListOfWord(List<AlphabetCoreTrackableEventHandler> data)
    {

        distanceCheck(data);
        result = "";
      /*  List<AlphabetCoreTrackableEventHandler> reference = returnList();
        foreach (AlphabetCoreTrackableEventHandler datachar in reference)
        {
            result += datachar.id;
        }*/
        return returnList();
    }
    
	
}
