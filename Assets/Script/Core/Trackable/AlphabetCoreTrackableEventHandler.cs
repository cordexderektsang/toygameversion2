﻿using UnityEngine;
using System.Collections;
using Vuforia;
public class AlphabetCoreTrackableEventHandler : AlphabetCoreClickableObjBehaviour, ITrackableEventHandler
{
    #region PRIVATE_MEMBER_VARIABLES

    private TrackableBehaviour mTrackableBehaviour;
	private static AlphabetCoreSystem mSystem;
	private bool isFind = false, isReady = false;
	public bool isTrack = false;
	private int index;
	
    #endregion // PRIVATE_MEMBER_VARIABLES
	
	public bool isClickable = false;
    public bool isFree = false;
	public string id;

    #region UNTIY_MONOBEHAVIOUR_METHODS

    void Start()
    {
		mTrackableBehaviour = GetComponent<TrackableBehaviour>();
		Init ();
		mSystem = NGUITools.FindInParents <AlphabetCoreSystem> (transform);
    }
	
	public void Init ()
	{
        if (mTrackableBehaviour && !isReady)
        {
			isReady = true;
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }
	}
	
	public void Deinit ()
	{
        if (mTrackableBehaviour && isReady)
        {
		    isReady = false;
            StartCoroutine ("stopTracking");
        }	
	}
	
	[System.Reflection.Obfuscation(ApplyToMembers=false)]
	private IEnumerator stopTracking ()
	{
		yield return new WaitForEndOfFrame ();
		mTrackableBehaviour.UnregisterTrackableEventHandler(this);
	}
	
    #endregion // UNTIY_MONOBEHAVIOUR_METHODS

    #region PUBLIC_METHODS

    /// <summary>
    /// Implementation of the ITrackableEventHandler function called when the
    /// tracking state changes.
    /// </summary>
    public void OnTrackableStateChanged(
                                    TrackableBehaviour.Status previousStatus,
                                    TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            OnTrackingFound ();
			// Version 1
			//mSystem.OnAlphabetChange(true, id);
        }
        else if(isFind)
        {
            OnTrackingLost ();
			// Version 1
			//mSystem.OnAlphabetChange(false, id);
        }
    }

    #endregion // PUBLIC_METHODS

    #region PRIVATE_METHODS
	[System.Reflection.Obfuscation(ApplyToMembers=false)]
	public override void OnClick ()
	{
		if (isClickable) 
		{
			mSystem.OnMarkerClick (transform, id);
		}
	}

	private void OnTrackingFound()
	{
		isFind = true;
		isTrack = true;
		Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
		Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);
		Animation[] animationComponents = GetComponentsInChildren<Animation>(true);

		// Enable rendering:
		foreach (Renderer component in rendererComponents)
		{
			component.enabled = true;
		}

		// Enable colliders:
		foreach (Collider component in colliderComponents)
		{
			component.enabled = true;
		}

		// Enable animations:
		foreach (Animation component in animationComponents)
		{
			component.Play();
		}
		if (AlphabetGameMode1UISystemver2.TrackableMarkers != null && !AlphabetGameMode1UISystemver2.TrackableMarkers.Contains(this))
		{
			AlphabetGameMode1UISystemver2.TrackableMarkers.Add(this);
		}

		// Version 2
		//mSystem.OnAlphabetChange(true, id, transform.position.x, ref index);

		// Version 3
		//GyroController.isFirstStart = false;
		mSystem.OnAlphabetChange(true, this);
	
	}

    private void OnTrackingLost()
    {
		isTrack = false;
        Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
        Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);
		Animation[] animationComponents = GetComponentsInChildren<Animation>(true);

        // Disable rendering:
        foreach (Renderer component in rendererComponents)
        {
            component.enabled = false;
        }

        // Disable colliders:
        foreach (Collider component in colliderComponents)
        {
            component.enabled = false;
        }
		
		foreach (Animation component in animationComponents)
		{
			component.Stop ();
		}
		
		// Version 2
		//mSystem.OnAlphabetChange(false, id, transform.position.x, ref index);
		
		// Version 3
		if (AlphabetGameMode1UISystemver2.TrackableMarkers != null && AlphabetGameMode1UISystemver2.TrackableMarkers.Contains(this))
		{
			AlphabetGameMode1UISystemver2.TrackableMarkers.Remove(this);
		}
		mSystem.OnAlphabetChange (false, this);

    }

	/// <summary>
	/// use to reactive the object when object attach to this object
	///follow the current trackable event
	/// </summary>
	public void ResetActiveTrackableEvent ()
	{
	//	Debug.Log ("id Track " + isTrack + " name :" + this.gameObject.name);
		Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
		Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);
		Animation[] animationComponents = GetComponentsInChildren<Animation>(true);

		// Disable rendering:
		foreach (Renderer component in rendererComponents)
		{
			component.enabled = isTrack;
		}

		// Disable colliders:
		foreach (Collider component in colliderComponents)
		{
			component.enabled = isTrack;
		}
		foreach (Animation component in animationComponents)
		{
			if (!isTrack) {
				component.Stop ();
			} else {
				component.Play ();
			}
		}
			
	}
    #endregion // PRIVATE_METHODS
}
