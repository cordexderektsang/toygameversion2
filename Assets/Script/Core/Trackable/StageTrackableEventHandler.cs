﻿using UnityEngine;
using System.Collections;
using Vuforia;

public class StageTrackableEventHandler : MonoBehaviour, 
										  ITrackableEventHandler
{
    public bool isTrackerFind;

    private ImageTargetBehaviour itb;

    #region PRIVATE_MEMBER_VARIABLES
    private TrackableBehaviour mTrackableBehaviour;
	private static AlphabetCoreSystem mSystem;
	private bool isFind = false, isReady = false;
    #endregion // PRIVATE_MEMBER_VARIABLES
	

    #region UNTIY_MONOBEHAVIOUR_METHODS
    void Start()
    {
		isTrackerFind = false;
		mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        itb = GetComponent<ImageTargetBehaviour>();
        Init ();
		mSystem = NGUITools.FindInParents <AlphabetCoreSystem> (transform);
    }
	
	public void Init ()
	{
        if (mTrackableBehaviour && !isReady)
        {
			isReady = true;
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }
	}
	
	public void Deinit ()
	{
		CCLog.SpecialMsg ("Deinit");
        if (mTrackableBehaviour && isReady)
        {
            isReady = false;
            StartCoroutine("stopTracking");
        }	
	}
	
	[System.Reflection.Obfuscation(ApplyToMembers=false)]
    private IEnumerator stopTracking ()
    {
		CCLog.SpecialMsg ("stopTracking");
        yield return new WaitForEndOfFrame();
        mTrackableBehaviour.UnregisterTrackableEventHandler(this);
        yield return new WaitForEndOfFrame();
    }
    #endregion // UNTIY_MONOBEHAVIOUR_METHODS

    #region PUBLIC_METHODS
    /// <summary>
    /// Implementation of the ITrackableEventHandler function called when the
    /// tracking state changes.
    /// </summary>
    public void OnTrackableStateChanged(
                                    TrackableBehaviour.Status previousStatus,
                                    TrackableBehaviour.Status newStatus)
    {
		// CCLog.SpecialMsg ("OnTrackableStateChanged");
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            OnTrackingFound ();
        }
        else if(isFind)
        {
            OnTrackingLost ();
        }
    }
    #endregion // PUBLIC_METHODS

    #region PRIVATE_METHODS
    private void OnTrackingFound()
    {

        isTrackerFind = true;
        isFind = true;
        //CCPlayerStatusManager.ChangePlayerStatus (AlphabetCoreConstants.PLAYER_STATUS.BLACK_BOARD);
        //CCPlayerStatusManager.IsMagicScreenScanned = true;

//        mSystem.	OnStageMarkerEvent(true, itb);
        Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
        Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);
		
        // Enable rendering:
        foreach (Renderer component in rendererComponents)
        {
            if (!component.name.Contains("Col ") && !component.name.Equals("Back"))
            {
                component.enabled = true;
            }
        }

        // Enable colliders:
        foreach (Collider component in colliderComponents)
        {
            component.enabled = true;
        }
		
    }

    private void OnTrackingLost()
    {
		CCLog.SpecialMsg ("OnTrackingLost");
        isTrackerFind = false;
        //CCPlayerStatusManager.IsMagicScreenScanned = false;

//        mSystem.OnStageMarkerEvent(false, itb);
        if (mSystem.currentITB.Count == 0)
        {
            Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
            Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);

            // Disable rendering:
            foreach (Renderer component in rendererComponents)
            {
                if (!component.name.Contains("Col ") && !component.name.Equals("Back"))
                {
                    component.enabled = false;
                }
            }

            // Disable colliders:
            foreach (Collider component in colliderComponents)
            {
                component.enabled = false;
            }

        }
    }
    #endregion // PRIVATE_METHODS
}
