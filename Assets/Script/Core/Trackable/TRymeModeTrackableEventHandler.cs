﻿using UnityEngine;
using System.Collections;
using Vuforia;
public class TRymeModeTrackableEventHandler : AlphabetCoreClickableObjBehaviour, ITrackableEventHandler
{
    #region PRIVATE_MEMBER_VARIABLES
    private TrackableBehaviour mTrackableBehaviour;
    private static AlphabetCoreSystem mSystem;
    private bool isFind = false, isReady = false;
	private int index;

    #endregion // PRIVATE_MEMBER_VARIABLES

    public bool isClickable = false;
    public string id;

    #region UNTIY_MONOBEHAVIOUR_METHODS

    void Start()
    {
		mTrackableBehaviour = GetComponent<TrackableBehaviour>();
		Init ();
        mSystem = NGUITools.FindInParents<AlphabetCoreSystem>(transform);
    }
	
	public void Init ()
	{
        if (mTrackableBehaviour && !isReady)
        {
			isReady = true;
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }
	}
	
	public void Deinit ()
	{
        if (mTrackableBehaviour && isReady)
        {
			isReady = false;
            StartCoroutine ("stopTracking");
        }	
	}
	
	[System.Reflection.Obfuscation(ApplyToMembers=false)]
	private IEnumerator stopTracking ()
	{
		yield return new WaitForEndOfFrame ();
		mTrackableBehaviour.UnregisterTrackableEventHandler(this);
	}
	
    #endregion // UNTIY_MONOBEHAVIOUR_METHODS

    #region PUBLIC_METHODS

    /// <summary>
    /// Implementation of the ITrackableEventHandler function called when the
    /// tracking state changes.
    /// </summary>
    public void OnTrackableStateChanged(
                                    TrackableBehaviour.Status previousStatus,
                                    TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            OnTrackingFound ();
        }
        else if(isFind)
        {
            OnTrackingLost ();
        }
    }

    #endregion // PUBLIC_METHODS

    #region PRIVATE_METHODS
    [System.Reflection.Obfuscation(ApplyToMembers = false)]
    public override void OnClick()
    {
        if (isClickable)
        {
            mSystem.OnMarkerClick(transform, id);
        }
    }

    private void OnTrackingFound()
    {
		isFind = true;
        Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
        Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);
		Animation[] animationComponents = GetComponentsInChildren<Animation>(true);
		
        // Enable rendering:
        foreach (Renderer component in rendererComponents)
        {
            component.enabled = true;
        }

        // Enable colliders:
        foreach (Collider component in colliderComponents)
        {
            component.enabled = true;
        }
		
		// Enable animations:
		foreach (Animation component in animationComponents)
		{
			component.Play ();
		}
    }

    private void OnTrackingLost()
    {
        Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
        Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);
		Animation[] animationComponents = GetComponentsInChildren<Animation>(true);

        // Disable rendering:
        foreach (Renderer component in rendererComponents)
        {
            component.enabled = false;
        }

        // Disable colliders:
        foreach (Collider component in colliderComponents)
        {
            component.enabled = false;
        }
		
		foreach (Animation component in animationComponents)
		{
			component.Stop ();
		}
    }

    #endregion // PRIVATE_METHODS
}
