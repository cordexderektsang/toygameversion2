﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Vuforia;
public class ARDemoTrackableEventHandler : MonoBehaviour, ITrackableEventHandler
{
    public BaseTrymeController trymeController;

    public ImageTargetBehaviour itb;
    #region PRIVATE_MEMBER_VARIABLES
    private TrackableBehaviour mTrackableBehaviour;
	bool isFind = false, isReady = false;
    public bool isTrack = false;
	private int index;
	
    #endregion // PRIVATE_MEMBER_VARIABLES

    #region UNTIY_MONOBEHAVIOUR_METHODS

    void Start()
    {
		mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        itb = GetComponent<ImageTargetBehaviour>();
        Init ();
    }
	
	public void Init ()
	{
        if (mTrackableBehaviour && !isReady)
        {
			isReady = true;
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }
	}
	
	public void Deinit ()
	{
        if (mTrackableBehaviour && isReady)
        {
		    isReady = false;
            StartCoroutine ("stopTracking");
        }	
	}
	
	[System.Reflection.Obfuscation(ApplyToMembers=false)]
	private IEnumerator stopTracking ()
	{
		yield return new WaitForEndOfFrame ();
		mTrackableBehaviour.UnregisterTrackableEventHandler(this);
	}
	
    #endregion // UNTIY_MONOBEHAVIOUR_METHODS

    #region PUBLIC_METHODS

    /// <summary>
    /// Implementation of the ITrackableEventHandler function called when the
    /// tracking state changes.
    /// </summary>
    public void OnTrackableStateChanged(
                                    TrackableBehaviour.Status previousStatus,
                                    TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            OnTrackingFound ();
			// Version 1
			//mSystem.OnAlphabetChange(true, id);
        }
        else if(isFind)
        {
            OnTrackingLost ();
			// Version 1
			//mSystem.OnAlphabetChange(false, id);
        }
    }

    #endregion // PUBLIC_METHODS



    #region PRIVATE_METHODS


    private void OnTrackingFound()
    {
        Debug.Log("found " + name);
        isFind = true;
        isTrack = true;
        trymeController.isTrack(true, itb);

        Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
        Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);
		Animation[] animationComponents = GetComponentsInChildren<Animation>(true);



        // Enable rendering:
        foreach (Renderer component in rendererComponents)
        {
            component.enabled = true;
        }

        // Enable colliders:
        foreach (Collider component in colliderComponents)
        {
            component.enabled = true;
        }
		
		// Enable animations:
		foreach (Animation component in animationComponents)
		{
            component["new_try_me"].enabled = true;
            component["new_try_me"].speed = 1.05f;
        }

    }

    private void OnTrackingLost()
    {
        isTrack = false;
        Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
        Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);
		Animation[] animationComponents = GetComponentsInChildren<Animation>(true);
        Debug.Log("lost " + name);
        trymeController.isTrack(false, itb);

        if (trymeController.currentITB.Count == 0)
        {
            // Disable rendering:
            foreach (Renderer component in rendererComponents)
            {
                component.enabled = false;
            }

            // Disable colliders:
            foreach (Collider component in colliderComponents)
            {
                component.enabled = false;
            }

            foreach (Animation component in animationComponents)
            {
                component["new_try_me"].enabled = false;
            }
        }
    }

    #endregion // PRIVATE_METHODS
}
