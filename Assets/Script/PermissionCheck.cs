﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using System;

public class PermissionCheck : MonoBehaviour {

	private bool result ;
	// Use this for initialization
	void Start () {
		result = true;
		CheckCameraPermission ();
		CheckPhotoLibraryPermission ();
	}

	#region plugin

#if !UNITY_EDITOR && UNITY_IPHONE
	[DllImport ("__Internal")]
	private static extern void _CheckCameraPermission(string objectName, string callbackFunctionName);
	[DllImport ("__Internal")]
	private static extern void _CheckPhotoLibraryPermission(string objectName, string callbackFunctionName);
#endif

	public void CheckCameraPermission()
	{
		#if !UNITY_EDITOR && UNITY_IPHONE
		_CheckCameraPermission("Controllers", "PermissionResult");
		#endif
	}

	public void CheckPhotoLibraryPermission()
	{
		#if !UNITY_EDITOR && UNITY_IPHONE
		_CheckPhotoLibraryPermission("Controllers", "PermissionResult");;
		#endif
	}

	public void PermissionResult(string resultStr)
	{
		Debug.Log ("result str " + resultStr);
		result = Convert.ToBoolean (resultStr);
		Debug.Log ("result bool " + result);
	}

	public bool GetResult()
	{
		return result;
	}
	#endregion
}
