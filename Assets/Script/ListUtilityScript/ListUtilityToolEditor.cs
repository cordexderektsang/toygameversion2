﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ListUtilityTool))]
[ExecuteInEditMode]
public class ListUtilityToolEditor : Editor
{
   public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        ListUtilityTool Tool = (ListUtilityTool)target;
  

        if (GUILayout.Button("LoadDataToTargetList"))
        {
            Tool.SetUpTargetList();
        }

        if (GUILayout.Button("SortTargetList"))
        {
            Tool.SortListSeparately();
        }

        if (GUILayout.Button("AddNewDataToTargetList"))
        {
            Tool.AddNewItemToOldList();
        }
        if (Tool.isTypeGameObject() == true)
        {
            if (GUILayout.Button("SaveDataToFile"))
            {
               Tool.SaveData();
            }

            if (GUILayout.Button("WriteDataToTargetList"))
            {
                Tool.SaveData();
            }
        }
    }
}

#endif