﻿#if UNITY_EDITOR
using UnityEngine;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Collections;
using System.Linq;
using System;
using System.IO;

public class ListUtilityTool : MonoBehaviour
{
    private Component getScript;
    public string script;
    public string TargetListname;
    public String ResourceFilePath;
    private int smallestLetter = 97;//the number for a
    private int biggestLetter = 123; //the number for z
    private object listObject = null;// referenceObject
    private string fileName = "ListData.txt";// name for output data

    // Use this for initialization
    public void setUp()
    {
        getScript = GetComponent(script) as Component;
    }

    public object GetFieldValue()
    {
        FieldInfo getOrignalfield = getScript.GetType().GetField(TargetListname, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
        return getOrignalfield.GetValue(getScript);
    }

    void SetFieldValue(IList outList)
    {
        FieldInfo setTargetField = getScript.GetType().GetField(TargetListname, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
        setTargetField.SetValue(getScript, outList);

    }

    public void SetUpTargetList()
    {
        setUp();
        ListUtilitySwitch.Do2<IList>(
           (IList)GetFieldValue(),
           (y) => AddAllNewDataToList(y));
        /*   TypeSwitch.Do2<IList, IList>(
          (IList)GetFieldValue(),
           (IList)SetFieldValue(),
          (y, z) => AddNewClipToList(y, z));*/

    }

    public void AddAllNewDataToList (IList outList)
    {
        string[] assetsPaths = UnityEditor.AssetDatabase.GetAllAssetPaths();
        foreach (string assetPath in assetsPaths)
        {

            if (assetPath.Contains(ResourceFilePath))
            {
                try
                {
                    var audioClip = (object)UnityEditor.AssetDatabase.LoadAssetAtPath(assetPath, typeof(object));

                    outList.Add(audioClip);
                }
                catch (System.Exception ex)
                {
                    Debug.Log(ex);
                }
            }

        }
        SortbyObjectName(outList);
    }

    public void SortbyObjectName(IList inList)
    {
        object currentObject = null;//reference for object
        int o = 0;

        while (smallestLetter < biggestLetter)
        {

            for (int i = 0; i < inList.Count; i++)
            {
                char[] listOfLetter = inList[i].ToString().ToCharArray();
                int number = listOfLetter[0];
                if (number == smallestLetter)
                {
                    currentObject = inList[o];// assign the first object in the list to a variable
                    inList[o] = inList[i];// the first position will then assign  with the object 
                    inList[i] = currentObject; // the object position will then assign with the current object
                    o++; //increase the position
                }

            }
            smallestLetter++;

        }

    }

    public void AddNewItemToOldList()
    {
        setUp();
        IList outList = (IList)GetFieldValue();
        string[] assetsPaths = UnityEditor.AssetDatabase.GetAllAssetPaths();
        foreach (string assetPath in assetsPaths)
        {
            bool ObjectExist = false;
            if (assetPath.Contains(ResourceFilePath))
            {
                try
                {
                     listObject = (object)UnityEditor.AssetDatabase.LoadAssetAtPath(assetPath, typeof(object));
                    for(int i = 0; i < outList.Count && ObjectExist == false; i ++)
                    {
                        if (outList[i].ToString().Equals(listObject.ToString()))
                        {
                            ObjectExist = true;
                        }
                    }

                    if (ObjectExist == false)
                    {
                        Debug.Log(listObject.ToString());
                        outList.Add(listObject); 
                    }


                }
                catch (System.Exception ex)
                {
                    Debug.Log(ex);
                }
            }
                SetFieldValue(outList);
        }
    }

    public void SortListSeparately()
    {
        setUp();
        ListUtilitySwitch.Do2<IList>(
        (IList)GetFieldValue(),
        (y) => SortbyObjectName(y));
    }

    public bool isTypeGameObject()
    {
        bool isTypeGameObject = false;
        setUp();
        object outList = GetFieldValue();
        if (outList == null)
        {
            return false;
        }
        if (outList is GameObject) // See if it is an integer.
        {
            isTypeGameObject = true;
        }
        return isTypeGameObject;

    }

    public void SaveData()
    {
        IList outList = (IList)GetFieldValue();
        if(outList == null)
        {
            Debug.Log("nothing in the list");
            return;
        }
        if (File.Exists(Application.dataPath + "/" + fileName))
        {
            Debug.Log(fileName + " Already Exists");
            return;
        }
        StreamWriter sr = File.CreateText(Application.dataPath + "/" + fileName);
        for (int i = 0; i < outList.Count; i++)
        {
                GameObject Item = (GameObject)outList[i];
                sr.WriteLine(outList[i].ToString());
                sr.WriteLine(Item.transform.position.ToString("G4"));
                sr.WriteLine(Item.transform.eulerAngles.ToString("G4"));
                sr.WriteLine(Item.transform.localScale.ToString("G4"));      
        }

        sr.Close();

    }

    public void WriteData()
    {
        List<GameObject> NewList = new List<GameObject>();
        if (File.Exists(Application.dataPath + "/" + fileName))
        {
            int fileIndex = 0;
            StreamReader sr = new StreamReader(Application.dataPath + "/" + fileName);
            string Data = sr.ReadToEnd();
            sr.Close();
            string[] lines = Data.Split("\n"[0]);
            string nameOfTheGameObject = "";
            Vector3 positon = new Vector3(0, 0, 0);
            Vector3 rotation = new Vector3(0, 0, 0);
            Vector3 scale = new Vector3(0, 0, 0);
            GameObject CurrentGameobject = null;

            for (int i = 0; i < lines.Length; i++)
            {
                try
                {
                    if (lines[i].Length > 0)
                    {
                  
                         if (fileIndex == 0)// name of the actually object
                        {
                            nameOfTheGameObject = lines[i].TrimEnd();
                            CurrentGameobject = FindData(nameOfTheGameObject);// find the object in folder then add it into the list
                        }
                        else

                         if (fileIndex == 1)// position
                        {
                            string[] positions = lines[i].Split(',');
                            positon = new Vector3(float.Parse(positions[0].Substring(1)), float.Parse(positions[1]), float.Parse(positions[2].Replace(")", "")));

                        }
                        else

                         if (fileIndex == 2)// rotation
                        {
                            string[] rotations = lines[i].Split(',');
                            rotation = new Vector3(float.Parse(rotations[0].Substring(1)), float.Parse(rotations[1]), float.Parse(rotations[2].Replace(")", "")));

                        }
                        else

                         if (fileIndex == 3) //scale
                        {
                            string[] scales = lines[i].Split(',');
                            scale = new Vector3(float.Parse(scales[0].Substring(1)), float.Parse(scales[1]), float.Parse(scales[2].Replace(")", "")));

                        }
                        fileIndex++;
                        if (fileIndex == 4)
                        {
                            if (CurrentGameobject != null)
                            {
                                CurrentGameobject.transform.position = positon;
                                CurrentGameobject.transform.eulerAngles = rotation;
                                CurrentGameobject.transform.localScale = scale;
                                NewList.Add(CurrentGameobject);
                            }
                            fileIndex = 0;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Debug.Log(ex);
                }

            }
        }
        SetFieldValue(NewList);
    }

    private GameObject FindData(string GameObjectName)
    {
        GameObject targetGameObject = null;
        setUp();
        IList outList = (IList)GetFieldValue();
        foreach (object target in outList)
        {

            GameObject current = (GameObject)target;
            if (current.name.Equals(GameObjectName))
            {
                return current;
            }
        }
        string[] assetsPaths = UnityEditor.AssetDatabase.GetAllAssetPaths();
        foreach (string assetPath in assetsPaths)
        {
            if (assetPath.Contains(ResourceFilePath))
            {
                targetGameObject = (GameObject)UnityEditor.AssetDatabase.LoadAssetAtPath(assetPath, typeof(GameObject));
                if (targetGameObject.name.Equals(GameObjectName))
                {
                    return targetGameObject;
                }
            }
        }

        Debug.Log(GameObjectName + "does not find in file path and object list");
        return null;
               
    }

}
#endif



