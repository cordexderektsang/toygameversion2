﻿using System;
using System.Collections;

static class ListUtilitySwitch
{
    public class CaseInfo
    {
        public bool IsDefault { get; set; }
        public Type Target { get; set; }
        public Action<object> Action { get; set; }
        // public Action< IList, IList> ActionList { get; set; }
        public Action<IList> ActionList { get; set; }
    }

    public static void Do(object source, params CaseInfo[] cases)
    {
        var type = source.GetType();
        foreach (var entry in cases)
        {
            if (entry.IsDefault || entry.Target.IsAssignableFrom(type))
            {
                entry.Action(source);
                break;
            }
        }
    }

    public static void Do2<T1>( IList outList, Action< IList> actionList)
   {
       CaseInfo caseInfo = new CaseInfo();
       caseInfo.IsDefault = false;

       caseInfo.ActionList = (y) => actionList(outList);

       caseInfo.ActionList(outList);
   }

    /* public static void Do2<T1, T2>( IList inList, IList outList, Action< IList, IList> actionList)
     {
         //var type = source.GetType();

         CaseInfo caseInfo = new CaseInfo();
         caseInfo.IsDefault = false;
       //  caseInfo.Target = type;
         caseInfo.ActionList = ( y, z) => actionList( inList, outList);

         caseInfo.ActionList( inList, outList);
     }*/

    public static CaseInfo Case<T>(Action action)
    {
        return new CaseInfo()
        {
            Action = x => action(),
            Target = typeof(T)
        };
    }

    public static CaseInfo Case<T>(Action<T> action)
    {
        return new CaseInfo()
        {
            Action = (x) => action((T)x),
            Target = typeof(T)
        };
    }

    public static CaseInfo Default(Action action)
    {
        return new CaseInfo()
        {
            Action = x => action(),
            IsDefault = true
        };
    }
}