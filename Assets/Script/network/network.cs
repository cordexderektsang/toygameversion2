﻿using UnityEngine;
using System.Collections;
using System;

public class network : MonoBehaviour
{
    private const string linkForGoogle = "market://details?id=com.cordex.aralphabet";
    private const string linkForITune = "https://itunes.apple.com/us/app/ar-alphabet/id1112044965?mt=8";
    private const string URL = "https://www.aralphabet.com/api/version.php?os=";
    private string websiteForVersion = "";
    private string downloadLink = "";
    //   "https://play.google.com/store/apps/details?id=com.cordex.aralphabet";
    private const string android = "android";
    private const string ios = "ios";
    public GameObject versionNoticeBanner;
    string jsonText = "";
    public static bool isCheckVersion = false;

    // Use this for initialization
    void Start () {

        if (Application.internetReachability != NetworkReachability.NotReachable && !isCheckVersion )
        {
#if UNITY_ANDROID

            websiteForVersion = URL + android;
            StartCoroutine("getAppVersion");
#endif

#if UNITY_IOS

  
              websiteForVersion = URL + ios;
              StartCoroutine("getAppVersion");
#endif
        }

    }

    IEnumerator getAppVersion()
    {
   
        WWW www = new WWW(websiteForVersion);
        Debug.Log("url: " + www.url);
        yield return www;

        if (string.IsNullOrEmpty(www.error))
        {
            jsonText = www.text;
            Debug.Log(jsonText);
			if (checkNewVersion(jsonText))
            {
                versionNoticeBanner.SetActive(true);
            }
        }
        else
        {
            yield break;
        }

    }

	private bool checkNewVersion(string webVersionStr)
	{
		string currentAppVersionStr = Application.version.Replace(".", "");
		webVersionStr = webVersionStr.Replace(".", "");

		int currentAppVersion = Convert.ToInt32 (currentAppVersionStr);
		int webVersion = Convert.ToInt32 (webVersionStr);

		if (webVersion > currentAppVersion) 
		{
			return true;
			//webVersionStr
		} 
		else 
		{
			return false;
		}
	}

    public void onUpdateVersionClicked()
    {
      versionNoticeBanner.SetActive(false);
        isCheckVersion = true;
#if UNITY_ANDROID

        downloadLink = linkForGoogle;
        Application.OpenURL(downloadLink);
#endif
#if UNITY_IOS
 
            downloadLink = linkForITune;
            Application.OpenURL(downloadLink); 

#endif
    }

    public void onLaterClicked()
    {
        isCheckVersion = true;
        versionNoticeBanner.SetActive(false);
    }
}
