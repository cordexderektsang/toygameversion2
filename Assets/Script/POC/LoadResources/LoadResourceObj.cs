using UnityEngine;
using System.Collections;

public class LoadResourceObj : MonoBehaviour 
{
	public string obj_name;
	Object loaded_asset;
	GameObject instance_obj;
	int index = 200;
	CCManualLoadResourceObj obj;
	
	// Use this for initialization
	void Start () 
	{
		Application.targetFrameRate = 60;
		
		Resources.UnloadUnusedAssets ();
		System.GC.Collect();
	}
	
	void OnGUI ()
	{
		if(GUI.Button (new Rect (10, 10, 250, 200), "Show"))
		{
			/*
			loaded_asset = Resources.Load <Object> (obj_name);
			instance_obj = Instantiate(loaded_asset) as GameObject;
			*/
			obj = new CCManualLoadResourceObj ();
			obj.loadGameObject (obj_name);
			++index;
		}
		if(GUI.Button (new Rect (300, 10, 250, 200), "88"))
		{
			/*
			GameObject.DestroyImmediate (instance_obj, true);
			instance_obj = null;
			loaded_asset = null;
			Resources.UnloadUnusedAssets ();
			System.GC.Collect();
			*/
			obj.unloadGameObject ();
			obj = null;
		}
		
		GUI.Label (new Rect (10, 250, 100,100), ""+index);
	}
}
