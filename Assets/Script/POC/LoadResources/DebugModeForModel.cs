﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DebugModeForModel : MonoBehaviour {
    private Object loaded_asset;
    public GameObject current_obj
    {
        get;
        private set;
    }
    private CCManualLoadResourceObj loader;
    public bool isDebugLog = false;
    public AlphabetCoreSystem listOfWord;
    public AlphabetGameMode2UISystem coreSystem;
    public Transform objParent;
    private List<string> fullWordList;
    private int startLenght = 0;
    private int endLength = 0;
    private string currentName;

    public Renderer markerBg;
    // Use this for initialization
    void Start () {
        loader = new CCManualLoadResourceObj();
        listOfWord.initDict("dictionary/dict", 6);
        fullWordList = listOfWord.returnFullListForDebug();
        endLength = fullWordList.Count -1;

    }
	
    private void LoadObject(string target)
    {
        try
        {
            loader.unloadGameObject();
        }
        catch { }
        Transform obj = loader.loadGameObject("obj/" + target).transform;

        // Reset position after assigning to parent
        Vector3 pos = obj.localPosition;
        Vector3 scale = obj.localScale;
        Vector3 rotation = obj.localEulerAngles;

        obj.parent = objParent;
        obj.localPosition = pos;
        obj.localScale = scale;
        obj.localEulerAngles = rotation;
    }
   
	// Update is called once per frame
    public void onClickLeft()
    {
        if (startLenght > 0)
        {
            startLenght--;
            currentName = fullWordList[startLenght];
            LoadObject(fullWordList[startLenght]);
        } else
        {
            LoadObject(fullWordList[0]);
            currentName = fullWordList[0];

        }
        markerBg.material.mainTexture = loader.loadTexture("floor/" + AlphabetCoreSystem.WordCategory(currentName));
    }

    // Update is called once per frame
    public void onClickRight()
    {
        if (startLenght < endLength-1)
        {
            startLenght++;
            LoadObject(fullWordList[startLenght]);

        }  else
        {
           LoadObject(fullWordList[endLength]);

            currentName = fullWordList[endLength];
        }
        markerBg.material.mainTexture = loader.loadTexture("floor/" + AlphabetCoreSystem.WordCategory(currentName));
    }


    public void On3dObjClick()
    {
    
        if (isDebugLog)
        {

            try
            {
                //			if (isTutorialTime)
                //			{

                //				if(tutorialController.currentState == 1 && !loader.current_obj.GetComponentInChildren<Animation>().isPlaying /*&& !canStage1EventSendOut*/)
                //				{
                ////					canStage1EventSendOut = true;
                //					// StartCoroutine ("pronounciation");
                //					loader.current_obj.GetComponentInChildren<Animation> ().Play ();
                ////					sendToTutorialSystem (1, loader.current_obj.GetComponentInChildren <Animation> ().clip.length);
                //				}
                //			}
                //			else 
                Debug.Log("On3dObjClick");
                if (!loader.current_obj.GetComponentInChildren<Animation>().isPlaying)
                {
                    //AlphabetCoreAudioSystem.instance.stopAll ();
                    //AlphabetCoreAudioSystem.instance.playOnce (last_spelling);
                  //  StartCoroutine("pronounciation");
                    loader.current_obj.GetComponentInChildren<Animation>().Play();
                }
                /*
                if(!loader.current_obj.GetComponent<Animation>().isPlaying)
                {
                    //AlphabetCoreAudioSystem.instance.stopAll ();
                    //AlphabetCoreAudioSystem.instance.playOnce (last_spelling);
                    StartCoroutine ("pronounciation");
                    loader.current_obj.GetComponent<Animation>().Play ();
                }
                */

            }
            catch (System.Exception ex)
            {
                Debug.Log(ex);
            }
        }
        /////////////////////////////////////////
    }

}
