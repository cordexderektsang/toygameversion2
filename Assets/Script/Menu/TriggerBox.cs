﻿using UnityEngine;

public class TriggerBox : MonoBehaviour {
	public static bool outOfBounds; // does it reach the top (or bottom) yet ?

	// Use this for initialization
	void Start () {
		outOfBounds = false;
	}
	
	// if the box collider hit front page carry one the movement otherwise stop move
	void OnTriggerEnter (Collider other) {
		Debug.Log(other.gameObject.name);
		if (other.gameObject.name.Equals("MarginBot")) {
			SpawnChecker.IsRun = true;
			outOfBounds = false;

		} else if (other.gameObject.name.Equals("FrontPage")) {
			outOfBounds = true;
		} 
	}
}
