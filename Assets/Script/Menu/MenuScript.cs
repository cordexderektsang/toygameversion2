﻿using UnityEngine;
using System.Collections;

public class MenuScript : MonoBehaviour 
{
	public AlphabetLoadingPageBehaviour loadingSystem;
	public PermissionCheck permissionCheck;
	public GameObject touchForStartPanel, buttonPage1, buttonPage2, msgPage, tutorialPage;
	public UITexture menuTextureObj;
	bool letterClicked = false;
	bool WordClicked = false;
	private int state = 0;

	//public float size = 100;
	
	public void Start ()
	{
		AppController.ManualReleaseMemory ();
		letterClicked = false;
		WordClicked = false;
		//size *= ((float)Screen.width/960.0f);
	}
    /*
	void OnGUI ()
	{
		if (GUI.Button (new Rect (10, 10, size, size), "Reset App"))
		{
			CCLocalStorageAPI.DeleteAll ();
			UnityEngine.SceneManagement.SceneManager.LoadScene (UnityEngine.SceneManagement.SceneManager.GetActiveScene ().name);
		}
	}
	*/

    /// <summary>
    /// Play one sound when starting panel is clicked
    /// </summary>
    [Beebyte.Obfuscator.SkipRename]
    public void onClickToWebPage()
    {
        Application.OpenURL("https://www.aralphabet.com/");
    }
    /// <summary>
    /// Play one sound when starting panel is clicked
    /// </summary>
    [Beebyte.Obfuscator.SkipRename]
    public void OnStartPanelClick ()
    {
        BoxCollider collider = touchForStartPanel.GetComponent<BoxCollider>();
        AlphabetCoreAudioSystem.instance.playOnce("Woo-hoo" ,AlphabetCoreConstants.AUDIO_Selections.SOUND);
        collider.enabled = false;
    }

	/// <summary>
	/// Button for game mode 1 and game 3 beginner
	/// case need tutorial
	/// or not need tutorial
	/// </summary>
	[Beebyte.Obfuscator.SkipRename]
	public void OnLetterClick ()
	{
		if (!SettingPanelBehaviour.IsTutorial)
        {
            SettingPanelBehaviour.IsTutorialMode = false;
			if (permissionCheck.GetResult()) {
				switch (state) {
				case 0: 
					StartCoroutine (changeScene ("GameMode1")); 
					break;
				case 1: 
				    //StartCoroutine (changeScene ("GameMode3Beginner"));
					StartCoroutine (changeScene ("GameMode3"));
					break;
				default :
					break;
				}
			} else {
				StartCoroutine (changeScene ("Setting"));
			}
		}
        else
        {
			tutorialPage.SetActive (true);
			buttonPage1.SetActive (false);
			buttonPage2.SetActive (false); 
		}
		letterClicked = true;
		WordClicked = false;
		//ScaleEffect.isPressed = false;
	}

	/// <summary>
	/// game mode 2 and game mode 3 avdance
	/// </summary>
	[Beebyte.Obfuscator.SkipRename]
	public void OnWordClick ()
	{
		if (!SettingPanelBehaviour.IsTutorial)
        {
            SettingPanelBehaviour.IsTutorialMode = false;

			if (permissionCheck.GetResult()) {
				switch (state) {
				case 0: 
					StartCoroutine (changeScene ("GameMode2"));
					break;
				case 1:
                    //StartCoroutine (changeScene ("GameMode3Advanced"));
					StartCoroutine (changeScene ("GameMode3"));
					break;
				default :
					break;
				}
			} else {
				StartCoroutine (changeScene ("Setting"));
			}
		}
        else
        {
			tutorialPage.SetActive (true);
			buttonPage1.SetActive (false);
			buttonPage2.SetActive (false);
		}
		letterClicked = false;
		WordClicked = true;
		//ScaleEffect.isPressed = false;
	}

	/// <summary>
	/// back buttom
	/// </summary>
	[Beebyte.Obfuscator.SkipRename]
	public void OnTutorialUIBackButtonClick ()
	{
		if (tutorialPage.activeSelf && state == 1) {
			tutorialPage.SetActive (false);
			buttonPage2.SetActive (true);
			//ScaleEffect.isPressed = false;
		} else {
			//UnityEngine.SceneManagement.SceneManager.LoadScene ("Menu", UnityEngine.SceneManagement.LoadSceneMode.Single);
			buttonPage1.SetActive (true);
			buttonPage2.SetActive (false);
			tutorialPage.SetActive (false);
			//ScaleEffect.isPressed = false;
		}
	}

	/// <summary>
	/// back buttom
	/// </summary>
	[Beebyte.Obfuscator.SkipRename]
	public void NoramlUIBackButtonClick ()
	{
		buttonPage1.SetActive (true);
		buttonPage2.SetActive (false);
		tutorialPage.SetActive (false);
		letterClicked = false;
		WordClicked = false;
		//ScaleEffect.isPressed = false;
		state = 0;
	}

	/// <summary>
	/// play click event for entering into game mode.
	/// </summary>
	/// <param name="IsTutorial">If set to <c>true</c> is tutorial.</param>
	[Beebyte.Obfuscator.SkipRename]
	public void OnPlayClick(bool isTutorialMode)
	{
		Debug.Log ("permissionCheck.GetResult() " + permissionCheck.GetResult ());
        SettingPanelBehaviour.IsTutorialMode = isTutorialMode;
        if (WordClicked)
        {
			if (permissionCheck.GetResult()) {
				Debug.Log ("SSSSxxxx ");
						switch (state)
	            {
				    case 0: 
					    StartCoroutine (changeScene ("GameMode2"));
					    break;
				    case 1: 
					    //StartCoroutine (changeScene ("GameMode3Advanced"));
	                    StartCoroutine(changeScene("GameMode3"));
	                    break;
				    default :
					    break;
				}
			} else {
				Debug.Log ("SSSS ");
						StartCoroutine (changeScene ("Setting"));
			}
		}
        else
        {
			if (permissionCheck.GetResult()) {
				Debug.Log ("SSSSxxxx2 ");
						switch (state)
	            {
				    case 0: 
					    StartCoroutine (changeScene ("GameMode1")); 
					    break;
				    case 1:
	                    //StartCoroutine (changeScene ("GameMode3Beginner")); 
	                    StartCoroutine(changeScene("GameMode3"));
	                    break;
				    default :
					    break;
				}
			} else {
				Debug.Log ("SSS2 ");
							StartCoroutine (changeScene ("Setting"));
			}
		}
	}

	/// <summary>
	/// Ar demo game mode 
	/// </summary>
	/// <param name="Sprite">Sprite.</param>
	 [Beebyte.Obfuscator.SkipRename]
	public void OnArClick (UISprite Sprite)
	{
		Sprite.spriteName = "btn_Try_me_outliner";
        //UnityEngine.SceneManagement.SceneManager.LoadScene ("ARDemo");	
		if (permissionCheck.GetResult ()) {
			StartCoroutine (changeScene ("ARDemo"));
		}
		else {
			StartCoroutine (changeScene ("Setting"));
		}
    }

	/// <summary>
	/// Try me demo game mode
	/// </summary>
	[Beebyte.Obfuscator.SkipRename]
	public void TryMeMode ()
	{
        StartCoroutine(changeScene("TryMeMode"));
        //UnityEngine.SceneManagement.SceneManager.LoadScene ("TryMeMode");

	}

	/// <summary>
	/// quiz buttom
	/// </summary>
	 [Beebyte.Obfuscator.SkipRename]
	public void OnGuessClick ()
	{
		switch (state) {
		case 0: 
			state = 1;
		// menuTextureObj.mainTexture = menuLevelTexture;
			buttonPage1.SetActive (false);
			buttonPage2.SetActive (true);
			break;
		case 1: 
			state = 0;
		// menuTextureObj.mainTexture = menuTexture;
			buttonPage2.SetActive (false);
			buttonPage1.SetActive (true);
			break;
		default :
			break;
		}
}

	/// <summary>
	/// Tutorial Selection (yes player do click on Play Tutorial buttom)
	/// </summary>
	[Beebyte.Obfuscator.SkipRename]
	public void OnIsNeedTutorialClick()
    {
		OnPlayClick (true);
	}

	/// <summary>
	/// Tutorial Selection (no player click one play buttom )
	/// </summary>
	[Beebyte.Obfuscator.SkipRename]
	public void OnPlayGameClick()
    {
		OnPlayClick (false);
	}

    /// <summary>
    /// Message Box clicked
    /// </summary>
    [Beebyte.Obfuscator.SkipRename]
    public void OnMsgButtonClick ()
	{
		//_AudioSource.PlayOneShot (WelcomeAudio);
        AlphabetCoreAudioSystem.instance.playOnce("Welcome to the AR School",AlphabetCoreConstants.AUDIO_Selections.SOUND);
		msgPage.SetActive (false);
	}

	[Beebyte.Obfuscator.SkipRename]
	public void OnSoundButtonClick ()
	{
		//_AudioSource.PlayOneShot (WelcomeAudio);
		AlphabetCoreAudioSystem.instance.playOnce("Welcome to the AR School",AlphabetCoreConstants.AUDIO_Selections.SOUND);
	
	}

    /// <summary>
	/// Game mode 3 beginner mode click
	/// </summary>
	[Beebyte.Obfuscator.SkipRename]
    public void OnBeginnerModeClick()
    {
        AlphabetGameMode3UISystem.IsBeginnerMode = true;
    }

    /// <summary>
    /// Game mode 3 advanced mode click
    /// </summary>
    [Beebyte.Obfuscator.SkipRename]
    public void OnAdvancedModeClick()
    {
        AlphabetGameMode3UISystem.IsBeginnerMode = false;
    }

    [System.Reflection.Obfuscation(ApplyToMembers=false)]
	private IEnumerator changeScene (string sceneName) 
	{
		/*#if UNITY_IPHONE
			//Handheld.SetActivityIndicatorStyle(UnityEngine.iOS.ActivityIndicatorStyle.Gray);
        #elif UNITY_ANDROID
            Handheld.SetActivityIndicatorStyle(AndroidActivityIndicatorStyle.Small);
        #endif
		Handheld.StartActivityIndicator ();*/
		int pageID = 0;
		if (sceneName.Contains ("2"))
		{
			pageID = 1;
		}
		else if (sceneName.Contains ("3"))
		{
			pageID = 2;
		} else if(sceneName.Contains("TryMe"))
        {
            pageID = 3;
        }
        else if (sceneName.Contains("ARDemo"))
        {
            pageID = 4;
        }

        yield return loadingSystem.startLoadingPageRandomly (pageID);
		
		yield return new WaitForSeconds (2.5f);
		UnityEngine.SceneManagement.SceneManager.LoadScene (sceneName);
	    // Application.LoadLevel (sceneName);
	}
}
