﻿using UnityEngine;
using System;

public class SettingPanelBehaviour : MonoBehaviour
{
	public delegate void OnBGMChange (int value);
	public static OnBGMChange BGMChange;

	public int currentAudio = 0;// the number for the current audio option
	private int enumLength = 0;// the length of the enum
	public string currentAudioOptionName = "";// the name for the current enum audio option
	public AlphabetCoreConstants.AUDIO_OPTIONS audioType = AlphabetCoreConstants.AUDIO_OPTIONS.US_AUDIO;
	
	public GameObject bg, body, bgm, tutorial, AudioOptionName;
    private UIButton bgmButton;
    private UISprite bgmSprite;
    private UISprite audioOptionUISprite;
    private UIButton audioOptionUIButton;
    private UISprite tutorialOptionUISprite;
    private UIButton tutorialOptionUIButton;


    private const string kTweenName = "kSettingTween";
	private const string kBGMValue = "kBGMValue";
	private const string kTutorialX = "kTutorialX";
	private const string audioOptionInt = "audioOptionAsNumber";
	private const string audioOptionStr = "audioOptionAsString";
	bool volumeSwitch = true;

	public static int BGMValue
	{
		get {
			return CCLocalStorageAPI.Contian (kBGMValue) ? CCLocalStorageAPI.GET <int> (kBGMValue) : 100;
		}
		set {
			CCLocalStorageAPI.SET<int> (kBGMValue, value);
		}
	}
	
	public static bool IsTutorial 
	{
		get
        {
			return !(CCLocalStorageAPI.GET <int> (kTutorialX) == 1);
		}
	}

    public static bool IsTutorialMode = false;

	public static string LanguageOption 
	{
		get
        {
			return (CCLocalStorageAPI.GET <string>(audioOptionStr));
		}
	}

    void Awake()
    {
        if (bgmSprite == null)
        {
            bgmSprite = bgm.GetComponent<UISprite>();
        }

        if (bgmButton == null)
        {
            bgmButton = bgm.GetComponent<UIButton>();
        }

        if (audioOptionUISprite == null)
        {
            audioOptionUISprite = AudioOptionName.GetComponent<UISprite>();
            audioOptionUIButton = AudioOptionName.GetComponent<UIButton>();
        }

        if (tutorialOptionUISprite == null)
        {
            tutorialOptionUISprite = tutorial.GetComponent<UISprite>();
            tutorialOptionUIButton = tutorial.GetComponent<UIButton>();
        }
    }

    /// <summary>
    /// prepare tutorial Image
    /// prepare Audio option image
    /// prepare volume image
    /// </summary>
    void OnEnable () 
	{
		bg.SetActive (true);
		//iTween.MoveTo (body, iTween.Hash ("y", 0, "islocal", true, "time", 1, "looptype", iTween.LoopType.none, "easetype", iTween.EaseType.easeOutBounce, "name", kTweenName));
		//bgm.GetComponent<UISprite> ().spriteName = "Sound_" + BGMValue + "%_btn";
		//bgm.GetComponent<UIButton> ().hoverSprite = "Sound_" + BGMValue + "%_btn";
		//bgm.GetComponent<UIButton> ().normalSprite = "Sound_" + BGMValue + "%_btn";
		//bgm.GetComponent<UIButton> ().pressedSprite = "Sound_" + BGMValue + "%_btn";
        bgmSprite.spriteName = "Sound_" + BGMValue + "%_btn";
        bgmButton.hoverSprite = "Sound_" + BGMValue + "%_btn";
        bgmButton.normalSprite = "Sound_" + BGMValue + "%_btn";
        bgmButton.pressedSprite = "Sound_" + BGMValue + "%_btn";
        enumLength = Enum.GetValues (typeof(AlphabetCoreConstants.AUDIO_OPTIONS)).Length;
		//bgm.GetComponent<UISprite>().SetDimensions(250, 250);
        try
        {
            TutorialImageChange(CCLocalStorageAPI.GET<int>(kTutorialX));
           // tutorialOn.SetActive(CCLocalStorageAPI.GET<int>(kTutorialX) == 0);
            //tutorialOff.SetActive(CCLocalStorageAPI.GET<int>(kTutorialX) == 1);
			SettingWhenEnable ();
			ChangeAudioOptionImage ();
		
        }
        catch (Exception)
        {
            TutorialImageChange(0);
        }
			
	}
	
	void OnDisable () 
	{
		//iTween.StopByName (kTweenName);
		
		//body.transform.localPosition = new Vector3 (0, 1000, 0);
		bg.SetActive (false);
	}
	
	public void OnBGMClick ()
	{
		int value = BGMValue;

		if (volumeSwitch == false)
        {
            switch(value)
            {
                case 100:
                    value = 50;
                    break;
                case 50:
                    value = 25;
                    break;
                case 25:
                    value = 0;
                    volumeSwitch = true;
                    break;
            }

			//if (value == 100) {
			//	value = 50;
			//} else if (value == 50) {
			//	value = 25;
			//} else if (value == 25) {
			//	value = 0;
			//	volumneSwitch = true;
			//}

		}
        else
        {
            switch(value)
            {
                case 0:
                    value = 25;
                    break;
                case 25:
                    value = 50;
                    break;
                case 50:
                    value = 100;
                    break;
                case 100:
                    value = 50;
                    volumeSwitch = false;
                    break;
            }

			//if (value == 0) {
			//	value = 25;
			//} else
			//if (value == 25) {
			//	value = 50;
			//} else if (value == 50) {
			//	value = 100;
			//	volumneSwitch = false;
			//	} else 
			//		if(value == 100){
			//		value = 50;
			//		volumneSwitch = false;
			//		}
		}
		
		BGMValue = value;
		
		//bgm.GetComponent<UISprite> ().spriteName = "Sound_" + value + "%_btn";
		//bgm.GetComponent<UIButton> ().hoverSprite = "Sound_" + value + "%_btn";
		//bgm.GetComponent<UIButton> ().normalSprite = "Sound_" + value + "%_btn";
		//bgm.GetComponent<UIButton> ().pressedSprite = "Sound_" + value + "%_btn";
        bgmSprite.spriteName = "Sound_" + BGMValue + "%_btn";
        bgmButton.hoverSprite = "Sound_" + BGMValue + "%_btn";
        bgmButton.normalSprite = "Sound_" + BGMValue + "%_btn";
        bgmButton.pressedSprite = "Sound_" + BGMValue + "%_btn";

        if (BGMChange != null)
		{
			BGMChange (value);
		}
	}
	
	public void OnTutorialClick ()
	{
		int value = (IsTutorial == true) ? 1 : 0;
        TutorialImageChange(value);
       // tutorialOn.SetActive (value == 0);
		//tutorialOff.SetActive (value == 1);
		//if (value == 1)
  //      {
		//	//BaseTutorialController.IsNeedTutorial = false;
  //          IsNeedTutorial = false;
  //      }
  //      else
  //      {
  //          //BaseTutorialController.IsNeedTutorial = true;
  //          IsNeedTutorial = true;
  //      }
		CCLocalStorageAPI.SET <int> (kTutorialX, value);
	}

    public  void TutorialImageChange (int value)
    {
        if(value == 0)
        {
            tutorialOptionUISprite.spriteName = "tutorial_on_btn";
            tutorialOptionUIButton.normalSprite = "tutorial_on_btn";
        } else
        {
            tutorialOptionUISprite.spriteName = "tutorial_off_btn";
            tutorialOptionUIButton.normalSprite = "tutorial_off_btn";
        }
    }

	/// <summary>
	/// change AudioSound
	/// </summary>
	public void OnAudioChange ()
	{
		// increment the enum as integer whenever player click
		if (currentAudio < enumLength - 1) {
			currentAudio++;
            AlphabetCoreAudioSystem.languageOption = "UK_AUDIO";
            AlphabetCoreAudioSystem.instance.playOnce("z" , AlphabetCoreConstants.AUDIO_Selections.LETTER_PRONUNCIATION);//UK
		} else {
			currentAudio = 0;
            AlphabetCoreAudioSystem.languageOption = "US_AUDIO";
            AlphabetCoreAudioSystem.instance.playOnce("z", AlphabetCoreConstants.AUDIO_Selections.LETTER_PRONUNCIATION);//US
        }
		// find the name of the enum
		ChangeAudioOptionImage ();
	}

	/// <summary>
	/// Changes the audio option image.
	/// </summary>
	void ChangeAudioOptionImage ()
    {
		foreach (AlphabetCoreConstants.AUDIO_OPTIONS audio in Enum.GetValues (typeof(AlphabetCoreConstants.AUDIO_OPTIONS)))
        {
			if((int)audio == currentAudio)
            {
				currentAudioOptionName = audio.ToString ();
			}
		}
        //AudioOptionName.GetComponent<UISprite> ().spriteName = currentAudioOptionName;
        //AudioOptionName.GetComponent<UIButton> ().hoverSprite = currentAudioOptionName;
        //AudioOptionName.GetComponent<UIButton> ().normalSprite = currentAudioOptionName;
        //AudioOptionName.GetComponent<UIButton> ().pressedSprite = currentAudioOptionName;
        audioOptionUISprite.spriteName = currentAudioOptionName;
        audioOptionUIButton.hoverSprite = currentAudioOptionName;
        audioOptionUIButton.normalSprite = currentAudioOptionName;
        audioOptionUIButton.pressedSprite = currentAudioOptionName;
        CCLocalStorageAPI.SET <string> (audioOptionStr,currentAudioOptionName); // store the integer;
	}

	void SettingWhenEnable ()
    {
		foreach (AlphabetCoreConstants.AUDIO_OPTIONS audio in  Enum.GetValues (typeof(AlphabetCoreConstants.AUDIO_OPTIONS)))
        {
			if((audio.ToString().Equals(CCLocalStorageAPI.GET<string>(audioOptionStr))))
            {
				currentAudio =(int) audio;
			}
			currentAudioOptionName = CCLocalStorageAPI.GET<string> (audioOptionStr);
		}
	}
}
