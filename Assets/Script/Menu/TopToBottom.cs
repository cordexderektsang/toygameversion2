﻿using UnityEngine;
using System.Collections;

public class TopToBottom : MonoBehaviour {
	public UISprite valueShiftOne;// get the anchor
	bool isRoll = false;// should the screen start to auto scroll down yet?
	public GameObject panelForLogic;
	public GameObject frontMenuUp;// front page with scroll
	public GameObject frontMenuBottom;//front page with no scroll
	public GameObject MSG;//message
	public GameObject pageOne;//first page of the game
	float maxWaittingTime = 0.5f; // max time for end scroll 
	float counterForTimeMsg = 0;//counter for end scroll 
	bool isFinishYet = false;// do the auto scroll process finish Yet?
	static int Position;

	void Awake()
	{
		if (!SpawnChecker.IsRun) {
			//{// if this is the first start of the app then turn off all UI Button
			MSG.SetActive (false);
			pageOne.SetActive (false);
			isRoll = false;
		} else {
			// if player still in the app without restart the whole app
			pageOne.SetActive (true);
			valueShiftOne.topAnchor.absolute += (int) Position;
			valueShiftOne.bottomAnchor.absolute += (int)Position;
			MSG.SetActive (false);
			panelForLogic.SetActive (false);

		}
	}

	// Update is called once per frame
	void FixedUpdate ()
	{
		if (SpawnChecker.IsRun == true) // if no longer first start then no need do anything
		{
			return;
		}

		/// move up the background until it reach the top if player click the screen and first start for the app 
		if(TriggerBox.outOfBounds == true && isRoll == true){
			transform.Translate (new Vector2(0,2) * Time.deltaTime);
			valueShiftOne.topAnchor.absolute += (int) 2;
			valueShiftOne.bottomAnchor.absolute += (int)2;
			Position += 2;// store the position for instant roll down
		}
	
	}
	void Update ()
	{
		if (isFinishYet == true) // process done or none first start
		{
			return;
		}

        if (TriggerBox.outOfBounds == false && isRoll == true && counterForTimeMsg < maxWaittingTime) {// if scroll end wait a sec then show message and ui button

				counterForTimeMsg += Time.deltaTime;

			} else if (counterForTimeMsg >= maxWaittingTime) {// scroll process finish , allow ui button to be visible and no longer first start until app kill
				isFinishYet = true;
				panelForLogic.SetActive (false);
				MSG.SetActive (true);
				pageOne.SetActive (true);

		}

	}


	/// <summary>
	/// if player click on screen
	/// </summary>
	public void StartRoLlFromTopToMenu ()
	{
		isRoll = true;
	}
		
}
