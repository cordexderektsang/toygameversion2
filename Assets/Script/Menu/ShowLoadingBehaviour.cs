﻿using UnityEngine;
using System.Collections;

public class ShowLoadingBehaviour : MonoBehaviour {

	// Use this for initialization
	[System.Reflection.Obfuscation(ApplyToMembers=false)]
	IEnumerator Start () {
		
		DontDestroyOnLoad (gameObject);
		 #if UNITY_IPHONE
		Handheld.SetActivityIndicatorStyle(UnityEngine.iOS.ActivityIndicatorStyle.Gray);
        #elif UNITY_ANDROID
            Handheld.SetActivityIndicatorStyle(AndroidActivityIndicatorStyle.Small);
        #endif
		yield return new WaitForEndOfFrame ();
		Handheld.StartActivityIndicator ();
	}
	
}
