﻿using System.Collections.Generic;
using UnityEngine;

public abstract class BaseTutorialController : MonoBehaviour
{
    private const float MIN_FADE_TIME = 0.1f;
    private const float MAX_FADE_TIME = 3f;
    private const float MIN_ALPHA = 0.0f;
    private const float MAX_ALPHA = 1.0f;

    #region EDITOR_SETTINGS
    [Header("Platform Marker Object")]
    public StageTrackableEventHandler magicScreen;
    public StageTrackableEventHandler magicScreen2;

    [Header("Override Game Tutorial Setting (for debugging purposes)")]
    public bool isOverrideTutorialSetting;
    public bool isNeedTutorial;

    [Header("Set Tutorial Pages Settings")]
    public List<TutorialState> tutorialStates;

    [Header("Set Instruction Pages Settings")]
    public List<InstructionState> instructionStates;

    public bool isMagicScreenTracked = false;

    [Space(10)]
    [Range(InstructionState.MIN_WAIT_TIME, InstructionState.MAX_WAIT_TIME)]
    public float endInstructionResetTime;
    // Tutorial
    //public List<AlphabetCoreConstants.CHARACTER_TAG_NAMES> tutorialTagNames;
    //[Range(MIN_FADE_TIME, MAX_FADE_TIME)]
    //public List<float> fadeInTimes;
    //[Range(MIN_ALPHA, MAX_ALPHA)]
    //public List<float> fadeInAlphaValues;
    //[Range(MIN_FADE_TIME, MAX_FADE_TIME)]
    //public List<float> fadeOutTimes;
    //public List<string> words;
    //public List<GameObject> panels;

    // Instructions
    //public int maxTimeReachCount;
    //public float timeIncrement;
    //public float glassCeiling;

    //public List<AlphabetCoreConstants.CHARACTER_TAG_NAMES> instructionTagNames;
    //public List<string> messages;
    //[Range(0, 30)]
    //public List<float> waitTimes;
    #endregion

    #region GAME_MODE_SETTINGS
    protected int gameModeNumber;
    private string _TUTORIAL;
    protected string TUTORIAL
    {
        get
        {
            return _TUTORIAL;
        }
    }
    private string _INSTRUCTION;
    protected string INSTRUCTION
    {
        get
        {
            return _INSTRUCTION;
        }
    }
    private string _ENCOURAGEMENT;
    protected string ENCOURAGEMENT
    {
        get
        {
            return _ENCOURAGEMENT;
        }
    }
    #endregion

    #region STATE_SETTINGS
    //public int tutorialState = 0;
    //public int instructionState = 0;
    private int _tutorialState;
    public int tutorialState
    {
        get
        {
            return _tutorialState;
        }
        private set
        {
            _tutorialState = value;
        }
    }

    private int _instructionState;
    public int instructionState
    {
        get
        {
            return _instructionState;
        }
        private set
        {
            _instructionState = value;
        }
    }
    #endregion

    // Character variables
    protected AlphabetCharacterController characterSystem;

    // Timer variables
    protected TimerController timerSystem;

    // Tutorial system variables
    protected UIPanel objPanel;
    protected UIButton finishButtonUI;
    protected BoxCollider finishButtonCollider;
    protected bool isTutorialDone = false;
    protected bool isFadeOut = false;
    protected bool isFadeIn = false;
    protected bool isFadeInAudioPlayed = false;
    protected bool isClickFinishButton = false;

    // Instruction system variables
    protected bool isInstructionStart = false;
    protected bool isInstructionStateChanged = false;
    protected bool isInstructionAudioPlayed = false;

    // Abstract functions for implementation
    public abstract void setupVariablesAndConstants();
    public abstract void setupGameModeTutorial();
    public abstract void setupGameModeInstructions();
    public abstract void checkTutorialState();
    public abstract void checkInstructionState();
    public abstract void onTutorialEnd();
    public abstract void onInstructionStart();

    void Start()
    {
        // Check if require tutorial from settings page, for debugging purposes
        if (!isOverrideTutorialSetting)
        {
            isNeedTutorial = SettingPanelBehaviour.IsTutorialMode;
        }

        if (isNeedTutorial)
        {
            onTutorialStart();
        }
        else
        {
            onTutorialSkip();
        }
    }

    #region BASE_SETUP
    private void onTutorialStart()
    {
        setupCommonFunctions();

        setupGameModeTutorial();
        setupTutorialStates();
        setupTutorialFirstPage();
        setupTutorialLastPage();
    }

    private void onTutorialSkip()
    {
        isNeedTutorial = false;
        isTutorialDone = true;
        isInstructionStart = true;

        setupCommonFunctions();

        //CCTimerSuperClass.SetupTimer(0, instructionStates[instructionState].time, maxTimeReachCount, timeIncrement, glassCeiling);
        //timerSystem.setupTimer(0, instructionStates[instructionState].time, maxTimeReachCount, timeIncrement, glassCeiling);
        timerSystem.setTimerActive(true);
    }

    private void setupCommonFunctions()
    {
        setupVariablesAndConstants();
        setupGameModeInstructions();
        setupInstructionStates();
    }
    #endregion

    #region TUTORIAL_FUNCTIONS
    protected void setupTutorialStates()
    {
        _TUTORIAL = AlphabetCoreConstants.TUTORIAL + gameModeNumber + ".";
        _ENCOURAGEMENT = AlphabetCoreConstants.ENCOURAGEMENT + gameModeNumber + ".";

        //tutorialStates = new List<TutorialState>();

        int stateCounter = 0;
        foreach (TutorialState tState in tutorialStates)
        {
            stateCounter++;
            string tutorialStateName = TUTORIAL + stateCounter;
            tState.clipName = tutorialStateName;

            // Sanity check to ensure fade in and out times are not 0
            if (tState.fadeInTime <= 0)
            {
                tState.fadeInTime = TutorialState.MIN_FADE_TIME;
            }

            if (tState.fadeOutTime <= 0)
            {
                tState.fadeOutTime = TutorialState.MIN_FADE_TIME;
            }
        }

        //for (int i = 0; i < fadeOutTimes.Count; i++)
        //{
        //    if (fadeOutTimes[i] <= 0)
        //    {
        //        fadeOutTimes[i] = 0.01f;
        //    }
        //}

        //for (int i = 0; i < fadeInTimes.Count; i++)
        //{
        //    if (fadeInTimes[i] <= 0)
        //    {
        //        fadeInTimes[i] = 0.01f;
        //    }
        //}
        //// End sanity check

        //for (int i = 0; i < tutorialTagNames.Count; i++)
        //{
        //    string tutorialStateName = TUTORIAL + (i + 1);
        //    TutorialState tsObj = new TutorialState(tutorialTagNames[i], fadeInTimes[i], fadeInAlphaValues[i], fadeOutTimes[i], tutorialStateName, words[i], panels[i]);
        //    tutorialStates.Add(tsObj);
        //}

        tutorialState = 0;

        objPanel = tutorialStates[tutorialState].panel.GetComponent<UIPanel>();
    }

    /// <summary>
    /// Setup the first page for fade in and display
    /// </summary>
    private void setupTutorialFirstPage()
    {
        isFadeIn = true;
        tutorialStates[tutorialState].panel.SetActive(true);
    }

    /// <summary>
    /// Setup the last page next button sprite and collider
    /// </summary>
    private void setupTutorialLastPage()
    {
        finishButtonUI = tutorialStates[tutorialStates.Count - 1].panel.GetComponentInChildren<UIButton>();
        finishButtonCollider = tutorialStates[tutorialStates.Count - 1].panel.GetComponentInChildren<BoxCollider>();
    }

    /// <summary>
    /// Prepares the state of the next tutorial.
    /// </summary>
    private void prepareNextTutorialState()
    {
        isFadeInAudioPlayed = false;
        tutorialStates[tutorialState].panel.SetActive(false);
        tutorialState++;
        tutorialStates[tutorialState] = tutorialStates[tutorialState];
        tutorialStates[tutorialState].panel.SetActive(true);
        objPanel = tutorialStates[tutorialState].panel.GetComponent<UIPanel>();
        isFadeOut = false;
        isFadeIn = true;
    }

    /// <summary>
    /// Sets the state of the next tutorial.
    /// </summary>
    private void setNextTutorialState()
    {
        isFadeIn = false;
    }

    /// <summary>
    /// Fade out the tutorial page
    /// </summary>
    private void fadeOut()
    {
        objPanel.alpha = GameObjectFade.AlphaFade(objPanel.alpha, MIN_ALPHA, tutorialStates[tutorialState].fadeOutTime);

        if (objPanel.alpha == MIN_ALPHA && !isTutorialDone)
        {
            prepareNextTutorialState();
        }
    }

    /// <summary>
    /// Fade in the tutorial page
    /// </summary>
    private void fadeIn()
    {
        // Custom logic to not move character out on last page
        //if (!RotateAlphaCharacter.IsCharacterAtTargetCycle(RotateAlphaCharacter.MOVING_STATE) && (tutorialState != tutorialStates.Count - 1))
        if ((isNeedTutorial || characterSystem.getCharacterState().Equals(AlphabetCoreConstants.CHARACTER_STATES.IN)) && (tutorialState != tutorialStates.Count - 1))
        {
            //RotateAlphaCharacter.ChangeEmotion(tutorialStates[tutorialState].tagName, tutorialStates[tutorialState].word);
            //RotateAlphaCharacter.SetReverseStatus(false, "", "");
            characterSystem.setCharacterToMoveOut(tutorialStates[tutorialState].tagName, tutorialStates[tutorialState].imageName, tutorialStates[tutorialState].message);
        }

        //if ((RotateAlphaCharacter.IsCharacterAtTargetCycle(RotateAlphaCharacter.OUT_STATE) || (tutorialState == tutorialStates.Count - 1)) && !isFadeInAudioPlayed)
        if ((characterSystem.getCharacterState().Equals(AlphabetCoreConstants.CHARACTER_STATES.OUT) || (tutorialState == tutorialStates.Count - 1)) && !isFadeInAudioPlayed)
        {
            playTutorialInstructionAudio(tutorialStates[tutorialState].clipName);
            if (!isFadeInAudioPlayed)
            {
                isFadeInAudioPlayed = true;
            }
        }

        objPanel.alpha = GameObjectFade.AlphaFade(objPanel.alpha, tutorialStates[tutorialState].fadeInAlphaValue, tutorialStates[tutorialState].fadeInTime);

        if (objPanel.alpha == tutorialStates[tutorialState].fadeInAlphaValue && isFadeInAudioPlayed)
        {
            setNextTutorialState();
        }
    }

    /// <summary>
    /// Check to enable the next button, and also when it is clicked
    /// </summary>
    protected void checkEnableTutorialNextButton()
    {
        if (!TutorialInstructionAudioController.IsPlaying && !finishButtonCollider.enabled)
        {
            finishButtonCollider.enabled = true;
            finishButtonUI.SetState(UIButtonColor.State.Normal, false);
        }
    }

    /// <summary>
    /// Core function to set to enable instructions mode after tutorial ends
    /// </summary>
    protected void onTutorialNextButtonClick()
    {
        if (isClickFinishButton)
        {
            isTutorialDone = true;
            isFadeOut = true;
            tutorialStates[tutorialState].panel.SetActive(false);
        }
    }

    /// <summary>
    /// Perform core tutorial logic
    /// </summary>
    private void checkCoreTutorialState()
    {
        if (isFadeOut && !TutorialInstructionAudioController.IsPlaying)
        {
            fadeOut();
        }
        else if (isFadeIn)
        {
            fadeIn();
        }
        else if (!isFadeOut && !isFadeIn)
        {
            checkTutorialState();
        }
    }
    #endregion

    #region INSTRUCTION_FUNCTIONS
    protected void setupInstructionStates()
    {
        _INSTRUCTION = AlphabetCoreConstants.INSTRUCTION + gameModeNumber + ".";

        //instructionStates = new List<InstructionState>();

        int stateCounter = 0;
        foreach (InstructionState iState in instructionStates)
        {
            stateCounter++;
            string instructionStateName = INSTRUCTION + stateCounter;
            iState.clipName = instructionStateName;
        }

        //for (int i = 0; i < instructionTagNames.Count; i++)
        //{
        //    string instructionStateName = INSTRUCTION + (i + 1);
        //    InstructionState isObj = new InstructionState(instructionTagNames[i], instructionStateName, messages[i], waitTimes[i]);
        //    instructionStates.Add(isObj);
        //}

        instructionState = 0;

        characterSystem = GetComponent<AlphabetCharacterController>();
        //timerSystem = new TimerController(0, instructionStates[instructionState].waitTime, maxTimeReachCount, timeIncrement, glassCeiling);
        //timerSystem = new TimerController();
        //timerSystem.setupTimer(0, instructionStates[instructionState].waitTime,
        //                                     instructionStates[instructionState].maxTimeReachCount,
        //                                     instructionStates[instructionState].timeIncrement,
        //                                     instructionStates[instructionState].absoluteMaxTime);
        timerSystem = new TimerController(instructionStates[instructionState]);

        TutorialInstructionAudioController.instance.stopAll();
    }

    /// <summary>
    /// Sets the state of the next tutorial.
    /// </summary>
    public void setNextInstructionState()
    {
        if (instructionState < instructionStates.Count - 1)
        {
            instructionState++;
            setNextInstructionStateProperties();
            //CCTimerSuperClass.SetupTimer(0, instructionStates[instructionState].time, maxTimeReachCount, timeIncrement, glassCeiling);
            //timerSystem.setupTimer(0, instructionStates[instructionState].waitTime, maxTimeReachCount, timeIncrement, glassCeiling);
            //isInstructionStateChanged = true;
        }
    }

    /// <summary>
    ///  pick up a random instruction state 
    ///  can include a list of exclude state if necesssary
    /// </summary>
    /// <param name="first"></param>
    /// <param name="second"></param>
    /// <param name="excludeNumer"></param>
    public void SetRandomInstructrionState(int first = -1, int second = -1, List<int> excludeNumer= null)
    {
        Debug.Log(first + " : " +  second);

        if (instructionState < instructionStates.Count - 1)
        {
            if (second > first && first > -1)
            {
                instructionState = Random.Range(first, second + 1);
                if (excludeNumer != null)
                {
                    while (excludeNumer.Contains(instructionState))
                    {
                        instructionState = Random.Range(first, second + 1);
                    }
                }
            } else if(first > -1 && second == -1)
            {
                instructionState = first;
            }
            setNextInstructionStateProperties();

        }
    }

    public void playLastIndex()
    {
        if (instructionState < instructionStates.Count - 1)
        {
            instructionState = instructionStates.Count - 1;
            setNextInstructionStateProperties();

        }
    }
    /// <summary>
    /// Sets the state of the next tutorial.
    /// </summary>
    /// <param name="targetState">Set the target state number.</param>
    public void setNextInstructionState(int targetState)
    {
        if (targetState <= instructionStates.Count - 1)
        {
            instructionState = targetState;
            setNextInstructionStateProperties();
            //CCTimerSuperClass.SetupTimer(0, instructionStates[instructionState].time, maxTimeReachCount, timeIncrement, glassCeiling);
            //timerSystem.setupTimer(0, instructionStates[instructionState].waitTime, maxTimeReachCount, timeIncrement, glassCeiling);
            //isInstructionStateChanged = true;
        }
    }

    private void setNextInstructionStateProperties()
    {
        //timerSystem.setupTimer(0, instructionStates[instructionState].waitTime,
        //                              instructionStates[instructionState].maxTimeReachCount,
        //                              instructionStates[instructionState].timeIncrement,
        //                              instructionStates[instructionState].absoluteMaxTime);
        timerSystem.setupTimerForInstructionState(instructionStates[instructionState]);
        isInstructionStateChanged = true;
    }

    /// <summary>
    /// Setup the first page for fade in and display
    /// </summary>
    private void setupInstructionFirstPage()
    {
        isFadeIn = true;
        tutorialStates[tutorialState].panel.SetActive(true);
    }

    /// <summary>
    /// Reset current instruction state timer
    /// </summary>
    protected void resetInstructionState()
    {
        TutorialInstructionAudioController.instance.stopAll();
        //RotateAlphaCharacter.ResetReverseStatus();
        characterSystem.setCharacterToMoveIn();
        //CCTimerSuperClass.ResetTimeValueToZero();
        timerSystem.resetTimer();
        isInstructionStateChanged = false;
        isInstructionAudioPlayed = false;
    }

    /// <summary>
    /// Reset instructions to first stage
    /// </summary>
    public void resetAllInstructions()
    {
        resetInstructionState();
        instructionState = 0;
        isMagicScreenTracked = false;
        //timerSystem.setupTimer(0, instructionStates[instructionState].waitTime, maxTimeReachCount, timeIncrement, glassCeiling);
        //timerSystem.setupTimer(0, instructionStates[instructionState].waitTime,
        //                              instructionStates[instructionState].maxTimeReachCount,
        //                              instructionStates[instructionState].timeIncrement,
        //                              instructionStates[instructionState].absoluteMaxTime);
        timerSystem.setupTimerForInstructionState(instructionStates[instructionState]);
    }

    /// <summary>
    /// Perform core instruction logic
    /// </summary>
    private void checkCoreInstructionState()
    {
        // If state changed, reset timers, stop audio and reverse character back in
        if (isInstructionStateChanged)
        {
            resetInstructionState();
        }
        //else if (CCTimerSuperClass.MaxTimeReached())
        else if (timerSystem != null && timerSystem.isMaxTimeReached())
        {
            // Slide character out
            //if (RotateAlphaCharacter.IsCharacterAtTargetCycle(RotateAlphaCharacter.IN_STATE))
            if (characterSystem.getCharacterState().Equals(AlphabetCoreConstants.CHARACTER_STATES.IN))
            {
                //RotateAlphaCharacter.ChangeEmotion(instructionStates[instructionState].tagname, instructionStates[instructionState].msg);
                //RotateAlphaCharacter.SetReverseStatus(false, "", "");
                characterSystem.setCharacterToMoveOut(instructionStates[instructionState].tagName, instructionStates[instructionState].imageName , instructionStates[instructionState].message , instructionStates[instructionState].requireSuggestedWord);

            }
            // Once the character is fully out, play audio
            //else if (RotateAlphaCharacter.IsCharacterAtTargetCycle(RotateAlphaCharacter.OUT_STATE) && !isInstructionAudioPlayed)
            else if (characterSystem.getCharacterState().Equals(AlphabetCoreConstants.CHARACTER_STATES.OUT) && !isInstructionAudioPlayed)
            {
                playTutorialInstructionAudio(instructionStates[instructionState].clipName);
                isInstructionAudioPlayed = true;
            }
            // Once the audio is finished playing, reset character too
            else if (!TutorialInstructionAudioController.IsPlaying && isInstructionAudioPlayed)
            {
                //timerSystem.checkMaxTimeIncrement();
                timerSystem.checkInstructionTimerIncrement();
                resetInstructionState();
            }
        }

        checkInstructionState();
    }
    #endregion

    void Update()
    {
        if (timerSystem != null)
        {
            timerSystem.runTimer();
        }

        if (!isTutorialDone)
        {
            checkCoreTutorialState();
        }
        else if (isInstructionStart)
        {
            checkCoreInstructionState();
        }
    }

    ///// <summary>
    ///// Play game audio without duplication.
    ///// </summary>
    //protected void playGameAudio(string audioName)
    //{
    //    // Limit audio to one track at a time
    //    if (AlphabetCoreAudioSystem.IsPlaying)
    //    {
    //        return;
    //    }

    //    AlphabetCoreAudioSystem.instance.stopAll();
    //    AlphabetCoreAudioSystem.instance.playOnce(audioName);
    //}

    /// <summary>
    /// Play game tutorial or instruction audio without duplication.
    /// </summary>
    protected void playTutorialInstructionAudio(string audioName)
    {
        // Limit audio to one track at a time
        if (TutorialInstructionAudioController.IsPlaying)
        {
            return;
        }

        TutorialInstructionAudioController.instance.stopAll();
        TutorialInstructionAudioController.instance.playOnce(audioName);
    }

    public bool isPanelFadingIn()
    {
        return isFadeIn;
    }

    public bool isFadeInAudioPlayedOnce()
    {
        return isFadeInAudioPlayed;
    }

    public bool isTutorialFinished()
    {
        return isTutorialDone;
    }

    /*
    // Timer debug
    void OnGUI()
    {
        if (timerSystem != null)
        {
            int yPos = 90;
            GUI.color = Color.blue;
            //GUI.Label (new Rect (250, 10, 200, 50), "PlayerStatus : " + CCPlayerStatusManager .ReturnCurrentPlayerStatus ());
            //	GUI.Label (new Rect (250, 50, 400, 50), "Compare PlayStatus(True-Different / False-Same): " +  CCPlayerStatusManger .ReturCompareStatus ().ToString());
            GUI.Label(new Rect(250, yPos += 30, 200, 50), "Timer: " + timerSystem.time.ToString());
            GUI.Label(new Rect(250, yPos += 30, 200, 50), "Max Time: " + timerSystem.maxTime.ToString());
            //GUI.Label(new Rect(250, 150, 200, 50), "Time increment Counter: " + timerSystem.timeReachCount.ToString());
            //GUI.Label(new Rect(250, 210, 200, 50), "Max Time Curve Counter: " + timerSystem.maxTimeReachCount.ToString());
            //GUI.Label(new Rect(250, 150, 200, 50), "Time increment: " + timerSystem.timeIncrement.ToString());
            GUI.Label(new Rect(250, yPos += 30, 200, 50), "Max time type: " + timerSystem.timerMaxIncreaseType.ToString());
            GUI.Label(new Rect(250, yPos += 30, 200, 50), "Slope: " + timerSystem.slope.ToString());
            GUI.Label(new Rect(250, yPos += 30, 200, 50), "Power: " + timerSystem.power.ToString());
            GUI.Label(new Rect(250, yPos += 30, 200, 50), "Absolute Max Time: " + timerSystem.absoluteMaxTime.ToString());
            GUI.Label(new Rect(250, yPos += 30, 200, 50), "instructionState: " + instructionState.ToString());
            //GUI.Label (new Rect (250, 240, 200, 50), "Begin: " + CCPlayerStatusManager .BeginStatus);
            //GUI.Label (new Rect (250, 270, 200, 50), "End: " +  	CCPlayerStatusManager .EndStatus);
            //GUI.Label (new Rect (250, 300, 200, 50), "MagicScreen : " + CCPlayerStatusManager .IsMagicScreenScanned);
            //	GUI.Label (new Rect (250, 330, 200, 50), "Is Compare Result Finish yet ? : " +  CCPlayerStatusManger .ReturnCompareResult().ToString());
        }
    }
    */
}