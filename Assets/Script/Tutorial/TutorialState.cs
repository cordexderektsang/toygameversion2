﻿using UnityEngine;

[System.Serializable]
public class TutorialState
{
    public const float MIN_FADE_TIME = 0.1f;
    public const float MAX_FADE_TIME = 3f;
    public const float MIN_ALPHA = 0.0f;
    public const float MAX_ALPHA = 1.0f;

    [Header("Tutorial Settings")]
    public AlphabetCoreConstants.CHARACTER_TAG_NAMESS tagName;
    public AlphabetCoreConstants.CHARACTER_Image_Icon imageName;
    [Range(MIN_FADE_TIME, MAX_FADE_TIME)]
    public float fadeInTime;
    [Range(MIN_ALPHA, MAX_ALPHA)]
    public float fadeInAlphaValue;
    [Range(MIN_FADE_TIME, MAX_FADE_TIME)]
    public float fadeOutTime;
    [System.NonSerialized]
    public string clipName;
    public string message;
    public GameObject panel;

    /// <summary>
    /// Initializes a new instance of the <see cref="TutorialState"/> class.
    /// </summary>
    /// <param name="tagName">Tag name for character picture</param>
    /// <param name="fadeInTime">Fade in time.</param>
    /// <param name="fadeInAlphaValue">Fade in to alpha value.</param>
    /// <param name="fadeOutTime">Fade out time.</param>
    /// <param name="clipName">Name of audio clip.</param>
    /// <param name="message">Message to display.</param>
    /// <param name="panel">Panel.</param> 
    public TutorialState(AlphabetCoreConstants.CHARACTER_TAG_NAMESS tagName, AlphabetCoreConstants.CHARACTER_Image_Icon imageName, float fadeInTime, float fadeInAlphaValue, float fadeOutTime, string clipName, string message, GameObject panel)
    {
        this.tagName = tagName;
        this.imageName = imageName;
        this.fadeInTime = fadeInTime;
        this.fadeInAlphaValue = fadeInAlphaValue;
        this.fadeOutTime = fadeOutTime;
        this.clipName = clipName;
        this.message = message;
        this.panel = panel;
    }
}