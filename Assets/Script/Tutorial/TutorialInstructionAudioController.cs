﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TutorialInstructionAudioController : MonoBehaviour 
{
	public List<AudioClip> clips_list = new List<AudioClip> ();
	
	private static TutorialInstructionAudioController _obj;
	
	public static bool IsPlaying = false;
	
	// dot instance to call class level method/function 
	public static TutorialInstructionAudioController instance
	{
		get
		{
			if(_obj == null)
			{
				_obj = GameObject.Find ("TutorialInstructionAudioController").GetComponent<TutorialInstructionAudioController> ();
			}
			
			return _obj;
		}
		set
		{
			_obj = value;
		}
	}
	
	public void playOnce (string name)
	{
		IsPlaying = true;
		try {
			AudioClip _clip = null;
            _clip = clips_list.Find(delegate (AudioClip clip) { return clip.name == name; });
			//_clip = clips_list.FindAll(delegate (AudioClip clip) {return clip.name == name;}) [0];
				
			GetComponent<AudioSource>().PlayOneShot (_clip);
			StartCoroutine ("countTime", _clip.length);
		} catch {
			CCLog.SpecialMsg ("Speech From Web : " + name);
			
			if (name.Length < 3) return;
			
			if(Application.internetReachability != NetworkReachability.NotReachable) // Demo use
			{
				StartCoroutine ("text2Speech", name);
			}
			else
			{
				IsPlaying = true;
			}
		}
	}

	public void stopAll ()
	{
		StopCoroutine ("text2Speech");
		StopCoroutine ("countTime");
		GetComponent<AudioSource>().enabled = false;
		GetComponent<AudioSource>().enabled = true;
		IsPlaying = false;
	}
	
	[System.Reflection.Obfuscation(ApplyToMembers=false)]
	private IEnumerator countTime (float t)
	{
		yield return new WaitForSeconds (t);
		IsPlaying = false;
	}
	
	// Use web Voice
	[System.Reflection.Obfuscation(ApplyToMembers=false)]
	private IEnumerator text2Speech (string text)
	{
		string url = 
		"https://s.yimg.com/tn/dict/ox/mp3/v1/" + WWW.EscapeURL(text) +"@_us_1.mp3";
		//"http://tts-api.com/tts.mp3?q=" + WWW.EscapeURL(text);
		//"http://translate.google.com/translate_tts?tl=en&q=" +  WWW.EscapeURL(text);
		
		
		WWW www = new WWW (url);
		yield return www;
		GetComponent<AudioSource>().clip = www.GetAudioClip (false, false, AudioType.MPEG);
		GetComponent<AudioSource>().Play ();
		
		yield return StartCoroutine ("countTime", GetComponent<AudioSource>().clip.length);
		IsPlaying = false;
	}
}
//https://www.facebook.com/dialog/feed?app_id=145634995501895&display=popup&caption=By%20Cordex&picture=http://dreamatico.com/data_images/apple/apple-1.jpg&redirect_uri=https://developers.facebook.com/tools/explorer