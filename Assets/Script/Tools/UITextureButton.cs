﻿using UnityEngine;
using System.Collections;

public class UITextureButton : UIButton {
	
	/// <summary>
	/// Texture of the normal state texture.
	/// </summary>
	public Texture normalTexture;
	
	/// <summary>
	/// Texture of the hover state texture.
	/// </summary>
	public Texture hoverTexture;

	/// <summary>
	/// Texture of the pressed texture.
	/// </summary>
	public Texture pressedTexture;
	
	/// <summary>
	/// Texture of the disabled texture.
	/// </summary>
	public Texture disabledTexture;

	// Cached value
	[System.NonSerialized] UITexture mTexture;

	/// <summary>
	/// Cache the texture we'll be working with.
	/// </summary>
	protected override void OnInit ()
	{
		base.OnInit();
		mTexture = (mWidget as UITexture);
		if (mTexture != null) 
		{
			normalTexture = mTexture.mainTexture;
		}
	}
	
	/// <summary>
	/// Change the visual state.
	/// </summary>
	public override void SetState (State state, bool immediate)
	{
		base.SetState (state, immediate);

		if (mTexture != null)
		{
			switch (state)
			{
				case State.Normal: SetTexture (normalTexture); break;
				case State.Hover: SetTexture (hoverTexture == null ? normalTexture : hoverTexture); break;
				case State.Pressed: SetTexture (pressedTexture); break;
				case State.Disabled: SetTexture (disabledTexture); break;
			}
		}
	}
	
	/// <summary>
	/// Convenience function that changes the texture.
	/// </summary>
	protected void SetTexture (Texture texture)
	{
		if (mTexture != null && texture != null && mTexture.mainTexture != texture)
		{
			mTexture.mainTexture = texture;
			if (pixelSnap) mTexture.MakePixelPerfect ();
		}
	}
}
