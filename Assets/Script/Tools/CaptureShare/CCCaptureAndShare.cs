/*
 * Example *
StartCoroutine
(
	CCCaptureAndShare.StartTakeScreenshot 
	(
		(Texture2D screenCapt) => 
		{
			SaveToAlbum (screenCapt, "123");
		}
	)
);
*/
using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

// Cordex Core Tools For Capture And Share.
public class CCCaptureAndShare 
{
	// Centralized
	private static System.Action <int> callback;
	
	#if !UNITY_EDITOR && UNITY_ANDROID
		private static readonly string CaptureSaveAndroidClassName = "com.cordex.saveorshare.SaveOrShare";
		private static readonly string CaptureSaveAndroidInterfaceName = CaptureSaveAndroidClassName + "$UnityCallback";
	    private static AndroidJavaClass javaClass;
	    public static AndroidJavaClass SaveOrShareClass 
	    {
		    get
		    {
			    if (javaClass == null)
			    {
				    javaClass = new AndroidJavaClass (CaptureSaveAndroidClassName);
			    }
			    return javaClass;
		    }
		    set
		    {
			    javaClass = value;
		    }
	    }
		public class UnityCallback : AndroidJavaProxy
		{
			public UnityCallback () : base (CaptureSaveAndroidInterfaceName)
			{
			}
		
			public void CaptureComplete (int value) 
			{
				if (CCCaptureAndShare.callback != null)
				{
					// Call the centralized callback and pass the msg to outside
					CCCaptureAndShare.callback (value);
					CCCaptureAndShare.callback = null;
				}
	        }
		}
	#elif !UNITY_EDITOR && UNITY_IPHONE
	    // Bridge for communicating between C# and Obj-C
	
	    [DllImport ("__Internal")]
	    private static extern void StartSharingImageToSocialUnity (byte[] ptr, int length, string type, string title, string msg, CaptureShareCallbackDelegate callback);

	    [DllImport ("__Internal")]
	    private static extern void SaveImageToAlbumUnity (byte[] ptr, int length, CaptureShareCallbackDelegate callback);
		
		// A delegate which match in Obj-C side
		private delegate void CaptureShareCallbackDelegate (int result);
	
		// The delegate method implementation
		[AOT.MonoPInvokeCallbackAttribute (typeof (CaptureShareCallbackDelegate))]
		private static void CaptureShareCallbackMethod (int result) 
		{
			if (CCCaptureAndShare.callback != null)
			{
				// Call the centralized callback and pass the msg to outside
				CCCaptureAndShare.callback (result);
				CCCaptureAndShare.callback = null;
			}
		} 
	#endif
	
	/// <summary>
	/// Capture new screenshot. **IEnumerator**
	/// </summary>
	/// <param>
	/// An action with texture taken 
	/// </param>
	public static IEnumerator CaptureNewScreenshotWithInlineCallback (System.Action<Texture2D> inlineCallback)
	{
		yield return new WaitForEndOfFrame ();
		if (inlineCallback != null)
		{
			// Read pixels to texture but slow per each device
			Texture2D screenshot = new Texture2D (Screen.width, Screen.height, TextureFormat.RGB24, false);
			screenshot.ReadPixels (new Rect (0, 0, Screen.width, Screen.height), 0, 0);
			screenshot.Apply (); // This method call will freeze the app a bit, but haven't a better way
			yield return new WaitForEndOfFrame ();
			inlineCallback (screenshot);
		}
	}
	
	/// <summary>
	/// Starts the capture and save to album
	/// </summary>
	/// <param name='screenshot'>
	/// This image which need to save to album
	/// </param>
	/// <param name='subFolderName'>
	/// Name of subFolderName only for android.
	/// </param>
	public static void SaveToAlbum (Texture2D screenshot, string subFolderName, System.Action <int> callback)
	{	
		/*
		if (Application.platform == RuntimePlatform.OSXEditor || Application.platform == RuntimePlatform.WindowsEditor)
		{
			CCLog.SpecialMsg ("Capture Share not plan to support Editor");
			return;
		}
		*/

		// Centralized the callback method to this class
		CCCaptureAndShare.callback = callback;

		// Get the byte array of the image
		byte [] byteArray = screenshot.EncodeToPNG ();
		
		#if !UNITY_EDITOR && UNITY_ANDROID
			string filePath = SaveOrShareClass.CallStatic <string> ("SaveBitmapToImage", CCAndroidHelper.UnityAndroidCurrentActivity, byteArray, subFolderName, "png", new UnityCallback ());
		    SaveOrShareClass.CallStatic ("RescanMedia", CCAndroidHelper.UnityAndroidCurrentActivity, filePath, "image/png");
		#elif !UNITY_EDITOR && UNITY_IPHONE
			//Native call
			SaveImageToAlbumUnity (byteArray, byteArray.Length, CCCaptureAndShare.CaptureShareCallbackMethod);
		#else
			CCLog.SpecialMsg ("Screenshot save in " + Application.dataPath + "/screenshot.png");
			System.IO.File.WriteAllBytes (Application.dataPath + "/screenshot.png", byteArray);
		#endif
		
		// Memory control here
		byteArray = null;
	}
	
	/// <summary>
	/// Same as SaveToAlbum but capture by native
	/// </summary>
	public static void CaptureAndSaveToAlbum (string subFolderName, System.Action <int> callback)
	{		
		if (Application.platform == RuntimePlatform.OSXEditor || Application.platform == RuntimePlatform.WindowsEditor)
		{
			CCLog.SpecialMsg ("CaptureAndSaveToAlbum not plan to support Editor");
			return;
		}

		// Centralized the callback method to this class
		CCCaptureAndShare.callback = callback;

		#if !UNITY_EDITOR && UNITY_ANDROID
			string filePath = SaveOrShareClass.CallStatic <string> ("SaveBitmapToImage", CCAndroidHelper.UnityAndroidCurrentActivity, null, subFolderName, "png", new UnityCallback ());
		    SaveOrShareClass.CallStatic ("RescanMedia", CCAndroidHelper.UnityAndroidCurrentActivity, filePath, "image/png");
		#elif !UNITY_EDITOR && UNITY_IPHONE
			// Native call
	    	SaveImageToAlbumUnity (null, 0, CCCaptureAndShare.CaptureShareCallbackMethod);
		#endif		
	}

	/// <summary>
	/// Type of iOS/Android social media.
	/// iOS type is starting with SL
	/// </summary>
	public enum SocialType
	{
		SLServiceTypeUIActivity,
		SLServiceTypeFacebook,
		SLServiceTypeTwitter,
		SLServiceTypeSinaWeibo = 5,
		AndroidTypeDefault,
	}
	
	/// <summary>
	/// Starts sharing image to social
	/// </summary>
	/// <param name='screenshot'>
	/// This image which need to save to album
	/// </param>
	/// <param name='type'>
	/// Type for sharing.
	/// </param>
	/// <param name='title'>
	/// Title for sharing.
	/// </param>
	/// <param name='msg'>
	/// Message for sharing.
	/// </param>
	/// <param name='subFolderName'>
	/// Name of subFolderName only for android.
	/// </param>
	public static void StartSharing (Texture2D screenshot, SocialType type, string title, string msg, string subFolderName, System.Action <int> callback)
	{
		/*
		if (Application.platform == RuntimePlatform.OSXEditor || Application.platform == RuntimePlatform.WindowsEditor)
		{
			CCLog.SpecialMsg ("Capture Share not plan to support Editor");
			return;
		}
		*/
		// Centralized the callback method to this class
		CCCaptureAndShare.callback = callback;

		byte [] byteArray = screenshot.EncodeToPNG ();

        #if !UNITY_EDITOR && UNITY_ANDROID
		    // If wrong type for Android, default will replace it
		    if ((int)type < 6) 
		    {
			    type = SocialType.AndroidTypeDefault;
		    }
			string filePath = SaveOrShareClass.CallStatic <string> ("SaveBitmapToImage", CCAndroidHelper.UnityAndroidCurrentActivity, byteArray, subFolderName, "png", new UnityCallback ());
            SaveOrShareClass.CallStatic ("RescanMedia", CCAndroidHelper.UnityAndroidCurrentActivity, filePath, "image/png");
		    SaveOrShareClass.CallStatic ("ShareImage", CCAndroidHelper.UnityAndroidCurrentActivity, filePath, "image/png");
        #elif !UNITY_EDITOR && UNITY_IPHONE
			// If wrong type for iOS, default will replace it
		    if ((int)type > 5) 
		    {
			    type = SocialType.SLServiceTypeUIActivity;
		    }
		
		    // Native call
		    StartSharingImageToSocialUnity (byteArray, byteArray.Length, type.ToString (), title, msg, CCCaptureAndShare.CaptureShareCallbackMethod);
		#else
			CCLog.WarningMsg ("Share are not planning to support Editor");
			CCLog.SpecialMsg ("Screenshot save in " + Application.dataPath + "/screenshot.png");
			System.IO.File.WriteAllBytes (Application.dataPath + "/screenshot.png", byteArray);
		#endif
		
		// Memory control here
		byteArray = null;
	}
	
	/// <summary>
	/// Same as StartSharing but capture by native
	/// </summary>
	public static void CaptureAndStartSharing (SocialType type, string title, string msg, string subFolderName, System.Action <int> callback)
	{		
		if (Application.platform == RuntimePlatform.OSXEditor || Application.platform == RuntimePlatform.WindowsEditor)
		{
			CCLog.SpecialMsg ("CaptureAndStartSharing are not planning to support Editor");
			return;
		}

		// Centralized the callback method to this class
		CCCaptureAndShare.callback = callback;
		
		#if !UNITY_EDITOR && UNITY_ANDROID
		    // If wrong type for Android, default will replace it
		    if ((int)type < 6) 
		    {
			    type = SocialType.AndroidTypeDefault;
		    }
		
			string filePath = SaveOrShareClass.CallStatic <string> ("SaveBitmapToImage", CCAndroidHelper.UnityAndroidCurrentActivity, null, subFolderName, "png", new UnityCallback ());
            SaveOrShareClass.CallStatic ("RescanMedia", CCAndroidHelper.UnityAndroidCurrentActivity, filePath, "image/png");
		    SaveOrShareClass.CallStatic ("ShareImage", CCAndroidHelper.UnityAndroidCurrentActivity, filePath, "image/png");
			if (callback != null) callback (1);
		#elif !UNITY_EDITOR && UNITY_IPHONE
		    // If wrong type for iOS, default will replace it
		    if ((int)type > 5) 
		    {
			    type = SocialType.SLServiceTypeUIActivity;
		    }
		
		    // Native call
		    StartSharingImageToSocialUnity (null, 0, type.ToString (), title, msg, CCCaptureAndShare.CaptureShareCallbackMethod);
		#endif		
	}
}
