﻿using UnityEngine;
using System.Collections;

public class POCCaptureShare : MonoBehaviour 
{
	public int rectSize = 150;
	
	void OnGUI ()
	{
		if (GUI.Button (new Rect (10, 10, rectSize, rectSize), "SaveToAlbum"))
		{
			StartCoroutine
			(
				CCCaptureAndShare.CaptureNewScreenshotWithInlineCallback 
				(
					(Texture2D screenCapt) => 
					{
						CCCaptureAndShare.SaveToAlbum (
							screenCapt,
							"",
							(int result) => {print ("Callback In POCCaptureShare: " + result);}
						);
					}
				)
			);
		}
		
		if (GUI.Button (new Rect (10, 10 + rectSize * 1, rectSize, rectSize), "CaptureAndSaveToAlbum"))
		{
			CCCaptureAndShare.CaptureAndSaveToAlbum (
				"", 
				(int result) => {print ("Callback In POCCaptureShare: " + result);}
			);
		}
		
		if (GUI.Button (new Rect (10, 10 + rectSize * 2, rectSize, rectSize), "ShareToSocial"))
		{
			StartCoroutine
			(
				CCCaptureAndShare.CaptureNewScreenshotWithInlineCallback 
				(
					(Texture2D screenCapt) => 
					{
						CCCaptureAndShare.StartSharing (
							screenCapt,
							CCCaptureAndShare.SocialType.SLServiceTypeUIActivity,
							"CordexAR",
							"Enjoy it!",
							"CordexAR",
							(int result) => {print ("Callback In POCCaptureShare: " + result);}
						);
					}
				)
			);
		}
		
		if (GUI.Button (new Rect (10, 10 + rectSize * 3, rectSize, rectSize), "CaptureAndStartSharing"))
		{
			CCCaptureAndShare.CaptureAndStartSharing (
				CCCaptureAndShare.SocialType.SLServiceTypeUIActivity,
				"CordexAR",
				"Enjoy it!",
				"CordexAR",
				(int result) => {print ("Callback In POCCaptureShare: " + result);}
			);
		}
		
		GUI.Label(new Rect (Screen.width-45, Screen.height - 20, 45, 20), "Cordex");

	}
}