using UnityEngine;
using System.Collections;

public class OnObjClickPlaySound : AlphabetCoreClickableObjBehaviour 
{
	public string id;
	public override void OnClick ()
	{
		AlphabetCoreAudioSystem.instance.stopAll ();
		AlphabetCoreAudioSystem.instance.playOnce (id ,AlphabetCoreConstants.AUDIO_Selections.SOUND);
	}
}
