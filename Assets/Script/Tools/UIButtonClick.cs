﻿using UnityEngine;
using System.Collections.Generic;

public class UIButtonClick : MonoBehaviour // UIButtonColor
{
	public List<EventDelegate> onClick = new List<EventDelegate> ();
	
	static public UIButtonClick current;
	
	protected void OnClick ()
	{
		if (current != null || onClick.Count < 1) return;
		current = this;
		EventDelegate.Execute (onClick);
		current = null;
	}
}
