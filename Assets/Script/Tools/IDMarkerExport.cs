﻿using UnityEngine;
using System.Collections;
using Vuforia;
using System.IO;

public class IDMarkerExport : MonoBehaviour {

	// Use this for initialization
	void Start () {
        
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void ExportIDToTxt()
    {
        string markerIDStr = "";
        MarkerBehaviour[] be = GetComponentsInChildren<MarkerBehaviour>(true);
        string[] markerList = new string[be.Length];
        for (int i = 0; i < be.Length; i++)
        {
           markerList[i] = be[i].GetComponent<AlphabetCoreTrackableEventHandler>().id + "_" + be[i].Marker.MarkerID;
        }

        File.WriteAllLines(Application.persistentDataPath + "/marker.txt", markerList);
        Debug.Log(Application.persistentDataPath + "/marker.txt");
    }
}
