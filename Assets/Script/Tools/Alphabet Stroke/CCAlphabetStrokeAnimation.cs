﻿using UnityEngine;
using System.Collections;

/// <summary>
/// This class need to apply on a GameObject which has a material of Alpha CutOff Shader.
/// It can animate the Alpha channle to finish expected animation
/// </summary>
public class CCAlphabetStrokeAnimation : MonoBehaviour
{
	/* Testing Use 
	void OnGUI ()
	{
		if(GUI.Button(new Rect (10, 10, 300, 200), "Play"))
			play ();
		
		if(GUI.Button(new Rect (10, 210, 300, 200), "Reset"))
			resetToDefault ();
	}
	*/
	
	/// <summary>
	/// The name of animation script, to indentfy animation to start and stop
	/// </summary>
	private readonly string AnimationName = "StrokeAnimation";

	/// <summary>
	/// End seek value of animation 
	/// </summary>
	public float endSeek = 0.01f;
	
	/// <summary>
	/// Start/Default seek value of animation
	/// </summary>
	public float startSeek = 1.0f;
	
	/// <summary>
	/// How logn the animation play within range.
	/// </summary>
	public float animationTime = 10.0f;
	
	/// <summary>
	/// The shader of material the is the core object of the script.
	/// </summary>
	private Material _mat;
	public Material tagetMaterial
	{
		get
		{
			// Check if null and asign value the mat  
			if (_mat == null) 
			{
				_mat = GetComponent<Renderer>().material;
			}
			return _mat;
		}
		set
		{
			_mat = value;
		}
	}
	
	/// <summary>
	/// Play the stroke animation
	/// </summary>
	public void play ()
	{
		// Reset to start to avoid animation mulit play
		resetToDefault ();
		animationTime = 8;
		//Debug.Log (animationTime);
		// Tween value from start to end within animationTime you set
		iTween.ValueTo (gameObject, iTween.Hash 
									(
										"from", startSeek,
										"to", endSeek,
										"time", animationTime,
										"easetype", iTween.EaseType.linear,
										"name", AnimationName,
										"onupdatetarget", gameObject,
										"onupdate", "OnValueChange"
									));
	}
	
	/// <summary>
	/// Reset and stop the stroke animation
	/// </summary>
	public void resetToDefault ()
	{	
		// Stop animation by name
		iTween.StopByName (gameObject, AnimationName);
		
		// Set the value to default
		tagetMaterial.SetFloat ("_Cutoff", startSeek);
	}
	
	/// <summary>
	/// Set the texture to target material.
	/// </summary>
	/// <param name='texture'>
	/// The texture you want to set.
	/// </param>
	public void setTextureToMaterial (Texture texture)
	{
		// Setup texture to current material
		tagetMaterial.mainTexture = null;
		tagetMaterial.mainTexture = texture;
		tagetMaterial.SetFloat ("_Cutoff", startSeek);
		Resources.UnloadUnusedAssets ();
		System.GC.Collect();
	}
	
	/// <summary>
	/// Set the color to target material.
	/// </summary>
	/// <param name='color'>
	/// The color you want to set.
	/// </param>
	public void setColorToMaterial (Color color)
	{
		// Setup color to current material
		tagetMaterial.color = color;
	}
	
	/// <summary>
	/// Callback of Value From iTween
	/// </summary>
	/// <param name='newValue'>
	/// Value From iTween
	/// </param>
	[System.Reflection.Obfuscation(ApplyToMembers=false)]
	private void OnValueChange(float newValue)
	{
		// Setting value to mat to play the storke animation
		tagetMaterial.SetFloat ("_Cutoff", newValue);
	}
}
