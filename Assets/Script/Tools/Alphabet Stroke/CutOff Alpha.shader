﻿Shader "Cutoff Alpha" {
    Properties 
	{
        _MainTex ("Base (RGB) Transparency (A)", 2D) = "" {}
        _Cutoff ("Alpha cutoff", Range (0.1,1)) = 1
		_Color ("Main Color", Color) = (1, 1, 1, 1)
    }
    SubShader 
	{
		Tags {"Queue" = "Overlay+100" }
        Pass 
		{
            // Use the Cutoff parameter defined above to determine
            // what to render.
            AlphaTest GEqual [_Cutoff]
            Lighting off
            SetTexture [_MainTex] 
			{ 
				constantColor [_Color]
				combine texture * constant, texture * primary 
			}
        }
    }
}