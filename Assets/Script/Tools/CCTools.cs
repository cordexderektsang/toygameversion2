﻿using UnityEngine;
using System.Text;
using System.Collections;
using System.Collections.Generic;

public static class CCTools 
{
	/// <summary>
	/// Randomizes the list order.
	/// ** Pass by ref **
	/// </summary>
	public static void RandomizeIList <T> (this IList <T> list)  
	{  
		System.Random rng = new System.Random ();  
	    int n = list.Count;  
	    while (n > 1) {  
	        n--;  
	        int k = rng.Next(n + 1);  
	        T value = list[k];  
	        list[k] = list[n];  
	        list[n] = value;  
	    }  
	}
	
	/// <summary>
	/// Checks the spelling between user input and words.
	/// </summary>
	public static List<string> StringCompeareWithList (this string input, IList <string> list)
	{
		if (list == null || list.Count < 1) return new List <string> (0);
		List<string> output = new List <string> (list.Count);
		foreach(string word in list)
		{	
			output.Add(StringCompeare (input, word));
		}
		return output;
	}
	
	/// <summary>
	/// Checks the spelling between user input and word.
	/// </summary>
	public static string StringCompeare (this string input, string word)
	{
		if (string.IsNullOrEmpty (word)) return "";
		string output = "";

		if(word.StartsWith (input))
		{
			output = input;
		}
		else if (word [0] == input [0]) 
		{
			int index = 1;
			for (; index < word.Length; index++)
			{
				if (word [index] != input [index]) break;
			}
			output = word.Substring (0, index);
		}
		else if(word.Contains (input))
		{
			int index_of_word = word.IndexOf (input);
			output = word.Substring (index_of_word, index_of_word + input.Length);
		}
		else
		{
			output = "";
		}
		
		return output;
	}
	
	public class Convert
	{
		/// <summary>
		/// Strings to ASCI int.
		/// </summary>
		public static int StringToASCII (string input)
		{
			return (int)(input.ToCharArray()[0]);
		}
		
		/// <summary>
		/// ASCI int to string.
		/// </summary>
		public static string ASCIIToString (int input)
		{
			return ((char) input).ToString ();
		}
		
		/// <summary>
		/// Strings to string arr with split mark.
		/// ** split mark may be empth to fulfill needs **
		/// </summary>
		public static string[] StringToStringArr(string str_Temp, string split)
		{
			return str_Temp.Split(new string[] { split }, System.StringSplitOptions.RemoveEmptyEntries);
		}
		
		/// <summary>
		/// String Array to string with split mark.
		/// ** split mark may be empth to fulfill needs **
		/// </summary>
		public static string StringArrToString (string[] arr, string split)
		{
			string output = "";
			int max = arr.Length - 1;
			for (int i = 0; i < max - 1; i++)
			{
				output += arr[i] + split;
			}
			output += arr[max];
			return output;
		}
	}
}
