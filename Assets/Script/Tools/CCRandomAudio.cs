﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CCRandomAudio : MonoBehaviour 
{
	public List<AudioClip> clips = new List<AudioClip> ();
	public bool loop;
    [Range(0.0f, 0.01f)]
    public float volumeMultiplier;
	private float volume = 0.1f;
	
	private AudioSource _AudioSource;
	
	// Use this for initialization
	void Start () 
	{
		_AudioSource = GetComponent<AudioSource> ();
		SettingPanelBehaviour.BGMChange += OnBGMEnableChange;
		init ();
	}
	
	void OnDestroy ()
	{
		SettingPanelBehaviour.BGMChange -= OnBGMEnableChange;
	}
	
	private void init ()
	{
        if (SettingPanelBehaviour.BGMValue == 0) 
		{
			return;
		}
		
		int ran = Random.Range (0, clips.Count);
		_AudioSource.volume = volume * SettingPanelBehaviour.BGMValue * volumeMultiplier; //0.005f
		_AudioSource.loop = loop;
		_AudioSource.clip = clips[ran];
		_AudioSource.Play ();
	}
	
	public void OnBGMEnableChange (int value)
	{
		_AudioSource.Stop ();
		_AudioSource.enabled = false;
		_AudioSource.enabled = true;
		if (value != 0)
		{
			init ();
		}
	}
}
