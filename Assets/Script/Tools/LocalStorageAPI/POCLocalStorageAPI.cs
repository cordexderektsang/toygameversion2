﻿using UnityEngine;
using System.Collections;

public class POCLocalStorageAPI : MonoBehaviour 
{
	public int rectSize = 150;
	
	void OnGUI ()
	{
		if (GUI.Button (new Rect (10, 10, rectSize, rectSize), "Contian"))
		{
			CCLog.SpecialMsg (CCLocalStorageAPI.Contian ("abc"));
		}
		
		if (GUI.Button (new Rect (10 + rectSize * 1, 10, rectSize, rectSize), "Reset"))
		{
			CCLog.SpecialMsg (CCLocalStorageAPI.ResetToDefault <int> ("abc"));
		}
		
		if (GUI.Button (new Rect (10, 10 + rectSize * 1, rectSize, rectSize), "GET"))
		{
			CCLog.SpecialMsg (CCLocalStorageAPI.GET <int> ("abc"));
		}
		
		if (GUI.Button (new Rect (10 + rectSize * 1, 10 + rectSize * 1, rectSize, rectSize), "Delete All"))
		{
			CCLocalStorageAPI.DeleteAll ();
			CCLog.SpecialMsg (true);
		}
		
		if (GUI.Button (new Rect (10, 10 + rectSize * 2, rectSize, rectSize), "SET"))
		{
			CCLog.SpecialMsg (CCLocalStorageAPI.SET <int> ("abc", 123));
		}
	}
}
