﻿public class CCLocalStorageAPI 
{
	/// <summary>
	/// Saves preferences now.
	/// Unity writes preferences to disk on Application Quit  
	/// In case when the game crashes or otherwise prematuraly exits, you might want to write the preferences at sensible 'checkpoints' in your game
	/// </summary> 
	public static void SaveNow ()
	{
		// Causing a small hiccup, but save
		UnityEngine.PlayerPrefs.Save ();
	}
	
	/// <summary>
	/// Contian the specified field_name in LocalStorage.
	/// </summary>
	/// <param name='field_name'>
	/// If LocalStorage has the field_name
	/// </param>
	public static bool Contian (string field_name)
	{
		// Return if LocalStorage contian the specific field 
		return UnityEngine.PlayerPrefs.HasKey (field_name);
	}
	
	/// <summary>
	/// Delete all data in LocalStorage.
	/// </summary>
	public static void DeleteAll ()
	{
		// Delete all data in LocalStorage
		UnityEngine.PlayerPrefs.DeleteAll ();
	}
	
	/// <summary>
	/// Reset  the specified field_name data in LocalStorage to default value.
	/// </summary>
	/// <param name='field_name'>
	/// Which field data may result.
	/// </param>
	/// <typeparam name='T'>
	/// The type of parameter of the field.
	/// </typeparam>
	public static bool ResetToDefault<T> (string field_name)
	{
		// Type Variable
		System.Type incomeType = typeof (T);
		
		// Using for return
		object value = null;
		
		// May need casting the value to with specific type
		if (incomeType == typeof (int) || incomeType == typeof (float))
		{
			// Set to default
			value = 0;
		}
		else if (incomeType == typeof (string))
		{
			// Set to default
			value = "";
		}
		else
		{
			// Do not execute any if type are not int ,float or string
			return false;
		}
		
		// Set the value to default
		return SET<T> (field_name, value);
	}

	/// <summary>
	/// GET the specified field value.
	/// </summary>
	/// <param name='field_name'>
	/// The specified field name in LocalStorage.
	/// </param>
	/// <typeparam name='T'>
	/// The type of parameter of the field.
	/// </typeparam>
	public static T GET<T> (string field_name)
	{
		// Type Variable
		System.Type incomeType = typeof (T);
		
		// Using for return
		object value = null;
		
		// May need execute different function with specific type
		if (incomeType == typeof (int))
		{
			// Get value if exist but default 0
			value = UnityEngine.PlayerPrefs.GetInt (field_name, 0);
		}
		else if (incomeType == typeof (float))
		{
			// Get value if exist but default 0
			value = UnityEngine.PlayerPrefs.GetFloat (field_name, 0);
		}		
		else if (incomeType == typeof (string))
		{
			// Get value if exist but default empty
			value = UnityEngine.PlayerPrefs.GetString (field_name, "");
		}
		else
		{
			// Do not execute any if type are not int ,float or string
			// default (T) mean the default value of the specific type
			CCLog.ErrorMsg ("Input type need to be int, float, string");
			return default (T);
		}
		
		// Return the value with type which is set
		return (T) System.Convert.ChangeType (value, incomeType);
	}
	
	/// <summary>
	/// SET the specified field data to input value.
	/// </summary>
	/// <param name='field_name'>
	/// The specified field name in LocalStorage.
	/// </param>
	/// <param name='value'>
	/// The value to save
	/// </param>
	/// <typeparam name='T'>
	/// The type of parameter of the field.
	/// </typeparam>
	public static bool SET<T> (string field_name, object value)
	{
		// Type variable
		System.Type incomeType = typeof (T);
		
		// Incoming type T must equal to type of value
		if (incomeType != value.GetType ())
		{
			CCLog.ErrorMsg ("Type of value and T are not same");
			return false;
		}
		
		// May need casting the value to with specific type
		if (incomeType == typeof (int))
		{
			// Save to the Local
			UnityEngine.PlayerPrefs.SetInt (field_name, (int) value);
		}
		else if (incomeType == typeof (float))
		{
			// Save to the Local
			UnityEngine.PlayerPrefs.SetFloat (field_name, (float) value);
		}		
		else if (incomeType == typeof (string))
		{
			// Save to the Local
			UnityEngine.PlayerPrefs.SetString (field_name, (string) value);
		}
		else
		{
			// Do not execute any if type are not int ,float or string
			CCLog.ErrorMsg ("Input type need to be int, float, string");
			return false;
		}
		
		return true;
	}
}
