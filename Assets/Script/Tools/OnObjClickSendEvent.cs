using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class OnObjClickSendEvent : AlphabetCoreClickableObjBehaviour 
{
	[System.Reflection.Obfuscation(ApplyToMembers=false)]
	public List<EventDelegate> onClick = new List<EventDelegate> ();
	
	static public OnObjClickSendEvent current;
	
	[System.Reflection.Obfuscation(ApplyToMembers=false)]
	public override void OnClick ()
	{
		if (current != null || onClick.Count < 1) return;
		current = this;
		EventDelegate.Execute (onClick);
		current = null;
	}
}
