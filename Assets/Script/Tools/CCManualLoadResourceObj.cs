﻿
using UnityEngine;

public class CCManualLoadResourceObj : UnityEngine.Object
{
	private Object loaded_asset;
    private Object[] loaded_assets;
	public GameObject current_obj
	{
		get;
		private set;
	}
	
	/// <summary>
	/// Instantiate the specified Obj inside Resources file.
	/// </summary>
	/// <param name='obj_path'>
	/// Path of obj inside Resources file.
	/// e.g. Resources->obj->car  =  "obj/car"
	/// </param>
	public GameObject loadGameObject (string obj_path)
	{
		// Load file from local disk to memory
		loaded_asset = Resources.Load <Object> (obj_path);
		
		// This line use to Instaniate to Scene
		// Save the obj let unload easier
		current_obj = GameObject.Instantiate(loaded_asset) as GameObject;
		return current_obj;
	}
	
	/// <summary>
	/// Unload the current instance GameObject.
	/// </summary>
	public void unloadGameObject ()
	{
		if(current_obj == null)
		{
			return;
		}
		// Manually release all obj
		GameObject.DestroyImmediate (current_obj, true);
		current_obj = null;
		loaded_asset = null;
		Resources.UnloadUnusedAssets ();
		System.GC.Collect();
	}
	
	/// <summary>
	/// Instantiate the specified Texture inside Resources file.
	/// </summary>
	/// <param name='obj_path'>
	/// Path of texture inside Resources file.
	/// e.g. Resources->png->car  =  "png/car"
	/// </param>
	public Texture loadTexture (string obj_path)
	{
		// Load file from local disk to memory
		loaded_asset = Resources.Load <Object> (obj_path);
		
		// Clone asset to Texture, need to prove is clone or not
		Texture output = (Texture) loaded_asset;
		
		// Release memory
		unloadTexture ();
		
		return output;
	}

    public Texture2D[] loadSprite(string obj_path)
    {
        // Load file from local disk to memory
        Texture2D[] output = Resources.LoadAll<Texture2D>(obj_path);
        Debug.Log(obj_path);
        // Clone asset to Texture, need to prove is clone or not


        // Release memory
        unloadTexture();

        return output;
    }

    /// <summary>
	/// Instantiate the specified Multiple Sprite File inside Resources folder.
	/// </summary>
	/// <param name='obj_path'>
	/// Path of texture inside Resources file.
	/// e.g. Resources->png->car  =  "png/car"
	/// </param>
	public Sprite[] loadSpriteMultiple(string obj_path , int numberOfPiece = 12)
    {
        int numberOfSprite = 0;
        // Load file from local disk to memory
         Sprite[] output = Resources.LoadAll<Sprite>(obj_path);
        // get number of unique sprite 
        numberOfSprite = output.Length/ numberOfPiece;

        //// Clone asset to Texture, need to prove is clone or not
        //Sprite[] output = (Sprite[])loaded_assets;

        // Release memory
        unloadTexture();

        //if only one or less simply return the array
        if (numberOfSprite <= 1)
        {
            return output;
        } else
        {
            //if not, pick a random in between sprite
            int randomPick = Random.Range(1, numberOfSprite+1);
            int endPosition = randomPick * numberOfPiece;// get end position
            int startPosition = endPosition - numberOfPiece;//get start position
            Sprite[] singleOutput = new Sprite [numberOfPiece];// create a new array
            int o = 0;
            for(int i = startPosition; i < endPosition; i++)
            {
                singleOutput[o] = output[i];/// add 12 piece sprite into a new array
                o++;
            }
            return singleOutput; 

        }
    }

    /// <summary>
    /// Unload the current instance Texture.
    /// </summary>
    public void unloadTexture ()
	{
		// Release memory
		loaded_asset = null;
		Resources.UnloadAsset (loaded_asset);
		Resources.UnloadUnusedAssets ();
		System.GC.Collect();
	}
	
	/// <summary>
	/// Loads the .txt file inside Resources file.
	/// </summary>
	/// <returns>
	/// The text of .txt file.
	/// </returns>
	/// <param name='obj_path'>
	/// Path of .txt file inside Resources file.
	/// e.g. Resources->dict  =  "dict"
	/// </param>
	public static string LoadText (string obj_path)
	{
		Object loaded_asset = null;
		
		// Load file from local disk to memory
		loaded_asset = Resources.Load(obj_path);
		
		// Clone asset to TextAsset, need to prove is clone or not
		TextAsset output = (TextAsset) loaded_asset;
		
		// Release memory
		loaded_asset = null;
		Resources.UnloadAsset (loaded_asset);
		Resources.UnloadUnusedAssets ();
		System.GC.Collect();
		
		return output.text;
	}
}
