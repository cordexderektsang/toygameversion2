﻿using UnityEngine;

[System.Serializable]
public class InstructionState
{
    public const int MIN_WAIT_TIME = 0;
    public const int MAX_WAIT_TIME = 30;
    public const int MIN_SLOPE = 1;
    public const int MAX_SLOPE = 10;
    public const int MIN_POWER = 1;
    public const int MAX_POWER = 5;

    [Header("Instruction Settings")]
    public AlphabetCoreConstants.CHARACTER_TAG_NAMESS tagName;
    public AlphabetCoreConstants.CHARACTER_Image_Icon imageName;
    public string message;
    public bool requireSuggestedWord = false;
    [System.NonSerialized]
    public string clipName;
    [Range(MIN_WAIT_TIME, MAX_WAIT_TIME)]
    public float waitTime;

    [Header("Timer Settings")]
    public AlphabetCoreConstants.TIMER_MAX_INCREASE_TYPES timerMaxIncreaseType;
    
    // Remove after
    //public int maxTimeReachCount;
    //public float timeIncrement;

    // For linear increase
    [Range(MIN_SLOPE, MAX_SLOPE)]
    public float slope;

    // For exponential increase
    [Range(MIN_POWER, MAX_POWER)]
    public float power;

    // For all increase
    public float yIntercept;
    public float absoluteMaxTime;

    /// <summary>
    /// Initializes a new instance of the <see cref="InstructionState"/> class.
    /// </summary>
    /// <param name="tagName">Tag name for character picture</param>
    /// <param name="clipName">Name of audio clip.</param>
    /// <param name="message">Message to display.</param>
    /// <param name="waitTime">Time to wait until displayed</param>
    public InstructionState(AlphabetCoreConstants.CHARACTER_TAG_NAMESS tagName, AlphabetCoreConstants.CHARACTER_Image_Icon imageName, string clipName, string message, float waitTime)
	{
		this.tagName = tagName;
        this.imageName = imageName;
        this.clipName = clipName;
        this.message = message;
		this.waitTime = waitTime;
	}
}
