﻿/*
 * Copyright (c) 2016 Beebyte Limited. All rights reserved. 
 */
using System;

namespace Beebyte.Obfuscator
{
	[AttributeUsage(AttributeTargets.Class|AttributeTargets.Method|AttributeTargets.Enum|AttributeTargets.Field|AttributeTargets.Property|AttributeTargets.Event)]
	public class ReplaceLiteralsWithNameAttribute : System.Attribute
	{
		public ReplaceLiteralsWithNameAttribute()
		{
		}
	}
}
