﻿/*
 * Copyright (c) 2015 Beebyte Limited. All rights reserved. 
 */
using System;

namespace Beebyte.Obfuscator
{
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method | AttributeTargets.Field | AttributeTargets.Parameter | AttributeTargets.Event | AttributeTargets.Enum | AttributeTargets.Property)]
	public class SkipAttribute : System.Attribute
	{
		public SkipAttribute()
		{
		}
	}
}
