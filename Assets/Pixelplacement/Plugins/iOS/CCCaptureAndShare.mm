#import <Social/Social.h>
#import <AssetsLibrary/AssetsLibrary.h>

#import "CCCaptureAndShare.h"

@implementation CCCaptureAndShare

// iOS function for sharing image to app which has installed in device
- (void)StartSharingImage: (UIImage *)imageToShare
                    title:(NSString *)social_type
                      msg:(NSString *)textToShare
{
    // Pack the args to array
    NSArray *activityItems = [[NSArray alloc] initWithObjects: textToShare, imageToShare, nil];
    
    // Create share panel with args
    UIActivityViewController *objVC = [[UIActivityViewController alloc]initWithActivityItems:activityItems applicationActivities:nil];
    
    // Callback of completion
    [objVC setCompletionHandler:^(NSString *activityType, BOOL completed)
     {
         if(completed)
         {
             // Success
             UIAlertView *objAlert = [[UIAlertView alloc]
                                      initWithTitle:@"Share"
                                      message:@"Share Success"
                                      delegate:nil
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil];
             // Show view
             [objAlert show];
         }
         else
         {
             // Faile
             UIAlertView *objAlert = [[UIAlertView alloc]
                                      initWithTitle:@"Share"
                                      message:@"Share Error"
                                      delegate:nil
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil];
             // Show view
             [objAlert show];
         }
     }];
    
    // Show view
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        // iPhone may use old process
        [UnityGetGLViewController() presentViewController:objVC animated:YES completion:nil];
    }
    
    else
    {
        // iPad
        // Change to use UIPopoverController for iPad
        UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:objVC];
        CGRect rect = UnityGetGLViewController().view.frame;
        
        [popup presentPopoverFromRect:CGRectMake(rect.size.width/2, rect.size.height/4, 0, 0)
                               inView:UnityGetGLViewController().view
             permittedArrowDirections:UIPopoverArrowDirectionAny
                             animated:YES];
    }
}

- (void)StartSharingImageToSocial:(UIImage *)imageToShare
                             type:(NSString *)social_type
                            title:(NSString *)titleToShare
                              msg:(NSString *)textToShare
{
    // Check type
    if ([social_type isEqualToString:@"SLServiceTypeUIActivity"])
    {
        // ServiceWithUIActivityView type may use other function
        [self StartSharingImage:imageToShare title:titleToShare msg:textToShare];
        return;
    }
    
    // Create a share panel by type
    SLComposeViewController *socialVC = [SLComposeViewController composeViewControllerForServiceType:social_type];
    
    // Callback of completion
    SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result)
    {
        // Check success or not
        // Use alert view to notie
        if(result == SLComposeViewControllerResultDone)
        {
            // Success
            UIAlertView *objAlert = [[UIAlertView alloc]
                                     initWithTitle:@"Share"
                                     message:@"Share Success"
                                     delegate:nil
                                     cancelButtonTitle:@"OK"
                                     otherButtonTitles:nil];
            // Show view
            [objAlert show];
        }
        else
        {
            // Faile
            UIAlertView *objAlert = [[UIAlertView alloc]
                                     initWithTitle:@"Share"
                                     message:@"Share Error"
                                     delegate:nil
                                     cancelButtonTitle:@"OK"
                                     otherButtonTitles:nil];
            // Show view
            [objAlert show];
        }
        
        // Close the specific view controller
        [socialVC dismissViewControllerAnimated:YES completion:Nil];
    };
    
    // Setup callback
    socialVC.completionHandler = myBlock;
    
    // Setup the args
    [socialVC setInitialText:textToShare];
    [socialVC addImage:imageToShare];
    [socialVC setTitle:titleToShare];
    
    
    // Show view
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        // iPhone may use old process
        [UnityGetGLViewController() presentViewController:socialVC animated:YES completion:nil];
    }
    
    else
    {
        // iPad
        // Change to use UIPopoverController for iPad
        UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:socialVC];
        CGRect rect = UnityGetGLViewController().view.frame;
        
        [popup presentPopoverFromRect:CGRectMake(rect.size.width/2, rect.size.height/4, 0, 0)
                               inView:UnityGetGLViewController().view
             permittedArrowDirections:UIPopoverArrowDirectionAny
                             animated:YES];
    }
}

// iOS function Save image to iOS Album
- (void)saveImage:(UIImage *)image
{
    // Start a thread from main thread to finish the task
    // also a callback function to notice us success or not
    [[NSOperationQueue mainQueue] addOperationWithBlock:
     ^{UIImageWriteToSavedPhotosAlbum(image,
                                      self,
                                      @selector(imageSaveCallback:didFinishSavingWithError:contextInfo:),
                                      nil);
     }];
}

// iOS function The Callback of save image
- (void)imageSaveCallback		: (UIImage *)image
   didFinishSavingWithError: (NSError *)error
            contextInfo				: (void *)contextInfo
{
    // An view use to notice success or not
    UIAlertView* alertView = nil;
    
    // Success msg
    NSString* str = @"Save";
    
    // Check error
    if (error != NULL)
    {
        // Set error msg
        ALAuthorizationStatus status = [ALAssetsLibrary authorizationStatus];
        if (status != ALAuthorizationStatusAuthorized)
        {
            str = @"Please give this app permission to access your photo library";
        }
        else str = [error localizedDescription];
    }
    
    // Init view
    alertView = [[UIAlertView alloc] initWithTitle:@"Image Album"
                                           message:str
                                          delegate:nil
                                 cancelButtonTitle:@"OK"
                                 otherButtonTitles:nil];
    
    // Show view
    [alertView show];
}

- (UIImage *)captureScreen
{
    UIImage *image;
    UIGraphicsBeginImageContextWithOptions(UnityGetGLView().bounds.size , NO , [UIScreen mainScreen].scale);
    
    if ([UnityGetGLView() respondsToSelector:@selector(drawViewHierarchyInRect:afterScreenUpdates:)]){
        [UnityGetGLView() drawViewHierarchyInRect:UnityGetGLView().bounds afterScreenUpdates:YES];
    } else {
        [UnityGetGLView().layer renderInContext:UIGraphicsGetCurrentContext()];
    }
    
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

@end

// Instance
static CCCaptureAndShare *root = nil;
static CCCaptureAndShare *instance()
{
    if(root == nil)
    {
        root = [[CCCaptureAndShare alloc] init];
    }
    return root;
}

// Bridge From C to iOS
extern "C"
{
    // C function to Save image to iOS Album
    void SaveImageToAlbumUnity (const char** byteArrayPtr, int arrayLength)
    {
        NSLog(@"UnityIOS SaveImageToAlbum");
        UIImage *image;
        
        if(arrayLength != 0)
        {
            NSData *imageData = [NSData dataWithBytes:(const void *)byteArrayPtr length:(sizeof(unsigned char) *arrayLength)];
            
            // Create iOS Image from NSData
            image = [UIImage imageWithData:imageData];
        }
        else
        {
            // Native call
            image = [instance() captureScreen];
        }
        
        // Native call
        [instance() saveImage:image];
    }
    
    // C function to Share an image to social
    void StartSharingImageToSocialUnity (const char** byteArrayPtr, int arrayLength, const char* type, const char* title, const char* msg)
    {
        NSLog(@"UnityIOS StartSharingImage");
        UIImage  *image;
        
        if(arrayLength != 0)
        {
            // Convert C# byte[] to iOS NSData
            NSData *imageData   = [NSData dataWithBytes:(void *)byteArrayPtr length:(sizeof(unsigned char) * arrayLength)];
            
            // Create iOS Image from NSData
            image = [UIImage imageWithData:imageData];
        }
        else
        {
            // Native call
            image = [instance() captureScreen];
        }
        
        // Some args convert C char to iOS NSString
        NSString *social_type = [NSString stringWithUTF8String: type];
        NSString *share_title = [NSString stringWithUTF8String: title];
        NSString *share_msg   = [NSString stringWithUTF8String: msg];
        
        // Native call
        [instance() StartSharingImageToSocial:image
                                         type:social_type
                                        title:share_title
                                          msg:share_msg];
    }
}